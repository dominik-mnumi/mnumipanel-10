#sfActionCredentialsGetterPlugin#

sfActionCredentialsGetterPlugin is a simple symfony plugin to enable you to get the required credentials for a given module name and action name, and to determine whether the action is secure.

The plugin provides a single class, sfActionCredentialsGetter, which allows you to query a module name and action name to determine whether the action is secure, and if so, to get the list of credentials required to execute the action.

Developed on/for symfony 1.4.1 with Doctrine ORM.  Should also work fine with Propel.

## Installation ##

  * Install the plugin.
      Using symfony command line:
          ./symfony plugin:install sfActionCredentialsGetterPlugin
      Or get the latest version from the subversion repository:
          svn co http://svn.symfony-project.com/plugins/sfActionCredentialsGetterPlugin plugins/sfActionCredentialsGetterPlugin

  * If you installed by subversion, activate the plugin in the config/ProjectConfiguration.class.php file (this will have already been done automatically if you used the plugin:install symfony command).

        [php]
        class ProjectConfiguration extends sfProjectConfiguration
        {
          public function setup()
          {
            ...
            $this->enablePlugins('sfTextDateInputJQueryDatePickerPlugin');
            ...
          }
        }

## How to use ##

  * Put something like this in the controller code for your module or action:

        public function executeMyAction(sfWebRequest $request) {
            $user = $this->getUser();
            $credentialsGetter = new sfActionCredentialsGetter($this->getContext());

            ...

            // Determine whether the user has access to the "blah" action in the "duh" module.
            $module = 'duh';
            $action = 'blah';

            if ($credentialsGetter->isUserAllowedToExecuteAction($user, $module, $action)) {
                // (put code here to emit the link to the action)
            }
        }

  * To determine whether an action is secure:

        $actionIsSecure = $credentialsGetter->isActionSecure($module, $action);

  * To get the list of required credentials for a secure action:

        $requiredCredentials = $credentialsGetter->getActionCredentials($module, $action);

