<?php
class sfActionCredentialsGetter {
	protected $context;
	protected $security;
	protected $securityByModule;

	public function __construct(sfContext &$context) {
		$this->context = &$context;
		$this->security = array();
		$this->securityByModule = array();
	}

	public function isActionSecure($module, $action) {
		return $this->getModuleSecurityValue($module, $action, 'is_secure', false);
	}

	public function getActionCredentials($module, $action) {
		return $this->getModuleSecurityValue($module, $action, 'credentials');
	}

	public function isUserAllowedToExecuteAction(sfBasicSecurityUser $user, $module, $action) {
		$isAuthenticated = $user->isAuthenticated();
		$isSuperAdmin = $isAuthenticated && $user->isSuperAdmin();
		if ($isSuperAdmin || (!$this->isActionSecure($module, $action))) {
			return true;
		}
		if (($isAuthenticated) &&
			($user->hasCredential($this->getActionCredentials($module, $action)))) {
			return true;
		}
		return false;
	}

	protected function getModuleSecurityValue($module, $action, $name, $default = null) {
		if (!isset($this->securityByModule[$module])) {
			unset($this->security);
			$this->security = array();
			if ($fn = $this->context->getConfigCache()->checkConfig
					('modules/'.$module.'/config/security.yml', true)) {
				require($fn);
			}
			$this->securityByModule[$module] = $this->security;
			unset($this->security);
		}

		$action = strtolower($action);
		if (isset($this->securityByModule[$module][$action][$name])) {
			return $this->securityByModule[$module][$action][$name];
		}
		if (isset($this->securityByModule[$module]['all'][$name])) {
			return $this->securityByModule[$module]['all'][$name];
		}
		return $default;
	}
}
