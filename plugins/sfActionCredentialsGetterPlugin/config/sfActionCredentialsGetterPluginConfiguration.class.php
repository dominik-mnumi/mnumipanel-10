<?php

/**
 * sfActionCredentialsGetterPlugin configuration.
 * 
 * @package     sfActionCredentialsGetterPlugin
 * @subpackage  config
 * @author      Ronald B. Cemer
 * @version     SVN: $Id: PluginConfiguration.class.php 17207 2009-04-10 15:36:26Z Kris.Wallsmith $
 */
class sfActionCredentialsGetterPluginConfiguration extends sfPluginConfiguration
{
  const VERSION = '1.0.0-DEV';

  /**
   * @see sfPluginConfiguration
   */
  public function initialize()
  {
  }
}
