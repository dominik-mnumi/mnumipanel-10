<?php

$finder = Symfony\CS\Finder\DefaultFinder::create()
    ->exclude(array(
        'Symfony/Component/Console/Tests/Fixtures/application_1.xml',
        'Symfony/Component/Console/Tests/Fixtures/application_2.xml',
        'Symfony/Component/Console/Tests/Helper/TableHelperTest.php',
        'Symfony/Component/DependencyInjection/Tests/Fixtures/yaml/services1.yml',
        'Symfony/Component/DependencyInjection/Tests/Fixtures/yaml/services8.yml',
        'Symfony/Component/Yaml/Tests/Fixtures/sfTests.yml',
    ))
    ->in(__DIR__.'/S2/src')
;

return Symfony\CS\Config\Symfony23Config::create()
    ->finder($finder)
;
