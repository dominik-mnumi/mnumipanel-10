#!/bin/bash

if [ "$(id -u)" != "0" ]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

DIR=`php -r "echo dirname(realpath('$0'));"`
cd $DIR

echo "Prepare for functional tests:"

echo " > Install/update required packages (nginx, xvfb)..."
# apt-get update > /dev/null 2>&1
apt-get -qq install xvfb > /dev/null 2>&1
apt-get -qq install nginx > /dev/null 2>&1
apt-get -qq install php5-cgi > /dev/null 2>&1

echo " > Run NGINX service..."
pkill "nginx -c test/sahi/config/nginx.conf" -f > /dev/null
cd $DIR/../../
nginx -v && nginx -c test/sahi/config/nginx.conf -p "./" > /dev/null 2>&1 &

sleep 1

if [ `pgrep "nginx -c test/sahi/config/nginx.conf" -f | wc -l` == 0 ]; 
then
   echo "Could not run NGINX. Exiting!"
   nginx -c test/sahi/config/nginx.conf -p "./"
   exit 1
fi

echo " > Run PHP service..."
pkill "php-cgi" > /dev/null
php-cgi -b 127.0.0.1:9099 > /dev/null 2>&1 &

if [ `pgrep "php-cgi" | wc -l` == 0 ]; 
then
   echo "Could not run PHP. Exiting!"
   exit 1
fi

echo "Done: http://localhost:8099/"
