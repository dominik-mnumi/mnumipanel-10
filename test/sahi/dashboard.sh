#!/bin/bash

if [ "$(id -u)" == "0" ]; then
  echo "This script must be run normal user (not ROOT)" 1>&2
  exit 1
fi

DIR=`php -r "echo dirname(realpath('$0'));"`

export SAHI_HOME="$DIR/../../lib/vendor/sahi/"
export SAHI_CLASS_PATH=$SAHI_HOME/lib/sahi.jar:$SAHI_HOME/extlib/rhino/js.jar:$SAHI_HOME/extlib/apc/commons-codec-1.3.jar
export SAHI_EXT_CLASS_PATH=

echo " > Run SAHI service..."
pkill "/lib/sahi.jar" -f > /dev/null
sleep 3
java -classpath $SAHI_EXT_CLASS_PATH:$SAHI_CLASS_PATH net.sf.sahi.ui.Dashboard "$SAHI_HOME" "$DIR/userdata" 

