<?php

/*
 * This file is part of the symfony package.
 * (c) 2004-2006 Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

require_once dirname(__FILE__) . '/../../config/ProjectConfiguration.class.php';
mnumiTestFunctional::getConfiguration();

// remove all cache
sfToolkit::clearDirectory(sfConfig::get('sf_app_cache_dir'));

class mnumiTestFunctional
{
    static $configuration = false;
    
    public static function getConfiguration()
    {
        if(self::$configuration == false)
        {
            self::$configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'test', true);
            sfContext::createInstance(self::$configuration);
        }
        
        return self::$configuration;
    }
    
    public static function getLoggedInstance($user = 'admin')
    {
        $browser = new sfTestFunctional(new sfBrowser());
        
        switch ($user) {
          
          case 'office':
            $user_data = array(
                    'username' => 'office',
                    'password' => 'office',
            );
            break;
          
          case 'UserClientActionsFunctional':
            $user_data = array(
                    'username' => 'UserClientActionsFunctional',
                    'password' => 'UserClientActionsFunctional',
            );
            break;
            
          case 'admin':
            $user_data = array(
                    'username' => 'admin',
                    'password' => 'admin',
            );
            break;
          
          default:
            $user_data = array(
                    'username' => 'admin',
                    'password' => 'admin',
            );
            break;
        }
        
        $browser->get('/login')->
            with('response')->
            begin()->
            isStatusCode(401)->
            getResponseDom()->
            end()->
            click('Login', array('signin' => $user_data))->
            //
            followRedirect()->
            with('response')->
            begin()->
            isStatusCode(302)->
            end(); 
        
        return $browser;
    }
    
    public static function loadData()
    {    
        $configuration = self::getConfiguration();
        
        $doctrine = new sfDoctrineDropDbTask($configuration->getEventDispatcher(), new sfAnsiColorFormatter());
        $doctrine->run(array(), array("--no-confirmation", "--env=test", "--application=mnumicore"));

        $doctrine = new sfDoctrineBuildDbTask($configuration->getEventDispatcher(), new sfAnsiColorFormatter());
        $doctrine->run(array(), array("--env=test", "--application=mnumicore"));

        $doctrine = new sfDoctrineInsertSqlTask($configuration->getEventDispatcher(), new sfAnsiColorFormatter());
        $doctrine->run(array(), array("--env=test", "--application=mnumicore"));                 

        Doctrine_Core::loadData(sfConfig::get('sf_test_dir').'/fixtures');
    }

}