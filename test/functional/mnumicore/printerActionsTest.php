<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/settings/printer')->
    
  // login username  
  click('Login', array('signin' => array(
    'username' => 'admin',
    'password' => 'admin',
  )))->
        
  with('response')->begin()->
    isRedirected()-> 
    isStatusCode(302)->
  end()->
        
  get('/settings/printer')->

  with('request')->begin()->
    isParameter('module', 'printer')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Group: Printer/')->
  end();

$c = new sfDomCssSelector($browser->getResponseDom());

$browser->
  click($c->matchSingle('.table-actions a.edit')->getNode())->
    
  with('request')->begin()->
    isParameter('module', 'printer')->
    isParameter('action', 'edit')->
  end()->
    
  click(' Save', 
      array('printer' => array('name' => 'Default Printer', 'printsize_width' => '297', 'printsize_height' => '420'))
  )->

  followRedirect()->
    
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('body', '/Saved successfully./')->
  end()->
  
  click(' Save', 
      array('printer' => array('name' => 'Default Printer', 'printsize_width' => 'fsdfds', 'printsize_height' => 'fdsh'))
  )->
  with('response')->begin()->
    isStatusCode(200)->
    checkElement('article ul[class="message error no-margin"]', true)->
  end()
  
;
