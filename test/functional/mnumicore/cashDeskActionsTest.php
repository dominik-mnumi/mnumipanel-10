<?php

include(dirname(__FILE__).'/../../bootstrap/functional.php');

mnumiTestFunctional::loadData();

#
# checking visibility of cash block edit page links
#
// for user without permission
$browser = mnumiTestFunctional::getLoggedInstance('office');
$browser
  ->info('  Checking cash edit block')
  ->get(sfContext::getInstance()->getRouting()->generate('cashDesk'))
  ->info('    User from fixtures does not have edit_cash_block permission')
  ->with('response')->
    begin()
      ->isStatusCode(200)
      ->info('      does not see "Add block" button')
      ->checkElement('#dialog-add-block-click', false)   
    ->end();

// for user with permission
$browser = mnumiTestFunctional::getLoggedInstance();
$browser
        ->info('  Checking cash edit block')
        ->get(sfContext::getInstance()->getRouting()->generate('cashDesk'))
        ->info('    User from fixtures has edit_cash_block permission')
        ->with('response')->
        begin()
        ->isStatusCode(200)
        ->info('      see "Add block" button')
        ->checkElement('#dialog-add-block-click')
        ->end();

// deletes block
$objToDelete = CashDeskTable::getInstance()->findOneByType('block');
$urlToDelete = sfContext::getInstance()->getRouting()->generate('cashDeskDelete',
        $objToDelete);

$browser->
        info('#try delete block')->
        get($urlToDelete)->
        with('response')->
        begin()->
        isRedirected()->
        isStatusCode(302)->
        end()->
        //
        followRedirect()->
        with('response')->
        begin()->
        checkElement('body', '/Deleted successfully/')->
        isStatusCode(200)->
        end()->
        info('#block deleted successfully');
