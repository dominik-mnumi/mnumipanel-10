<?php

require_once(__DIR__.'/../../bootstrap/boostrap.php');

require_once 'PHPUnit/Extensions/SeleniumTestCase.php';

/**
 * @TODO when permission section will be completed then finish this class.
 */



class CouponTest extends sfBasePhpunitSeleniumTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    protected function _start()
    {
        //$this->fixture()->clean()->loadCommon('default_selenium');
    }

    /**
     *  Test index action
     */
    public function testIndex()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("mnumicore_test_selenium.php/coupon");
        try
        {
            $this->assertTrue($this->isTextPresent("Coupons"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        try
        {
            $this->assertTrue($this->isTextPresent("BonusCoupon"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

     /**
     *  Test add action
     */
    public function testAdd()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        $this->open("mnumicore_test_selenium.php/coupon/create");
        try
        {
            $this->assertTrue($this->isTextPresent("Create coupon"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        $this->type("id=CouponForm_name", "NewCoupon");
        $this->type("id=CouponForm_value", "10");
        $this->click("id=CouponForm_products_");
        $this->click("id=CouponForm_pricelist_");
        $this->type("id=CouponForm_expire_at", "2111-12-12");
        $this->click("//form[@id='coupon_form']/article/section/div/div/p[4]");
        $this->click("id=CouponForm_one_time_usage");
        $this->click("id=CouponForm_used");
        $this->click("id=saveAndReturn");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertTrue($this->isTextPresent("Saved successfully."));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        try
        {
            $this->assertTrue($this->isTextPresent("Edit coupon"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }

     /**
     *  Test edit and update action
     */
    public function testEditAndUpdate()
    {
        $this->open("/mnumicore_test_selenium.php");
        $this->type("id=signin_username", "admin");
        $this->type("id=signin_password", "admin");
        $this->click("css=button.float-right");
        $this->waitForPageToLoad("30000");
        
        $this->open("mnumicore_test_selenium.php/coupon");
        $this->click("css=a.without-tip.edit > img");
        $this->waitForPageToLoad("30000");
        $this->type("id=CouponForm_value", "0.11");
        try
        {
            $this->assertTrue($this->isTextPresent("Edit coupon"));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
        $this->click("id=saveAndReturn");
        $this->waitForPageToLoad("30000");
        try
        {
            $this->assertTrue($this->isTextPresent("Saved successfully."));
        }
        catch (PHPUnit_Framework_AssertionFailedError $e)
        {
            array_push($this->verificationErrors, $e->toString());
        }
    }
}

?>