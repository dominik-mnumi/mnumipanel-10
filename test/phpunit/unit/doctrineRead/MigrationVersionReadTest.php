<?php

require_once(__DIR__ . '/../../bootstrap/boostrap2.php');


class MigrationVersionReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
    	// get Migration version from "lib/migration" directory
    	$fileMigration = new Doctrine_Migration(sfConfig::get('sf_root_dir').'/lib/migration/doctrine/');
    	$this->fileLatestVersion = $fileMigration->getLatestVersion();
    	
    	// get Migration version from fixtures file
    	$fixturesMigration = sfYaml::load(sfConfig::get('sf_data_dir').'/fixtures/migration.yml');
    	$this->fixturesLatestVersion = $fixturesMigration['MigrationVersion']['default_version']['version'];
    }

    public function testMigrationVersion()
    {
        $this->assertEquals($this->fileLatestVersion, $this->fixturesLatestVersion);
    }
    
}