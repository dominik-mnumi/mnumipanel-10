<?php

require_once(__DIR__ . '/../../../bootstrap/boostrap2.php');


class MnumiQLClauseSelectReadTest extends BasePhpunitTestCase
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->query1 = "SELECT User.username, UserContact.value 
                FROM UserContact INNER JOIN UserContact.User User LEFT JOIN User.Orders Orders
                WHERE DATE_FORMAT(Orders.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Orders.created_at, '%Y-%m-%d') < '2014-10-10' 
                GROUP BY User.id, UserContact.id HAVING COUNT(Orders.id) > 5 AND SUM(Orders.price_net) > 5000";
                
        $this->query2 = "SELECT COUNT(Order.id), SUM(Order.price_net)
                FROM Order
                WHERE DATE_FORMAT(Order.created_at, '%Y-%m-%d') > '2011-10-10' AND DATE_FORMAT(Order.created_at, '%Y-%m-%d') < '2014-10-10'  ";
    }

    /**
     * Tests parse() method;
     */
    public function testParse()
    {
        // 1.
        $selectObj = new MnumiQLClauseSelect($this->query1, array());
        $this->assertEquals('User.username, UserContact.value', $selectObj->parse());
        
        // 2.
        $selectObj = new MnumiQLClauseSelect($this->query2, array());
        $this->assertEquals('COUNT(Order.id), SUM(Order.price_net)', $selectObj->parse());

    } 

    /**
     * Tests getQuery() method;
     */
    public function testGetQuery()
    {
        // 1. Checks if arguments were properly passed to Doctrine_Query
        $selectObj = new MnumiQLClauseSelect($this->query1, array());
        $this->assertEquals('SELECT User.username as User.username, UserContact.value as UserContact.value', trim($selectObj->getQuery()->getDql()));
   
        // 2. 
        $selectObj = new MnumiQLClauseSelect($this->query2, array());
        $this->assertEquals('SELECT COUNT(Order.id) as COUNT(Order.id), SUM(Order.price_net) as SUM(Order.price_net)', trim($selectObj->getQuery()->getDql()));
   
    } 
}