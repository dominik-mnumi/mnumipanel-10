<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class OrderPackageReadTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{
    
    protected function _start()
    {
        $this->package = OrderPackageTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests filtered payment.
     */
    public function testIfPackageImplementsToString()
    {
        $this->assertTrue(method_exists($this->package,'__toString'));
    }

    /**
     * Tests OrderPackage isAffectedByCoupon method.
     */
    public function testIsAffectedByCoupon()
    {
        $coupon = CouponTable::getInstance()->find('BonusCoupon');
        $coupon2 = CouponTable::getInstance()->find('BonusCoupon2');
        $coupon3 = CouponTable::getInstance()->find('CouponInactive');
        
        $this->assertTrue($coupon instanceof Coupon);
        $this->assertTrue($coupon2 instanceof Coupon);
        $this->assertTrue($coupon3 instanceof Coupon);
        
        // afected by table coupon_product
        $this->assertTrue($this->package->isAffectedByCoupon($coupon));
        
        // affected by apply for all products
        $this->assertTrue($this->package->isAffectedByCoupon($coupon2));
        
        // not affected (pricelist)
        $this->assertFalse($this->package->isAffectedByCoupon($coupon3));  
    }

    /**
     * Tests OrderPackage getOrdersBasketQuery method.
     */
    public function getOrdersBasketQuery()
    {
        $query = $this->getOrdersBasketQuery();

        $price = OrderTable::getInstance()->addTax($this->calculateOrdersBaseAmount($query));
        $this->assertEquals($price, 424.35);
    }

    /**
     * Tests OrderPackage getOrdersWithoutDraftQuery method.
     */
    public function getOrdersWithoutDraftQuery()
    {
        $query = $this->getOrdersBasketQuery();
        $query = $this->getOrdersWithoutDraftQuery($query);

        $price = OrderTable::getInstance()->addTax($this->calculateOrdersBaseAmount($query));
        $this->assertEquals($price, 412.05);
    }
    
    /**
     * Tests OrderPackage getPriceGross method.
     */
    public function testGetPriceGross()
    {
        $this->assertEquals($this->package->getPriceGross(), 424.35);
    }
    
    /**
     * Tests OrderPackage getOrdersPriceNet method.
     */
    public function testGetOrdersPriceNet()
    {
        $this->assertEquals($this->package->getOrdersPriceNet(), 345);
        $this->assertEquals($this->package->getOrdersPriceNet(true), 335);
    }
    
    /**
     * Tests OrderPackage getPriceNet method.
     */
    public function testGetPriceNet()
    {
        $this->assertEquals($this->package->getPriceNet(), 345);
    }

    /**
     * Tests OrderPackage getUsedLoyaltyPoints method.
     */
    public function testGetUsedLoyaltyPoints()
    {
        $this->assertEquals($this->package->getUsedLoyaltyPoints(), 100);
    }
    
    /**
     * Tests OrderPackage getUsedLoyaltyPointsValue method.
     */
    public function testGetUsedLoyaltyPointsValue()
    {
        $this->assertEquals($this->package->getUsedLoyaltyPointsValue(), 50);
    }
   
}