<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class FieldItemReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->pricelist = PricelistTable::getInstance()->getDefaultPricelist()->getId();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests getFieldItemPrice() method. FieldItem.class.php.
     */
    public function testGetFieldItemPrice()
    {
        //gets field item - paper_x
        $fieldItemObj = FieldItemTable::getInstance()->findOneByName('Paper X');

        $fieldItemPriceObj = $fieldItemObj->getFieldItemPrice($this->pricelist);

        $this->assertEquals('FieldItemPrice', get_class($fieldItemPriceObj));
    }
}