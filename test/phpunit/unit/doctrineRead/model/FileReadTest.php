<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class FileReadTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{
    
    protected function _start()
    {
        $this->fileObj = FileTable::getInstance()->findAll()->getFirst();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests getThumbFullFilePath method.
     */
    public function testGetThumbFullFilePath()
    {
        $this->assertTrue(
                (boolean)preg_match('|(.*)/data/files/'.date('Y/m/d', strtotime($this->fileObj->getOrder()->getCreatedAt())).'/'.$this->fileObj->getOrder()->getId()."/thumbs_90x90/".md5($this->fileObj->getFilename().'_'.$this->fileObj->getId()).'.jpg|', 
                $this->fileObj->getThumbFullFilePath()));
    }

}