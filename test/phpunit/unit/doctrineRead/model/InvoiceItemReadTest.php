<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class InvoiceItemReadTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{
    
    protected function _start()
    {
        // gets invoice item object
        $invoice = InvoiceTable::getInstance()
        	->findOneByNumber(4);
        
        $this->invoiceItemObj = $invoice->getInvoiceItems()->getFirst();
    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->loadSnapshot('common'));
    }

    /**
     * Tests getPriceNetTotalAfterDiscount() method. 
     */
    public function testGetPriceNetTotalAfterDiscount()
    {               
        $this->assertEquals($this->invoiceItemObj->getPriceNetTotalAfterDiscount(), 4400.00);
    }
    
    /**
     * Tests getPriceGrossTotal() method. 
     */
    public function testGetPriceGrossTotal()
    {       
        $this->assertEquals($this->invoiceItemObj->getPriceGrossTotal(), 5412.00);
    }
    
    /**
     * Tests getTaxAmount() method. 
     */
    public function testGetTaxAmount()
    {            
        $this->assertEquals($this->invoiceItemObj->getTaxAmount(), 1012.00);
    }
    
    /**
     * Tests isFromOrder
     */
    public function testIsFromOrder()
    {
        // is not from order
        $this->assertTrue($this->invoiceItemObj->isFromOrder());
    }
    
}