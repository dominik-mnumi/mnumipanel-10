<?php

require_once(__DIR__.'/../../../bootstrap/boostrap2.php');


class FieldsetReadTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {

    }

    /**
     * Tests data load.
     */
    public function testDataLoad()
    {
        $this->assertInstanceOf('sfPhpunitFixtureDoctrine', $this->fixture()->clean()->loadSnapshot('common'));
    }

    /**
     * Tests Fieldset tabs.
     */
    public function testGetFieltItemsByRootIdsQuery()
    {
    	$fieldset = FieldsetTable::getInstance()->findOneByName('SIDES');
    	
    	$fieldItems = FieldsetTable::getFieldItemsByRootIdsQuery(array($fieldset->getId()))->execute();
    	
    	$this->assertInstanceOf('Doctrine_Collection', $fieldItems);
    	
    	foreach($fieldItems as $item)
    	{
    		if($item->name != 'Single' && $item->name != 'Double')
    		{
    			$this->fail('Wrong FieldItem name: '.$item->name);
    		}
    	}
    }
    
    public function testCanDelete()
    {
    	$obj = FieldsetTable::getInstance()->createQuery('f')
	    	->andWhere('(f.lft+1) = f.rgt')
	    	->andWhere('f.hidden = ?', false)
	    	->andWhere('f.changeable = ?', true)
	    	->andWhere('f.label = ?', 'can_delete')
	    	->leftJoin('FieldItem fi')
	    	->select('f.id, (select count(*) from field_item fi where fi.field_id = f.id) as nb')
	    	->having('nb = 0')
	    	->execute();
    	$fieldset = FieldsetTable::getInstance()->find($obj[0]->id);
    	$this->assertTrue($fieldset->canDelete(), '->canDelete() return true');
    	
    	$obj = FieldsetTable::getInstance()->createQuery('f')
	    	->andWhere('(f.lft+1) = f.rgt')
	    	->leftJoin('FieldItem fi')
	    	->select('f.id, (select count(*) from field_item fi where fi.field_id = f.id) as nb')
	    	->having('nb > 0')
	    	->execute();
    	$fieldset = FieldsetTable::getInstance()->find($obj[0]->id);
    	$this->assertFalse($fieldset->canDelete(), '->canDelete() return false');
    	
    	$obj = FieldsetTable::getInstance()->createQuery('f')
	    	->andWhere('f.lft < (f.rgt+2)')
	    	->leftJoin('FieldItem fi')
	    	->select('f.id, (select count(*) from field_item fi where fi.field_id = f.id) as nb')
	    	->having('nb = 0')
	    	->execute();
    	$fieldset = FieldsetTable::getInstance()->find($obj[0]->id);
    	$this->assertFalse($fieldset->canDelete(), '->canDelete() return false');
    }
    
    public function testGetProductFieldsetDefault()
    {
		$key = array('GENERAL', 'WIZARD', 'COUNT', 'QUANTITY', 'SIZE',
    			'MATERIAL', 'PRINT', 'SIDES', 'FIXEDPRICE', 'OTHER'
    	);
    	
    	$fieldset = FieldsetTable::getInstance()->getProductFieldsetDefault();
    	
    	$list = array();
    	$i=0;
    	foreach($fieldset as $item)
    	{
    		$list[] = $item['name'];
    		$i++;
    	}
    	sort($key);
    	sort($list);
    	$this->assertEquals($key, $list);
    }


}