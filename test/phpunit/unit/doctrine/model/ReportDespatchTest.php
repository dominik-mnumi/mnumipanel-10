<?php
require_once(__DIR__.'/../../../bootstrap/boostrap.php');

class ReportDespatchTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{
    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start() 
    {
        $this->fixture()->clean()->loadOwn('report');
    }

    public function testCloseBooks()
    {
        $this->assertEquals(count(OrderPackageTable::getInstance()->getDespatchPackages()) , 1);
        $reportDespatch = ReportDespatchTable::getInstance()->closeBook('pl-poczta-polska');
        $this->assertEquals(count(OrderPackageTable::getInstance()->getDespatchPackages()) , 0);
    }

}