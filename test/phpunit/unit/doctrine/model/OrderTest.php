<?php

require_once(__DIR__.'/../../../bootstrap/boostrap.php');


class OrderTest extends BasePhpunitTestCase implements sfPhpunitFixtureDoctrineAggregator
{

    /**
     * Order instance
     * @var Order
     */
    private $order;

    protected function _start()
    {
    	$this->fixture()->loadSnapshot('common');
    	
      	$this->package = OrderPackageTable::getInstance()->createQuery('m')->limit(1)->fetchOne();

        if(!$this->package instanceof OrderPackage)
        {
            throw new Exception('Sample OrderPackage does not exist. Check fixtures. ');
        }
        $this->order = OrderTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
        $this->orderToDuplicate = OrderTable::getInstance()->createQuery('m')->where("m.name = 'Order to duplicate'")->limit(1)->fetchOne();
        //import file here to get real file on server
        $this->orderToDuplicate->importFile(sfConfig::get('sf_test_dir').'/phpunit/data/sample_image.jpg');
        $this->emptyOrder = OrderTable::getInstance()->createQuery('m')->where("m.name = 'Empty order'")->limit(1)->fetchOne();
        $this->orderDraft = OrderTable::getInstance()->createQuery('m')->where("m.name = 'Order draft'")->limit(1)->fetchOne();

        if(!$this->order instanceof Order)
        {
            throw new Exception('Sample Order does not exist. Check fixtures. ');
        }

        $this->file = FileTable::getInstance()->createQuery('m')->limit(1)->fetchOne();
        if(!$this->file instanceof File)
        {
            throw new Exception('Sample File does not exist. Check fixtures. ');
        }
    }

    public function testImportFile()
    {
        $filename = sfConfig::get('sf_test_dir').'/phpunit/data/sample_image.jpg';
        $fileObj = $this->order->importFile($filename);
        $this->assertInstanceOf('File', $fileObj);
    }

    /**
     * Test duplicate method 
     */
    public function testDuplicate()
    {
        $this->orderToDuplicate = OrderTable::getInstance()->createQuery('m')->where("m.name = 'Order to duplicate'")->limit(1)->fetchOne();
        $duplicateOrder = $this->orderToDuplicate->duplicate();

        $this->assertEquals($duplicateOrder->getPriceNet(),
                $this->orderToDuplicate->getPriceNet());
        $this->assertEquals($this->orderToDuplicate->getOrderStatusName(),
                'realization');
        $this->assertEquals($duplicateOrder->getOrderStatusName(), 'new');
        $this->assertNotEquals($duplicateOrder->getOrderPackageId(),
                $this->orderToDuplicate->getOrderPackageId());
        $this->assertEquals($duplicateOrder->getOrderPackageId(), null);
    }

    /**
     * Test clone files
     */
    public function testCloneFiles()
    {
        $this->emptyOrder->cloneFiles($this->orderToDuplicate);
        $array1 = $this->emptyOrder->getFiles()->toArray();
        $array2 = $this->orderToDuplicate->getFiles()->toArray();

        unset($array1[0]['Order'], $array1[0]['id'], $array1[0]['order_id'],
                $array1[0]['created_at'], $array1[0]['updated_at']);
        unset($array2[0]['Order'], $array2[0]['id'], $array2[0]['order_id'],
                $array2[0]['created_at'], $array2[0]['updated_at']);

        $this->assertEquals($array1, $array2);
    }

    /**
     * Test clone traders
     */
    public function testCloneTraders()
    {
        $this->emptyOrder->cloneTraders($this->orderToDuplicate);
        $array1 = $this->emptyOrder->getOrderTraders()->toArray();
        $array2 = $this->orderToDuplicate->getOrderTraders()->toArray();

        unset($array1[0]['Order'], $array1[0]['id'], $array1[0]['order_id'],
                $array1[0]['created_at'], $array1[0]['updated_at'],
                $array1[0]['sfGuardUser']);
        unset($array2[0]['Order'], $array2[0]['id'], $array2[0]['order_id'],
                $array2[0]['created_at'], $array2[0]['updated_at'],
                $array2[0]['sfGuardUser']);

        $this->assertEquals($array1, $array2);
    }

    /**
     * Test clone CustomPrices
     */
    public function testCloneCustomPrices()
    {
        $this->emptyOrder->cloneCustomPrices($this->orderToDuplicate);
        $array1 = $this->emptyOrder->getCustomPrices()->toArray();
        $array2 = $this->orderToDuplicate->getCustomPrices()->toArray();

        unset($array1[0]['Order'], $array1[0]['id'], $array1[0]['order_id']);
        unset($array2[0]['Order'], $array2[0]['id'], $array2[0]['order_id']);

        $this->assertEquals($array1, $array2);
    }

    /**
     * Test clone Attributes
     */
    public function testCloneAttributes()
    {
        $this->emptyOrder->cloneAttributes($this->orderToDuplicate);
        $array1 = $this->emptyOrder->getOrderAttributes()->toArray();
        $array2 = $this->orderToDuplicate->getOrderAttributes()->toArray();

        unset($array1[0]['Order'], $array1[0]['id'], $array1[0]['order_id'],
                $array1[0]['User'], $array1[0]['ProductField'], $array1[0]['Editor']);
        unset($array2[0]['Order'], $array2[0]['id'], $array2[0]['order_id'],
                $array2[0]['User'], $array2[0]['ProductField']);
        
        $this->assertEquals($array1, $array2);
    }

    /**
     * Tests Order moveToShelf method. Moves all package orders to shelf.
     */
    public function testMoveToShelf()
    {
        $this->assertTrue(!$this->package->isOrderOnShelf());

        $orders = $this->package->getOrders();

        foreach($orders as $rec)
        {
            $rec->moveToShelf();
        }

        $this->assertTrue($this->package->isOrderOnShelf());

        // checks shelf id is 1 (test is here because moveToShelf sets this value)
        $this->assertEquals($this->package->getShelfId(), 1);
    }
    
    /**
     * Tests setStatusDeleted
     */
    public function testSetStatusDeleted()
    {
        $this->assertNotNull($this->order->getOrderPackageId());
        $this->order->setOrderStatusName('deleted');
        $this->order->save();
        $this->assertNull($this->order->getOrderPackageId());
    }

    /**
     * Tests getOrderStatusIcon
     */
    public function testGetOrderStatusIcon()
    {
        $this->assertEquals($this->order->getOrderStatusIcon(),
                '/images/icons/orderStatus/status-real.gif');

        // set status to ready and package to book
        $this->order->setOrderStatusName('ready');
        $this->order->save();
        $this->order->getOrderPackage()->setToBook(1)->save();

        $this->assertEquals($this->order->getOrderStatusIcon(),
                '/images/icons/orderStatus/status-dofakt.gif');
    }
    
    /**
     * Tests hasDiscount method
     */
    public function testHasDiscount()
    {
        $this->assertTrue($this->order->hasDiscount());
        
        $this->order->setDiscountType(null);
        $this->order->setDiscountValue(null);
        $this->order->save();
        
        $this->assertFalse($this->order->hasDiscount());
    }
    
    /**
     * Test automatic setting acceptedAt
     */
    public function testAutoSetAcceptedAt()
    {
        $this->assertNull($this->orderDraft->getAcceptedAt());
        $this->orderDraft->setOrderStatusName(OrderStatusTable::$new);
        $this->orderDraft->save();
        
        $this->assertNotNull($this->orderDraft->getAcceptedAt());
     }

    /**
     * Tests postSave method. 
     */
    public function testPostSave()
    {
        // HOTFOLDER
        $filename = sfConfig::get('sf_test_dir').'/phpunit/data/sample_image.jpg';
        $fileObj = $this->order->importFile($filename);
        
        $filename2 = sfConfig::get('sf_test_dir').'/phpunit/data/sample_image2.jpg';
        $fileObj2 = $this->order->importFile($filename2);
        
        $this->order->refresh(true);
        
        // changes order status
        $this->order->setOrderStatusName(OrderStatusTable::$bindery);
        
        // hotfolder images should change directory
        $this->order->save();
    }
}
