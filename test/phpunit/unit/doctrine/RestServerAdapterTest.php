<?php

require_once(__DIR__ . '/../../bootstrap/boostrap.php');

class RestServerAdapterTest extends BasePhpunitTestCase
        implements sfPhpunitFixtureDoctrineAggregator
{

    /**
     * initalize protected varibles
     */
    protected $object;
    protected $webapi_key;
    protected $product_name;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function _start()
    {
        $this->fixture()->clean()->loadSnapshot('common');

        $this->object = new RestServerAdapter();
        $this->webapi_key = 'LDAvTuD7i53Ef5yXEOabAvt9';
        $this->product_name = 'ulotki-dwustronne';
    }

    /**
     * Test login rest method
     */
    public function testDoLogin()
    {
        $rest = $this->object->doLogin($this->webapi_key, 'admin', 'admin', 'session_handle');
        $this->assertEquals(1, $rest['user_id']);
        $this->assertEquals(RestServerAdapter::E_MSG_SUCCESS, $rest['code']);
        
        // wrong login
        $rest = $this->object->doLogin($this->webapi_key, 'admin_bad', 'admin', 'session_handle');
        $this->assertEquals('E_MSG_ERROR', $rest['code']);
        
        //wrong password
        $rest = $this->object->doLogin($this->webapi_key, 'admin', 'admin_bad', 'session_handle');
        $this->assertEquals('E_MSG_ERROR', $rest['code']);
    }
    
    /**
     * Tests doGetMyData().
     */
    public function testDoGetMyData()
    {
        $rest = $this->object->doLogin($this->webapi_key, 'admin', 'admin', '');
        $this->assertEquals(1, $rest['user_id']);
        $this->assertEquals(RestServerAdapter::E_MSG_SUCCESS, $rest['code']);
      
        $data = $this->object->doGetMyData($this->webapi_key, $rest['session_handle'], false);

        // main data
        $this->assertEquals('admin', $data['user_data']['login']);
        $this->assertEquals('john.doe@gmail.com', $data['user_data']['email']);
        $this->assertEquals('admin', $data['user_data']['display_name']);
        $this->assertEquals('John Doe', $data['user_data']['contact_person']);
        $this->assertEquals('450', $data['user_data']['loyalty_points']);
        $this->assertEquals('1', $data['user_data']['pricelist_id']);
        $this->assertEquals('100.00', $data['user_data']['credit_limit']);
        $this->assertEquals('1234567819', $data['user_data']['id_no']);
        $this->assertEquals('510222132', $data['user_data']['mobile']);
        $this->assertEquals(true, $data['user_data']['notification']['email']);
        $this->assertEquals(true, $data['user_data']['notification']['sms']);
        
        // invoice data
        $this->assertEquals('client1', $data['user_data']['invoice']['name']);
        $this->assertEquals('Ulica 22', $data['user_data']['invoice']['address']);
        $this->assertEquals('01-001', $data['user_data']['invoice']['postal_code']);
        $this->assertEquals('City', $data['user_data']['invoice']['city']);
        $this->assertEquals('1234567819', $data['user_data']['invoice']['tax_id']);
        
        // delivery data
        $delivery = reset($data['user_data']['delivery']['addresses']);
        
        $this->assertEquals('address1', $delivery['name']);
        $this->assertEquals('ulica1', $delivery['address']);
        $this->assertEquals('46-222', $delivery['postal_code']);
        $this->assertEquals('miasto1', $delivery['city']);
        
        // basket data
        $this->assertEquals(1, $data['basket']['no_items']);
        $this->assertEquals(0, $data['basket']['no_itemsCalc']);

        // item
        $this->assertEquals(100.00, $data['basket']['items'][0]['BodyPlusPrice']);
        $this->assertEquals(123.00, $data['basket']['items'][0]['priceWithTax']);
        $this->assertEquals(23, $data['basket']['items'][0]['Tax']);
        $this->assertEquals('Photo Album Large - 30x26 cm', $data['basket']['items'][0]['name']);
        $this->assertEquals(0, $data['basket']['items'][0]['files_count']);
    }
    
    

}
