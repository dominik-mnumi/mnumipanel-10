<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Shipping\APaczkaBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MnumiShippingAPaczkaBundle extends Bundle
{
}
