<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Shipping\APaczkaBundle\API\Exception;

/**
 * Class AuthorizationFailureException
 * @package Mnumi\Shipping\APaczkaBundle\API\Exception
 */
class AuthorizationFailureException extends ApiException
{
    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf("Authorization error: %s %s", $this->getCode(), $this->getMessage());
    }
} 