<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Shipping\APaczkaBundle\API;

use Exception;
use Mnumi\Shipping\APaczkaBundle\API\Exception\UnsupportedOperation;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order;
use SoapClient;

/**
 * Class to connect with APaczka API
 *
 * Class Api
 * @package Mnumi\Shipping\APaczkaBundle\API
 */
class Api
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $wsdl = '?wsdl';

    /**
     * @var string
     */
    private $apiKey = null;

    /**
     * @var string
     */
    private $apiLogin = null;

    /**
     * @var string
     */
    private $apiPassword = null;

    /**
     * @var null|\SoapClient
     */
    private $soapClient = null;
    /**
     * @var bool
     */
    private $test = false;

    private $operations = array(
        "placeOrder",
        "validateAuthData",
        "getCountries",
        "getCollectiveWaybillDocument",
        "getWaybillDocument",
        "getCollectiveTurnInCopyDocument"
    );

    /**
     * @param string $apiUrl
     * @param string $apiLogin
     * @param string $apiPassword
     * @param string $apiKey
     * @param bool $test
     */
    public function __construct($apiUrl, $apiLogin, $apiPassword, $apiKey, $test = false)
    {
        $this->url = $apiUrl;
        $this->apiLogin = $apiLogin;
        $this->apiPassword = $apiPassword;
        $this->apiKey = $apiKey;
        $this->test = $test;

        $this->soapClient = new SoapClient(
            $this->getWsdl(),
            array('trace' => 1, 'exceptions' => 0, 'encoding' => 'UTF-8')
        );

        $this->validateAuthData();
    }

    public 	function placeOrder(Order $order) {
        $PlaceOrderRequest = array();
        $PlaceOrderRequest['authorization'] = $this->getAuth();
        $PlaceOrderRequest['order'] = $order->getOrder();

        $response = $this->soapCall("placeOrder", array('placeOrder' => array('PlaceOrderRequest' => $PlaceOrderRequest)));
        $this->checkResponseError($response);

        return $response->return->order;
    }
    
    public function getCollectiveWaybillDocument($idsArray = false) {
        $req = array();
        $req['authorization'] = $this->getAuth();
        $req['orderIds'] = array();

        if ($idsArray) {
            if (is_array($idsArray)) {
                $req['orderIds'] = array('long' => $idsArray);
            } else {
                $req['orderIds'] = array('long' => $idsArray);
            }
        }

        $getCollectiveWaybillDocumentData = array();
        $getCollectiveWaybillDocumentData['getCollectiveWaybillDocument']['CollectiveWaybillRequest'] = $req;

        $response = $this->soapCall("getCollectiveWaybillDocument", $getCollectiveWaybillDocumentData);
        $this->checkResponseError($response);

        return $response->return->waybillDocument;
    }
    
    public function getCollectiveTurnInCopyDocument($idsArray = false)
    {
        $req = array();
        $req['authorization'] = $this->getAuth();
        $req['orderIds'] = array();

        if ($idsArray)
            if (is_array($idsArray)) {
                $req['orderIds'] = array('long' => $idsArray);
            } else {
                $req['orderIds'] = array('long' => $idsArray);
            }

        $getCollectiveTurnInCopyDocumentData = array();
        $getCollectiveTurnInCopyDocumentData['getCollectiveTurnInCopyDocument']['CollectiveTurnInCopyRequest'] = $req;

        $response = $this->soapCall("getCollectiveTurnInCopyDocument", $getCollectiveTurnInCopyDocumentData);
        $this->checkResponseError($response);

        return $response->return->turnInCopyDocument;
    } 

    /**
     * @return bool|mixed
     * @throws \Mnumi\Shipping\APaczkaBundle\API\Exception\ResponseException
     */
    public function getCountries() {
        $getCountriesData = array();
        $getCountriesData['getCountries']['CountryRequest']['authorization'] = $this->getAuth();

        $response = $this->soapCall("getCountries", $getCountriesData);
        $this->checkResponseError($response);
        return (array)$response->return->countries->Country;
    }

    /**
     * @throws \Mnumi\Shipping\APaczkaBundle\API\Exception\ResponseException
     * @throws \Mnumi\Shipping\APaczkaBundle\API\Exception\AuthorizationFailureException
     * @return bool
     */
    private function validateAuthData()
    {
        $validateAuthData = array();
        $validateAuthData['validateAuthData']['Authorization'] = $this->getAuth();

        $response = $this->soapCall("validateAuthData", $validateAuthData);
        $this->checkResponseError($response, '\Mnumi\Shipping\APaczkaBundle\API\Exception\AuthorizationFailureException');

        return true;
    }

    /**
     * @return array
     */
    public function getAuth()
    {
        $auth = array();
        $auth['apiKey'] = $this->apiKey;
        $auth['login'] = $this->apiLogin;
        $auth['password'] = $this->apiPassword;
        return $auth;
    }

    /**
     * @param mixed $response
     * @param string $exception Exception class
     * @throws \Mnumi\Shipping\APaczkaBundle\API\Exception\ResponseException
     */
    private function checkResponseError($response, $exception = '\Mnumi\Shipping\APaczkaBundle\API\Exception\ResponseException')
    {
        $error = array();
        $messages = (array)$response->return->result->messages;
        if (!empty($messages)) {
            foreach ($messages as $message) {
                $message = (array)$message;
                $error[] = isset($message['description'])?$message['description']:$message['code'];
            }
            if (!empty($error)) {
                throw new $exception(implode(", ", $error));
            }
        }
    }

    /**
     * @param string $operation
     * @param array $SoapBody
     * @return bool|mixed
     * @throws \Mnumi\Shipping\APaczkaBundle\API\Exception\UnsupportedOperation
     */
    private function soapCall($operation, $SoapBody)
    {
        if (!in_array($operation, $this->operations)) {
            throw new UnsupportedOperation(sprintf('Unsupported operation: [%s]', $operation));
        }

        try {
            $response = $this->soapClient->__soapCall($operation, $SoapBody);
            //save soap request and response to file
        } catch (\Exception $ex) {
            return false;
        }

        return $response;
    }
    /**
     * Get WSDL url
     *
     * @return string
     */
    private function getWsdl()
    {
        return $this->url.$this->wsdl;
    }
} 