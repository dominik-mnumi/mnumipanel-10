<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Shipping\APaczkaBundle\API\Resource;

use Mnumi\Bundle\ShippingBundle\Library\ShippingAddress;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order\Address;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order\ReceiverAddress;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order\SenderAddress;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order\Shipment;

/**
 * Class Order
 * @package Mnumi\Shipping\APaczkaBundle\API
 * This library was fork from Apaczka PHP client library
 * @link http://www.apaczka.pl/dla-programisty/
 */
class Order
{
    private $notificationDelivered = array();
    private $notificationException = array();
    private $notificationNew = array();
    private $notificationSent = array();

    private $accountNumber = "";
    private $codAmount = "";

    private $orderPickupType = "SELF";
    private $pickupTimeFrom = "";
    private $pickupTimeTo = "";
    private $pickupDate = "";

    private $options = "";

    private $addressReceiver = array();
    private $addressSender = array();

    private $referenceNumber = '';
    private $serviceCode = "";
    private $isDomestic = "true";
    private $contents = "";

    private $shipments = array();

    private static $dictServiceCode = array(
        'UPS_K_STANDARD',
        'UPS_K_EX_SAV',
        'UPS_K_EX',
        'UPS_K_EXP_PLUS',
        'DPD_CLASSIC',
        'DHLSTD',
        'DHL12',
        'DHL09',
        'DHL1722',
        'UPS_Z_STANDARD',
        'UPS_Z_EX_SAV',
        'UPS_Z_EX',
        'UPS_Z_EXPEDITED',
        'KEX_EXPRESS',
        'POCZTA_POLSKA',
        'SIODEMKA_STD'
    );
    private static $dictOrderPickupType = array('COURIER', 'SELF', 'EVERYDAY', 'PHONE');
    private static $dictOrderOptions = array('POBRANIE', 'ZWROT_DOK', 'DOR_OSOBA_PRYW', 'DOST_SOB', 'PODPIS_DOROS');

    private $notification = array(
        'isReceiverEmail' => true,
        'isReceiverSms' => false,
        'isSenderEmail' => true,
        'isSenderSms' => false
    );

    /**
     *
     */
    public function __construct()
    {
        $this->setNotificationDelivered($this->notification);
        $this->setNotificationException($this->notification);
        $this->setNotificationNew($this->notification);
        $this->setNotificationSent($this->notification);
    }

    /**
     * @return array
     */
    public function getOrder() {
        $order = array();

        if (!($this->accountNumber == "" || $this->codAmount == "")) {
            $order['accountNumber'] = $this->accountNumber;
            $order['codAmount'] = $this->codAmount;
        }

        $order['notificationDelivered'] = $this->notificationDelivered;
        $order['notificationException'] = $this->notificationException;
        $order['notificationNew'] = $this->notificationNew;
        $order['notificationSent'] = $this->notificationSent;

        $order['orderPickupType'] = $this->orderPickupType;

        if ($this->pickupTimeFrom != '' and $this->pickupTimeTo != '') {
            $order['pickupTimeFrom'] = $this->pickupTimeFrom;
            $order['pickupTimeTo'] = $this->pickupTimeTo;
            $order['pickupDate'] = $this->pickupDate;
        }

        $order['options'] = $this->options;

        $order['serviceCode'] = $this->serviceCode;
        $order['referenceNumber'] = $this->referenceNumber;
        $order['isDomestic'] = $this->isDomestic;
        $order['contents'] = $this->contents;

        $order['receiver'] = $this->addressReceiver;
        $order['sender'] = $this->addressSender;

        $order['shipments'] = $this->createShipment();

        return $order;
    }

    /**
     * @param Shipment $shipment
     * @return $this
     */
    public function addShipment(Shipment $shipment) {
        $this->shipments[] = $shipment;

        return $this;
    }

    /**
     * @param string $option
     * @throws \Exception
     */
    public function addOrderOption($option)
    {
        if (!in_array($option, self::$dictOrderOptions)) {
            throw new \Exception(sprintf('UNSUPPORTED order option: [%s] must be one of: %s', $option, implode(", ", self::$dictOrderOptions)));
        }

        if ($this->options == "") {
            $this->options = array('string' => $option);
        } else if (!is_array($this->options['string'])) {
            $tmp_option = $this->options['string'];

            if ($tmp_option != $option) {
                $this->options['string'] = array($tmp_option, $option);
            }
        } else {
            if (in_array($option, self::$dictOrderOptions)) {
                $this->options['string'][] = $option;
            }
        }
    }

    /**
     * @param $orderPickupType
     * @param $pickupTimeFrom
     * @param $pickupTimeTo
     * @param $pickupDate
     * @throws \Exception
     */
    private function setPickup($orderPickupType, $pickupTimeFrom, $pickupTimeTo, $pickupDate)
    {
        if (!in_array($orderPickupType, self::$dictOrderPickupType))
        {
            throw new \Exception(sprintf('UNSUPPORTED order pickup type: [%s] must be one of: %s', $orderPickupType, implode(", ", self::$dictOrderPickupType)));
        }

        $this->orderPickupType = $orderPickupType;
        $this->pickupTimeFrom = $pickupTimeFrom;
        $this->pickupDate = $pickupDate;
        $this->pickupTimeTo = $pickupTimeTo;
    }

    /**
     * @return array
     */
    public function createShipment() {
        $return = array();
        $position = 0;
        $t_tmp = $this->shipments;

        if (!is_array($t_tmp)) {
            $t_tmp = array($t_tmp);
        }

        /** @var Shipment $a */
        foreach ($t_tmp as $a) {
            $ship = array();
            $ship['dimension1'] = $a->length;
            $ship['dimension2'] = $a->width;
            $ship['dimension3'] = $a->height;
            $ship['weight'] = $a->weight;
            $ship['shipmentTypeCode'] = $a->getShipmentTypeCode();
            $ship['position'] = $position;

            if ($a->getShipmentValue() > 0) {
                $ship['shipmentValue'] = $a->getShipmentValue();
            }

            $ship['options'] = $a->getOptions();
            
            $position++;
            $return[$position] = $ship;
        }

        if ($position === 1) {
            return array('Shipment' => $return[1]);
        }

        return array('Shipment' => $return);
    }

    /**
     * Set receiver address for current order
     *
     * @param array $address
     */
    function setReceiverAddress(array $address)
    {
        $this->addressReceiver = $address;
    }

    /**
     * Set sender address for current order
     *
     * @param SenderAddress $address
     */
    function setSenderAddress(SenderAddress $address)
    {
        $this->addressSender = $address->get();
    }

    /**
     * @return array
     */
    public function getNotificationDelivered()
    {
        return $this->notificationDelivered;
    }

    /**
     * @param array $notificationDelivered
     */
    public function setNotificationDelivered($notificationDelivered)
    {
        $this->notificationDelivered = $notificationDelivered;
    }

    /**
     * @return array
     */
    public function getNotificationException()
    {
        return $this->notificationException;
    }

    /**
     * @param array $notificationException
     */
    public function setNotificationException($notificationException)
    {
        $this->notificationException = $notificationException;
    }

    /**
     * @return array
     */
    public function getNotificationNew()
    {
        return $this->notificationNew;
    }

    /**
     * @param array $notificationNew
     */
    public function setNotificationNew($notificationNew)
    {
        $this->notificationNew = $notificationNew;
    }

    /**
     * @return array
     */
    public function getNotificationSent()
    {
        return $this->notificationSent;
    }

    /**
     * @param array $notificationSent
     */
    public function setNotificationSent($notificationSent)
    {
        $this->notificationSent = $notificationSent;
    }

    /**
     * @return string
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * @param string $referenceNumber
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;
    }

    /**
     * @return string
     */
    public function getServiceCode()
    {
        return $this->serviceCode;
    }

    /**
     * @param string $serviceCode
     * @return Order
     * @throws \Exception
     */
    public function setServiceCode($serviceCode)
    {
        if (!in_array($serviceCode, self::$dictServiceCode))
        {
            throw new \Exception(sprintf('UNSUPPORTED service code: [%s] must be one of: %s', $serviceCode, implode(", ", self::$dictServiceCode)));
        }

        $this->serviceCode = $serviceCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * @param string $contents
     */
    public function setContents($contents)
    {
        $this->contents = $contents;
    }
    
    
} 