<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Shipping\APaczkaBundle\API\Resource\Order;

use Doctrine\Common\Collections\Criteria;
use Mnumi\Bundle\OrderBundle\Entity\Order;
use Mnumi\Bundle\ShippingBundle\Library\ShippingInformation;

/**
 * Class ReceiverAddress
 * @package Mnumi\Shipping\APaczkaBundle\API\Order
 */
class ReceiverAddress extends ShippingInformation
{
    /**
     * @var \Mnumi\Bundle\OrderBundle\Entity\Order
     */
    private $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    
    /**
     * @return array
     */
    public function get()
    {
        $this->setCity($this->order->getDeliveryCity());
        $this->setName($this->order->getDeliveryName());
        $this->setStreet($this->order->getDeliveryStreet());
        $this->setPostcode($this->order->getDeliveryPostcode());
        $this->setClientName($this->order->getClient()->getFullname());
        $this->setCountryCode($this->order->getDeliveryCountry());
        $this->setDescription($this->order->getDescription());
        $criteria = Criteria::create()->where(Criteria::expr()->in('notificationType', array(2)));
        $phone = $this->order->getUser()->getUserContact()->matching($criteria)->first();
        $this->setPhoneNumber($phone->getValue());

        $this->setEmail($this->order->getUser()->getEmailAddress());
        
        $address = array();
        $address['name'] = substr($this->getClientName(), 0, 50);
        $address['contactName'] = $this->getName();

        $street = $this->convertAddressLine($this->getStreet());
        $address['addressLine1'] = $street[0];
        $address['addressLine2'] = isset($street[1])?$street[1]:null;
        $address['city'] = $this->getCity();
        $address['countryId'] = $this->getCountryCode();
        $address['postalCode'] = $this->getPostcode();

        $address['email'] = $this->getEmail();
        $address['phone'] = $this->getPhoneNumber();
        
        return $address;
    }
}