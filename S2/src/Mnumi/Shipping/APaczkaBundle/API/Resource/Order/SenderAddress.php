<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Shipping\APaczkaBundle\API\Resource\Order;

use Mnumi\Bundle\ShippingBundle\Library\ShippingInformation;

/**
 * Class SenderAddress
 * @package Mnumi\Shipping\APaczkaBundle\API\Order
 */
class SenderAddress extends ShippingInformation
{
    /**
     * @var array
     */
    private $data;

    /**
     * Set data for address
     *
     * @param array $data configuration from carrier
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    
    /**
     * @return array
     */
    public function get()
    {
        $this->setCity($this->data['shipper_address_city']);
        $this->setName($this->data['shipper_name']);
        $this->setStreet($this->data['shipper_address_street']);
        $this->setPostcode($this->data['shipper_address_postal_code']);
        $this->setCountryCode($this->data['shipper_address_country_code']);
        $this->setDescription(null);

        $this->setPhoneNumber($this->data['shipper_phone']);
        $this->setEmail($this->data['shipper_email']);
        
        $address = array();
        $address['name'] = substr($this->getName(), 0, 50);
        $address['contactName'] = $this->getName();

        $street = $this->convertAddressLine($this->getStreet());
        $address['addressLine1'] = $street[0];
        $address['addressLine2'] = isset($street[1])?$street[1]:null;
        $address['city'] = $this->getCity();
        $address['countryId'] = $this->getCountryCode();
        $address['postalCode'] = $this->getPostcode();

        $address['email'] = $this->getEmail();
        $address['phone'] = $this->getPhoneNumber();
        return $address;
    }
}