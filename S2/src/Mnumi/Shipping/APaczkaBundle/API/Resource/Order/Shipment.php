<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Shipping\APaczkaBundle\API\Resource\Order;

/**
 * Class Shipment
 * @package Mnumi\Shipping\APaczkaBundle\API\Resource\Order
 */
class Shipment {

    var $length = '';
    var $width = '';
    var $height = '';
    var $weight = '';
    private $shipmentTypeCode = '';
    private $shipmentValue = '';
    private $options = '';
    private static $dictShipmentOptions = array('UBEZP', 'PRZES_NIETYP', 'DUZA_PACZKA');
    private static $dictShipmentTypeCode = array('LIST', 'PACZ', 'PALETA');

    /**
     * @param string $shipmentTypeCode
     * @param string $length
     * @param string $width
     * @param string $height
     * @param string $weight
     */
    public function __construct($shipmentTypeCode = '', $length = '', $width = '', $height = '', $weight = '') {
        if ($shipmentTypeCode == 'LIST') {
            $this->createShipment($shipmentTypeCode, 0, 0, 0, 0);
        } else {
            if ($length != '' && $width != '' && $height != '' && $weight != '' && $shipmentTypeCode != '') {
                $this->createShipment($shipmentTypeCode, $length, $width, $height, $weight);
            }
        }
    }

    /**
     * @return string
     */
    function getShipmentTypeCode() {
        return $this->shipmentTypeCode;
    }

    /**
     * @param $shipmentTypeCode
     * @throws \Exception
     */
    function setShipmentTypeCode($shipmentTypeCode) {
        if (!in_array($shipmentTypeCode, self::$dictShipmentTypeCode)) {
            throw new \Exception(sprintf('UNSUPPORTED service code: [%s] must be one of: %s', $shipmentTypeCode, implode(", ", self::$dictShipmentTypeCode)));
        }

        $this->shipmentTypeCode = $shipmentTypeCode;
    }

    /**
     * @return string
     */
    function getShipmentValue() {
        return $this->shipmentValue;
    }

    /**
     * @param $value
     * @throws \Exception
     */
    function setShipmentValue($value) {
        if (!$value > 0) {
            throw new \Exception(sprintf('UNSUPPORTED ShipmentValue: [%s] ShipmentValue must be greater then 0', $value));
        }

        $this->shipmentValue = $value;
        $this->addOrderOption('UBEZP');
    }

    /**
     * @return string
     */
    function getOptions() {
        return $this->options;
    }

    /**
     * @param $option
     * @throws \Exception
     */
    function addOrderOption($option) {
        if (!in_array($option, self::$dictShipmentOptions)) {
            throw new \Exception(sprintf('UNSUPPORTED order option: [%s] must be one of: %s', $option, implode(", ", self::$dictShipmentOptions)));
        }

        if ($this->options == "") {
            $this->options = array('string' => $option);
        } else if (!is_array($this->options['string'])) {
            $tmp_option = $this->options['string'];

            if ($tmp_option != $option) {
                $this->options['string'] = array($tmp_option, $option);
            }
        } else {
            $this->options['string'][] = $option;
        }
    }

    /**
     * @param $shipmentTypeCode
     * @param string $length
     * @param string $width
     * @param string $height
     * @param string $weight
     */
    private function createShipment($shipmentTypeCode, $length = '', $width = '', $height = '', $weight = '') {

        $this->setShipmentTypeCode($shipmentTypeCode);

        $this->length = $length;
        $this->width = $width;
        $this->height = $height;

        $this->weight = $weight;

        if ((300 < (2 * $length + 2 * $width + $height) && (2 * $length + 2 * $width + $height) < 330) || (32 < $weight) && ($weight < 70)) {
            $this->addOrderOption('PRZES_NIETYP');
        }
        /*
        if(330<(2*$length+2*$width+$height)<419 || $weight >= 70){
            $this->addOrderOption('DUZA_PACZKA');
        }
        */
    }

} 