<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Shipping\APaczkaBundle;

use Mnumi\Bundle\ShippingBundle\Library\ShippingInformation;
use Mnumi\Bundle\ShippingBundle\Library\ShippingInterface;
use Mnumi\Shipping\APaczkaBundle\API\Api;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order\ReceiverAddress;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order\SenderAddress;
use Mnumi\Shipping\APaczkaBundle\API\Resource\Order\Shipment;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

/**
 * Class APaczkaShipping
 * @package Mnumi\Shipping\APaczkaBundle
 */
class APaczkaShipping implements ShippingInterface
{
    /**
     * @var string
     */
    private $dataDir;

    /**
     * @var string
     */
    private $cacheDir;

    /**
     * @var int
     */
    private $packageId;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var Api
     */
    private $api;
    private $outputFilenameReport;
    private $outputFilename;
    /**
     * @var ShippingAddress
     */
    private $shippingAddress;

    /**
     * Transport number
     * 
     * @var string
     */
    private $transportNumber;

    /**
     * Creates instance of Shipping class.
     *
     * @param array $configuration
     * @param $dataDir
     * @param $cacheDir
     * @throws \ConfigurationException
     */
    public function __construct(array $configuration, $dataDir, $cacheDir)
    {
        $this->configuration = $configuration;
        if($this->checkConfiguration())
        {
            $this->api = new Api(
                $this->configuration['api_url'],
                $this->configuration['api_login'],
                $this->configuration['api_password'],
                $this->configuration['api_key']
            );
        }

        $this->dataDir = $this->getDirectory($dataDir);
        $this->cacheDir = $this->getDirectory($cacheDir);
    }

    /**
     * Is not used, except with the UPS
     * @return void
     */
    public function getImageFormat()
    {
        return 'pdf';
    }

    public function generatePrintLabel(ShippingInformation $receiverAddress)
    {
        $this->shippingAddress = $receiverAddress;
        $filename = $this->generateFilename();

        $this->outputFilenameReport = $this->cacheDir. '/' . $filename.'.xml';

        $senderAddress = new SenderAddress($this->configuration);

        try
        {
            $address = array();
            $address['name'] = substr($receiverAddress->getClientName(), 0, 30);
            $address['contactName'] = $receiverAddress->getName();

            $street = $receiverAddress->convertAddressLine($receiverAddress->getStreet());
            $address['addressLine1'] = $street[0];
            $address['addressLine2'] = isset($street[1])?$street[1]:null;
            $address['city'] = $receiverAddress->getCity();
            $address['countryId'] = $receiverAddress->getCountryCode();
            $address['postalCode'] = $receiverAddress->getPostcode();

            $address['email'] = $receiverAddress->getEmail();
            $address['phone'] = $receiverAddress->getPhoneNumber() ?: $receiverAddress->getAdditionalInformationByKey('phone');

            $order = new Order();
            $order->setReceiverAddress($address);
            $order->setSenderAddress($senderAddress);
            $order->setServiceCode($this->configuration['service_code']);

            /**
             * Set reference number as package ID
             */
            if(isset($this->configuration['reference_number']) && !empty($this->configuration['reference_number']))
            {
                $order->setReferenceNumber(sprintf('Package: %s', $this->configuration['reference_number']));
            }
            $this->outputFilename = $this->dataDir.'/'.$filename.'.'.$this->getImageFormat();

            $order->setContents("Web2print");
            $shipment = new Shipment(
                $this->configuration['shipment_type_code'],
                $this->configuration['package_dimensions_length'],
                $this->configuration['package_dimensions_width'],
                $this->configuration['package_dimensions_height'],
                $this->configuration['package_weight']
            );
            $order->addShipment($shipment);

            $order = $this->api->placeOrder($order);
            $id = $order->id;
            $this->transportNumber = $order->waybillNumber;
            file_put_contents($this->outputFilename, $this->api->getCollectiveWaybillDocument($id));

            return $this->filename;
        } catch(\Exception $e)
        {
            throw new \Exception('Error: ' . $e->getMessage() . '. Please check log file: ' . $this->outputFilenameReport);
        }
    }

    /**
     * @throws InvalidConfigurationException
     * @return bool
     */
    private function checkConfiguration()
    {
        $error = array();
        if(!isset($this->configuration['api_url']) || empty($this->configuration['api_url']))
        {
            $error[] = 'ApiUrl can not be empty.';
        }
        if(!isset($this->configuration['api_login']) || empty($this->configuration['api_login']))
        {
            $error[] = 'ApiLogin can not be empty';
        }
        if(!isset($this->configuration['api_password']) || empty($this->configuration['api_password']))
        {
            $error[] = 'ApiPassword can not be empty';
        }
        if(!isset($this->configuration['api_key']) || empty($this->configuration['api_key']))
        {
            $error[] = 'ApiKey can not be empty';
        }
        if(!isset($this->configuration['service_code']) || empty($this->configuration['service_code']))
        {
            $error[] = 'Service code can not be empty';
        }
        if(!isset($this->configuration['shipment_type_code']) || empty($this->configuration['shipment_type_code']))
        {
            $error[] = 'Shipment type code can not be empty';
        }
        if(!isset($this->configuration['package_weight']) || empty($this->configuration['package_weight']))
        {
            $error[] = 'Package weight can not be empty';
        }
        if(!isset($this->configuration['package_dimensions_length']) || empty($this->configuration['package_dimensions_length']))
        {
            $error[] = 'Package dimensions length can not be empty';
        }
        if(!isset($this->configuration['package_dimensions_width']) || empty($this->configuration['package_dimensions_width']))
        {
            $error[] = 'Package dimensions width can not be empty';
        }
        if(!isset($this->configuration['package_dimensions_height']) || empty($this->configuration['package_dimensions_height']))
        {
            $error[] = 'Package dimensions height can not be empty';
        }
        if($error)
        {
            throw new InvalidConfigurationException(sprintf('General error: %s %s. FIx configuration for APaczka shipping method.', 500, implode(", ", $error)));
        }

        return true;
    }

    /**
     * Return filename
     * @return string
     */
    public function getFilename() {

        return $this->filename;
    }

    /**
     * Set package id
     * @param int $id
     */
    public function setPackageId($id) {

        $this->packageId = $id;
    }

    /**
     * Get package id
     * @return int
     */
    public function getPackageId() {

        return $this->packageId;
    }


    /**
     * Returns pdf filename.
     *
     * @throws InvalidConfigurationException
     * @return string
     */
    private function generateFilename()
    {

        $this->filename = sprintf('%s_%s_%s_%s',
            $this->shippingAddress->getPackageId(),
            substr(strrchr(__CLASS__, "\\"), 1),
            date('Y_m_d_G_i_s'),
            md5(rand(0, 99999))
        );
        return $this->filename;
    }

    /**
     * @param string $dir
     * @return string
     * @throws InvalidConfigurationException
     */
    private function getDirectory($dir)
    {
        if (!file_exists($dir)) {
            throw new InvalidConfigurationException(sprintf('%s does not exist', $dir));
        }

        return realpath($dir);
    }

    public function getTransportNumber()
    {
        return $this->transportNumber;
    }
}