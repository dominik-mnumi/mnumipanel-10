<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CacheProductCalculationProductData class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Shipping\ElektronicznyNadawcaBundle\API\Resources;

use PocztaPolska\ElektronicznyNadawca\adresType;

class Package
{
    const TYPE_BUSINESS = 'business';
    const TYPE_TRADING = 'trading';

    /**
     * Type to class mapping
     * @var array
     */
    private static $packageTypes = array(
        'business' => 'PocztaPolska\ElektronicznyNadawca\przesylkaBiznesowaType',
        'trading' => 'PocztaPolska\ElektronicznyNadawca\przesylkaHandlowaType'
    );

    /**
     * @var PocztaPolska\ElektronicznyNadawca\przesylkaType
     */
    private $package;

    /** @var integer  */
    private $sendOfficeId;

    public function __construct($packageType)
    {
        $this->checkType($packageType);

        $packageClass = self::$packageTypes[$packageType];
        $this->package = new $packageClass();
    }

    /**
     * Set package address
     *
     * @param adresType $address
     */
    public function setAddress(adresType $address)
    {
        $this->package->adres = $address;
    }

    /**
     * Get package address object
     *
     * @return adresType
     */
    public function getAddress()
    {
        return $this->package->adres;
    }

    /**
     * Set package size
     *
     * @param string $size
     */
    public function setPackageSize($size)
    {
        $this->package->gabaryt = $size;
    }

    /**
     * Get package's size
     *
     * @return string
     */
    public function getPackageSize()
    {
        return $this->package->gabaryt;
    }

    /**
     * Set package guid
     *
     * @param string $guid
     */
    public function setGuid($guid)
    {
        $this->package->guid = $guid;
    }

    /**
     * Get package's guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->package->guid;
    }

    /**
     * Set package description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->package->opis = $description;
    }

    /**
     * Get package's description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->package->opis;
    }

    /**
     * Set send office id
     *
     * @param integer $id
     */
    public function setSendOfficeId($id)
    {
        $this->sendOfficeId = $id;
    }

    /**
     * Get send office id
     *
     * @return integer
     */
    public function getSendOfficeId()
    {
        return $this->sendOfficeId;
    }

    /**
     * Checks if given package type is valid
     *
     * @param  string     $packageType
     * @throws \Exception
     */
    private function checkType($packageType)
    {
        if (!isset(self::$packageTypes[$packageType])) {

            throw new \Exception('Wrong package type');
        }
    }

    /**
     * Checks if package is correctly configured
     *
     * @return boolean
     * @throws \Exception
     */
    private function checkPackage()
    {
        $c = get_class($this->package);
        if (!$this->package->adres instanceof adresType) {

            throw new \Exception('Package address in invalid, shoud be instance of "adresType"');
        }

        if (strlen($this->package->guid) < 32) {

            throw new \Exception('Wrong package\'s GUID');
        }

        if (empty($this->package->gabaryt)) {

            throw new \Exception('Package size is not set');
        }

        return true;
    }

    /**
     * Returns configured package
     *
     * @return PocztaPolska\ElektronicznyNadawca\przesylkaType
     */
    public function getSoapPackage()
    {
        $this->checkPackage();

        return $this->package;
    }
}
