<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CacheProductCalculationProductData class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Shipping\ElektronicznyNadawcaBundle\API\Resources;

use Mnumi\Bundle\ShippingBundle\Library\ShippingInformation;
use PocztaPolska\ElektronicznyNadawca\adresType;

/**
 * API Address class
 */
class Address
{
    /** @var ShippingInformation */
    private $shippingInformation;

    /**
     * @param ShippingInformation $shippingInformation
     */
    public function __construct(ShippingInformation $shippingInformation)
    {

        $this->shippingInformation = $shippingInformation;
    }

    /**
     * Converts ShippingInformation into  adresType object
     * @return adresType
     */
    public function getSoapAddress()
    {
        $address = new adresType();

        $address->nazwa = $this->shippingInformation->getName();
        $address->ulica = $this->shippingInformation->getStreet();
        $address->miejscowosc = $this->shippingInformation->getCity();
        $address->kodPocztowy = $this->shippingInformation->getPostcode();
        $address->email = $this->shippingInformation->getEmail();
        $address->telefon = $this->shippingInformation->getPhoneNumber();

        return $address;
    }
}
