<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CacheProductCalculationProductData class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Shipping\ElektronicznyNadawcaBundle\API\Resources;

class PrintLabel
{

    /**  @var string */
    private $transportNumber;

    /** @var string */
    private $pdfContent;

    /**
     * Set transport number
     *
     * @param string $transportNumber
     */
    public function setTransportNumber($transportNumber)
    {
        $this->transportNumber = $transportNumber;
    }

    /**
     * Get transport number
     *
     * @return string
     */
    public function getTransportNumber()
    {
        return $this->transportNumber;
    }

    /**
     * Set pdf content
     *
     * @param string $content
     */
    public function setPdfContent($content)
    {
        $this->pdfContent = $content;
    }

    /**
     * Get pdf content
     * @return string
     */
    public function getPdfContent()
    {
        return $this->pdfContent;
    }
}
