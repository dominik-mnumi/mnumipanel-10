<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Api class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Shipping\ElektronicznyNadawcaBundle\API;

use Mnumi\Shipping\ElektronicznyNadawcaBundle\API\Resources\Package;
use Mnumi\Shipping\ElektronicznyNadawcaBundle\API\Resources\PrintLabel;
use PocztaPolska\ElektronicznyNadawca\ElektronicznyNadawca;
use PocztaPolska\ElektronicznyNadawca\getPasswordExpiredDate;
use PocztaPolska\ElektronicznyNadawca\getProfilList;
use PocztaPolska\ElektronicznyNadawca\getUrzedyNadania;
use PocztaPolska\ElektronicznyNadawca\clearEnvelope;
use PocztaPolska\ElektronicznyNadawca\addShipment;
use PocztaPolska\ElektronicznyNadawca\getAddresLabelByGuid;
use PocztaPolska\ElektronicznyNadawca\sendEnvelope;

class Api
{
    const PROD_ENV = 'prod_environment';
    const TEST_ENV =  'test_environment';

    private $eNadawca;

    private static $wsdl = array(
        'prod_environment' => 'Resources/WSDL/en.wsdl',
        'test_environment' => 'Resources/WSDL/en-test.wsdl'
    );

    /** @var string */
    private $passwordExpiredDate;

    /** @var boolean */
    private $isLogged;

    public function __construct($login, $password, $env = 'prod_environment')
    {
        if (!isset(self::$wsdl[$env])) {

            throw new Exception('Wrong "env" parameter');
        }

        $wsdlPath = __DIR__ . '/' . self::$wsdl[$env];
        $this->eNadawca = new ElektronicznyNadawca($wsdlPath, array(
            'classmap' => $this->getClassMap(),
            'login' => $login,
            'password' => $password
        ));

        try {
            $getPassExpired = new getPasswordExpiredDate();
            $this->passwordExpiredDate = $this->eNadawca->getPasswordExpiredDate($getPassExpired)->dataWygasniecia;
            $this->isLogged = true;

        } catch (\SoapFault $e) {

            $this->isLogged = false;
        }
    }

    /**
     * Get is logged
     *
     * @return boolean
     */
    public function isLogged()
    {
        return $this->isLogged;
    }

    /**
     * Get ElektroniczyNadawca soap client
     *
     * @return ElektronicznyNadawca
     */
    private function getElektronicznyNadawca()
    {
        return $this->eNadawca;
    }

    /**
     * Get senders profiles of ElektronicznyNadawca's account
     *
     * @return array
     * @throws Exception
     */
    public function getSenderProfiles()
    {
        $profiles = array();

        try {

            $eNadawca = $this->getElektronicznyNadawca();
            $getProfileList = new getProfilList();

            $profilesList = $eNadawca->getProfilList($getProfileList);

            if (empty($profilesList->profil)) {
                throw new Exception('Problem with getting profiles list by ElektronicznyNadawca\'s webapi occured');
            }

            $profilesList = is_array($profilesList->profil) ?
                    $profilesList->profil :
                    array($profilesList->profil);

            foreach ($profilesList as $profile) {
                $profiles[$profile->idProfil] = sprintf("%s, %s %s / %s, %s",
                        $profile->nazwaSkrocona,
                        $profile->ulica,
                        $profile->numerDomu,
                        $profile->numerLokalu,
                        $profile->miejscowosc);
            }
        } catch (\SoapFault $e) {

        }

        return $profiles;
    }

    /**
     * Get send offices configured in ElektroniczyNadawca's account
     *
     * @return array
     * @throws Exception
     */
    public function getSendOffices()
    {
        $eNadawca = $this->getElektronicznyNadawca();
        $sendOffices = $eNadawca->getUrzedyNadania(
                new getUrzedyNadania());

        if (empty($sendOffices->urzedyNadania)) {
            throw new Exception('Problem with getting send offices by ElektronicznyNadawca\'s webapi occured');
        }

        $sendOffices = is_array($sendOffices->urzedyNadania) ?
                $sendOffices->urzedyNadania :
                array($sendOffices->urzedyNadania);

        $sendOfficesList = array();

        foreach ($sendOffices as $office) {
            $sendOfficesList[$office->urzadNadania] = $office->nazwaWydruk;
        }

        return $sendOfficesList;
    }

    /**
     * Clear envelope
     */
    private function clearEnvelope()
    {
        $this->eNadawca->clearEnvelope(
                    new clearEnvelope());
    }

    /**
     * Send package
     *
     * @param Package $package
     */
    public function sendPackage(Package $package)
    {
        $this->clearEnvelope();

        $packageSoap = $package->getSoapPackage();

        $addShipment = new addShipment();
        $addShipment->przesylki = array($packageSoap);

        $this->eNadawca->addShipment($addShipment);

        $getAddresLabelByGuid = new getAddresLabelByGuid();
        $getAddresLabelByGuid->guid = array($package->getGuid());
        $addressLabelResponse = $this->eNadawca->getAddresLabelByGuid($getAddresLabelByGuid);
        $responseContent = $addressLabelResponse->content;

        $printLabel = new PrintLabel();
        $printLabel->setPdfContent($responseContent->pdfContent);
        $printLabel->setTransportNumber($responseContent->nrNadania);

        $sendEnvelope = new sendEnvelope();

        $sendEnvelope->urzadNadania = $package->getSendOfficeId();
        $this->eNadawca->sendEnvelope($sendEnvelope);

        return $printLabel;
    }

    /**
     * Generate guid
     *
     * @return string
     */
    public function generateGuid()
    {
        mt_srand((double) microtime() * 10000);
        $randomString = strtoupper(md5(uniqid(rand(), true)));
        $guid = substr($randomString, 0, 32);

        return $guid;
    }

    private function getClassMap()
    {
        return array(
            'addShipment' => 'PocztaPolska\ElektronicznyNadawca\addShipment',
            'addShipmentResponse' => 'PocztaPolska\ElektronicznyNadawca\addShipmentResponse',
            'przesylkaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaType',
            'pocztexKrajowyType' => 'PocztaPolska\ElektronicznyNadawca\pocztexKrajowyType',
            'umowaType' => 'PocztaPolska\ElektronicznyNadawca\umowaType',
            'masaType' => 'PocztaPolska\ElektronicznyNadawca\masaType',
            'numerNadaniaType' => 'PocztaPolska\ElektronicznyNadawca\numerNadaniaType',
            'changePassword' => 'PocztaPolska\ElektronicznyNadawca\changePassword',
            'changePasswordResponse' => 'PocztaPolska\ElektronicznyNadawca\changePasswordResponse',
            'terminRodzajType' => 'PocztaPolska\ElektronicznyNadawca\terminRodzajType',
            'uiszczaOplateType' => 'PocztaPolska\ElektronicznyNadawca\uiszczaOplateType',
            'wartoscType' => 'PocztaPolska\ElektronicznyNadawca\wartoscType',
            'kwotaPobraniaType' => 'PocztaPolska\ElektronicznyNadawca\kwotaPobraniaType',
            'sposobPobraniaType' => 'PocztaPolska\ElektronicznyNadawca\sposobPobraniaType',
            'sposobPrzekazaniaType' => 'PocztaPolska\ElektronicznyNadawca\sposobPrzekazaniaType',
            'sposobDoreczeniaPotwierdzeniaType' => 'PocztaPolska\ElektronicznyNadawca\sposobDoreczeniaPotwierdzeniaType',
            'iloscPotwierdzenOdbioruType' => 'PocztaPolska\ElektronicznyNadawca\iloscPotwierdzenOdbioruType',
            'dataDlaDostarczeniaType' => 'PocztaPolska\ElektronicznyNadawca\dataDlaDostarczeniaType',
            'razemType' => 'PocztaPolska\ElektronicznyNadawca\razemType',
            'nazwaType' => 'PocztaPolska\ElektronicznyNadawca\nazwaType',
            'nazwa2Type' => 'PocztaPolska\ElektronicznyNadawca\nazwa2Type',
            'ulicaType' => 'PocztaPolska\ElektronicznyNadawca\ulicaType',
            'numerDomuType' => 'PocztaPolska\ElektronicznyNadawca\numerDomuType',
            'numerLokaluType' => 'PocztaPolska\ElektronicznyNadawca\numerLokaluType',
            'miejscowoscType' => 'PocztaPolska\ElektronicznyNadawca\miejscowoscType',
            'kodPocztowyType' => 'PocztaPolska\ElektronicznyNadawca\kodPocztowyType',
            'paczkaPocztowaType' => 'PocztaPolska\ElektronicznyNadawca\paczkaPocztowaType',
            'kategoriaType' => 'PocztaPolska\ElektronicznyNadawca\kategoriaType',
            'gabarytType' => 'PocztaPolska\ElektronicznyNadawca\gabarytType',
            'paczkaPocztowaPLUSType' => 'PocztaPolska\ElektronicznyNadawca\paczkaPocztowaPLUSType',
            'przesylkaPobraniowaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaPobraniowaType',
            'przesylkaNaWarunkachSzczegolnychType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaNaWarunkachSzczegolnychType',
            'przesylkaPoleconaKrajowaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaPoleconaKrajowaType',
            'przesylkaHandlowaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaHandlowaType',
            'przesylkaListowaZadeklarowanaWartoscType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaListowaZadeklarowanaWartoscType',
            'przesylkaFullType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaFullType',
            'errorType' => 'PocztaPolska\ElektronicznyNadawca\errorType',
            'adresType' => 'PocztaPolska\ElektronicznyNadawca\adresType',
            'sendEnvelope' => 'PocztaPolska\ElektronicznyNadawca\sendEnvelope',
            'sendEnvelopeResponseType' => 'PocztaPolska\ElektronicznyNadawca\sendEnvelopeResponseType',
            'urzadNadaniaType' => 'PocztaPolska\ElektronicznyNadawca\urzadNadaniaType',
            'getUrzedyNadania' => 'PocztaPolska\ElektronicznyNadawca\getUrzedyNadania',
            'getUrzedyNadaniaResponse' => 'PocztaPolska\ElektronicznyNadawca\getUrzedyNadaniaResponse',
            'clearEnvelope' => 'PocztaPolska\ElektronicznyNadawca\clearEnvelope',
            'clearEnvelopeResponse' => 'PocztaPolska\ElektronicznyNadawca\clearEnvelopeResponse',
            'urzadNadaniaFullType' => 'PocztaPolska\ElektronicznyNadawca\urzadNadaniaFullType',
            'guidType' => 'PocztaPolska\ElektronicznyNadawca\guidType',
            'ePrzesylkaType' => 'PocztaPolska\ElektronicznyNadawca\ePrzesylkaType',
            'eSposobPowiadomieniaType' => 'PocztaPolska\ElektronicznyNadawca\eSposobPowiadomieniaType',
            'eKontaktType' => 'PocztaPolska\ElektronicznyNadawca\eKontaktType',
            'urzadWydaniaEPrzesylkiType' => 'PocztaPolska\ElektronicznyNadawca\urzadWydaniaEPrzesylkiType',
            'pobranieType' => 'PocztaPolska\ElektronicznyNadawca\pobranieType',
            'anonymous52' => 'PocztaPolska\ElektronicznyNadawca\anonymous52',
            'anonymous53' => 'PocztaPolska\ElektronicznyNadawca\anonymous53',
            'przesylkaPoleconaZagranicznaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaPoleconaZagranicznaType',
            'przesylkaZadeklarowanaWartoscZagranicznaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaZadeklarowanaWartoscZagranicznaType',
            'krajType' => 'PocztaPolska\ElektronicznyNadawca\krajType',
            'getUrzedyWydajaceEPrzesylki' => 'PocztaPolska\ElektronicznyNadawca\getUrzedyWydajaceEPrzesylki',
            'getUrzedyWydajaceEPrzesylkiResponse' => 'PocztaPolska\ElektronicznyNadawca\getUrzedyWydajaceEPrzesylkiResponse',
            'uploadIWDContent' => 'PocztaPolska\ElektronicznyNadawca\uploadIWDContent',
            'getEnvelopeStatus' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeStatus',
            'getEnvelopeStatusResponse' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeStatusResponse',
            'envelopeStatusType' => 'PocztaPolska\ElektronicznyNadawca\envelopeStatusType',
            'downloadIWDContent' => 'PocztaPolska\ElektronicznyNadawca\AdownloadIWDContent',
            'downloadIWDContentResponse' => 'PocztaPolska\ElektronicznyNadawca\AdownloadIWDContentResponse',
            'przesylkaShortType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaShortType',
            'addShipmentResponseItemType' => 'PocztaPolska\ElektronicznyNadawca\addShipmentResponseItemType',
            'getKarty' => 'PocztaPolska\ElektronicznyNadawca\getKarty',
            'getKartyResponse' => 'PocztaPolska\ElektronicznyNadawca\getKartyResponse',
            'getPasswordExpiredDate' => 'PocztaPolska\ElektronicznyNadawca\getPasswordExpiredDate',
            'getPasswordExpiredDateResponse' => 'PocztaPolska\ElektronicznyNadawca\getPasswordExpiredDateResponse',
            'setAktywnaKarta' => 'PocztaPolska\ElektronicznyNadawca\setAktywnaKarta',
            'setAktywnaKartaResponse' => 'PocztaPolska\ElektronicznyNadawca\setAktywnaKartaResponse',
            'getEnvelopeContentFull' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeContentFull',
            'getEnvelopeContentFullResponse' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeContentFullResponse',
            'getEnvelopeContentShort' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeContentShort',
            'getEnvelopeContentShortResponse' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeContentShortResponse',
            'hello' => 'PocztaPolska\ElektronicznyNadawca\hello',
            'helloResponse' => 'PocztaPolska\ElektronicznyNadawca\helloResponse',
            'kartaType' => 'PocztaPolska\ElektronicznyNadawca\kartaType',
            'telefonType' => 'PocztaPolska\ElektronicznyNadawca\telefonType',
            'getAddressLabel' => 'PocztaPolska\ElektronicznyNadawca\getAddressLabel',
            'getAddressLabelResponse' => 'PocztaPolska\ElektronicznyNadawca\getAddressLabelResponse',
            'addressLabelContent' => 'PocztaPolska\ElektronicznyNadawca\addressLabelContent',
            'getOutboxBook' => 'PocztaPolska\ElektronicznyNadawca\getOutboxBook',
            'getOutboxBookResponse' => 'PocztaPolska\ElektronicznyNadawca\getOutboxBookResponse',
            'getFirmowaPocztaBook' => 'PocztaPolska\ElektronicznyNadawca\getFirmowaPocztaBook',
            'getFirmowaPocztaBookResponse' => 'PocztaPolska\ElektronicznyNadawca\getFirmowaPocztaBookResponse',
            'getEnvelopeList' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeList',
            'getEnvelopeListResponse' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeListResponse',
            'envelopeInfoType' => 'PocztaPolska\ElektronicznyNadawca\envelopeInfoType',
            'przesylkaZagranicznaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaZagranicznaType',
            'przesylkaRejestrowanaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaRejestrowanaType',
            'przesylkaNieRejestrowanaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaNieRejestrowanaType',
            'anonymous94' => 'PocztaPolska\ElektronicznyNadawca\anonymous94',
            'przesylkaBiznesowaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaBiznesowaType',
            'gabarytBiznesowaType' => 'PocztaPolska\ElektronicznyNadawca\gabarytBiznesowaType',
            'subPrzesylkaBiznesowaType' => 'PocztaPolska\ElektronicznyNadawca\subPrzesylkaBiznesowaType',
            'subPrzesylkaBiznesowaPlusType' => 'PocztaPolska\ElektronicznyNadawca\subPrzesylkaBiznesowaPlusType',
            'getAddresLabelByGuid' => 'PocztaPolska\ElektronicznyNadawca\getAddresLabelByGuid',
            'getAddresLabelByGuidResponse' => 'PocztaPolska\ElektronicznyNadawca\getAddresLabelByGuidResponse',
            'przesylkaBiznesowaPlusType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaBiznesowaPlusType',
            'opisType' => 'PocztaPolska\ElektronicznyNadawca\opisType',
            'numerPrzesylkiKlientaType' => 'PocztaPolska\ElektronicznyNadawca\numerPrzesylkiKlientaType',
            'pakietType' => 'PocztaPolska\ElektronicznyNadawca\pakietType',
            'opakowanieType' => 'PocztaPolska\ElektronicznyNadawca\opakowanieType',
            'typOpakowaniaType' => 'PocztaPolska\ElektronicznyNadawca\typOpakowaniaType',
            'getPlacowkiPocztowe' => 'PocztaPolska\ElektronicznyNadawca\getPlacowkiPocztowe',
            'getPlacowkiPocztoweResponse' => 'PocztaPolska\ElektronicznyNadawca\getPlacowkiPocztoweResponse',
            'getGuid' => 'PocztaPolska\ElektronicznyNadawca\getGuid',
            'getGuidResponse' => 'PocztaPolska\ElektronicznyNadawca\getGuidResponse',
            'kierunekType' => 'PocztaPolska\ElektronicznyNadawca\kierunekType',
            'getKierunki' => 'PocztaPolska\ElektronicznyNadawca\getKierunki',
            'prefixKodPocztowy' => 'PocztaPolska\ElektronicznyNadawca\prefixKodPocztowy',
            'getKierunkiResponse' => 'PocztaPolska\ElektronicznyNadawca\getKierunkiResponse',
            'czynnoscUpustowaType' => 'PocztaPolska\ElektronicznyNadawca\czynnoscUpustowaType',
            'miejsceOdbioruType' => 'PocztaPolska\ElektronicznyNadawca\miejsceOdbioruType',
            'sposobNadaniaType' => 'PocztaPolska\ElektronicznyNadawca\sposobNadaniaType',
            'getKierunkiInfo' => 'PocztaPolska\ElektronicznyNadawca\getKierunkiInfo',
            'getKierunkiInfoResponse' => 'PocztaPolska\ElektronicznyNadawca\getKierunkiInfoResponse',
            'kwotaTranzakcjiType' => 'PocztaPolska\ElektronicznyNadawca\kwotaTranzakcjiType',
            'uslugiType' => 'PocztaPolska\ElektronicznyNadawca\uslugiType',
            'idWojewodztwoType' => 'PocztaPolska\ElektronicznyNadawca\idWojewodztwoType',
            'placowkaPocztowaType' => 'PocztaPolska\ElektronicznyNadawca\placowkaPocztowaType',
            'anonymous124' => 'PocztaPolska\ElektronicznyNadawca\anonymous124',
            'anonymous125' => 'PocztaPolska\ElektronicznyNadawca\anonymous125',
            'punktWydaniaPrzesylkiBiznesowejPlus' => 'PocztaPolska\ElektronicznyNadawca\punktWydaniaPrzesylkiBiznesowejPlus',
            'statusType' => 'PocztaPolska\ElektronicznyNadawca\statusType',
            'terminRodzajPlusType' => 'PocztaPolska\ElektronicznyNadawca\terminRodzajPlusType',
            'typOpakowanieType' => 'PocztaPolska\ElektronicznyNadawca\typOpakowanieType',
            'getEnvelopeBufor' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeBufor',
            'getEnvelopeBuforResponse' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeBuforResponse',
            'clearEnvelopeByGuids' => 'PocztaPolska\ElektronicznyNadawca\clearEnvelopeByGuids',
            'clearEnvelopeByGuidsResponse' => 'PocztaPolska\ElektronicznyNadawca\clearEnvelopeByGuidsResponse',
            'zwrotDokumentowType' => 'PocztaPolska\ElektronicznyNadawca\zwrotDokumentowType',
            'odbiorPrzesylkiOdNadawcyType' => 'PocztaPolska\ElektronicznyNadawca\odbiorPrzesylkiOdNadawcyType',
            'potwierdzenieDoreczeniaType' => 'PocztaPolska\ElektronicznyNadawca\potwierdzenieDoreczeniaType',
            'rodzajListType' => 'PocztaPolska\ElektronicznyNadawca\rodzajListType',
            'potwierdzenieOdbioruType' => 'PocztaPolska\ElektronicznyNadawca\potwierdzenieOdbioruType',
            'sposobPrzekazaniaPotwierdzeniaOdbioruType' => 'PocztaPolska\ElektronicznyNadawca\sposobPrzekazaniaPotwierdzeniaOdbioruType',
            'doreczenieType' => 'PocztaPolska\ElektronicznyNadawca\AdoreczenieType',
            'doreczenieUslugaPocztowaType' => 'PocztaPolska\ElektronicznyNadawca\AdoreczenieUslugaPocztowaType',
            'doreczenieUslugaKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\AdoreczenieUslugaKurierskaType',
            'oczekiwanaGodzinaDoreczeniaType' => 'PocztaPolska\ElektronicznyNadawca\oczekiwanaGodzinaDoreczeniaType',
            'oczekiwanaGodzinaDoreczeniaUslugiType' => 'PocztaPolska\ElektronicznyNadawca\oczekiwanaGodzinaDoreczeniaUslugiType',
            'paczkaZagranicznaType' => 'PocztaPolska\ElektronicznyNadawca\paczkaZagranicznaType',
            'setEnvelopeBuforDataNadania' => 'PocztaPolska\ElektronicznyNadawca\setEnvelopeBuforDataNadania',
            'setEnvelopeBuforDataNadaniaResponse' => 'PocztaPolska\ElektronicznyNadawca\setEnvelopeBuforDataNadaniaResponse',
            'lokalizacjaGeograficznaType' => 'PocztaPolska\ElektronicznyNadawca\lokalizacjaGeograficznaType',
            'wspolrzednaGeograficznaType' => 'PocztaPolska\ElektronicznyNadawca\wspolrzednaGeograficznaType',
            'zwrotType' => 'PocztaPolska\ElektronicznyNadawca\zwrotType',
            'sposobZwrotuType' => 'PocztaPolska\ElektronicznyNadawca\sposobZwrotuType',
            'listZwyklyType' => 'PocztaPolska\ElektronicznyNadawca\AlistZwyklyType',
            'reklamowaType' => 'PocztaPolska\ElektronicznyNadawca\reklamowaType',
            'getEPOStatus' => 'PocztaPolska\ElektronicznyNadawca\getEPOStatus',
            'getEPOStatusResponse' => 'PocztaPolska\ElektronicznyNadawca\getEPOStatusResponse',
            'statusEPOEnum' => 'PocztaPolska\ElektronicznyNadawca\statusEPOEnum',
            'EPOType' => 'PocztaPolska\ElektronicznyNadawca\EPOType',
            'EPOSimpleType' => 'PocztaPolska\ElektronicznyNadawca\EPOSimpleType',
            'EPOExtendedType' => 'PocztaPolska\ElektronicznyNadawca\EPOExtendedType',
            'zasadySpecjalneEnum' => 'PocztaPolska\ElektronicznyNadawca\zasadySpecjalneEnum',
            'przesylkaEPOType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaEPOType',
            'przesylkaFirmowaPoleconaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaFirmowaPoleconaType',
            'EPOInfoType' => 'PocztaPolska\ElektronicznyNadawca\EPOInfoType',
            'awizoPrzesylkiType' => 'PocztaPolska\ElektronicznyNadawca\awizoPrzesylkiType',
            'doreczeniePrzesylkiType' => 'PocztaPolska\ElektronicznyNadawca\AdoreczeniePrzesylkiType',
            'zwrotPrzesylkiType' => 'PocztaPolska\ElektronicznyNadawca\zwrotPrzesylkiType',
            'miejscaPozostawieniaAwizoEnum' => 'PocztaPolska\ElektronicznyNadawca\miejscaPozostawieniaAwizoEnum',
            'podmiotDoreczeniaEnum' => 'PocztaPolska\ElektronicznyNadawca\podmiotDoreczeniaEnum',
            'przyczynaZwrotuEnum' => 'PocztaPolska\ElektronicznyNadawca\przyczynaZwrotuEnum',
            'getAddresLabelCompact' => 'PocztaPolska\ElektronicznyNadawca\getAddresLabelCompact',
            'getAddresLabelCompactResponse' => 'PocztaPolska\ElektronicznyNadawca\getAddresLabelCompactResponse',
            'getAddresLabelByGuidCompact' => 'PocztaPolska\ElektronicznyNadawca\getAddresLabelByGuidCompact',
            'getAddresLabelByGuidCompactResponse' => 'PocztaPolska\ElektronicznyNadawca\getAddresLabelByGuidCompactResponse',
            'ubezpieczenieType' => 'PocztaPolska\ElektronicznyNadawca\ubezpieczenieType',
            'rodzajUbezpieczeniaType' => 'PocztaPolska\ElektronicznyNadawca\rodzajUbezpieczeniaType',
            'kwotaUbezpieczeniaType' => 'PocztaPolska\ElektronicznyNadawca\kwotaUbezpieczeniaType',
            'emailType' => 'PocztaPolska\ElektronicznyNadawca\emailType',
            'mobileType' => 'PocztaPolska\ElektronicznyNadawca\mobileType',
            'EMSType' => 'PocztaPolska\ElektronicznyNadawca\EMSType',
            'EMSTypOpakowaniaType' => 'PocztaPolska\ElektronicznyNadawca\EMSTypOpakowaniaType',
            'getEnvelopeBuforList' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeBuforList',
            'getEnvelopeBuforListResponse' => 'PocztaPolska\ElektronicznyNadawca\getEnvelopeBuforListResponse',
            'buforType' => 'PocztaPolska\ElektronicznyNadawca\buforType',
            'createEnvelopeBufor' => 'PocztaPolska\ElektronicznyNadawca\createEnvelopeBufor',
            'createEnvelopeBuforResponse' => 'PocztaPolska\ElektronicznyNadawca\createEnvelopeBuforResponse',
            'moveShipments' => 'PocztaPolska\ElektronicznyNadawca\moveShipments',
            'moveShipmentsResponse' => 'PocztaPolska\ElektronicznyNadawca\moveShipmentsResponse',
            'updateEnvelopeBufor' => 'PocztaPolska\ElektronicznyNadawca\updateEnvelopeBufor',
            'updateEnvelopeBuforResponse' => 'PocztaPolska\ElektronicznyNadawca\updateEnvelopeBuforResponse',
            'getUbezpieczeniaInfo' => 'PocztaPolska\ElektronicznyNadawca\getUbezpieczeniaInfo',
            'getUbezpieczeniaInfoResponse' => 'PocztaPolska\ElektronicznyNadawca\getUbezpieczeniaInfoResponse',
            'ubezpieczeniaInfoType' => 'PocztaPolska\ElektronicznyNadawca\ubezpieczeniaInfoType',
            'isMiejscowa' => 'PocztaPolska\ElektronicznyNadawca\isMiejscowa',
            'isMiejscowaResponse' => 'PocztaPolska\ElektronicznyNadawca\isMiejscowaResponse',
            'trasaRequestType' => 'PocztaPolska\ElektronicznyNadawca\trasaRequestType',
            'trasaResponseType' => 'PocztaPolska\ElektronicznyNadawca\trasaResponseType',
            'deklaracjaCelnaType' => 'PocztaPolska\ElektronicznyNadawca\deklaracjaCelnaType',
            'szczegolyDeklaracjiCelnejType' => 'PocztaPolska\ElektronicznyNadawca\szczegolyDeklaracjiCelnejType',
            'przesylkaPaletowaType' => 'PocztaPolska\ElektronicznyNadawca\przesylkaPaletowaType',
            'rodzajPaletyType' => 'PocztaPolska\ElektronicznyNadawca\rodzajPaletyType',
            'paletaType' => 'PocztaPolska\ElektronicznyNadawca\paletaType',
            'platnikType' => 'PocztaPolska\ElektronicznyNadawca\platnikType',
            'subPrzesylkaPaletowaType' => 'PocztaPolska\ElektronicznyNadawca\subPrzesylkaPaletowaType',
            'getBlankietPobraniaByGuids' => 'PocztaPolska\ElektronicznyNadawca\getBlankietPobraniaByGuids',
            'getBlankietPobraniaByGuidsResponse' => 'PocztaPolska\ElektronicznyNadawca\getBlankietPobraniaByGuidsResponse',
            'updateAccount' => 'PocztaPolska\ElektronicznyNadawca\updateAccount',
            'updateAccountResponse' => 'PocztaPolska\ElektronicznyNadawca\updateAccountResponse',
            'accountType' => 'PocztaPolska\ElektronicznyNadawca\accountType',
            'permisionType' => 'PocztaPolska\ElektronicznyNadawca\permisionType',
            'getAccountList' => 'PocztaPolska\ElektronicznyNadawca\getAccountList',
            'getAccountListResponse' => 'PocztaPolska\ElektronicznyNadawca\getAccountListResponse',
            'profilType' => 'PocztaPolska\ElektronicznyNadawca\profilType',
            'getProfilList' => 'PocztaPolska\ElektronicznyNadawca\getProfilList',
            'getProfilListResponse' => 'PocztaPolska\ElektronicznyNadawca\getProfilListResponse',
            'updateProfil' => 'PocztaPolska\ElektronicznyNadawca\updateProfil',
            'updateProfilResponse' => 'PocztaPolska\ElektronicznyNadawca\updateProfilResponse',
            'statusAccountType' => 'PocztaPolska\ElektronicznyNadawca\statusAccountType',
            'uslugaPaczkowaType' => 'PocztaPolska\ElektronicznyNadawca\uslugaPaczkowaType',
            'subUslugaPaczkowaType' => 'PocztaPolska\ElektronicznyNadawca\subUslugaPaczkowaType',
            'terminPaczkowaType' => 'PocztaPolska\ElektronicznyNadawca\terminPaczkowaType',
            'opakowaniePocztowaType' => 'PocztaPolska\ElektronicznyNadawca\opakowaniePocztowaType',
            'uslugaKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\uslugaKurierskaType',
            'subUslugaKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\subUslugaKurierskaType',
            'createAccount' => 'PocztaPolska\ElektronicznyNadawca\createAccount',
            'createAccountResponse' => 'PocztaPolska\ElektronicznyNadawca\createAccountResponse',
            'createProfil' => 'PocztaPolska\ElektronicznyNadawca\createProfil',
            'createProfilResponse' => 'PocztaPolska\ElektronicznyNadawca\createProfilResponse',
            'terminKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\terminKurierskaType',
            'opakowanieKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\opakowanieKurierskaType',
            'zwrotDokumentowPaczkowaType' => 'PocztaPolska\ElektronicznyNadawca\zwrotDokumentowPaczkowaType',
            'potwierdzenieOdbioruPaczkowaType' => 'PocztaPolska\ElektronicznyNadawca\potwierdzenieOdbioruPaczkowaType',
            'sposobPrzekazaniaPotwierdzeniaOdbioruPocztowaType' => 'PocztaPolska\ElektronicznyNadawca\sposobPrzekazaniaPotwierdzeniaOdbioruPocztowaType',
            'zwrotDokumentowKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\zwrotDokumentowKurierskaType',
            'terminZwrotDokumentowKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\terminZwrotDokumentowKurierskaType',
            'terminZwrotDokumentowPaczkowaType' => 'PocztaPolska\ElektronicznyNadawca\terminZwrotDokumentowPaczkowaType',
            'potwierdzenieOdbioruKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\potwierdzenieOdbioruKurierskaType',
            'sposobPrzekazaniaPotwierdzeniaOdbioruKurierskaType' => 'PocztaPolska\ElektronicznyNadawca\sposobPrzekazaniaPotwierdzeniaOdbioruKurierskaType',
            'addReklamacje' => 'PocztaPolska\ElektronicznyNadawca\addReklamacje',
            'addReklamacjeResponse' => 'PocztaPolska\ElektronicznyNadawca\addReklamacjeResponse',
            'getReklamacje' => 'PocztaPolska\ElektronicznyNadawca\getReklamacje',
            'getReklamacjeResponse' => 'PocztaPolska\ElektronicznyNadawca\getReklamacjeResponse',
            'getZapowiedziFaktur' => 'PocztaPolska\ElektronicznyNadawca\getZapowiedziFaktur',
            'getZapowiedziFakturResponse' => 'PocztaPolska\ElektronicznyNadawca\getZapowiedziFakturResponse',
            'addOdwolanieDoReklamacji' => 'PocztaPolska\ElektronicznyNadawca\addOdwolanieDoReklamacji',
            'addOdwolanieDoReklamacjiResponse' => 'PocztaPolska\ElektronicznyNadawca\addOdwolanieDoReklamacjiResponse',
            'addRozbieznoscDoZapowiedziFaktur' => 'PocztaPolska\ElektronicznyNadawca\addRozbieznoscDoZapowiedziFaktur',
            'addRozbieznoscDoZapowiedziFakturResponse' => 'PocztaPolska\ElektronicznyNadawca\addRozbieznoscDoZapowiedziFakturResponse',
            'reklamowanaPrzesylkaType' => 'PocztaPolska\ElektronicznyNadawca\reklamowanaPrzesylkaType',
            'powodReklamacjiType' => 'PocztaPolska\ElektronicznyNadawca\powodReklamacjiType',
            'reklamacjaRozpatrzonaType' => 'PocztaPolska\ElektronicznyNadawca\reklamacjaRozpatrzonaType',
            'rozstrzygniecieType' => 'PocztaPolska\ElektronicznyNadawca\rozstrzygniecieType',
            'getListaPowodowReklamacji' => 'PocztaPolska\ElektronicznyNadawca\getListaPowodowReklamacji',
            'getListaPowodowReklamacjiResponse' => 'PocztaPolska\ElektronicznyNadawca\getListaPowodowReklamacjiResponse',
            'powodSzczegolowyType' => 'PocztaPolska\ElektronicznyNadawca\powodSzczegolowyType',
            'kategoriePowodowReklamacjiType' => 'PocztaPolska\ElektronicznyNadawca\kategoriePowodowReklamacjiType',
            'listBiznesowyType' => 'PocztaPolska\ElektronicznyNadawca\AlistBiznesowyType',
            'zamowKuriera' => 'PocztaPolska\ElektronicznyNadawca\zamowKuriera',
            'zamowKurieraResponse' => 'PocztaPolska\ElektronicznyNadawca\zamowKurieraResponse',
            'getEZDOList' => 'PocztaPolska\ElektronicznyNadawca\getEZDOList',
            'getEZDOListResponse' => 'PocztaPolska\ElektronicznyNadawca\getEZDOListResponse',
            'getEZDO' => 'PocztaPolska\ElektronicznyNadawca\getEZDO',
            'getEZDOResponse' => 'PocztaPolska\ElektronicznyNadawca\getEZDOResponse',
            'EZDOPakietType' => 'PocztaPolska\ElektronicznyNadawca\EZDOPakietType',
            'EZDOPrzesylkaType' => 'PocztaPolska\ElektronicznyNadawca\EZDOPrzesylkaType',
            'wplataCKPType' => 'PocztaPolska\ElektronicznyNadawca\wplataCKPType',
            'getWplatyCKP' => 'PocztaPolska\ElektronicznyNadawca\getWplatyCKP',
            'getWplatyCKPResponse' => 'PocztaPolska\ElektronicznyNadawca\getWplatyCKPResponse',
            'globalExpresType' => 'PocztaPolska\ElektronicznyNadawca\AglobalExpresType',
            'cancelReklamacja' => 'PocztaPolska\ElektronicznyNadawca\cancelReklamacja',
            'cancelReklamacjaResponse' => 'PocztaPolska\ElektronicznyNadawca\cancelReklamacjaResponse',
            'zalacznikDoReklamacjiType' => 'PocztaPolska\ElektronicznyNadawca\zalacznikDoReklamacjiType',
            'addZalacznikDoReklamacji' => 'PocztaPolska\ElektronicznyNadawca\addZalacznikDoReklamacji',
            'addZalacznikDoReklamacjiResponse' => 'PocztaPolska\ElektronicznyNadawca\addZalacznikDoReklamacjiResponse',
        );
    }
}
