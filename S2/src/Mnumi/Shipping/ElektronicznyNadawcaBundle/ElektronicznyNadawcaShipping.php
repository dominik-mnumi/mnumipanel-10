<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CacheProductCalculationProductData class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Shipping\ElektronicznyNadawcaBundle;

use Mnumi\Bundle\ShippingBundle\Library\ShippingInterface;
use Mnumi\Shipping\ElektronicznyNadawcaBundle\API\Resources\Address;
use Mnumi\Shipping\ElektronicznyNadawcaBundle\API\Resources\Package;

class ElektronicznyNadawcaShipping implements ShippingInterface
{

    private $api;
    private $configuration;
    private $transportNumber;
    private $printlabelDir;
    private $reportDir;

    /**
     * Creates instance of shipping class
     *
     * @param array  $configuration
     * @param string $printlabelDir Directory path for saving print label files
     * @param string $reportDir     Directory path for saving report files
     */
    public function __construct(array $configuration, $printlabelDir, $reportDir)
    {
        $this->configuration = $configuration;
        $this->printlabelDir = $printlabelDir;
        $this->reportDir = $reportDir;

        $this->api = new API\Api(
                $configuration['api_login'],
                $configuration['api_password'],
                $configuration['api_environment']);
    }

    /**
     * Generate print label
     *
     * @param  \Mnumi\Bundle\ShippingBundle\Library\ShippingInformation $shippingInformation
     * @return string                                                   Generated filename
     */
    public function generatePrintLabel(\Mnumi\Bundle\ShippingBundle\Library\ShippingInformation $shippingInformation)
    {
        $guid = $this->api->generateGuid();

        $addressConv = new Address($shippingInformation);
        $address = $addressConv->getSoapAddress();

        $package = new Package($this->configuration['package_type']);
        $package->setAddress($address);
        $package->setPackageSize($this->configuration['package_size']);
        $package->setDescription($shippingInformation->getDescription());
        $package->setGuid($guid);
        $package->setSendOfficeId($this->configuration['send_office']);

        $printLabel = $this->api->sendPackage($package);

        $this->transportNumber = $printLabel->getTransportNumber();

        // prepares output filename
        if (!file_exists($this->printlabelDir)) {
            mkdir($this->printlabelDir, 0777, true);
        }

        $filename = 'enadawca-' . $this->transportNumber;
        $this->printLabelPath = $this->printlabelDir . '/' . $filename;
        $h = fopen($this->printLabelPath . '.' . $this->getImageFormat(), "w");
        fwrite($h, $printLabel->getPdfContent());
        fclose($h);

        return $filename;

    }

    /**
     * Returns image format for print label file
     * @return string
     */
    public function getImageFormat()
    {
        return 'pdf';
    }

    /**
     * Get transport number
     *
     * @return string
     */
    public function getTransportNumber()
    {
        return $this->transportNumber;
    }
}
