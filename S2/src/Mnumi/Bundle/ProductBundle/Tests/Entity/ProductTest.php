<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\ProductBundle\Tests\Entity;

use Mnumi\Bundle\ProductBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * ProductTest class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ProductTest extends WebTestCase
{
    private function getKernel()
    {
        $kernel = $this->createKernel();
        $kernel->boot();

        return $kernel;
    }

    public function testCustomValidator()
    {
        $kernel = $this->getKernel();
        $validator = $kernel->getContainer()->get('validator');

        $violationList = $validator->validate(new Product());

        $this->assertEquals(1, $violationList->count());

        // or any other like:
        $this->assertEquals('Product name should not be blank', $violationList[0]->getMessage());
    }
}
