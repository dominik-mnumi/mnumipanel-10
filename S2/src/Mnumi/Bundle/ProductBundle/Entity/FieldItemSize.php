<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * FieldItemSize
 *
 * @ORM\Table(name="field_item_size", indexes={@ORM\Index(columns={"field_item_id"})})
 * @ORM\Entity(repositoryClass="Mnumi\Bundle\ProductBundle\Entity\Repository\FieldItemSize")
 */
class FieldItemSize
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"ProductList"})
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="width", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"ProductList", "OrdersList"})
     */
    private $width;

    /**
     * @var float
     *
     * @ORM\Column(name="height", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"ProductList", "OrdersList"})
     */
    private $height;

    /**
     * @var \FieldItem
     *
     * @ORM\ManyToOne(targetEntity="FieldItem", inversedBy="size")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_item_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude
     */
    private $fieldItem;

    /**
     * Set width
     *
     * @param  float         $width
     * @return FieldItemSize
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param  float         $height
     * @return FieldItemSize
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fieldItem
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem
     * @return FieldItemSize
     */
    public function setFieldItem(\Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem = null)
    {
        $this->fieldItem = $fieldItem;

        return $this;
    }

    /**
     * Get fieldItem
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\FieldItem
     */
    public function getFieldItem()
    {
        return $this->fieldItem;
    }

    /**
     * Get fieldItem id
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("fieldItemId")
     */
    public function getFieldItemId() {

        return $this->getFieldItem()->getId();
    }
}
