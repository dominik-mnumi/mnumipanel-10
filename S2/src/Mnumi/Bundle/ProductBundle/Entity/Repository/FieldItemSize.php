<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CacheProductCalculationProductData class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
namespace Mnumi\Bundle\ProductBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;

class FieldItemSize extends EntityRepository
{

    public function getByProductSlug($slug)
    {
        return $this->createQueryBuilder('fis')
                //->select('fis')
                //->from('MnumiProductBundle:FieldItemSize', 'fis')
                ->leftJoin('fis.fieldItem', 'fi')
                ->leftJoin('fi.productFieldItems', 'pfi')
                ->leftJoin('pfi.productField', 'pf')
                ->leftJoin('pf.product', 'p')
                ->where('p.slug = :slug')
                ->setParameter('slug', $slug)
                ->getQuery()
                ->getResult();

    }
}