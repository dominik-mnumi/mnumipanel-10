<?php

namespace Mnumi\Bundle\ProductBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * FieldItem
 *
 * @ORM\Table(name="coupon")
 * @ORM\Entity
 */
class Coupon {

    const AMOUNT= 'amount';
    const PERCENT = 'percent';
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @ORM\Id
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float", precision=18, scale=2, nullable=false)
     */
    private $value;

    /**
     * @var boolean
     *
     * @ORM\Column(name="apply_for_all_products", type="boolean", nullable=false)
     */
    private $applyForAllProducts;

    /**
     * @var boolean
     *
     * @ORM\Column(name="apply_for_all_pricelist", type="boolean", nullable=false)
     */
    private $applyForAllPricelist;

    /**
     * @var date
     *
     * @ORM\Column(name="expire_at", type="date", nullable=true)
     */
    private $expireAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="one_time_usage", type="boolean", nullable=false)
     */
    private $oneTimeUsage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="used", type="boolean", nullable=false)
     */
    private $used;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $updatedAt;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Coupon
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return Coupon
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return Coupon
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set applyForAllProducts
     *
     * @param boolean $applyForAllProducts
     *
     * @return Coupon
     */
    public function setApplyForAllProducts($applyForAllProducts)
    {
        $this->applyForAllProducts = $applyForAllProducts;

        return $this;
    }

    /**
     * Get applyForAllProducts
     *
     * @return boolean
     */
    public function getApplyForAllProducts()
    {
        return $this->applyForAllProducts;
    }

    /**
     * Set applyForAllPricelist
     *
     * @param boolean $applyForAllPricelist
     *
     * @return Coupon
     */
    public function setApplyForAllPricelist($applyForAllPricelist)
    {
        $this->applyForAllPricelist = $applyForAllPricelist;

        return $this;
    }

    /**
     * Get applyForAllPricelist
     *
     * @return boolean
     */
    public function getApplyForAllPricelist()
    {
        return $this->applyForAllPricelist;
    }

    /**
     * Set expireAt
     *
     * @param \DateTime $expireAt
     *
     * @return Coupon
     */
    public function setExpireAt($expireAt)
    {
        $this->expireAt = $expireAt;

        return $this;
    }

    /**
     * Get expireAt
     *
     * @return \DateTime
     */
    public function getExpireAt()
    {
        return $this->expireAt;
    }

    /**
     * Set oneTimeUsage
     *
     * @param boolean $oneTimeUsage
     *
     * @return Coupon
     */
    public function setOneTimeUsage($oneTimeUsage)
    {
        $this->oneTimeUsage = $oneTimeUsage;

        return $this;
    }

    /**
     * Get oneTimeUsage
     *
     * @return boolean
     */
    public function getOneTimeUsage()
    {
        return $this->oneTimeUsage;
    }

    /**
     * Set used
     *
     * @param boolean $used
     *
     * @return Coupon
     */
    public function setUsed($used)
    {
        $this->used = $used;

        return $this;
    }

    /**
     * Get used
     *
     * @return boolean
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Coupon
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Coupon
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}
