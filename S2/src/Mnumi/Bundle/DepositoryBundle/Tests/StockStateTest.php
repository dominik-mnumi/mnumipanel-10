<?php

namespace Mnumi\Bundle\DepositoryBundle\Tests;

use Mnumi\Bundle\DepositoryBundle\Library\StockState;

class StockStateTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks passed parameters to StockState are consistent with the
     * passed data
     */
    public function testCheckStockStateConsistency()
    {
        $oldStock = 4321;
        $newStock = 9213;

        $stockState = new StockState($oldStock, $newStock);

        $this->assertEquals($oldStock, $stockState->getOldStock());
        $this->assertEquals($newStock, $stockState->getNewStock());
    }
}
