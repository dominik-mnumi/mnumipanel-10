<?php

namespace Mnumi\Bundle\DepositoryBundle\Tests;

use Mnumi\Bundle\DepositoryBundle\Library\StockChange;
use Mnumi\Bundle\DepositoryBundle\Library\StockChanges;

class StockChangesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks if StockChange is added properly to StockChanges class
     */
    public function testCheckIfStockChangesAddsStockChange()
    {
        $stockId = 213;
        $stockAmountDifference = -121;
        $stockChange = new StockChange($stockId, $stockAmountDifference);

        $stockChanges = new StockChanges();
        $stockChanges->addStockChange($stockChange);

        $changes = $stockChanges->getChanges();
        $this->assertEquals(1, $changes->count());

        foreach ($changes as $change) {
            $this->assertEquals($stockChange->getId(), $change->getId());
            $this->assertEquals($stockChange->getAmountDifference(), $change->getAmountDifference());
        }
    }

    /**
     * Test checks if StockChange is removed properly from StockChanges class
     */
    public function testCheckIfStockChangesRemovesStockChange()
    {
        $stockId = 413;
        $stockAmountDifference = 72;
        $stockChange = new StockChange($stockId, $stockAmountDifference);

        $stockChanges = new StockChanges();
        $stockChanges->addStockChange($stockChange);
        $stockChanges->removeStockChange($stockChange);

        $this->assertEquals(0, $stockChanges->getChanges()->count());
    }

    /**
     * Test checks if StockChange's of the same stockId are merged together
     */
    public function testCheckIfStockChangesWithSameIdAreMerged()
    {
        $stockId = 213;
        $stockAmountDifference1 = -11;
        $stockAmountDifference2 = 21;
        $stockAmountDifferenceExpectedResult = $stockAmountDifference1 + $stockAmountDifference2;

        $stockChanges = new StockChanges();
        $stockChanges->addStockChange(new StockChange($stockId, $stockAmountDifference1));
        $stockChanges->addStockChange(new StockChange($stockId, $stockAmountDifference2));

        $changes = $stockChanges->getChanges();

        $this->assertEquals(1, $changes->count());

        foreach ($changes as $change) {
            $this->assertEquals($stockId, $change->getId());
            $this->assertEquals($stockAmountDifferenceExpectedResult, $change->getAmountDifference());
        }
    }

    /**
     * Test checks if adding StockChange with no amount difference is ignored by
     * StockChanges class
     */
    public function testCheckIfStockChangesIgnoresStockChangeWithZeroAmountDifference()
    {
        $stockId = 213;
        $stockAmountDifference = 0;
        $stockChange = new StockChange($stockId, $stockAmountDifference);

        $stockChanges = new StockChanges();
        $stockChanges->addStockChange($stockChange);

        $this->assertEquals(0, $stockChanges->getChanges()->count());
    }
}
