<?php

namespace Mnumi\Bundle\DepositoryBundle\Tests\StockCalculation;

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\NotUsedStockCalculation;

class NotUsedStockCalculationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks if NotUsedStockCalculation always returns 0, which means
     * that no change in stock is going to be made
     */
    public function testCheckIfNotUsedStockCalculationAlwaysReturnsZero()
    {
        $quantity   = 7356;
        $count      = 1238;

        $quantityStockCalculation = new NotUsedStockCalculation();

        $this->assertEquals(0, $quantityStockCalculation->calculate($quantity, $count));
    }
}
