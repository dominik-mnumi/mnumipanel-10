<?php

namespace Mnumi\Bundle\DepositoryBundle\Tests\StockCalculation;

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\QuantityStockCalculation;

class QuantityStockCalculationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks if QuantityStockCalculation always returns Stock Amount equal to used quantity
     */
    public function testCheckIfQuantityStockCalculationUsesQuantity()
    {
        $quantity   = 7356;
        $count      = 1238;

        $quantityStockCalculation = new QuantityStockCalculation();

        $this->assertEquals($quantity, $quantityStockCalculation->calculate($quantity, $count));
    }
}
