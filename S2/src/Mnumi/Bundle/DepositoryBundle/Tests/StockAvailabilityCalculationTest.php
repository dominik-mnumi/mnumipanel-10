<?php

namespace Mnumi\Bundle\DepositoryBundle\Tests;

use Mnumi\Bundle\DepositoryBundle\Library\StockAvailabilityCalculation;

class StockAvailabilityCalculationTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks if StockChange is added properly to Calculator
     */
    public function testCheckIfAddsStockAvailability()
    {
        $fieldItemId = 5421;
        $availability = 312.231;

        $stockAvailabilityCalculation = new StockAvailabilityCalculation();
        $stockAvailabilityCalculation->addStockFieldAvailability($fieldItemId, $availability);

        $reflector = new \ReflectionObject($stockAvailabilityCalculation);
        $property = $reflector->getProperty('stockChanges');
        $property->setAccessible(true);

        $stockChanges = $property->getValue($stockAvailabilityCalculation);
        $stockChange = $stockChanges->getStockChangeById($fieldItemId);

        $this->assertNotEquals(false, $stockChange);
        $this->assertEquals($fieldItemId, $stockChange->getId());
        $this->assertEquals($availability, $stockChange->getAmountDifference());
    }

    /**
     * Test checks if StockChange is added properly multiple times to Calculator
     */
    public function testCheckIfAddsStockAvailabilityMultipleTimes()
    {
        $fieldItemId = 5421;
        $availability = 312.231;
        $times = 5;

        $stockAvailabilityCalculation = new StockAvailabilityCalculation();
        for ($i = 0; $i < $times; $i++) {
            $stockAvailabilityCalculation->addStockFieldAvailability($fieldItemId, $availability);
        }

        $reflector = new \ReflectionObject($stockAvailabilityCalculation);
        $property = $reflector->getProperty('stockChanges');
        $property->setAccessible(true);

        $stockChanges = $property->getValue($stockAvailabilityCalculation);
        $stockChange = $stockChanges->getStockChangeById($fieldItemId);

        $this->assertNotEquals(false, $stockChange);
        $this->assertEquals($fieldItemId, $stockChange->getId());
        $this->assertEquals($availability * $times, $stockChange->getAmountDifference());
    }

    /**
     * Test checks if availability = 0, is added
     *
     * StockChanges class, filters out change which do not change stock.
     * Such change is e.g: availability = 0.
     * When stock for calculation is filled it changes 0 availability, to be,
     * e.g.: -0.0001, which will be saved (as is different than 0), and makes stock
     * unavailable (as all requests will result in negative availability
     *
     * @see StockChanges
     * @see StockAvailabilityCalculation::addStockFieldAvailability
     */
    public function testCheckIfAddsStockAvailabilityEqualZero()
    {
        $fieldItemId = 18;
        $availability = 0;

        $stockAvailabilityCalculation = new StockAvailabilityCalculation();
        $stockAvailabilityCalculation->addStockFieldAvailability($fieldItemId, $availability);

        $reflector = new \ReflectionObject($stockAvailabilityCalculation);
        $property = $reflector->getProperty('stockChanges');
        $property->setAccessible(true);

        $stockChanges = $property->getValue($stockAvailabilityCalculation);
        $stockChange = $stockChanges->getStockChangeById($fieldItemId);

        $this->assertNotEquals(false, $stockChange);
        $this->assertEquals($fieldItemId, $stockChange->getId());
        $this->assertLessThanOrEqual(0, $stockChange->getAmountDifference());
    }

    /**
     * Test checks if by default there is nothing unavailable that can cause
     * unexpected behaviour
     */
    public function testCheckIfNothingIsUnavailableByDefault()
    {
        $fieldItemId = 18;
        $availability = 0;

        $stockAvailabilityCalculation = new StockAvailabilityCalculation();
        $stockAvailabilityCalculation->addStockFieldAvailability($fieldItemId, $availability);

        $reflector = new \ReflectionObject($stockAvailabilityCalculation);
        $property = $reflector->getProperty('unavailable');
        $property->setAccessible(true);

        $unavailable = $property->getValue($stockAvailabilityCalculation);
        $this->assertEmpty($unavailable, 'Unavailable property should by empty by default');
    }

    /**
     * Test checks if Availability is returned in expected format:
     *
     * array(
     *     ORDER_ID => array(
     *         FIELD_ITEM_ID => array(
     *             FIELD_ITEM_LABEL
     *         )
     *     )
     * )
     */
    public function testCheckIfAvailabilityIsReturnedInCorrectFormat()
    {
        $fieldItemId = 18;
        $availability = 0;

        $stockAvailabilityCalculation = new StockAvailabilityCalculation();
        $stockAvailabilityCalculation->addStockFieldAvailability($fieldItemId, $availability);

        $orderId = 1876;
        $fieldItemLabel = 'Dummy';
        $requestedAvailability = 8;
        $productFieldItemRequiredAmount = 1;
        $stockAvailabilityCalculation->requestStockChange($orderId, $fieldItemId, $fieldItemLabel, $requestedAvailability, $productFieldItemRequiredAmount);

        $availability = $stockAvailabilityCalculation->checkAvailability();
        $this->assertArrayHasKey($orderId, $availability);
        $this->assertArrayHasKey($fieldItemId, $availability[$orderId]);
        $this->assertEquals($fieldItemLabel, $availability[$orderId][$fieldItemId]);
    }
}
