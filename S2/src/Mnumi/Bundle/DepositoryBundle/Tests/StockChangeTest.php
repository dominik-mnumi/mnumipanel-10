<?php

namespace Mnumi\Bundle\DepositoryBundle\Tests;

use Mnumi\Bundle\DepositoryBundle\Library\StockChange;

class StockChangeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks passed parameters to StockState are consistent with the
     * passed data
     */
    public function testCheckStockChangeConsistency()
    {
        $stockId = 4321;
        $stockAmountDifference = 9213;

        $stockChange = new StockChange($stockId, $stockAmountDifference);

        $this->assertEquals($stockId, $stockChange->getId());
        $this->assertEquals($stockAmountDifference, $stockChange->getAmountDifference());
    }
}
