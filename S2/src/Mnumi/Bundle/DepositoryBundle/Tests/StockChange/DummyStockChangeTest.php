<?php

namespace Mnumi\Bundle\DepositoryBundle\Tests\StockChange;

use Mnumi\Bundle\DepositoryBundle\Library\StockChange\DummyStockChange;

use Mnumi\Bundle\DepositoryBundle\Library\StockState\QuantityStockState;
use Mnumi\Bundle\DepositoryBundle\Library\StockState\CountStockState;

class DummyStockChangeTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test checks if DummyStockChange class always returns no Changes (is ignored)
     */
    public function testCheckDummyStockChangeReturnsNoChanges()
    {
        $oldQuantity = 10;
        $newQuantity = 20;

        $oldCount = 84;
        $newCount = 23;

        $oldStockId = 231;
        $newStockId = 874;

        $quantityStockState = new QuantityStockState($oldQuantity, $newQuantity);
        $countStockState = new CountStockState($oldCount, $newCount);

        $dummyStockChange = new DummyStockChange($oldStockId, $newStockId);

        $this->assertEquals(0, $dummyStockChange->getStockChanges($quantityStockState, $countStockState)->getChanges()->count());
    }
}
