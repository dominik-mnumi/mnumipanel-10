<?php

namespace Mnumi\Bundle\DepositoryBundle\Library;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockChange;
use Mnumi\Bundle\DepositoryBundle\Library\StockChanges;

/**
 * Class for calculating Availability
 */
class StockAvailabilityCalculation
{
    private $stockChanges;
    private $unavailable;

    public function __construct() {
        $this->stockChanges = new StockChanges();
        $this->unavailable = array();
    }

    /**
     * Adds StockAvailability
     *
     * This method should be used to fill calculation with currently available
     * stock.
     *
     * StockChanges class used inside, filters out changes which do not change
     * stock availability. Such change is: availability = 0.
     * When stock for calculation is filled using this method, it changes every
     * availability equal == 0 && !== NULL, to be: -0.0001, which will be saved
     * (as is different than 0), and makes stock unavailable (as all requests
     * will result in negative availability
     *
     * @see StockChanges
     *
     * @param int $id ID of FieldItem
     * @param float $availability Availability of FieldItem
     * @return void
     */
    public function addStockFieldAvailability($id, $availability)
    {
        if ($availability !== NULL) {
            // StockChanges class automatically ignores Change with amount 0,
            // which describes no change. We overwrite this to make sure that it
            // is saved as unavailable
            if ($availability == 0) {
                $availability = -0.0001;
            }

            $this->stockChanges->addStockChange(new StockChange($id, $availability));
        }
    }

    /**
     * Calculates amount including requiredAmount from ProductFieldItem
     *
     * @param float $amount
     * @param float $requiredAmount
     * @return float
     */
    private function getRequireAvailability($amount, $requiredAmount)
    {
        return ($requiredAmount * $amount);
    }

    /**
     * Requests change in Stock
     *
     * @param $orderId
     * @param $fieldItemId
     * @param $fieldItemLabel
     * @param $requestedAvailability
     * @param $productFieldItemRequiredAmount
     */
    public function requestStockChange($orderId, $fieldItemId, $fieldItemLabel, $requestedAvailability, $productFieldItemRequiredAmount) {
        $stockChange = $this->stockChanges->getStockChangeById($fieldItemId);
        if ($stockChange !== false) {
            if ($stockChange->getAmountDifference() < $this->getRequireAvailability($requestedAvailability, $productFieldItemRequiredAmount)) {
                $this->unavailable[$orderId][$fieldItemId] = $fieldItemLabel;
            }
            else {
                $this->stockChanges->addStockChange(new StockChange($fieldItemId, -$this->getRequireAvailability($requestedAvailability, $productFieldItemRequiredAmount)));
            }
        }
    }

    /**
     * Checks Availability of passed Orders
     *
     * @return array Array containing fieldItem labels keyed by fieldItemId and
     *   OrderId, which are unavailable in stock after all calculations. Array is
     *   empty if everything is available.
     */
    public function checkAvailability()
    {
        return $this->unavailable;
    }
}
