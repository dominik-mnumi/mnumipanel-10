<?php

namespace Mnumi\Bundle\DepositoryBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockChange;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class represents collection of stock changes in depository
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class StockChanges
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->changes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Adds stockChange
     *
     * If there is already aggregated StockChange with the same id as the one
     * passed in argument, method merges both StockChange amounts.
     *
     * If passed StockChange has no amountDifference, method ignores it
     *
     * @param StockChange $stockChange
     *
     * @return StockChanges
     */
    public function addStockChange(StockChange $stockChange)
    {
        if ($stockChange->getAmountDifference() != 0) {
            foreach ($this->changes as &$change) {
                if ($change->getId() == $stockChange->getId()) {
                    $amount = ($change->getAmountDifference() + $stockChange->getAmountDifference());

                    $stockChange = new StockChange($stockChange->getId(), $amount);

                    $this->removeStockChange($change);
                }
            }

            $this->changes->add($stockChange);
        }

        return $this;
    }

    /**
     * Gets stockChange with the given ID
     *
     * If there StockChange is found it is returned. If there is no StockChange
     * with specified ID, boolean false is returned
     *
     * @param int $id
     *
     * @return boolean|StockChange
     */
    public function getStockChangeById($id)
    {
        foreach ($this->changes as $change) {
            if ($change->getId() == $id) {
                return $change;
            }
        }
        return false;
    }

    /**
     * Removes stockChange
     *
     * @param StockChange $stockChange
     */
    public function removeStockChange(StockChange $stockChange)
    {
        $this->changes->removeElement($stockChange);
    }

    /**
     * Gets stockChanges
     *
     * @return \Doctrine\Common\Collections\Collection\ArrayCollection
     */
    public function getChanges()
    {
        return $this->changes;
    }

}
