<?php

namespace Mnumi\Bundle\DepositoryBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockState\QuantityStockState;
use Mnumi\Bundle\DepositoryBundle\Library\StockState\CountStockState;

/**
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
interface StockChangeableInterface
{
    /**
     * Returns StockChanges collection class, containing changes based on
     * specified quantity and count
     *
     * @param QuantityStockState $quantity
     * @param CountStockState    $count
     *
     * @return StockChanges
     */
    public function getStockChanges(QuantityStockState $quantity, CountStockState $count);
}
