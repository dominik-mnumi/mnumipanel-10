<?php

namespace Mnumi\Bundle\DepositoryBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockStateInterface;

/**
 * Indicates Stock resource state (before and after change)
 *
 * Values holding in this class could be different among used types. For
 * quantity/count it will be just amount, for Material/Other Id of used resource
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class StockState implements StockStateInterface
{
    protected $oldStock;
    protected $newStock;

    /**
     * Constructor
     *
     * @param $oldStock stock value before state change
     * @param $newStock stock value after state change
     */
    public function __construct($oldStock, $newStock)
    {
        $this->oldStock = $oldStock;
        $this->newStock = $newStock;
    }

    /**
     * @inheritdoc
     */
    public function getOldStock()
    {
        return $this->oldStock;
    }

    /**
     * @inheritdoc
     */
    public function getNewStock()
    {
        return $this->newStock;
    }
}
