<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockChange;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockState;
use Mnumi\Bundle\DepositoryBundle\Library\StockChange;
use Mnumi\Bundle\DepositoryBundle\Library\StockChanges;
use Mnumi\Bundle\DepositoryBundle\Library\StockChangeableInterface;

use Mnumi\Bundle\DepositoryBundle\Library\StockState\QuantityStockState;
use Mnumi\Bundle\DepositoryBundle\Library\StockState\CountStockState;

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation;

/**
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class OtherStockChange extends StockState implements StockChangeableInterface
{
    protected $calculationToolOldStock;
    protected $calculationToolNewStock;

    /**
     * Constructor
     *
     * @param StockCalculation $calculationToolOldStock tool used to calculate change amount of material for old setting
     * @param StockCalculation $calculationToolNewStock tool used to calculate change amount of material for new setting
     * @param int              $oldStock                old MaterialID
     * @param int              $newStock                new MaterialID
     */
    public function __construct(StockCalculation $calculationToolOldStock, StockCalculation $calculationToolNewStock, $oldStock, $newStock)
    {
        parent::__construct($oldStock, $newStock);
        $this->calculationToolOldStock = $calculationToolOldStock;
        $this->calculationToolNewStock = $calculationToolNewStock;
    }

    /**
     * @inheritdoc
     */
    public function getStockChanges(QuantityStockState $quantity, CountStockState $count)
    {
        $stockChanges = new StockChanges();
        $stockChanges->addStockChange(new StockChange($this->getOldStock(), -$this->getAmount($this->calculationToolOldStock, $quantity->getOldStock(), $count->getOldStock())));
        $stockChanges->addStockChange(new StockChange($this->getNewStock(),  $this->getAmount($this->calculationToolNewStock, $quantity->getNewStock(), $count->getNewStock())));

        return $stockChanges;
    }

    protected function getAmount(StockCalculation $calculationTool, $quantity, $count)
    {
        return $calculationTool->calculate($quantity, $count);
    }
}
