<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockChange;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockState;
use Mnumi\Bundle\DepositoryBundle\Library\StockChanges;
use Mnumi\Bundle\DepositoryBundle\Library\StockChangeableInterface;

use Mnumi\Bundle\DepositoryBundle\Library\StockState\QuantityStockState;
use Mnumi\Bundle\DepositoryBundle\Library\StockState\CountStockState;

/**
 * Dummy class used for ignoring settings which do not support Depository
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class DummyStockChange extends StockState implements StockChangeableInterface
{
    /**
     * @inheritdoc
     */
    public function getStockChanges(QuantityStockState $quantity, CountStockState $count)
    {
        return new StockChanges();
    }
}
