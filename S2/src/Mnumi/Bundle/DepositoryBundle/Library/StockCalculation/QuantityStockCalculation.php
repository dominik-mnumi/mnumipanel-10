<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockCalculation;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation;

/**
 * StockCalculation class used for Items which amount is based on quantity
 */
class QuantityStockCalculation extends StockCalculation
{
    /**
     * @inheritdoc
     *
     * Method ignores count parameter.
     */
    public function calculate($quantity, $count)
    {
        return $quantity;
    }
}
