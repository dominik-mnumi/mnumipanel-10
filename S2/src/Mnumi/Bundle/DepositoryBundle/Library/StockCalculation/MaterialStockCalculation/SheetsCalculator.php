<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\CalculationBundle\Library\CalculationFactor;
use Mnumi\Bundle\CalculationBundle\Library\Material\MeasureUnit;
use Mnumi\Bundle\CalculationBundle\Library\Exception\CalculationInvalidArgumentException;

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation;

/**
 * Sheets Material Calculator
 *
 * Allows calculating number of sheets of the given material
 */
class SheetsCalculator extends MaterialStockCalculation
{
    /**
     * Returns amount of material based on quantity and count
     *
     * This method is a wrapper for: CalculationFactor::count() returning how
     * many printer pages must be used
     *
     * @see Mnumi\Bundle\CalculationBundle\Library\CalculationFactor
     *
     * @param int $quantity
     * @param int $count
     *
     * @throws \Mnumi\Bundle\CalculationBundle\Library\Exception\CalculationInvalidArgumentException
     * @return int number of Material sheets to be used
     */
    public function calculate($quantity, $count)
    {
        $sheetSize = $this->getSheetSize();
        $useSize = $this->getUseSize();

        $calculationFactor = CalculationFactor::count(
            $sheetSize->getWidth(), $sheetSize->getHeight(),// sheet size
            $useSize->getWidth(), $useSize->getHeight(),    // use size
            $quantity * $count,                             // real amount of uses
            MeasureUnit::getName($this->getMeasureUnit())
        );

        if ($calculationFactor['factor'] <= 0) {
            throw new CalculationInvalidArgumentException('countFactor method returned 0');
        }

        return $calculationFactor['factor'];
    }
}
