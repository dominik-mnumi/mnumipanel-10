<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation;

/**
 * Square Metre Material Calculator
 *
 * Allows calculating amount of material using SquareMetre calculation type
 */
class SquareMetreCalculator extends MaterialStockCalculation
{
    /**
     * Returns amount of material based on quantity and count
     *
     * Function content was copied from: CalculationTool::getPriceSquareMetre()
     * @see Mnumi\Bundle\CalculationBundle\Library\CalculationTool::getPriceSquareMetre()
     *
     * @param int $quantity
     * @param int $count
     *
     * @return number of Material sheets to be used
     */
    public function calculate($quantity, $count)
    {
        $sheetSize = $this->getSheetSize();
        $printSizeWidth = $sheetSize->getWidth();
        $printSizeHeight = $sheetSize->getHeight();
        $metricFactor = $this->getMeasureUnit();

        // ($printSizeWidth * $printSizeHeight) / (2 * $metricFactor)
        $squareMetre = $printSizeHeight / $metricFactor * $printSizeWidth / $metricFactor;

        return $quantity * $count * $squareMetre;
    }
}
