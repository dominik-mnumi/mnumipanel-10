<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation;

/**
 * Linear Metre Material Calculator
 *
 * Allows calculating amount of material using LinearMetre calculation type
 */
class LinearMetreCalculator extends MaterialStockCalculation
{
    /**
     * Returns amount of material based on quantity and count
     *
     * Function content was copied from: CalculationTool::getPriceLinearMetre()
     * @see Mnumi\Bundle\CalculationBundle\Library\CalculationTool::getPriceLinearMetre()
     *
     * @param int $quantity
     * @param int $count
     *
     * @return number of Material sheets to be used
     */
    public function calculate($quantity, $count)
    {
        $sheetSize = $this->getSheetSize();
        $printSizeWidth = $this->getWidth();
        $printSizeHeight = $this->getHeight();
        $metricFactor = $this->getMeasureUnit();

        // 2 / $metricFactor * ($printSizeWidth + $printSizeHeight)
        $linearMetre = 2 * $printSizeHeight / $metricFactor + 2 * $printSizeWidth / $metricFactor;

        return $quantity * $count * $linearMetre;
    }
}
