<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockCalculation;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\CalculationBundle\Library\Size;
use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation;

/**
 * Base class for all Material Calculation types
 */
abstract class MaterialStockCalculation extends StockCalculation
{
    private $sheetSize = 0;
    private $useSize = 0;
    private $measureUnit = 0;

    /**
     * Constructor
     *
     * @param Size $sheetSize Material Sheet size
     * @param Size $useSize Use size
     * @param int $measureUnit Measure Unit factor
     */
    public function __construct(Size $sheetSize, Size $useSize, $measureUnit)
    {
        $this->sheetSize = $sheetSize;
        $this->useSize = $useSize;
        $this->measureUnit = $measureUnit;
    }

    /**
     * Gets Sheet size for the current Material calculation
     *
     * @return Size
     */
    public function getSheetSize()
    {
        return $this->sheetSize;
    }

    /**
     * Gets Use size for the current Material calculation
     *
     * @return Size
     */
    public function getUseSize()
    {
        return $this->useSize;
    }

    /**
     * Gets Measure unit for the current Material calculation
     *
     * @return int
     */
    public function getMeasureUnit()
    {
        return $this->measureUnit;
    }
}
