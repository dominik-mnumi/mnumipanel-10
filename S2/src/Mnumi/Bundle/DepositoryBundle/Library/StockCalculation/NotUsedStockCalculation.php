<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockCalculation;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculation;

/**
 * StockCalculation class used for Items which are not used in Stock
 */
class NotUsedStockCalculation extends StockCalculation
{
    /**
     * @inheritdoc
     *
     * Method ignores both parameters returning always: 0, as this results in no
     * change in Stock
     */
    public function calculate($quantity, $count)
    {
        return 0;
    }
}
