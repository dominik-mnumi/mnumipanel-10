<?php

namespace Mnumi\Bundle\DepositoryBundle\Library\StockState;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockState;

/**
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class QuantityStockState extends StockState
{
}
