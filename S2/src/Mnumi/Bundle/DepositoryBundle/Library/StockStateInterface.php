<?php

namespace Mnumi\Bundle\DepositoryBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Interface for all stock state aware resources
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
interface StockStateInterface
{
    /**
     * Value before change
     *
     * @return int
     */
    public function getOldStock();

    /**
     * Value after change
     *
     * @return int
     */
    public function getNewStock();
}
