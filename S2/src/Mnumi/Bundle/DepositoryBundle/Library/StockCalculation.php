<?php

namespace Mnumi\Bundle\DepositoryBundle\Library;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\DepositoryBundle\Library\StockCalculationInterface;

/**
 *
 */
abstract class StockCalculation implements StockCalculationInterface
{
}
