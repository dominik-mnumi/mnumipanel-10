<?php

namespace Mnumi\Bundle\DepositoryBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class StockChange
{
    protected $id;
    protected $amountDifference;

    /**
     * Constructor
     */
    public function __construct($id, $amountDifference)
    {
        $this->id = $id;
        $this->amountDifference = $amountDifference;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAmountDifference()
    {
        return $this->amountDifference;
    }
}
