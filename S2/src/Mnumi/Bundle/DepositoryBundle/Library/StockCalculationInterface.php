<?php

namespace Mnumi\Bundle\DepositoryBundle\Library;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 *
 */
interface StockCalculationInterface
{
    /**
     * Calculates amount of some resource, based on quantity and count
     *
     * @param $quantity amount of quantity
     * @param $count amount of count
     *
     * @return amount of the resource
     */
    public function calculate($quantity, $count);
}
