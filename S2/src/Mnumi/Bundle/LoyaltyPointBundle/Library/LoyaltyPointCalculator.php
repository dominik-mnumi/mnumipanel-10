<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\LoyaltyPointBundle\Library;

use Mnumi\Bundle\CurrencyBundle\Library\TaxCalculator;
use Mnumi\Bundle\OrderBundle\Entity\Order;

/**
 * LoyaltyPointCalculator class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
class LoyaltyPointCalculator
{

    /**
     * @var float $revertedScale
     */
    private $revertedScale;

    /**
     * @var TaxCalculator $taxCalculator
     */
    private $taxCalculator;

    /**
     * @param float         $revertedScale
     * @param TaxCalculator $taxCalculator
     */
    public function __construct($revertedScale, TaxCalculator $taxCalculator)
    {
        $this->revertedScale = $revertedScale;
        $this->taxCalculator = $taxCalculator;
    }

    /**
     * Get order loyalty points netto value
     *
     * @param  Order $order
     * @return float
     */
    public function getOrderLoyaltyPointsNetValue(Order $order)
    {
        $user = $order->getUser();
        $availableLoyaltyPoints = $user->countAvailableLoyaltyPoints();
        $availableLoyaltyPointsValue = $this->getLoyaltyPointsValue($availableLoyaltyPoints);
        $basketPriceNet = $order->calculateTotalAmount();

        if ($basketPriceNet > $availableLoyaltyPointsValue) {

            $loyaltyPointsValue = $this->getLoyaltyPointsValue($availableLoyaltyPoints);

        } else {

            $loyaltyPointsValue = $order->calculateTotalAmount();
        }

        return $loyaltyPointsValue;
    }

    /**
     * Get order loyalty points gross value
     *
     * @param  Order $order
     * @return float
     */
    public function getOrderLoyaltyPointsGrossValue(Order $order)
    {
        $customTax = $order->getClient()->getWntUe() ? 0 : false;

        return $this->taxCalculator(
                $this->getOrderLoyaltyPointsNetValue($order),
                $customTax);
    }

    /**
     * Get order price with loyalty points netto
     *
     * @param  Order $order
     * @return float
     */
    public function getOrderPriceNetWithLoyaltyPoints(Order $order)
    {
        $user = $order->getUser();
        $availableLoyaltyPoints = $user->countAvailableLoyaltyPoints();
        $availableLoyaltyPointsValue = $this->getLoyaltyPointsValue($availableLoyaltyPoints);
        $basketPriceNet = $order->calculateTotalAmount();

        if ($basketPriceNet > $availableLoyaltyPointsValue) {

            $orderPriceWithLoyaltyPoints = $basketPriceNet - $availableLoyaltyPointsValue;

        } else {

            $orderPriceWithLoyaltyPoints = 0;
        }

        // add delivery price
        $orderPriceWithLoyaltyPoints += $order->calculateDeliveryNetPrice();

        return $orderPriceWithLoyaltyPoints;
    }

    /**
     * Get order price with loyalty points gross
     *
     * @param  Order $order
     * @return float
     */
    public function getOrderPriceGrossWithLoyaltyPoints(Order $order)
    {
        $customTax = $order->getClient()->getWntUe() ? 0 : false;

        return $this->taxCalculator(
                $this->getOrderPriceNetWithLoyaltyPoints($order),
                $customTax);
    }

    /**
     *   Get points value
     *
     * @return float
     */
    public function getLoyaltyPointsValue($count)
    {
        return $count * $this->revertedScale;
    }

    /**
     *   Get points gross value in currency
     *
     * @return float
     */
    public function getLoyaltyPointsValueGross($count)
    {
        return $this->taxCalculator->addTax(
                $this->getLoyaltyPointsValue($count));
    }
}
