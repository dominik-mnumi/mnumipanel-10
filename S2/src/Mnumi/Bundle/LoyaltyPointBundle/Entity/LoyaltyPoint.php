<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\LoyaltyPointBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * LoyaltyPoint
 *
 * @ORM\Table(name="loyalty_points")
 * @ORM\Entity
 */
class LoyaltyPoint
{

    const STATUS_ACCEPTED = 'accepted';
    const STATUS_USED = 'used';
    const STATUS_LOST = 'lost';
    const STATUS_NEW = 'new';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=false)
     */
    private $status;

    /**
     * @var integer
     *
     * @ORM\Column(name="points", type="integer", nullable=false)
     */
    private $points;

    /**
     * @var integer
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * Order create date
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Order update date
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=18, scale=2, nullable=true)
     */
    private $amount;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\RestServerBundle\Entity\User", inversedBy="loyaltyPoints")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var User
     *
     * @ORM\OneToOne(targetEntity="Mnumi\Bundle\InvoiceBundle\Entity\InvoiceItem")
     * @ORM\JoinColumn(name="invoice_item_id", referencedColumnName="id", nullable=true)
     */
    private $invoiceItem;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\OrderBundle\Entity\Order", inversedBy="loyaltyPoints")
     * @ORM\JoinColumn(name="order_package_id", referencedColumnName="id", nullable=true)
     */
    private $order;

    /**
     * @var \Currency
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\CurrencyBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param  integer                                              $status
     * @return \Mnumi\Bundle\LoyaltyPointBundle\Entity\LoyaltyPoint
     */
    public function setStatsu($status)
    {
        $this->status =  $status;

        return $this;
    }

    /**
     * Get points
     *
     * @return integer
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * Set points
     *
     * @param  integer                                              $points
     * @return \Mnumi\Bundle\LoyaltyPointBundle\Entity\LoyaltyPoint
     */
    public function setPoints($points)
    {
        $this->points = $points;

        return $this;
    }

    /**
     * Checks if points are used
     *
     * @return boolean
     */
    public function isUsed()
    {
        return $this->getStatus() == self::STATUS_USED;
    }
}
