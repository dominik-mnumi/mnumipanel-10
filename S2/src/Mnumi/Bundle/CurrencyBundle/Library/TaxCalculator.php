<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\CurrencyBundle\Library;

/**
 * TaxCalculator class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
class TaxCalculator
{

    /**
     * @var float
     */
    private $priceTax;

    public function __construct($priceTax)
    {
        $this->priceTax = $priceTax;
    }

    public function addTax($price, $customTax = false)
    {
        $tax = is_numeric($customTax) ? (float) $customTax : $this->priceTax;

        return round($price * (1 + $tax), 2);
    }
}
