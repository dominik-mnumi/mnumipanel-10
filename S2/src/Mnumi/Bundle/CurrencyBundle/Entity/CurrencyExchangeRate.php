<?php

namespace Mnumi\Bundle\CurrencyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CurrencyExchangeRate
 *
 * @ORM\Table(name="currency_exchange_rate", uniqueConstraints={@ORM\UniqueConstraint(name="u_currency_id_date_idx", columns={"date", "currency_id"})}, indexes={@ORM\Index(name="currency_exchange_rate_currency_id_idx", columns={"currency_id"})})
 * @ORM\Entity
 */
class CurrencyExchangeRate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="rate", type="decimal", precision=8, scale=4, nullable=false)
     */
    private $rate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=128, nullable=false)
     */
    private $source;

    /**
     * @var integer
     *
     * @ORM\Column(name="conversion", type="integer", nullable=false)
     */
    private $conversion;

    /**
     * @var string
     *
     * @ORM\Column(name="table_number", type="string", length=64, nullable=false)
     */
    private $tableNumber;

    /**
     * @var \Currency
     *
     * @ORM\ManyToOne(targetEntity="Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

}
