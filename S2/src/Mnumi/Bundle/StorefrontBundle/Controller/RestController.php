<?php

namespace Mnumi\Bundle\StorefrontBundle\Controller;

use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Mnumi\Bundle\PaymentBundle\Entity\Payment;
use Mnumi\Bundle\OrderBundle\Entity\Order;
use Mnumi\Bundle\StorefrontBundle\Exception\NotFoundBasketException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Mnumi\Bundle\RestServerBundle\Exception\InvalidFormException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Mnumi\Bundle\StorefrontBundle\Validator\BasketValidatorError;
use Symfony\Component\Intl\Intl;

/**
 * @Annotations\NamePrefix("api_storefront_")
 */
class RestController extends FOSRestController
{
    /**
     * Receive order summary required data
     *
     * @ApiDoc(
     *  resource = true,
     *  authenticationRoles = {
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *      200 = "Returned when order summary exist",
     *      500 = "Returned when error",
     *      404 = "Returned when session does not exists",
     *  },
     *  parameters = {
     *      {
     *          "name" = "sessionHandle",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "User session"
     *      },
     *      {
     *          "name" = "locale",
     *          "dataType" = "string",
     *          "required" = false,
     *          "description" = "User session"
     *      }
     *  }
     * )
     * @Annotations\Get("/session/{sessionHandle}/{locale}/basket/summary")
     * @Annotations\View(serializerGroups={"StoreFront"})
     * @param  string $sessionHandle
     * @return array
     */
    public function getOrderSummaryAction($sessionHandle, $locale = 'en')
    {
        $em = $this->getDoctrine()->getManager();
        $translator = $this->get('translator');
        $translator->setLocale($locale);

        $basket = $this->getBasket($sessionHandle);

        if ($basket == null) {

            throw new NotFoundBasketException(sprintf('Basket for given session handler: [%s] does not exists', $sessionHandle));
        }

        $loyaltyPointsCalculator = $this->get('loyalty_points.calculator');
        $taxCalculator = $this->get('tax_calculator');

        $carrierRepo = $em->getRepository('MnumiCarrierBundle:Carrier');
        $carriers = $carrierRepo->getCarriersForClient($basket->getClient());

        if (count($carriers) == 1) {
            $basket->setCarrier($carriers[0]);
        }

        $paymentRepo = $em->getRepository('MnumiPaymentBundle:Payment');
        $payments = $paymentRepo->getAvailablePaymentsForOrder($basket);

        if (count($payments) == 1) {
            $basket->setPayment($payments[0]);
        }

        $em->flush();

        $response = array();
        $response['basket'] = $basket;

        $customTax = $basket->getClient()->getWntUe() ? 0 : false;
        $loyaltyPointsValue = $loyaltyPointsCalculator->getOrderLoyaltyPointsNetValue($basket);
        $priceWithLoyaltyPoints = $loyaltyPointsCalculator->getOrderPriceNetWithLoyaltyPoints($basket);
        $paymentPrice = $basket->getPaymentNetPrice();
        $deliveryPrice = $basket->calculateDeliveryNetPrice();
        $deliveryAndPaymentPrice = $paymentPrice + $deliveryPrice;
        $summaryBaseAmount = $basket->calculateBaseAmount() + $deliveryAndPaymentPrice;
        $summaryTotalAmount = $basket->calculateTotalAmount() + $deliveryAndPaymentPrice;

        $response['basket_summary_amounts'] = array(
            'loyalty_points_value' => $taxCalculator->addTax($loyaltyPointsValue, $customTax),
            'price_with_loyalty_points' => $taxCalculator->addTax($priceWithLoyaltyPoints, $customTax),
            'total_basket_price' => $taxCalculator->addTax($basket->calculateBaseAmount(), $customTax),
            'payment_price' => $taxCalculator->addTax($paymentPrice, $customTax),
            'delivery_price' => $taxCalculator->addTax($deliveryPrice, $customTax),
            'total_price' => $taxCalculator->addTax($summaryBaseAmount, $customTax),
            'total_price_with_discount' => $taxCalculator->addTax($summaryTotalAmount, $customTax),
            'discount_value' => $taxCalculator->addTax($basket->calculateBaseAmount() - $basket->calculateTotalAmount(), $customTax)
            );

        $response['requireShipmentData'] = $basket->getCarrier() ?
                $basket->getCarrier()->getRequireShipmentData() :
                false;
        $currencyCode = $this->getCurrencyCode($basket);
        $response['currency_code'] = $currencyCode;
        $response['currency_symbol'] = Intl::getCurrencyBundle()->getCurrencySymbol($currencyCode, $locale);
        $response['carriers'] =  $carriers;
        $response['payments'] = $payments;
        $response['addresses'] = $basket->getClient()->getClientAddresses();

        $basketValidator = $this->get('basket.validator');
        $basketValidator->setBasket($basket);
        $response['errors'] = $basketValidator->getErrors();

        $response['isReadyToPost'] = $basketValidator->isValid();

        return $response;
    }

    /**
     * Update basket
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  parameters={
     *      {
     *          "name" = "sessionHandle",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "User session"
     *      }
     *  },
     *  tags = {
     *      "alfa" = "#ffa500"
     *  }
     * )
     *
     * @Method({"PATCH"})
     *
     * @Annotations\Patch("/session/{sessionHandle}/basket/patch")
     * @Annotations\View(serializerGroups={"StoreFront"})
     * @param string $sessionHandle
     */
    public function patchBasketAction($sessionHandle, Request $request)
    {
        try {

            $data = $request->request->all();
            $em = $this->getDoctrine()->getManager();

            $basket = $this->getBasket($sessionHandle);

            if ($basket === null) {
                throw new NotFoundBasketException(sprintf('Basket for given session handler: [%s] does not exists', $sessionHandle));
            }

            if ($data) {

                $currentPayment = $basket->getPayment();
                if (isset($data['carrier']) && $currentPayment) {

                    $paymentAllowed = $this->checkCarrierPayment($currentPayment->getId(),$data['carrier']);

                    if (!$paymentAllowed) {
                        $data['payment'] = null;
                    }
                }

                //if delivery address id is being patched
                if ($data['clientAddress']) {

                    $clientAddressRepo = $em->getRepository('MnumiClientBundle:ClientAddress');
                    $address = $clientAddressRepo->find($data['clientAddress']);

                    if ($address) {
                        $data['deliveryName'] = $address->getFullname();
                        $data['deliveryStreet'] = $address->getStreet();
                        $data['deliveryCity'] = $address->getCity();
                        $data['deliveryPostcode'] = $address->getPostcode();
                        $data['deliveryCountry'] = $address->getCountry();
                    }
                }

                $orderHandler = $this->container->get('rest.order.handler');
                /** @var Order $newObject */
                $newObject = $orderHandler->patch(
                    $basket,
                    $data
                );

                $em->persist($newObject);
                $em->flush($newObject);

                return new JsonResponse(array(
                    'status' => 'success',
                    'message' => 'Basket patched successfuly'), 200);
            }

            throw new HttpException(400, 'Parameters can not be empty');

        } catch (\Exception $exception) {

            throw new HttpException(400);
        }
    }

    /**
     * Create client address
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  parameters={
     *      {
     *          "name" = "sessionHandle",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "User session"
     *      }
     *  },
     *  tags = {
     *      "alfa" = "#ffa500"
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @Annotations\Post("/session/{sessionHandle}/{locale}/clientaddress")
     * @Annotations\View
     * @param string $sessionHandle
     */
    public function postClientAddressAction($sessionHandle, $locale = 'en', Request $request)
    {

        $sessionUser = $this->getSessionUser($sessionHandle);

        if (empty($sessionUser)) {

            throw new NotFoundHttpException('Session user not found');
        }

        $client = $sessionUser->getDefaultClient();

        if (empty($client)) {

            throw new NotFoundHttpException('Session client not found');
        }

        $translator = $this->get('translator');
        $translator->setLocale($locale);

        $data = $request->request->all();
        $data['client'] = $client->getId();
        $clientAddressHandler = $this->container->get('rest.clientaddress.handler');

        try {

            $newObject = $clientAddressHandler->post(
                $data,
                array("StoreFrontDelivery")
            );

            return array(
                'status' => 'success',
                'id' => $newObject->getId()
            );

        } catch (InvalidFormException $e) {

            $validator = $this->get('validator');
            $violations = $validator->validate($e->getForm()->getData(), array('StoreFrontDelivery'));

            $errors = array();
            foreach ($violations as $violation) {
                $errors[] = new BasketValidatorError($violation->getMessage(), 'clientaddress', $violation->getPropertyPath());
            }

            return array(
                'status' => 'failed',
                'errors' => $errors);
        }
    }

    /**
     * Create client address
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  parameters={
     *      {
     *          "name" = "sessionHandle",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "User session"
     *      },
     *      {
     *          "name" = "id",
     *          "dataType" = "integer",
     *          "required" = true,
     *          "description" = "ClientAddress id"
     *      }
     *  },
     *  tags = {
     *      "alfa" = "#ffa500"
     *  }
     * )
     *
     * @Method({"DELETE"})
     *
     * @Annotations\Delete("/session/{sessionHandle}/clientaddress/{id}/delete")
     * @Annotations\View
     * @param string  $sessionHandle
     * @param integer $id
     */
    public function deleteClientAddressAction($sessionHandle, $id)
    {
        $sessionUser = $this->getSessionUser($sessionHandle);

        if (empty($sessionUser)) {

            throw new NotFoundHttpException('Session user not found');
        }

        $client = $sessionUser->getDefaultClient();

        if (empty($client)) {

            throw new NotFoundHttpException('Session client not found');
        }

        $em = $this->getDoctrine()->getManager();
        $clientAddressRepo = $em->getRepository('MnumiClientBundle:ClientAddress');
        $clientAddress = $clientAddressRepo->findOneBy(array(
            'id' => $id,
            'client' => $client
        ));

        if (empty($clientAddress)) {

            throw new NotFoundHttpException('ClientAddress not found');
        }

        $basket = $this->getBasket($sessionHandle);
        if($basket &&
                $basket->getClientAddress() == $id)
        {
            $basket->cleanDeliveryData();
        }

        $em->remove($clientAddress);
        $em->flush();

        return array('status' => 'success');

    }

    /**
     * Checks if payment is allowed for carrier
     *
     * @param  integer $paymentId
     * @param  integer $carrierId
     * @return boolean
     */
    protected function checkCarrierPayment($paymentId, $carrierId)
    {
        $em = $this->getDoctrine()->getManager();
        $paymentRepo = $em->getRepository('MnumiPaymentBundle:Payment');
        $payments = $paymentRepo->getPaymentsForCarrier($carrierId);

        foreach ($payments as $payment) {
            if ($payment->getId() == $paymentId) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get basket
     *
     * @param  string $sessionHandle
     * @return Order
     */
    protected function getBasket($sessionHandle)
    {
        $em = $this->getDoctrine()->getManager();
        $orderRepo = $em->getRepository('MnumiOrderBundle:Order');
        $basket = $orderRepo->getBasketOrderForSession($sessionHandle);

        return $basket;
    }

    /**
     * Get session user
     *
     * @param  string $sessionHandle
     * @return User
     */
    protected function getSessionUser($sessionHandle)
    {
        $em = $this->getDoctrine()->getManager();
        $orderRepo = $em->getRepository('MnumiRestServerBundle:RestSession');
        $session = $orderRepo->findOneById($sessionHandle);

        $user = null;
        if ($session) {
            $user = $session->getUser();
        }

        return $user;
    }

    /**
     * Get currency code
     *
     * @param  Order  $basket
     * @return string
     */
    protected function getCurrencyCode(Order $basket)
    {
        $em = $this->getDoctrine()->getManager();
        $currencyCode = $this->getParameter('old_default_currency');
        $items = $basket->getItems();
        $item = $items->first();

        if ($item) {

            $shopName = $item->getShopName();

            $shopOption = $em->getRepository('MnumiShopBundle:ShopOption')
                    ->findOneBy(array(
                        'name' => 'currency_code',
                        'shopName' => $shopName));

            if ($shopOption) {
                $currency = $em->getRepository('MnumiCurrencyBundle:Currency')
                        ->findOneBy(array('code' => $shopOption->getValue()));
                if ($currency) {

                    $currencyCode = $currency->getCode();
                }
            }
        }

        return $currencyCode;
    }
}
