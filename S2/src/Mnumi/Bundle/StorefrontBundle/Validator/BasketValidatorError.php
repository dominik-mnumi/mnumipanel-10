<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\StorefrontBundle\Validator;

use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

/**
 * BasketValidatorError class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
class BasketValidatorError
{
    const TYPE_DEFAULT = 'default';
    const TYPE_CARRIER = 'carrier';
    const TYPE_DELIVERY = 'delivery';
    const TYPE_PAYMENT = 'payment';
    const TYPE_ADDRESS = 'address';
    const TYPE_INVOICE = 'invoice';

    /**
     * @var string $type
     *
     * @Serializer\Groups({"StoreFront"})
     */
    private $type;

    /**
     * @var string $info
     *
     * @Serializer\Groups({"StoreFront"})
     */
    private $info;

    /**
     * @var string $fieldName
     *
     * @Serializer\Groups({"StoreFront"})
     */
    private $fieldName;

    /**
     * @param string $info
     * @param string $type
     */
    public function __construct($info, $type, $fieldName = null)
    {
        $this->type = $type;
        $this->info = $info;
        $this->fieldName = $fieldName;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get info
     *
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * Get field name
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }
}
