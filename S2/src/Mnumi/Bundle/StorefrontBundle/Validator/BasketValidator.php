<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\StorefrontBundle\Validator;

use Mnumi\Bundle\OrderBundle\Entity\Order;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * BasketValidator class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
class BasketValidator
{
    /** @var RecursiveValidator $validator **/
    private $validator;

    /** @var TranslatorInterface $translator **/
    private $translator;

    /** @var Order $basket **/
    private $basket;

    /** @var boolean $proceed **/
    private $proceeded = false;

    /** @var boolean $isValid **/
    private $isValid;

    /**  @var array $errors **/
    private $errors;

    /** @var integer $errorsCount **/
    private $errorsCount = 0;

    /** @var array $carrierFields **/
    private static $carrierFields = array(
        'carrier'
    );

    /** @var array $paymentFields **/
    private static $paymentFields = array(
        'payment'
    );

    /** @var array $deliveryFields **/
    private static $deliveryFields = array(
        'clientAddress',
        'deliveryName',
        'deliveryStreet',
        'deliveryCity',
        'deliveryPostcode',
        'deliveryCountry'
    );

    /** @var array $invoiceFields **/
    private static $invoiceFields = array(
        'invoiceName',
        'invoiceStreet',
        'invoiceCity',
        'invoicePostcode',
        'invoiceCountry',
        'invoiceTaxId'
    );

    /**
     * BasketValidator constructor
     *
     * @param RecursiveValidator $validator
     */
    public function __construct(RecursiveValidator $validator, TranslatorInterface $translator)
    {
        $this->validator = $validator;
        $this->translator = $translator;
    }

    /**
     * Set basket
     *
     * @param Order $basket
     */
    public function setBasket(Order $basket)
    {
        $this->proceeded = false;
        $this->basket = $basket;
    }

    /**
     * Checks if provided basket is valid
     *
     * @return boolean
     */
    public function isValid()
    {
        if (!$this->proceeded) {

            $this->proceedValidation();
        }

        return $this->isValid;
    }

    /**
     * Get errors
     *
     * @return array
     */
    public function getErrors()
    {
        if (!$this->proceeded) {

            $this->proceedValidation();
        }

        return $this->errors;
    }

    /**
     * Add validator error
     *
     * @param type $info
     * @param type $type
     */
    protected function addError($info, $type, $fieldName = null)
    {
        if (!isset($this->errors[$type])) {
            $this->errors[$type] = array();
        }

        $this->errors[$type][] = new BasketValidatorError($info, $type, $fieldName);
        $this->errorsCount++;
    }

    /**
     * Proceed validation
     *
     * @throws \Exception
     */
    protected function proceedValidation()
    {

        $this->errors = array();
        $this->errorsCount = 0;

        if ($this->basket == null) {

            throw new \Exception('Basket is not set');
        }

        if ($this->basket->isBasket()) {
            throw new \Exception('Provided order is not a basket');
        }

        $this->isValid = true;

        $violations = $this->validator->validate($this->basket,
                $this->getValidationGroups());

        if ($violations->count() > 0) {

            $this->addError(
                    $this->translator->trans('Please fill missing data', array(), 'validators'),
                    BasketValidatorError::TYPE_DEFAULT);
        }

        foreach ($violations as $violation) {

            $info = $violation->getMessage();
            $fieldName = $violation->getPropertyPath();
            $type = $this->getErrorTypeByFieldName($fieldName);

            $this->addError($info, $type, $fieldName);
        }

        $this->isValid = $this->errorsCount == 0;

        $this->proceeded = true;
    }

    /**
     * Get error type by field name
     *
     * @param  string $fieldName
     * @return string
     */
    protected function getErrorTypeByFieldName($fieldName)
    {

        if (in_array($fieldName, self::$deliveryFields)) {
            return BasketValidatorError::TYPE_DELIVERY;
        }

        if (in_array($fieldName, self::$invoiceFields)) {
            return BasketValidatorError::TYPE_INVOICE;
        }

        if (in_array($fieldName, self::$carrierFields)) {
            return BasketValidatorError::TYPE_CARRIER;
        }

        if (in_array($fieldName, self::$paymentFields)) {
            return BasketValidatorError::TYPE_PAYMENT;
        }

        return BasketValidatorError::TYPE_DEFAULT;
    }

    /**
     * Get validation groups, based on basket properties
     *
     * @return array
     * @throws \Exception
     */
    protected function getValidationGroups()
    {
        if ($this->basket == null) {

            throw new \Exception('Basket is not set');
        }

        $groups = array('StoreFront');

        if ($this->basket->getWantInvoice()) {
            $groups[] = 'StoreFrontInvoice';
        }

        if($this->basket->getCarrier() &&
                $this->basket->getCarrier()->getRequireShipmentData())
        {
            $groups[] = 'StoreFrontDelivery';
        }

        return $groups;
    }
}
