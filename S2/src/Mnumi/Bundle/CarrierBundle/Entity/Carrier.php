<?php

namespace Mnumi\Bundle\CarrierBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Carrier
 *
 * @ORM\Table(name="carrier",uniqueConstraints={@ORM\UniqueConstraint(columns={"name"})}, indexes={@ORM\Index(columns={"carrier_report_type_name"})})
 * @ORM\Entity(repositoryClass="\Mnumi\Bundle\CarrierBundle\Entity\Repository\Carrier")
 */
class Carrier
{
    /**
     * Unique autoincrement ID
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $id;

    /**
     * Carrier name
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $name;

    /**
     * Carrier label for customers
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $label;

    /**
     * @var string
     *
     * @ORM\Column(name="delivery_time", type="string", length=255, nullable=false)
     * @Serializer\Exclude
     */
    private $deliveryTime;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $active = true;

    /**
     * @var boolean
     *
     * @ORM\Column(name="apply_on_all_pricelist", type="boolean", nullable=true)
     * @Serializer\Exclude
     */
    private $applyOnAllPricelist = true;

    /**
     * @var integer
     *
     * @ORM\Column(name="tax", type="integer", nullable=true)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $tax = 23;

    /**
     * @var string
     *
     * @ORM\Column(name="restrict_for_cities", type="string", length=255, nullable=true)
     * @Serializer\Exclude
     */
    private $restrictForCities;

    /**
     * @var boolean
     *
     * @ORM\Column(name="require_shipment_data", type="boolean", nullable=false)
     * @Serializer\Groups({"StoreFront"})
     */
    private $requireShipmentData = '1';

    /**
     * The additional carrier cost
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=2, nullable=true)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $cost;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=18, scale=2, nullable=true)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="free_shipping_amount", type="float", precision=18, scale=2, nullable=true)
     * @Serializer\Exclude
     */
    private $freeShippingAmount;

    /**
     * @var \CarrierReportType
     *
     * @ORM\ManyToOne(targetEntity="CarrierReportType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="carrier_report_type_name", referencedColumnName="name")
     * })
     */
    private $carrierReportTypeName;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=45, nullable=true)
     * @Serializer\Exclude
     */
    private $type;

    /**
     * @var configuration
     *
     * @ORM\Column(name="configuration", type="json_array", nullable=true)
     * @Serializer\Exclude
     */
    private $configuration;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="\Mnumi\Bundle\CarrierBundle\Entity\CarrierPayment", mappedBy="carrier")
     * @Serializer\Groups({"public"})
     */
    private $carrierPayments;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="\Mnumi\Bundle\PaymentBundle\Entity\Payment")
     * @ORM\JoinTable(name="carrier_payment",
     *     joinColumns={
     *         @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     *     })
     * @Serializer\Exclude
     */
    private $payments;

    /**
     * @var array
     *
     * @ORM\ManyToMany(targetEntity="\Mnumi\Bundle\CalculationBundle\Entity\PriceList")
     * @ORM\JoinTable(name="carrier_pricelist",
     *     joinColumns={
     *         @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     *     })
     * @Serializer\Exclude
     */
    private $pricelists;

    public function __construct() {
        $this->payments = new ArrayCollection();
        $this->pricelists = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->getId() . '=>' . $this->getName();
    }

    /**
     * Set name
     *
     * @param  string  $name
     * @return Carrier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param  string  $label
     * @return Carrier
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set deliveryTime
     *
     * @param  string  $deliveryTime
     * @return Carrier
     */
    public function setDeliveryTime($deliveryTime)
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    /**
     * Get deliveryTime
     *
     * @return string
     */
    public function getDeliveryTime()
    {
        return $this->deliveryTime;
    }

    /**
     * Set active
     *
     * @param  boolean $active
     * @return Carrier
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set applyOnAllPricelist
     *
     * @param  boolean $applyOnAllPricelist
     * @return Carrier
     */
    public function setApplyOnAllPricelist($applyOnAllPricelist)
    {
        $this->applyOnAllPricelist = $applyOnAllPricelist;

        return $this;
    }

    /**
     * Get applyOnAllPricelist
     *
     * @return boolean
     */
    public function getApplyOnAllPricelist()
    {
        return $this->applyOnAllPricelist;
    }

    /**
     * Set tax
     *
     * @param  integer $tax
     * @return Carrier
     */
    public function setTax($tax)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return integer
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set restrictForCities
     *
     * @param  string  $restrictForCities
     * @return Carrier
     */
    public function setRestrictForCities($restrictForCities)
    {
        $this->restrictForCities = $restrictForCities;

        return $this;
    }

    /**
     * Get restrictForCities
     *
     * @return string
     */
    public function getRestrictForCities()
    {
        return $this->restrictForCities;
    }

    /**
     * Set requireShipmentData
     *
     * @param  boolean $requireShipmentData
     * @return Carrier
     */
    public function setRequireShipmentData($requireShipmentData)
    {
        $this->requireShipmentData = $requireShipmentData;

        return $this;
    }

    /**
     * Get requireShipmentData
     *
     * @return boolean
     */
    public function getRequireShipmentData()
    {
        return $this->requireShipmentData;
    }

    /**
     * Set cost
     *
     * @param  float   $cost
     * @return Carrier
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set price
     *
     * @param  float   $price
     * @return Carrier
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set freeShippingAmount
     *
     * @param  float   $freeShippingAmount
     * @return Carrier
     */
    public function setFreeShippingAmount($freeShippingAmount)
    {
        $this->freeShippingAmount = $freeShippingAmount;

        return $this;
    }

    /**
     * Get freeShippingAmount
     *
     * @return float
     */
    public function getFreeShippingAmount()
    {
        return $this->freeShippingAmount;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \CarrierReportType
     */
    public function getCarrierReportTypeName()
    {
        return $this->carrierReportTypeName;
    }

    /**
     * Get configuration
     *
     * @return array
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * Set configuration
     *
     * @param array $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set type
     *
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get Tax value
     *
     * @return float
     */
    public function getTaxFloat()
    {
        return (float) $this->getTax() / 100;
    }

    /**
     * Get price gross
     *
     * @return float
     */
    public function getPriceGross()
    {
        return $this->getPrice() * (1 + $this->getTaxFloat());
    }
}
