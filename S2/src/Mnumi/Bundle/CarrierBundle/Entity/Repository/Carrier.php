<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\CarrierBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Mnumi\Bundle\ClientBundle\Entity\Client;

/**
 * Carrier repository class
 */
class Carrier  extends EntityRepository {

    /**
     * Get carriers for client
     *
     * @param Mnumi\Bundle\ClientBundle\Entity\Client $client
     * @param integer $paymentId
     */
    public function getCarriersForClient(Client $client, $paymentId = null)
    {
        $priceList = $client->getPricelist();

        $qb = $this->createQueryBuilder('c')
                ->leftJoin('c.pricelists', 'pl')
                ->where('c.active = 1')
                ->andWhere('pl.id = :priceList OR c.applyOnAllPricelist = 1')
                ->setParameter('priceList', $priceList);

        if($paymentId) {
            $qb
                ->leftJoin('c.payments', 'p')
                ->andWhere('p.id = :paymentId')
                ->setParameter('paymentId', $paymentId);
        }
                
        return $qb
                ->getQuery()
                ->getResult();

    }
}
