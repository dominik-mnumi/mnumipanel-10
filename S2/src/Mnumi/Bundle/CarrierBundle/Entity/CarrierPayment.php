<?php

namespace Mnumi\Bundle\CarrierBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Mnumi\Bundle\CarrierBundle\Entity\Carrier;
use Mnumi\Bundle\PaymentBundle\Entity\Payment;

/**
 * CarrierPayment
 *
 * @ORM\Table(name="carrier_payment",
 *     indexes={@ORM\Index(columns={"carrier_id"}),
 *              @ORM\Index(columns={"payment_id"})
 * })
 * @ORM\Entity
 */
class CarrierPayment
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"public", "StoreFront"})
     */
    private $id;

    /**
     * @var \Carrier
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\CarrierBundle\Entity\Carrier", inversedBy="carrierPayments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public", "StoreFront"})
     */
    private $carrier;

    /**
     * @var \Payment
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\PaymentBundle\Entity\Payment", inversedBy="carrierPayments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public", "StoreFront"})
     */
    private $payment;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get carrier
     *
     * @return Carrier
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set carrier
     *
     * @param Carrier $carrier
     * @return \Mnumi\Bundle\CarrierBundle\Entity\CarrierPayment
     */
    public function setCarrier(Carrier $carrier)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get payment
     *
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set payment
     *
     * @param Payment $payment
     * @return \Mnumi\Bundle\CarrierBundle\Entity\CarrierPayment
     */
    public function setPayment(Payment $payment)
    {
        $this->payment = $payment;

        return $this;
    }
}