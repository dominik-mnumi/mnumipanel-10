<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ClientAdapter class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
namespace Mnumi\Bundle\FotoliaBundle\Library;

use Mnumi\Bundle\FotoliaBundle\Client\Client;

class ClientAdapter
{

    /**
     * @var Client
     */
    private $client;

    /**
     * @var integer
     */
    private $languageId;

    public function __construct(Client $client, $languageId = Client::LANGUAGE_ID_PL_PL)
    {

        $this->client = $client;
        $this->languageId = $languageId;
    }

    public function getSearchResults($phrase, $limit, $offset, $languageId = null)
    {
        $languageId = $languageId ?: $this->languageId;

        $result = $this->client->getSearchResults(array(
            'words' => $phrase,
            'language_id' => $languageId,
            'limit' => $limit,
            'offset' => $offset
        ));

        return $result;
    }

    /**
     * This method return all information about a media
     *
     * @param  int   $id
     * @param  int   $thumbnail_size
     * @param  int   $language_id
     * @return array
     */
    public function getMediaData($id, $thumbnail_size = 110, $languageId = null)
    {
        $languageId = $languageId ?: $this->languageId;
        $result = $this->client->getMediaData($id, $thumbnail_size, $languageId);

        $fotoliaMedia = new FotoliaMedia();
        $fotoliaMedia->setId($result['id']);
        $fotoliaMedia->setTitle($result['title']);
        $fotoliaMedia->setThumbnailUrl($result['thumbnail_url']);
        $fotoliaMedia->setThumbnailWidth($result['thumbnail_width']);
        $fotoliaMedia->setThumbnailHeight($result['thumbnail_height']);

        return $fotoliaMedia;
    }
}
