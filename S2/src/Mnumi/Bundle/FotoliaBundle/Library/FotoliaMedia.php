<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FotoliaMedia class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Bundle\FotoliaBundle\Library;

use Mnumi\Bundle\OrderBundle\Entity\OrderItemFile;

class FotoliaMedia
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $thumbnailUrl;

    /**
     * @var integer
     */
    private $thumbnailWidth;

    /**
     * @var integer
     */
    private $thumbnailHeight;

    public function __construct()
    {
    }

    /**
     * Set id
     *
     * @param integer $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set thubnail url
     *
     * @param string $url
     */
    public function setThumbnailUrl($url)
    {
        $this->thumbnailUrl = $url;
    }

    /**
     * Get thumbnail url
     *
     * @return string
     */
    public function getThumbnailUrl()
    {
        return $this->thumbnailUrl;
    }

    /**
     * Set thumbnail width
     *
     * @param integer $width
     */
    public function setThumbnailWidth($width)
    {
        $this->thumbnailWidth = $width;
    }

    /**
     * Get thumbnail width
     *
     * @return integer
     */
    public function getThumbnailWidth()
    {
        return $this->thumbnailWidth;
    }

    /**
     * Set thumbnail height
     *
     * @param integer $height
     */
    public function setThumbnailHeight($height)
    {
        $this->thumbnailHeight = $height;
    }

    /**
     * Get thumbnail height
     *
     * @return integer
     */
    public function getThumbnailHeight()
    {
        return $this->thumbnailHeight;
    }

    /**
     * Get media file name
     *
     * @return string
     */
    public function getFilename()
    {
        $filename = '';

        if ($this->getThumbnailUrl()) {
            $filename = basename($this->getThumbnailUrl());
        }

        return $filename;
    }

    /**
     * Download and save file in given location
     *
     * @param  string    $dirPath
     * @throws Exception
     */
    public function saveThumbnailFile($dirPath)
    {
        if ($this->getFilename() === '') {
            throw new Exception('Filename nedded to save FotoliaMedia file');
        }

        $filePath = $dirPath . $this->getFilename();

        $httpClient = new \GuzzleHttp\Client();

        $resource = fopen($filePath, 'w');

        $httpClient->get($this->getThumbnailUrl(), array(
            'save_to' => $resource
        ));
    }

    /**
     * Update OrderItemFile with FotoliaMedia data
     *
     * @param  OrderItemFile $orderItemFile
     * @param  sting         $dataDir
     * @return OrderItemFile
     */
    public function updateThumbOrderItemFile(OrderItemFile $orderItemFile, $dataDir)
    {
        $orderItem = $orderItemFile->getOrderItem();
        $dirPath = $orderItem->preparePath($dataDir);

        $this->saveThumbnailFile($dirPath);

        $orderItemFile->setMetadataAttribute('fotolia_id', $this->getId());
        $orderItemFile->setMetadataAttribute('title', $this->getTitle());
        $orderItemFile->setMetadataAttribute('url', $this->getThumbnailUrl());
        $orderItemFile->setMetadataAttribute('widthPx', $this->getThumbnailWidth());
        $orderItemFile->setMetadataAttribute('heightPx', $this->getThumbnailHeight());
        $orderItemFile->setMetadataAttribute('format', 'JPEG');

        return $orderItemFile;
    }
}
