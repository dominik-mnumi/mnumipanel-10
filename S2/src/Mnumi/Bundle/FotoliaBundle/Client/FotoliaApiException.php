<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FotoliaApiException class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Bundle\FotoliaBundle\Client;

/**
 * Fotolia API Exception
 */
class FotoliaApiException extends \Exception {}
