<?php

namespace Mnumi\Bundle\InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * InvoiceType
 *
 * @ORM\Table(name="invoice_type")
 * @ORM\Entity
 */
class InvoiceType {

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false, unique=true)
     * @ORM\Id
     * @Serializer\Groups({"public"})
     */
    private $name;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InvoiceType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}
