<?php

namespace Mnumi\Bundle\InvoiceBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

class Invoice  extends EntityRepository {

    /**
     * Get invoice next number based date (Y and/or m)
     *
     * @param string $year
     * @param string $month
     * @return int
     */
    public function getNextNumber($year, $month = null)
    {
        /** @var Invoice $invoice */
        $emConfig = $this->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'Mnumi\Bundle\InvoiceBundle\Library\Query\Mysql\Year');
        if($month !== null) {
            $emConfig->addCustomDatetimeFunction('MONTH', 'Mnumi\Bundle\InvoiceBundle\Library\Query\Mysql\Month');
        }

        $qb = $this->createQueryBuilder('p');
        $qb->select('p')
            ->where('YEAR(p.sellAt) = :year');
        if($month !== null) {
            $qb->andWhere('MONTH(p.sellAt) = :month');
        }

        $qb->setParameter('year', $year);
        if($month !== null) {
            $qb->setParameter('month', $month);
        }

        try {
            $invoice = $qb->getQuery()->getSingleResult();
        } catch (NoResultException $e)
        {
            return 1;
        }

        return $invoice->getNumber() + 1;
    }

} 