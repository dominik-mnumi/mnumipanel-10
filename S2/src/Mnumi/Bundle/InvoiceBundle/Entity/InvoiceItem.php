<?php

namespace Mnumi\Bundle\InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Mnumi\Bundle\InvoiceBundle\Entity\Invoice;
use Mnumi\Bundle\OrderBundle\Entity\OrderItem;
use Mnumi\Bundle\OrderBundle\Entity\Unit;
use Mnumi\Bundle\RestServerBundle\Entity\User;

/**
 * InvoiceItem
 *
 * @ORM\Table(name="invoice_item")
 * @ORM\Entity
 */
class InvoiceItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @var Invoice
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\InvoiceBundle\Entity\Invoice", inversedBy="invoiceItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public"})
     */
    private $invoice;

    /**
     * @var OrderItem
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\OrderBundle\Entity\OrderItem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public"})
     */
    private $orderItem;

    /**
     * Description
     * @var string
     *
     * @ORM\Column(name="description", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $quantity;

    /**
     * @var float
     *
     * @ORM\Column(name="price_net", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $priceNet;

    /**
     * @var float
     *
     * @ORM\Column(name="price_discount", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $priceDiscount;

    /**
     * @var Unit
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\OrderBundle\Entity\Unit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unit_of_measure", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public"})
     */
    private $unit;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\RestServerBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="editor_id", referencedColumnName="id")
     * })
     */
    private $editor;

    /**
     * @var integer
     *
     * @ORM\Column(name="tax_value", type="integer", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $taxValue;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Serializer\Exclude
     */
    private $deletedAt;
    
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InvoiceItem
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     *
     * @return InvoiceItem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set priceNet
     *
     * @param float $priceNet
     *
     * @return InvoiceItem
     */
    public function setPriceNet($priceNet)
    {
        $this->priceNet = $priceNet;

        return $this;
    }

    /**
     * Get priceNet
     *
     * @return float
     */
    public function getPriceNet()
    {
        return $this->priceNet;
    }

    /**
     * Set priceDiscount
     *
     * @param float $priceDiscount
     *
     * @return InvoiceItem
     */
    public function setPriceDiscount($priceDiscount = null)
    {
        if($priceDiscount === null)
        {
            $priceDiscount = 0;
        }
        $this->priceDiscount = $priceDiscount;

        return $this;
    }

    /**
     * Get priceDiscount
     *
     * @return float
     */
    public function getPriceDiscount()
    {
        return $this->priceDiscount;
    }

    /**
     * Set unit
     *
     * @param Unit $unit
     *
     * @return InvoiceItem
     */
    public function setUnit(Unit $unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return Unit
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set taxValue
     *
     * @param integer $taxValue
     *
     * @return InvoiceItem
     */
    public function setTaxValue($taxValue)
    {
        $this->taxValue = $taxValue;

        return $this;
    }

    /**
     * Get taxValue
     *
     * @return integer
     */
    public function getRaxValue()
    {
        return $this->taxValue;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return InvoiceItem
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return InvoiceItem
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param \DateTime $deletedAt
     *
     * @return InvoiceItem
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set invoice
     *
     * @param Invoice $invoice
     *
     * @return InvoiceItem
     */
    public function setInvoice(Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set order item
     *
     * @param OrderItem $orderItem
     *
     * @return InvoiceItem
     */
    public function setOrderItem(OrderItem $orderItem = null)
    {
        $this->orderItem = $orderItem;

        return $this;
    }

    /**
     * Get order item
     *
     * @return OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * Set editor
     *
     * @param User $editor
     *
     * @return InvoiceItem
     */
    public function setEditor(User $editor = null)
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * Get editor
     *
     * @return User
     */
    public function getEditor()
    {
        return $this->editor;
    }
}
