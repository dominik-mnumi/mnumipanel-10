<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Doctrine\Common\Collections\ArrayCollection;
use Mnumi\Bundle\ClientBundle\Entity\Client;
use Mnumi\Bundle\InvoiceBundle\Entity\InvoiceStatus;
use Mnumi\Bundle\InvoiceBundle\Entity\InvoiceType;
use Mnumi\Bundle\PaymentBundle\Entity\Payment;
use Mnumi\Bundle\RestServerBundle\Entity\User;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="\Mnumi\Bundle\InvoiceBundle\Entity\Repository\Invoice")
 */
class Invoice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $name;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\ClientBundle\Entity\Client")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    private $client;

    /**
     * @var InvoiceType
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\InvoiceBundle\Entity\InvoiceType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_type_name", referencedColumnName="name")
     * })
     */
    private $invoiceType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sell_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $sellAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $number;
    
    /**
     * @var string
     *
     * @ORM\Column(name="seller_name", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $sellerName;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_address", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $sellerAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_postcode", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $sellerPostcode;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_city", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $sellerCity;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_tax_id", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $sellerTax;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_bank_name", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $sellerBankName;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_bank_account", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $sellerBankAccount;

    /**
     * @var string
     *
     * @ORM\Column(name="seller_country", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $sellerCountry;

    /**
     * @var string
     *
     * @ORM\Column(name="client_name", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $clientName;

    /**
     * @var string
     *
     * @ORM\Column(name="client_address", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $clientAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="client_postcode", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $clientPostcode;

    /**
     * @var string
     *
     * @ORM\Column(name="client_city", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $clientCity;

    /**
     * @var string
     *
     * @ORM\Column(name="client_tax_id", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $clientTax;

    /**
     * @var string
     *
     * @ORM\Column(name="client_country", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $clientCountry;

    /**
     * @var float
     *
     * @ORM\Column(name="price_net", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $priceNet;

    /**
     * @var float
     *
     * @ORM\Column(name="price_tax", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $priceTax;

    /**
     * @var float
     *
     * @ORM\Column(name="price_gross", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $priceGross;

    /**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\PaymentBundle\Entity\Payment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     * })
     */
    private $payment;

    /**
     * @var string
     *
     * @ORM\Column(name="payment_label", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $paymentLabel;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payment_date_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $paymentDateAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="payed_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $paidAt;

    /**
     * @var string
     *
     * @ORM\Column(name="notice", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $notice;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\RestServerBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="editor_id", referencedColumnName="id")
     * })
     */
    private $editor;

    /**
     * @var InvoiceStatus
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\InvoiceBundle\Entity\InvoiceStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="invoice_status_name", referencedColumnName="name")
     * })
     */
    private $invoiceStatus;

    /**
     * @var \Mnumi\Bundle\InvoiceBundle\Entity\Invoice
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\InvoiceBundle\Entity\Invoice")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="internal_note", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $internalNote;

    /**
     * @var boolean
     *
     * @ORM\Column(name="to_fiscal_print", type="boolean", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $toFiscalPrint = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="service_realized_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $serviceRealizedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="currency_exchange_rate_id", type="integer", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $currencyExchangeRate;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="currency_id", type="integer", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $currency;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $updatedAt;

    /**
     * @var InvoiceItem[]
     *
     * @ORM\OneToMany(targetEntity="Mnumi\Bundle\InvoiceBundle\Entity\InvoiceItem", mappedBy="invoice", cascade={"all"})
     */
    private $invoiceItem;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Invoice
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sellAt
     *
     * @param \DateTime $sellAt
     *
     * @return Invoice
     */
    public function setSellAt($sellAt)
    {
        $this->sellAt = $sellAt;

        return $this;
    }

    /**
     * Get sellAt
     *
     * @return \DateTime
     */
    public function getSellAt()
    {
        return $this->sellAt;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Invoice
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set sellerName
     *
     * @param string $sellerName
     *
     * @return Invoice
     */
    public function setSellerName($sellerName)
    {
        $this->sellerName = $sellerName;

        return $this;
    }

    /**
     * Get sellerName
     *
     * @return string
     */
    public function getSellerName()
    {
        return $this->sellerName;
    }

    /**
     * Set sellerAddress
     *
     * @param string $sellerAddress
     *
     * @return Invoice
     */
    public function setSellerAddress($sellerAddress)
    {
        $this->sellerAddress = $sellerAddress;

        return $this;
    }

    /**
     * Get sellerAddress
     *
     * @return string
     */
    public function getSellerAddress()
    {
        return $this->sellerAddress;
    }

    /**
     * Set sellerPostcode
     *
     * @param string $sellerPostcode
     *
     * @return Invoice
     */
    public function setSellerPostcode($sellerPostcode)
    {
        $this->sellerPostcode = $sellerPostcode;

        return $this;
    }

    /**
     * Get sellerPostcode
     *
     * @return string
     */
    public function getSellerPostcode()
    {
        return $this->sellerPostcode;
    }

    /**
     * Set sellerCity
     *
     * @param string $sellerCity
     *
     * @return Invoice
     */
    public function setSellerCity($sellerCity)
    {
        $this->sellerCity = $sellerCity;

        return $this;
    }

    /**
     * Get sellerCity
     *
     * @return string
     */
    public function getSellerCity()
    {
        return $this->sellerCity;
    }

    /**
     * Set sellerTax
     *
     * @param string $sellerTax
     *
     * @return Invoice
     */
    public function setSellerTax($sellerTax)
    {
        $this->sellerTax = $sellerTax;

        return $this;
    }

    /**
     * Get sellerTax
     *
     * @return string
     */
    public function getSellerTax()
    {
        return $this->sellerTax;
    }

    /**
     * Set sellerBankName
     *
     * @param string $sellerBankName
     *
     * @return Invoice
     */
    public function setSellerBankName($sellerBankName)
    {
        $this->sellerBankName = $sellerBankName;

        return $this;
    }

    /**
     * Get sellerBankName
     *
     * @return string
     */
    public function getSellerBankName()
    {
        return $this->sellerBankName;
    }

    /**
     * Set sellerBankAccount
     *
     * @param string $sellerBankAccount
     *
     * @return Invoice
     */
    public function setSellerBankAccount($sellerBankAccount)
    {
        $this->sellerBankAccount = $sellerBankAccount;

        return $this;
    }

    /**
     * Get sellerBankAccount
     *
     * @return string
     */
    public function getSellerBankAccount()
    {
        return $this->sellerBankAccount;
    }

    /**
     * Set sellerCountry
     *
     * @param string $sellerCountry
     *
     * @return Invoice
     */
    public function setSellerCountry($sellerCountry)
    {
        $this->sellerCountry = $sellerCountry;

        return $this;
    }

    /**
     * Get sellerCountry
     *
     * @return string
     */
    public function getSellerCountry()
    {
        return $this->sellerCountry;
    }

    /**
     * Set clientName
     *
     * @param string $clientName
     *
     * @return Invoice
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;

        return $this;
    }

    /**
     * Get clientName
     *
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * Set clientAddress
     *
     * @param string $clientAddress
     *
     * @return Invoice
     */
    public function setClientAddress($clientAddress)
    {
        $this->clientAddress = $clientAddress;

        return $this;
    }

    /**
     * Get clientAddress
     *
     * @return string
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * Set clientPostcode
     *
     * @param string $clientPostcode
     *
     * @return Invoice
     */
    public function setClientPostcode($clientPostcode)
    {
        $this->clientPostcode = $clientPostcode;

        return $this;
    }

    /**
     * Get clientPostcode
     *
     * @return string
     */
    public function getClientPostcode()
    {
        return $this->clientPostcode;
    }

    /**
     * Set clientCity
     *
     * @param string $clientCity
     *
     * @return Invoice
     */
    public function setClientCity($clientCity)
    {
        $this->clientCity = $clientCity;

        return $this;
    }

    /**
     * Get clientCity
     *
     * @return string
     */
    public function getClientCity()
    {
        return $this->clientCity;
    }

    /**
     * Set clientTax
     *
     * @param string $clientTax
     *
     * @return Invoice
     */
    public function setClientTax($clientTax)
    {
        $this->clientTax = $clientTax;

        return $this;
    }

    /**
     * Get clientTax
     *
     * @return string
     */
    public function getClientTax()
    {
        return $this->clientTax;
    }

    /**
     * Set clientCountry
     *
     * @param string $clientCountry
     *
     * @return Invoice
     */
    public function setClientCountry($clientCountry)
    {
        $this->clientCountry = $clientCountry;

        return $this;
    }

    /**
     * Get clientCountry
     *
     * @return string
     */
    public function getClientCountry()
    {
        return $this->clientCountry;
    }

    /**
     * Set priceNet
     *
     * @param float $priceNet
     *
     * @return Invoice
     */
    public function setPriceNet($priceNet)
    {
        $this->priceNet = $priceNet;

        return $this;
    }

    /**
     * Get priceNet
     *
     * @return float
     */
    public function getPriceNet()
    {
        return $this->priceNet;
    }

    /**
     * Set priceTax
     *
     * @param float $priceTax
     *
     * @return Invoice
     */
    public function setPriceTax($priceTax)
    {
        $this->priceTax = $priceTax;

        return $this;
    }

    /**
     * Get priceTax
     *
     * @return float
     */
    public function getPriceTax()
    {
        return $this->priceTax;
    }

    /**
     * Set priceGross
     *
     * @param float $priceGross
     *
     * @return Invoice
     */
    public function setPriceGross($priceGross)
    {
        $this->priceGross = $priceGross;

        return $this;
    }

    /**
     * Get priceGross
     *
     * @return float
     */
    public function getPriceGross()
    {
        return $this->priceGross;
    }

    /**
     * Set paymentLabel
     *
     * @param string $paymentLabel
     *
     * @return Invoice
     */
    public function setPaymentLabel($paymentLabel)
    {
        $this->paymentLabel = $paymentLabel;

        return $this;
    }

    /**
     * Get paymentLabel
     *
     * @return string
     */
    public function getPaymentLabel()
    {
        return $this->paymentLabel;
    }

    /**
     * Set paymentDateAt
     *
     * @param \DateTime $paymentDateAt
     *
     * @return Invoice
     */
    public function setPaymentDateAt($paymentDateAt)
    {
        $this->paymentDateAt = $paymentDateAt;

        return $this;
    }

    /**
     * Get paymentDateAt
     *
     * @return \DateTime
     */
    public function getPaymentDateAt()
    {
        return $this->paymentDateAt;
    }

    /**
     * Set paidAt
     *
     * @param \DateTime $paidAt
     *
     * @return Invoice
     */
    public function setPaidAt($paidAt)
    {
        $this->paidAt = $paidAt;

        return $this;
    }

    /**
     * Get paidAt
     *
     * @return \DateTime
     */
    public function getPaidAt()
    {
        return $this->paidAt;
    }

    /**
     * Set notice
     *
     * @param string $notice
     *
     * @return Invoice
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Get notice
     *
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * Set internalNote
     *
     * @param string $internalNote
     *
     * @return Invoice
     */
    public function setInternalNote($internalNote)
    {
        $this->internalNote = $internalNote;

        return $this;
    }

    /**
     * Get internalNote
     *
     * @return string
     */
    public function getInternalNote()
    {
        return $this->internalNote;
    }

    /**
     * Set toFiscalPrint
     *
     * @param boolean $toFiscalPrint
     *
     * @return Invoice
     */
    public function setToFiscalPrint($toFiscalPrint)
    {
        $this->toFiscalPrint = $toFiscalPrint;

        return $this;
    }

    /**
     * Get toFiscalPrint
     *
     * @return boolean
     */
    public function getToFiscalPrint()
    {
        return $this->toFiscalPrint;
    }

    /**
     * Set serviceRealizedAt
     *
     * @param \DateTime $serviceRealizedAt
     *
     * @return Invoice
     */
    public function setServiceRealizedAt($serviceRealizedAt)
    {
        $this->serviceRealizedAt = $serviceRealizedAt;

        return $this;
    }

    /**
     * Get serviceRealizedAt
     *
     * @return \DateTime
     */
    public function getServiceRealizedAt()
    {
        return $this->serviceRealizedAt;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Invoice
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Invoice
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Invoice
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set client
     *
     * @param Client $client
     *
     * @return Invoice
     */
    public function setClient(Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set invoiceType
     *
     * @param InvoiceType $invoiceType
     *
     * @return Invoice
     */
    public function setInvoiceType(InvoiceType $invoiceType = null)
    {
        $this->invoiceType = $invoiceType;

        return $this;
    }

    /**
     * Get invoiceType
     *
     * @return InvoiceType
     */
    public function getInvoiceType()
    {
        return $this->invoiceType;
    }

    /**
     * Set payment
     *
     * @param Payment $payment
     *
     * @return Invoice
     */
    public function setPayment(Payment $payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set editor
     *
     * @param User $editor
     *
     * @return Invoice
     */
    public function setEditor(User $editor = null)
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * Get editor
     *
     * @return User
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * Set invoiceStatus
     *
     * @param InvoiceStatus $invoiceStatus
     *
     * @return Invoice
     */
    public function setInvoiceStatus(InvoiceStatus $invoiceStatus = null)
    {
        $this->invoiceStatus = $invoiceStatus;

        return $this;
    }

    /**
     * Get invoiceStatus
     *
     * @return InvoiceStatus
     */
    public function getInvoiceStatus()
    {
        return $this->invoiceStatus;
    }

    /**
     * Set parent
     *
     * @param \Mnumi\Bundle\InvoiceBundle\Entity\Invoice $parent
     *
     * @return Invoice
     */
    public function setParent(\Mnumi\Bundle\InvoiceBundle\Entity\Invoice $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \Mnumi\Bundle\InvoiceBundle\Entity\Invoice
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param int $currency
     *
     * @return Invoice
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
        
        return $this;
    }

    /**
     * @return integer
     */
    public function getCurrencyExchangeRate()
    {
        return $this->currencyExchangeRate;
    }

    /**
     * @param integer $currencyExchangeRate
     *
     * @return Invoice
     */
    public function setCurrencyExchangeRate($currencyExchangeRate)
    {
        $this->currencyExchangeRate = $currencyExchangeRate;
        
        return $this;
    }

    /**
     * @return InvoiceItem[]
     */
    public function getInvoiceItem()
    {
        return $this->invoiceItem;
    }

    /**
     * @param InvoiceItem $invoiceItem
     *
     * @return Invoice
     */
    public function addInvoiceItem(InvoiceItem $invoiceItem)
    {
        $this->invoiceItem[] = $invoiceItem;
        
        return $this;
    }
}
