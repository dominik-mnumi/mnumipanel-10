<?php

namespace Mnumi\Bundle\InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * InvoiceStatus
 *
 * @ORM\Table(name="invoice_status")
 * @ORM\Entity
 */

class InvoiceStatus {

    /**
     * Description
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="name", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $name;

    /**
     * Description
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=false)
     * @Serializer\Groups({"public"})
     */
    private $title;

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InvoiceStatus
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return InvoiceStatus
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
