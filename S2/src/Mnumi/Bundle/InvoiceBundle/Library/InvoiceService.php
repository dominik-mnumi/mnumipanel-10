<?php
/*
 * This file is part of the MnumiPanel package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\InvoiceBundle\Library;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Mnumi\Bundle\InvoiceBundle\Entity\Invoice;
use Mnumi\Bundle\InvoiceBundle\Entity\InvoiceItem;
use Mnumi\Bundle\InvoiceBundle\Entity\Repository\Invoice as InvoiceRepository;
use Mnumi\Bundle\OrderBundle\Entity\Order;

/**
 * Class InvoiceService
 * @package Mnumi\Bundle\InvoiceBundle\Library
 */
class InvoiceService
{

    /**
     * @var array
     */
    private $companyData;

    /**
     * @var string
     */
    private $invoiceNamingPattern;

    /**
     * @var string
     */
    private $invoiceItemFormat;

    /**
     * @var string
     */
    private $defaultDaysToPay;

    /**
     * @var ArrayCollection
     */
    private $invoiceStatus;

    /**
     * @var ArrayCollection
     */
    private $invoiceType;

    /**
     * @var ArrayCollection
     */
    private $unit;
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @param array $companyData
     * @param string $oldInvoice
     * @param EntityRepository $invoiceStatus
     * @param EntityRepository $invoiceType
     * @param EntityRepository $unit
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(
        array $companyData,
        $oldInvoice,
        EntityRepository $invoiceStatus,
        EntityRepository $invoiceType,
        EntityRepository $unit,
        EntityManager $em
    )
    {
        $this->companyData = $companyData;
        $this->invoiceNamingPattern = $oldInvoice['naming_pattern'];
        $this->invoiceItemFormat = $oldInvoice['item_format'];
        $this->defaultDaysToPay = $oldInvoice['default_days_to_pay'];
        $this->invoiceStatus = $invoiceStatus->matching(new Criteria());
        $this->invoiceType = $invoiceType->matching(new Criteria());
        $this->unit = $unit->matching(new Criteria());
        $this->em = $em;
    }


    /**
     * @param Order $order
     * @param InvoiceRepository $invoiceRepository
     * @throws \Doctrine\Common\Proxy\Exception\InvalidArgumentException
     * @return Invoice
     */
    public function initialize(Order $order, InvoiceRepository $invoiceRepository)
    {
        $search = array(
            '/{id}/',
            '/{order}/',
            '/{product}/',
            '/{quantity}/'
        );

        if($order->getOrderPackageStatusName()->getName() === "basket")
        {
            throw new InvalidArgumentException('Order have status "basket"!');
        }
        $invoice = new Invoice();

        /**
         * Set seller data
         */
        $invoice->setSellerAddress($this->companyData['seller_address']);
        $invoice->setSellerBankAccount($this->companyData['seller_bank_account']);
        $invoice->setSellerBankName($this->companyData['seller_bank_name']);
        $invoice->setSellerCity($this->companyData['seller_city']);
        $invoice->setSellerCountry($this->companyData['seller_country']);
        $invoice->setSellerName($this->companyData['seller_name']);
        $invoice->setSellerPostcode($this->companyData['seller_postcode']);
        $invoice->setSellerTax($this->companyData['seller_tax_id']);

        /**
         * Set client data
         */

        $invoice->setClient($order->getClient());
        $invoice->setClientName($order->getInvoiceName());
        $invoice->setClientAddress($order->getInvoiceStreet());
        $invoice->setClientCity($order->getInvoiceCity());
        $invoice->setClientCountry($order->getDeliveryCountry());
        $invoice->setClientTax($order->getInvoiceTaxId());
        $invoice->setClientPostcode($order->getInvoicePostcode());

        /**
         * Set payment data
         */
        $invoice->setPayment($order->getPayment());
        if ($order->getPaymentStatusName() === "paid") {
            $paid = $this->invoiceStatus->matching(
                Criteria::create()->where(Criteria::expr()->eq("name", 'paid'))
            )->first();

            $invoice->setInvoiceStatus($paid);
            $invoice->setPaidAt(new \DateTime());
        } else {
            $unpaid = $this->invoiceStatus->matching(
                Criteria::create()->where(Criteria::expr()->eq("name", 'unpaid'))
            )->first();

            $invoice->setInvoiceStatus($unpaid);
        }
        $invoiceType = $this->invoiceType->matching(
            Criteria::create()->where(Criteria::expr()->eq("name", 'invoice'))
        )->first();

        $invoice->setInvoiceType($invoiceType);

        $paymentDateAt = new \DateTime();
        $paymentDateAt->modify('+' . $this->defaultDaysToPay . ' days');
        $invoice->setPaymentDateAt($paymentDateAt);
        $invoice->setServiceRealizedAt(new \DateTime());
        $invoice->setEditor($order->getUser());
        $invoice->setSellAt($order->getUpdatedAt());

        $invoiceFormat = new InvoiceFormat($invoice, $this->invoiceNamingPattern);
        /**
         * Get invoice next number
         */
        $month = null;
        if (strstr($invoiceFormat->getPattern(), 'mm')) {
            $month = date('m');
        }
        $number = $invoiceRepository->getNextNumber(date('Y'), $month);

        $invoice->setNumber($number);
        $invoice->setName($invoiceFormat->getFormattedNumber());

        $totalPriceGross = 0;
        $totalPriceNet = 0;
        $totalPriceTax = 0;

        foreach ($order->getItems() as $item) {
            $replaceWith = array(
                $item->getId(),
                $item->getName(),
                $item->getProduct()->getName(),
                $item->getQuantity(),
            );

            $parsed = preg_replace($search, $replaceWith, $this->invoiceItemFormat);
            $invoiceItem = new InvoiceItem();

            $invoiceItem->setEditor($order->getUser());
            $invoiceItem->setOrderItem($item);
            $invoiceItem->setDescription($parsed['order']);
            $invoiceItem->setUnit($this->unit->get(0));
            $invoiceItem->setTaxValue($item->getTaxValue());
            $invoiceItem->setPriceNet($item->getPriceNet());
            $invoiceItem->setPriceDiscount($item->getDiscountValue());
            $invoiceItem->setQuantity(1);
            $invoiceItem->setInvoice($invoice);
            $totalPriceNet += $item->getPriceNet();
            $totalPriceTax += round($item->getPriceNet() * $item->getTaxValue() / 100, 2);
            $totalPriceGross += round(
                    $item->getPriceNet() * $item->getTaxValue() / 100,
                    2
                ) + $item->getPriceNet();

            $invoice->addInvoiceItem($invoiceItem);
        }

        if($order->getDeliveryNetPrice() > 0)
        {
            $carrier = $order->getCarrier();
            $invoiceItem = new InvoiceItem();

            $taxValue = $carrier->getTax();
            // @TODO: Implement this in Order::getTax()
            if ($order->getClient()->getWntUe()) {
                $taxValue = 0;
            }

            $invoiceItem->setEditor($order->getUser());
            $invoiceItem->setOrderItem(null);
            $invoiceItem->setDescription($carrier->getLabel());
            $invoiceItem->setUnit($this->unit->get(0));
            $invoiceItem->setPriceDiscount(0);
            $invoiceItem->setTaxValue($taxValue);
            $invoiceItem->setPriceNet($carrier->getPrice());
            $invoiceItem->setQuantity(1);
            $invoiceItem->setInvoice($invoice);

            $totalPriceNet += $carrier->getPrice();
            $totalPriceTax += round($carrier->getPrice() * $taxValue / 100, 2);
            $totalPriceGross += round(
                    $carrier->getPrice() * $taxValue / 100,
                    2
                ) + $carrier->getPrice();

            $invoice->addInvoiceItem($invoiceItem);
            $shopName = $item->getShopName();
        }

        $shopOption = $this->em->getRepository('MnumiShopBundle:ShopOption')->findOneBy(array('name' => 'currency_code', 'shopName' => $shopName));
        if($shopOption)
        {
            $currency = $this->em->getRepository('MnumiCurrencyBundle:Currency')->findOneBy(array('code' => $shopOption->getValue()));
            if($currency)
            {
                $invoice->setCurrency($currency->getId());
            }
        }

        $shopOptionLanguage = $this->em->getRepository('MnumiShopBundle:ShopOption')->findOneBy(array('name' => 'lang', 'shopName' => $shopName));
        if($shopOptionLanguage)
        {
            $invoice->setLanguage($shopOptionLanguage->getValue());
        }

        if($order->getPaymentStatusName()->getName() == "paid")
        {
            $paid = $this->em->getRepository('MnumiInvoiceBundle:InvoiceStatus')->find('paid');
            $invoice->setInvoiceStatus($paid);
        }

        $invoice->setPriceGross($totalPriceGross);
        $invoice->setPriceNet($totalPriceNet);
        $invoice->setPriceTax($totalPriceTax);

        return $invoice;
    }
}