<?php
/*
 * This file is part of the MnumiPanel package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\InvoiceBundle\Library;


use Mnumi\Bundle\InvoiceBundle\Entity\Invoice;
use Mnumi\Bundle\OrderBundle\Entity\Order;

class MailerInvoiceService {

    private $mailer;
    
    Private $sender;
    
    private $senderName;
    
    private $template;

    /**
     * @param $mailer
     * @param $template
     * @param string $oldNotification
     */
    public function __construct($mailer, $template, $oldNotification)
    {
        $this->mailer = $mailer;
        $this->sender = $oldNotification['sender'];
        $this->senderName = $oldNotification['sender_name'];
        $this->template = $template;
    }
    
    public function send(Invoice $invoice, Order $order)
    {
        $message = $this->mailer->createMessage()
            ->setSubject('Faktura: ' . $invoice->getName())
            ->setFrom(array($this->sender => $this->senderName))
            ->setTo($order->getUser()->getEmailAddress())
            ->setBody(
                $this->template->render(
                    'MnumiInvoiceBundle:Emails:invoice.html.twig',
                    array('invoice' => $invoice)
                ),
                'text/html'
            );
        $this->mailer->send($message);
    }
} 