<?php
/*
 * This file is part of the MnumiPanel package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\InvoiceBundle\Library\Query\Mysql;

use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * Class Year
 * @package Mnumi\Bundle\InvoiceBundle\Library\Query\Mysql
 */
class Year extends FunctionNode {
    public $date;
    
    public function getSql(SqlWalker $sqlWalker)
    {
        return "YEAR(" . $sqlWalker->walkArithmeticPrimary($this->date) . ")";
    }
    
    public function parse(Parser $parser)
    {
        $lexer = $parser->getLexer();
        $parser->match(LExer::T_IDENTIFIER);
        $parser->match(LExer::T_OPEN_PARENTHESIS);
        $this->date = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
        
    }
} 