<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\InvoiceBundle\Library\Exception;

/**
 * Class InvoiceNumberException
 * @package Mnumi\Bundle\InvoiceBundle\Library\Exception
 */
class InvoiceNumberException extends \Exception {

} 