<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\InvoiceBundle\Library;


use Mnumi\Bundle\InvoiceBundle\Entity\Invoice;
use Mnumi\Bundle\InvoiceBundle\Entity\Repository\Invoice as InvoiceRepository;
use Mnumi\Bundle\InvoiceBundle\Library\Exception\InvoiceNumberException;
use Mnumi\Bundle\OrderBundle\Text\Pattern\PatternReplace;

/**
 * Class InvoiceFormat
 * @package Mnumi\Bundle\InvoiceBundle\Library
 */
class InvoiceFormat {

    private $invoiceNamingPattern = array(
        'invoice' => 'INVOICE/mm/yy/{number}',
        'correction' => 'CORRECTION/mm/yy/{number}',
        'preliminary' => 'PRELIMINARY/mm/yy/{number}',
        'receipt' => 'RECEIPT/mm/yy/{number}'
    );

    /**
     * @var Invoice
     */
    private $invoice;

    /**
     * @param Invoice $invoice
     * @param $invoiceNamingPatern
     */
    public function __construct(Invoice $invoice, $invoiceNamingPatern = null)
    {
        $this->invoice = $invoice;
        if(is_array($invoiceNamingPatern))
        {
            $this->invoiceNamingPattern = $invoiceNamingPatern;
        }
    }
    
    /**
     * Get invoice number pattern
     *
     * @return string
     */
    public function getPattern()
    {
        $patternKeyArr = explode(' ', $this->invoice->getInvoiceType()->getName());
        return $this->invoiceNamingPattern[$patternKeyArr[0]];
    }

    /**
     * @return int
     */
    public function getNumber()
    {
        if($this->invoice->getNumber() === null)
        {
            throw new InvoiceNumberException("Invoice doesn't have number");
        }
        return $this->invoice->getNumber();
    }
    
    /**
     * Get formatted invoice number
     *
     * @return string Formatted number
     */
    public function getFormattedNumber()
    {
        $pattern = $this->getPattern();
        $search = array(
            '/mm/',
            '/yy/',
            '/y/',
            '/\{number(.*)\}/'
        );

        $sellDate = $this->invoice->getSellAt()->getTimestamp();

        $replaceWith = array(
            date('m', $sellDate),
            date('Y', $sellDate),
            date('y', $sellDate),
            $this->getConvertedNumber($pattern)
        );

        return preg_replace($search, $replaceWith, $pattern);
    }

    private function getConvertedNumber($pattern)
    {
        preg_match("/\\{(.*?)\\}/", $pattern, $number);
        $patternReplace = new PatternReplace($this);
        return $patternReplace->convert($number[0]);
    }
} 