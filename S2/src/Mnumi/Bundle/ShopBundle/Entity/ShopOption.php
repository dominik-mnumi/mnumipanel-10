<?php
/*
 * This file is part of the Contempt package.
 *
 * (c) Mnumi
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 
 * Date: 07.10.15
 * Time: 13:37
 * Filename: ShopOption.php
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
namespace Mnumi\Bundle\ShopBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Rest session entity class
 *
 * @ORM\Entity
 * @ORM\Table(name="shop_option")
 */
class ShopOption {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_name", type="string", nullable=false)
     */
    private $shopName;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ShopOption
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getShopName()
    {
        return $this->shopName;
    }

    /**
     * @param string $shopName
     * @return ShopOption
     */
    public function setShopName($shopName)
    {
        $this->shopName = $shopName;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ShopOption
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }
}