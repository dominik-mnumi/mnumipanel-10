<?php

namespace Mnumi\Bundle\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Client
 *
 * @ORM\Table(name="client", indexes={@ORM\Index(columns={"last_address_id"}), @ORM\Index(columns={"pricelist_id"})})
 * @ORM\Entity(repositoryClass="\Mnumi\Bundle\ClientBundle\Entity\Repository\Client")
 */
class Client
{
    /**
     * Unique autoincrement ID
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"public", "UsersList", "ClientsList", "ClientsList", "OrdersList", "StoreFront"})
     */
    private $id;

    /**
     * Client full name
     * @var string
     *
     * @ORM\Column(name="fullname", type="string", length=255, nullable=false)
     * @Serializer\Groups({"public", "UsersList", "ClientsList", "OrdersList", "StoreFront"})
     */
    private $fullname;

    /**
     * Client credit limit amount
     * @var float
     *
     * @ORM\Column(name="credit_limit_amount", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"public", "UsersList", "ClientsList"})
     */
    private $creditLimitAmount;

    /**
     * @var integer
     *
     * @ORM\Column(name="credit_limit_overdue", type="integer", nullable=true)
     * @Serializer\Exclude
     */
    private $creditLimitOverdue;

    /**
     * City name
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=150, nullable=false),
     * @Serializer\Groups({"public", "UsersList", "ClientsList", "StoreFront", "OrdersList"})
     */
    private $city;

    /**
     * Post code
     * @var string
     *
     * @ORM\Column(name="postcode", type="string", length=10, nullable=false)
     * @Serializer\Groups({"public", "UsersList", "ClientsList", "StoreFront", "OrdersList"})
     */
    private $postcode;

    /**
     * Street
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=150, nullable=false)
     * @Serializer\Groups({"public", "UsersList", "ClientsList", "StoreFront", "OrdersList"})
     */
    private $street;

    /**
     * Tax value (eg. 23, 8)
     * @var string
     *
     * @ORM\Column(name="tax_id", type="string", length=20, nullable=true)
     * @Serializer\Groups({"public", "UsersList", "ClientsList", "StoreFront", "OrdersList"})
     */
    private $taxId;

    /**
     * @var float
     *
     * @ORM\Column(name="balance", type="float", precision=18, scale=2, nullable=false)
     * @Serializer\Groups({"public", "UsersList", "ClientsList"})
     */
    private $balance = '0.00';

    /**
     * @var integer
     *
     * @ORM\Column(name="last_order_id", type="integer", nullable=true)
     * @Serializer\Exclude
     */
    private $lastOrderId;

    /**
     * @var float
     *
     * @ORM\Column(name="invoice_cost_balance", type="float", precision=18, scale=2, nullable=true)
     * @Serializer\Exclude
     */
    private $invoiceCostBalance = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="account", type="string", length=255, nullable=true)
     * @Serializer\Exclude
     */
    private $account;

    /**
     * Country - Default: GB (eg. PL, GB, DE)
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=150, nullable=false)
     * @Serializer\Groups({"public", "UsersList", "ClientsList"})
     */
    private $country = 'GB';

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Serializer\Exclude
     */
    private $deletedAt;

    /**
     * Client last address
     * @var \ClientAddress
     *
     * @ORM\ManyToOne(targetEntity="ClientAddress")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="last_address_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public", "UsersList", "ClientsList"})
     * @Serializer\Type("string")
     */
    private $lastAddress;

    /**
     * Pricelist ID - Must exist on MnumiCore
     * @var \Pricelist
     *
     * @ORM\Column(name="pricelist_id", type="integer", nullable=true)
     * @Serializer\Groups({"public", "UsersList", "ClientsList"})
     */
    private $pricelist;

    /**
     * Users names array (eg. array("user1", "user2"))
     *
     * @ORM\OneToMany(targetEntity="\Mnumi\Bundle\ClientBundle\Entity\ClientUser", mappedBy="client", cascade={"all"})
     * @Serializer\Groups({"public", "ClientsList"})
     * @Serializer\Type("array")
     */
    private $users;

    /**
     * EU VAT
     * @var bool
     *
     * @ORM\Column(name="wnt_ue", type="boolean", nullable=false)
     * @Serializer\Groups({"public", "UsersList", "ClientsList"})
     */
    private $wntUe = false;

    /**
     * Collection of client's addresses
     *
     * @ORM\OneToMany(targetEntity="ClientAddress", mappedBy="client")
     */
    private $clientAddresses;

    /**
     * @var \ClientNotice
     *
     * @ORM\OneToMany(targetEntity="ClientNotice", mappedBy="client"))
     */
    private $clientNotices;

    /**
     * @var int
     *
     * @ORM\Column(name="external_id", type="integer", nullable=true)
     * @Serializer\Groups({"public", "UsersList", "ClientsList"})
     */
    private $externalId;

    public function __toString()
    {
        return $this->getFullname();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fullname
     *
     * @param  string $fullname
     * @return Client
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;

        return $this;
    }

    /**
     * Get fullname
     *
     * @return string
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set creditLimitAmount
     *
     * @param  float  $creditLimitAmount
     * @return Client
     */
    public function setCreditLimitAmount($creditLimitAmount)
    {
        $this->creditLimitAmount = $creditLimitAmount;

        return $this;
    }

    /**
     * Get creditLimitAmount
     *
     * @return float
     */
    public function getCreditLimitAmount()
    {
        return $this->creditLimitAmount;
    }

    /**
     * Set creditLimitOverdue
     *
     * @param  integer $creditLimitOverdue
     * @return Client
     */
    public function setCreditLimitOverdue($creditLimitOverdue)
    {
        $this->creditLimitOverdue = $creditLimitOverdue;

        return $this;
    }

    /**
     * Get creditLimitOverdue
     *
     * @return integer
     */
    public function getCreditLimitOverdue()
    {
        return $this->creditLimitOverdue;
    }

    /**
     * Set city
     *
     * @param  string $city
     * @return Client
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set postcode
     *
     * @param  string $postcode
     * @return Client
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set street
     *
     * @param  string $street
     * @return Client
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set taxId
     *
     * @param  string $taxId
     * @return Client
     */
    public function setTaxId($taxId)
    {
        $this->taxId = $taxId;

        return $this;
    }

    /**
     * Get taxId
     *
     * @return string
     */
    public function getTaxId()
    {
        return $this->taxId;
    }

    /**
     * Set balance
     *
     * @param  float  $balance
     * @return Client
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set lastOrderId
     *
     * @param  integer $lastOrderId
     * @return Client
     */
    public function setLastOrderId($lastOrderId)
    {
        $this->lastOrderId = $lastOrderId;

        return $this;
    }

    /**
     * Get lastOrderId
     *
     * @return integer
     */
    public function getLastOrderId()
    {
        return $this->lastOrderId;
    }

    /**
     * Set invoiceCostBalance
     *
     * @param  float  $invoiceCostBalance
     * @return Client
     */
    public function setInvoiceCostBalance($invoiceCostBalance)
    {
        $this->invoiceCostBalance = $invoiceCostBalance;

        return $this;
    }

    /**
     * Get invoiceCostBalance
     *
     * @return float
     */
    public function getInvoiceCostBalance()
    {
        return $this->invoiceCostBalance;
    }

    /**
     * Set account
     *
     * @param  string $account
     * @return Client
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set country
     *
     * @param  string $country
     * @return Client
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return Client
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime $updatedAt
     * @return Client
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param  \DateTime $deletedAt
     * @return Client
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Set pricelist
     *
     * @param  integer $pricelist
     * @return Client
     */
    public function setPricelist($pricelist)
    {
        $this->pricelist = $pricelist;

        return $this;
    }

    /**
     * Get pricelist
     *
     * @return integer
     */
    public function getPricelist()
    {
        return $this->pricelist;
    }

    /**
     * Set lastAddress
     *
     * @param  \Mnumi\Bundle\ClientBundle\Entity\ClientAddress $lastAddress
     * @return Client
     */
    public function setLastAddress(\Mnumi\Bundle\ClientBundle\Entity\ClientAddress $lastAddress = null)
    {
        $this->lastAddress = $lastAddress;

        return $this;
    }

    /**
     * Get lastAddress
     *
     * @return \Mnumi\Bundle\ClientBundle\Entity\ClientAddress
     */
    public function getLastAddress()
    {
        return $this->lastAddress;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add users
     *
     * @param  \Mnumi\Bundle\RestServerBundle\Entity\User $users
     * @return Client
     */
    public function addUser(\Mnumi\Bundle\RestServerBundle\Entity\User $users)
    {
        $this->users[] = $users;

        return $this;
    }

    /**
     * @param $users
     * @return Client
     */
    public function setUsers($users)
    {
        $this->users = $users;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \Mnumi\Bundle\RestServerBundle\Entity\ClientUser $users
     */
    public function removeUser(\Mnumi\Bundle\RestServerBundle\Entity\User $users)
    {
        $this->users->removeElement($users);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return bool
     */
    public function getWntUe()
    {
        return $this->wntUe;
    }

    /**
     * @param bool $wntUe
     */
    public function setWntUe($wntUe)
    {
        $this->wntUe = $wntUe;
    }

    /**
     * Add client address
     *
     * @param \Mnumi\Bundle\ClientBundle\Entity\ClientAddress $clientAddress
     */
    public function addClientAddress(ClientAddress $clientAddress)
    {
        $this->clientAddresses[] = $clientAddress;
    }

    /**
     * Remove client address
     *
     * @param \Mnumi\Bundle\ClientBundle\Entity\ClientAddress $clientAddress
     */
    public function removeClientAddress(ClientAddress $clientAddress)
    {
        $this->clientAddresses->remove($clientAddress);
    }

    /**
     * Set client addresses
     *
     * @param type $clientAddresses
     */
    public function setClientAddresses($clientAddresses)
    {
        $this->clientAddresses = $clientAddresses;
    }

    /**
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClientAddresses()
    {
        return $this->clientAddresses;
    }

    /**
     * @return int
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param int $externalId
     * @return Client
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * Get client barcode
     *
     * @return string
     */
    public function getBarcodeNumber()
    {
        return "11" . str_pad($this->getId(), 8, '0', STR_PAD_LEFT);
    }

    /**
     * @return \Client
     */
    public function getClientNotices()
    {
        return $this->clientNotices;
    }

    /**
     * @param \Client $clientNotices
     * @return Client
     */
    public function setClientNotices($clientNotices)
    {
        $this->clientNotices = $clientNotices;
        return $this;
    }

}
