<?php

namespace Mnumi\Bundle\ClientBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ClientAddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
                    'fullname',
                    'text',
                    array(
                        'description' => 'Fullname',
                    )
                )
                ->add(
                    'city',
                    'text',
                    array(
                        'description' => 'City name',
                    ))
                ->add(
                    'postcode',
                    'text',
                    array(
                        'description' => 'Post code',
                    ))
                ->add(
                    'street',
                    'text',
                    array(
                        'description' => 'Street',
                    ))
                ->add(
                    'country',
                    'text',
                    array(
                        'description' => 'Country (eg. PL, GB, DE)',
                    ))
                ->add('client', 'entity', array(
                    'description' => 'Client',
                    'required'  => true,
                    'class' => 'Mnumi\Bundle\ClientBundle\Entity\Client'
                ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'clientAddress';
    }
}
