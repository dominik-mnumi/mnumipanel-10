<?php
namespace Mnumi\Bundle\ClientBundle\Command;

use Doctrine\Common\Collections\Criteria;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class ViesCommand extends ContainerAwareCommand
{
    private $viesUrl = 'http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl';

    protected $em;

    protected $api;

    protected $countries = array(
        'AT',
        'BE',
        'BG',
        'CY',
        'CZ',
        'DE',
        'DK',
        'EE',
        'EL',
        'ES',
        'FI',
        'FR',
        'HR',
        'HU',
        'IE',
        'IT',
        'LU',
        'LV',
        'LT',
        'MT',
        'NL',
        'PT',
        'RO',
        'SE',
        'SI',
        'SK',
        'UK'
    );

    protected function configure()
    {
        $this->setName('vies:check')
             ->setDescription('Checking client vies');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->em = $this->getContainer()->get('doctrine')->getManager();
        $this->disableNoWnt();

        $this->api = new \Soapclient($this->viesUrl);
        if ($this->api) {
            $criteria = Criteria::create()
                ->where(Criteria::expr()->in('country', $this->countries));
            $clients = $this->getContainer()->get('doctrine')->getManager()->getRepository('MnumiClientBundle:Client')->matching($criteria);
            foreach ($clients as $client) {
                if ($this->checkVies($client->getTaxId(), $client->getCountry()) === true) {
                    $client->setWntUe(true);
                    $output->writeln($client->getCountry() . $client->getTaxId() . ' - VALID');
                } else {
                    $client->setWntUe(false);
                    $output->writeln($client->getCountry() . $client->getTaxId() . ' - INVALID');
                }
                $this->em->persist($client);
            }
            $this->em->flush();
        } else {
            $output->writeln('Can`t connect to VIES API');
        }
    }

    private function disableNoWnt()
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->notIn('country', $this->countries));
        $clients = $this->getContainer()->get('doctrine')->getManager()->getRepository('MnumiClientBundle:Client')->matching($criteria);
        foreach ($clients as $client) {
            $client->setWntUe(false);
        }
        $this->em->flush();
    }

    private function checkVies($vatNumber, $country)
    {
        $vatNumber = str_replace(array(' ', '.', '-', ',', ', '), '', trim($vatNumber));
        $vatNumber = preg_replace('/[^0-9]/', '', $vatNumber);
        if(is_numeric($vatNumber) &&
                $vatNumber > 0 &&
                in_array(strtoupper($country), $this->countries)
        )
        {
            $params = array(
                'countryCode' =>$country,
                'vatNumber' => $vatNumber
            );
            $r = $this->api->checkVat($params);
            if ($r->valid === true) {
                return true;
            }
        }

        return false;
    }
}
