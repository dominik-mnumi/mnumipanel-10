<?php
namespace Mnumi\Bundle\CalculationBundle\Library\Exception;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CalculationMissingPrintException class
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class CalculationMissingPrintException extends CalculationMissingProductFieldException
{
    /**
     * @param $settingId ID of Print object which is missing
     */
    public function __construct($settingId) {
        parent::__construct('Missing print object', $settingId);
    }
}
