<?php
namespace Mnumi\Bundle\CalculationBundle\Library\Exception;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CalculationMissingSideException class
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class CalculationMissingSideException extends CalculationMissingProductFieldException
{
    /**
     * @param $settingId ID of Side object which is missing
     */
    public function __construct($settingId) {
        parent::__construct('Missing sides object', $settingId);
    }
}
