<?php
namespace Mnumi\Bundle\CalculationBundle\Library\Exception;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CalculationInvalidArgumentException class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationInvalidArgumentException extends CalculationToolException
{

}
