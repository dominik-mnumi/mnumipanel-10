<?php
namespace Mnumi\Bundle\CalculationBundle\Library\Exception;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CalculationMissingProductFieldException class
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class CalculationMissingProductFieldException extends CalculationToolException
{
    protected $settingId = 0;

    /**
     * @param $message Descriptive message of missing object
     * @param $settingId ID of object which is missing
     */
    public function __construct($message, $settingId) {
        $this->message = $message;
        $this->settingId = $settingId;
    }

    /**
     * Gets ID of missing setting
     *
     * @return int
     */
    public function getSettingId() {
        return $this->settingId;
    }
}
