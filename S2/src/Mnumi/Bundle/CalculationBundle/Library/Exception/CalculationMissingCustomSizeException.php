<?php
namespace Mnumi\Bundle\CalculationBundle\Library\Exception;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CalculationMissingCustomSizeException class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationMissingCustomSizeException extends CalculationMissingProductFieldException
{
    public function __construct() {
        parent::__construct('Missing custom sizes: width or height', 0);
    }
}
