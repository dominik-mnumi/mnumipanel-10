<?php
namespace Mnumi\Bundle\CalculationBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * calculationTool interface
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>, Marek Balicki <marek.balicki@mnumi.com>
 */
 interface CalculationToolInterface
 {
     /**
      * Constructor
      *
      * @param CalculationProductData $calculationProductData
      */
     public function __construct($calculationProductData);

    /**
 	 * @param array $inputArray Include:
 	 *                          - COUNT
 	 *                          - QUANTITY
 	 *                          - MATERIAL
 	 *                          - SIZE
 	 *                          - PRINT
 	 * @param int $pricelistId Pricelist ID number (for prevent to many REST requests)
 	 *
 	 * Some values can be missed, that is why prepareFieldArray() is necessary
 	 */
     public function initialize(array $inputArray, $pricelistId);

    /**
 	 * Fetch report as array
 	 */
     public function fetchReport();
 }
