<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 
 * Date: 25.08.15
 * Time: 10:32
 * Filename: CalculateAvailability.php 
 */
namespace Mnumi\Bundle\CalculationBundle\Library;

/**
 * Class CalculateAvailability
 * @package Mnumi\Bundle\CalculationBundle\Library
 */
class CalculateAvailability {
    /**
     * @var array
     */
    private $fields;


    /**
     * Checking availability fpr current quantity
     * 
     * @param float $quantity
     * @return array
     */
    public function checkAvailability($quantity)
    {
        $unavailable = array();

        foreach($this->fields as $fieldItem)
        {
            $fieldAvailable = $fieldItem["fieldAvailable"];
            $requiredAmount = $fieldItem["requiredAmount"];
            if($this->isAvailable($quantity, $requiredAmount, $fieldAvailable) === false)
            {
                $unavailable[] = array(
                    "id" => $fieldItem["id"],
                    "label" => $fieldItem["label"],
                    "availability" => $fieldAvailable,
                    "required" => $this->getRequireAvailability($quantity, $requiredAmount)
                );
            }
        }

        return $unavailable;
    }

    /**
     * Add FieldItem
     *
     * @param int $id
     * @param float $fieldAvailable
     * @param string $label
     * @param float $requiredAmount
     * @return void
     */
    public function addFieldItem($id, $fieldAvailable, $label, $requiredAmount)
    {
        $this->fields[] = array(
            "id" => $id,
            "label" => $label,
            "fieldAvailable" => $fieldAvailable,
            "requiredAmount" => $requiredAmount
        );
    }


    /**
     * @param float $quantity
     * @param float $requiredAmount
     * @internal param float $required
     * @return float
     */
    private function getRequireAvailability($quantity, $requiredAmount)
    {
        return ($requiredAmount * $quantity);
    }

    /**
     * @param float $quantity
     * @param float $requiredAmount
     * @param float $fieldAvailable
     * @return bool
     */
    private function isAvailable($quantity, $requiredAmount, $fieldAvailable)
    {
        if($fieldAvailable === null)
        {
            return true;
        }

        return $fieldAvailable >= $this->getRequireAvailability($quantity, $requiredAmount);
    }
}