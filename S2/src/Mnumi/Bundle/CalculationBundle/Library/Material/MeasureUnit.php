<?php

namespace Mnumi\Bundle\CalculationBundle\Library\Material;

/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Available Measure Units
 */
class MeasureUnit {
    const MILIMETER     = 1000;
    const CENTIMETER    = 100;
    const METER         = 1;

    private static $metricFactor = array(
        'mm' => MeasureUnit::MILIMETER,
        'cm' => MeasureUnit::CENTIMETER,
        'm' => MeasureUnit::METER,
    );

    /**
     * Gets how many times given measure unit is contained in one metre
     *
     * @param string $name
     * @return int
     */
    public static function getUnit($name) {
        if(!isset(self::$metricFactor[$name])) {
            throw new \Exception(
                'Invalid measure unit: "'.$name.'". Allowed measure units: '.implode(', ', array_keys(self::$metricFactor))
            );
        }
        return self::$metricFactor[$name];
    }

    /**
     * Gets name of measure unit which is equal to one metre for specified factor
     *
     * @param int $factor
     * @return string String containing metric name (e.g.: mm, cm, m)
     */
    public static function getName($factor) {
        $key = array_search($factor, self::$metricFactor, true);
        if ($key === false) {
            throw new \Exception('Measure unit was not found for specified factor: ' . $factor);
        }
        return $key;
    }
}