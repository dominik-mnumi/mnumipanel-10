<?php
namespace Mnumi\Bundle\CalculationBundle\Library;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FixPrice class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class FixPrice
{
    /** @var int|float  */
    private $quantity;

    /** @var float */
    private $price;

    /** @var float */
    private $cost;

    /** @var float */
    private $pricePage;

    public function __construct($quantity, $price, $cost, $pricePage)
    {
        $this->quantity = $quantity;
        $this->price = $price;
        $this->cost = $cost;
        $this->pricePage = $pricePage;
    }

    /**
     * @return float|int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }
    /**
     * @return float
     */
    public function getPricePage()
    {
        return $this->pricePage;
    }
}
