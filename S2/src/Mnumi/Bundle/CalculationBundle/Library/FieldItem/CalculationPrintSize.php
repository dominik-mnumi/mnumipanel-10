<?php
namespace Mnumi\Bundle\CalculationBundle\Library\FieldItem;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\CalculationBundle\Library\Size;

/**
 * CalculationPrintSize class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationPrintSize extends Size
{
    private $printerName;

    /**
     * Constructor
     *
     * @param int    $width
     * @param int    $height
     * @param string $printerName
     */
    public function __construct($width, $height, $printerName)
    {
        parent::__construct($width, $height);
        $this->printerName = $printerName;
    }

    /**
     * @return int
     */
    public function getPrinterName()
    {
        return $this->printerName;
    }
}
