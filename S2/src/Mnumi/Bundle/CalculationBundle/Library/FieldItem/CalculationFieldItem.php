<?php
namespace Mnumi\Bundle\CalculationBundle\Library\FieldItem;
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CalculationFieldItem class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationFieldItem
{
    /** @var int */
    protected $id;

    /** @var string */
    protected $name;

    /** @var string */
    protected $label;

    /** @var float */
    protected $cost;

    /** @var bool */
    protected $hidden;

    /** @var CalculationFieldItemPriceCollection[] */
    protected $data = array();

    /** @var string */
    protected $measureUnit;

    /** @var CalculationFieldItemSize */
    protected $itemSize;

    /** @var CalculationPrintSize */
    protected $printSize;

    /**
     * Constructor
     *
     * @param int    $id
     * @param string $name
     * @param string $label
     * @param string $cost
     * @param string $measureUnit
     */
    public function __construct($id, $name, $label, $cost, $measureUnit)
    {
        $this->id = $id;
        $this->name = $name;
        $this->label = $label;
        $this->cost = $cost;
        $this->measureUnit = $measureUnit;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @deprecated since PAN-1410
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Add price list price collection to field item
     */
    public function add(CalculationFieldItemPriceCollection $collection)
    {
        if (is_numeric($collection->getPriceList())) {
            $primaryKey = $collection->getPriceList();
        } else {
            $primaryKey = $collection->getPriceList()->getId();
        }

        $this->data[$primaryKey] = $collection;
    }

    /**
     * Get Calculation field item price collection by price list id
     *
     * @param  int                                      $primaryKey Price List ID
     * @return bool|CalculationFieldItemPriceCollection
     */
    public function get($primaryKey)
    {
        if (!array_key_exists($primaryKey, $this->data)) {
            return false;
        }

        return $this->data[$primaryKey];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getMeasureUnit()
    {
        return $this->measureUnit;
    }

    /**
     * @return \CalculationFieldItemSize
     */
    public function getItemSize()
    {
        return $this->itemSize;
    }

    /**
     * @param \CalculationFieldItemSize $itemSize
     */
    public function setItemSize($itemSize)
    {
        $this->itemSize = $itemSize;
    }

    /**
     * @return \CalculationPrintSize
     */
    public function getPrintSize()
    {
        return $this->printSize;
    }

    /**
     * @param \CalculationPrintSize $printSize
     */
    public function setPrintSize($printSize)
    {
        $this->printSize = $printSize;
    }
}
