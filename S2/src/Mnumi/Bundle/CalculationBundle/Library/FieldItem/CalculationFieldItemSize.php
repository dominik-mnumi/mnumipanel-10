<?php
namespace Mnumi\Bundle\CalculationBundle\Library\FieldItem;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

use Mnumi\Bundle\CalculationBundle\Library\Size;

/**
 * CalculationFieldItemSize class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationFieldItemSize extends Size
{
}
