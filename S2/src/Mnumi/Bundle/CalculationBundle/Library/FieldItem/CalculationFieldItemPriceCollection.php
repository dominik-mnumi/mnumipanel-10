<?php
namespace Mnumi\Bundle\CalculationBundle\Library\FieldItem;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FieldItemPrice class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CalculationFieldItemPriceCollection implements \Countable, \IteratorAggregate, \Serializable
{
    /** @var int */
    private $priceListId;

    /** @var float */
    private $priceMinimal;

    /** @var null|float */
    private $priceMaximal;

    /**
     * @var CalculationFieldItemPrice[] $data an array containing the collection
     */
    protected $data = array();

    /**
     * @var float
     */
    private $plusPrice;

    /**
     * @var float
     */
    private $plusPriceDouble;

    /**
     * @var float
     */
    private $cost;

    /**
     * @param $priceList
     * @param $priceMinimal
     * @param float|null $priceMaximal
     * @param float|null $plusPrice
     * @param float|null $plusPriceDouble
     */
    public function __construct($priceList, $priceMinimal, $priceMaximal = null,
        $plusPrice = null, $plusPriceDouble = null, $cost = null)
    {
        $this->priceList = $priceList;
        $this->priceMinimal = $priceMinimal;
        $this->priceMaximal = $priceMaximal;
        $this->plusPrice = $plusPrice;
        $this->plusPriceDouble = $plusPriceDouble;
        $this->cost = $cost;
    }

    public function addPrice(CalculationFieldItemPrice $record)
    {
        $type = $record->getType();
        $quantity = $record->getQuantity();

        $this->data[$type][$quantity] = $record;
    }

    /**
     * Get element by Quantity
     *
     * @param  string                         $type
     * @param  float                          $quantity
     * @return bool|CalculationFieldItemPrice
     */
    public function getPrice($type, $quantity)
    {
        if (count($this->data) == 0 || !isset($this->data[$type])) {
            return false;
        }

        $current = false;

        $data = $this->data[$type];

        foreach ($data as $row) {
            if ($row->getQuantity() > $quantity) {
                continue;
            }
            if ($current && $row->getQuantity() < $current->getQuantity()) {
                continue;
            }
            $current = $row;
        }

        return $current;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Retrieve an external iterator
     * @link http://php.net/manual/en/iteratoraggregate.getiterator.php
     * @return Traversable An instance of an object implementing <b>Iterator</b> or
     *                     <b>Traversable</b>
     */
    public function getIterator()
    {
        $data = $this->data;

        return new ArrayIterator($data);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        $vars = get_object_vars($this);

        return serialize($vars);
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param  string $serialized <p>
     *                            The string representation of the object.
     *                            </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        $array = unserialize($serialized);

        foreach ($array as $name => $values) {
            $this->$name = $values;
        }
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Count elements of an object
     * @link http://php.net/manual/en/countable.count.php
     * @return int The custom count as an integer.
     *             </p>
     *             <p>
     *             The return value is cast to an integer.
     */
    public function count()
    {
        return count($this->data);
    }

    /**
     * @return int
     */
    public function getPriceList()
    {
        return $this->priceList;
    }

    /**
     * @return float
     */
    public function getPriceMinimal()
    {
        return (float) $this->priceMinimal;
    }

    /**
     * @return float
     */
    public function getPriceMaximal()
    {
        return (float) $this->priceMaximal;
    }

    /**
     * @return float
     */
    public function getPlusPrice()
    {
        return $this->plusPrice;
    }

    /**
     * @return float
     */
    public function getPlusPriceDouble()
    {
        return $this->plusPriceDouble;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

}
