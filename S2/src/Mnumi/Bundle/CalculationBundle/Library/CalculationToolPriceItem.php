<?php

namespace Mnumi\Bundle\CalculationBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class CalculationToolPriceItem
{
    private $name;
    private $fieldLabel;
    private $label;
    private $price;
    private $cost;
    private $type;

    public function getName()
    {
        return $this->name;
    }

    public function getFieldLabel()
    {
        return $this->fieldLabel;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setFieldLabel($fieldLabel)
    {
        $this->fieldLabel = $fieldLabel;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function setPrice($price)
    {
        $this->price = $price;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

}
