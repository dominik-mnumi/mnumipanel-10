<?php
namespace Mnumi\Bundle\CalculationBundle\Library;

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * calculationFactor help to calculate price for order
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>, Marek Balicki <marek.balicki@mnumi.com>
 */

class CalculationFactor
{
    public static $metricSize = array(
        'mm' => 1,
        'cm' => 10,
        'm'  => 1000,
    );

    /**
	 * Returns how much place takes one element depending on printer size quantity.
	 *
	 * @param integer $printerWidth
	 * @param integer $printerHeight
	 * @param integer $width
	 * @param integer $height
	 * @param integer $nrOfElements
	 * @return float
	 */
    public static function count($printerWidth, $printerHeight, $width, $height, $nrOfElements = 1, $sizeMetric = 'mm')
    {
        // printer height is defined in mm
        $metricFactor = self::$metricSize[$sizeMetric];

        $width = $width * $metricFactor;
        $height = $height * $metricFactor;

        //if printer is bigger than one item
        if(($printerWidth >= $width && $printerHeight >= $height)
                || ($printerWidth >= $height && $printerHeight >= $width))
        {
            $itemNb = 0;
            $itemNb1 = 0;
            $itemNb2 = 0;

            //checks normally
            $cols = (integer) floor($printerWidth / $width);
            $rows = (integer) floor($printerHeight / $height);

            $itemNb1 = $rows * $cols;

            //checks reverse
            if ($printerWidth >= $height && $printerHeight >= $width) {
                $cols = (integer) floor($printerWidth / $height);
                $rows = (integer) floor($printerHeight / $width);

                $itemNb2 = $rows * $cols;
            }

            ($itemNb1 >= $itemNb2) ? $itemNb = $itemNb1 : $itemNb = $itemNb2;
        }
        //if height or width is bigger than printer size
        else {
            $cols = ceil($width / $printerWidth);
            $rows = ceil($height / $printerHeight);
            $itemNb1 = $rows * $cols;

            $cols = ceil($width / $printerHeight);
            $rows = ceil($height / $printerWidth);
            $itemNb2 = $rows * $cols;

            ($itemNb1 < $itemNb2) ? $itemNb = $itemNb1 : $itemNb = $itemNb2;

            $itemNb = ($itemNb > 0) ? (1 / $itemNb) : 1;
        }

        //calculates how many printer pages must be used
        $factor = ceil($nrOfElements / $itemNb);

        return array('factor' => $factor,
                'itemNbPerPage' => $itemNb);
    }

}
