<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ShippingInformation class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Bundle\ShippingBundle\Library;

class ShippingInformation
{
    /** @var string $name */
    private $name;

    /** @var string $street */
    private $street;

    /** @var string $city */
    private $city;

    /** @var string $postcode */
    private $postcode;

    /** @var string $countryCode */
    private $countryCode;

    /** @var string $clientName */
    private $clientName;

    /** @var string $email */
    private $email;

    /** @var string $phoneNumber */
    private $phoneNumber;

    /** @var string $packageId */
    private $packageId;

    /** @var string $description */
    private $description;

    /** @var string $transportNumber */

    private $transportNumber = null;

    /** @var array $additionalShippingInformation **/
    private $additionalInformation;

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get street
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set street
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * Get city
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set city
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Get post code
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set post code
     * @param string $postcode
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;
    }

    /**
     * Get country code
     * @return string
     */
    public function getCountryCode()
    {
        return $this->countryCode;
    }

    /**
     * Set country code
     * @param string $countryCode
     */
    public function setCountryCode($countryCode)
    {
        $this->countryCode = $countryCode;
    }

    /**
     * Get client's name
     * @return string
     */
    public function getClientName()
    {
        return $this->clientName;
    }

    /**
     * Set client's name
     * @param string $clientName
     */
    public function setClientName($clientName)
    {
        $this->clientName = $clientName;
    }

    /**
     * Get email
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Get phone number
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set phone number
     * @param string $phoneNumber
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
    }

    /*
     * Get description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get package id
     * @return int
     */
    public function getPackageId()
    {
        return $this->packageId;
    }

    /**
     * Set package id
     * @param int $packageId
     */
    public function setPackageId($packageId)
    {
        $this->packageId = $packageId;
    }

    /**
     * Set description
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Set additional information
     *
     * @param array $additionalInformation
     */
    public function setAdditionalInformation($additionalInformation)
    {
        $this->additionalInformation = $additionalInformation;
    }

    /**
     * Return additional information
     *
     * @return array
     */
    public function getAdditionalInformation()
    {
        return $this->additionalInformation;
    }

    /**
     * Return additional information by key
     *
     * @param  string $key
     * @return mixed
     */
    public function getAdditionalInformationByKey($key)
    {
        return array_key_exists($key, $this->additionalInformation) ?
                $this->additionalInformation[$key] :
                false;
    }

    /**
     * Convert Polish chars (ąćźż) to universal chars (aczz)
     * @param  string $value
     * @return string
     */
    public function convertCharacters($value)
    {
        $value = iconv('utf-8', 'ascii//translit', $value);
        $value = trim($value);

        return $value;
    }

    /**
     * Cutting text every 35 characters while maintaining whole words.
     * And return 3 array items
     *
     * @param  string $value
     * @param int $maxLineLength
     * @return array
     */
    public function convertAddressLine($value, $maxLineLength = 35)
    {
        $arrayWords = explode(' ', $value);
        // Max size of each line
        // Auxiliar counters, foreach will use them
        $currentLength = 0;
        $index = 0;

        foreach ($arrayWords as $word) {
            // +1 because the word will receive back the space in the end that it loses in explode()
            $wordLength = strlen($word) + 1;
            if ( ( $currentLength + $wordLength ) <= $maxLineLength ) {
                $arrayOutput[$index][] = $word;
                $currentLength += $wordLength;
            } else {
                $index += 1;
                $currentLength = $wordLength;
                $arrayOutput[$index][] = $word;
            }
        }

        foreach ($arrayOutput as &$line) {
            $line = implode(" ", $line);
        }

        return array_reverse(array_slice($arrayOutput, 0, 3));
    }

    /**
     * @return string
     */
    public function getTransportNumber()
    {
        return $this->transportNumber;
    }

    /**
     * @param string $transportNumber
     * @return ShippingInformation
     */
    public function setTransportNumber($transportNumber)
    {
        $this->transportNumber = $transportNumber;
        return $this;
    }
}
