<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * DummyShipping class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Bundle\ShippingBundle\Library\Dummy;

use Mnumi\Bundle\ShippingBundle\Library\ShippingInterface;
use Mnumi\Bundle\ShippingBundle\Library\ShippingInformation;

class DummyShipping implements ShippingInterface
{
    /**
     * Creates instance of shipping class
     *
     * @param array  $configuration
     * @param string $printlabelDir Directory path for saving print label files
     * @param string $reportDir     Directory path for saving report files
     */
    public function __construct(array $configuration, $printlabelDir, $reportDir)
    {
        $this->configuration = $configuration;
        $this->printlabelDir = $printlabelDir;
        $this->reportDir = $reportDir;
    }

    public function getImageFormat()
    {
        return 'jpg';
    }

    public function generatePrintLabel(ShippingInformation $shippingInformation)
    {
        $this->shippingInformation = $shippingInformation;
        return 'dummy';
    }

    public function getTransportNumber()
    {
        $addInfo = $this->shippingInformation->getAdditionalInformation();
        return 'dummy-transport-number-' . $addInfo['whatever'];
    }
}
