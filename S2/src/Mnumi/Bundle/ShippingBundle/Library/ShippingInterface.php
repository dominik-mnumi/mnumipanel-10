<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ShippingInterface interface
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Bundle\ShippingBundle\Library;

interface ShippingInterface
{
    /**
     * Creates instance of shipping class
     *
     * @param array  $configuration
     * @param string $printlabelDir Directory path for saving print label files
     * @param string $reportDir     Directory path for saving report files
     */
    public function __construct(array $configuration, $printlabelDir, $reportDir);

    /**
     * Generate print label
     *
     * @param  \Mnumi\Bundle\ShippingBundle\Library\ShippingInformation $shippingInformation
     * @return string                                                   Generated filename
     */
    public function generatePrintLabel(ShippingInformation $shippingInformation);

    /**
     * Get transport number
     *
     * @return string
     */
    public function getTransportNumber();

    /**
     * Returns image format for print label file
     * @return string
     */
    public function getImageFormat();
}
