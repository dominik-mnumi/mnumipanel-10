<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ShippingInterface interface
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

namespace Mnumi\Bundle\ShippingBundle\Library\UPS;

use Mnumi\Bundle\ShippingBundle\Library\ShippingInterface;
use Mnumi\Bundle\ShippingBundle\Library\ShippingInformation;

class UPSShipping implements ShippingInterface
{
    protected
        $wsdl,
        $outputFilenameReport,
        $access,
        $userid,
        $passwd,
        $endpointurl,
        $printLabelPath,
        $configuration,
        $shippingInformation,
        $printlabelDir,
        $reportDir,
        $filename,
        $imageFormat = 'bmp',
        $transportNumber;

    /**
     * Creates instance of shipping class
     *
     * @param array  $configuration
     * @param string $printlabelDir Directory path for saving print label files
     * @param string $reportDir     Directory path for saving report files
     */
    public function __construct(array $configuration, $printlabelDir, $reportDir)
    {
        $this->configuration = $configuration;
        $this->printlabelDir = $printlabelDir;
        $this->reportDir = $reportDir;
    }

    /**
     * Generate print label
     *
     * @param  \Mnumi\Bundle\ShippingBundle\Library\ShippingInformation $shippingInformation
     * @throws \Exception
     * @return string                                                   Generated filename
     */
    public function generatePrintLabel(ShippingInformation $shippingInformation)
    {
        $this->shippingInformation = $shippingInformation;

        $this->access = $this->configuration['access'];
        $this->userid = $this->configuration['user_id'];
        $this->passwd = $this->configuration['password'];
        $this->endpointurl = $this->configuration['url'];

        // prepares wsdl path
        $this->wsdl = __DIR__ . '/wsdl/Ship.wsdl';

        // gets image filename
        $filename = $this->generateFilename();

        $this->outputFilenameReport = $this->reportDir. '/' . $filename.'.xml';

        // prepares output filename
        if (!file_exists($this->printlabelDir)) {
            mkdir($this->printlabelDir, 0777, true);
        }

        $this->printLabelPath = $this->printlabelDir . '/' . $filename . '.' . $this->getImageFormat();

        $fw = fopen($this->outputFilenameReport, 'a');
        fwrite($fw, "Begin connection at: ".date('r')."\n");

        try {
            // use soap 1.1 client
            $mode = array(
                'soap_version' => 'SOAP_1_1',
                'trace'        => 1);

            // initializes soap client
            $client = new \SoapClient($this->wsdl, $mode);

            fwrite($fw, "Set location: ".$this->endpointurl."\n");

            // sets endpoint url
            $client->__setLocation($this->endpointurl);

            // creates soap header
            $usernameToken['Username']                   = $this->configuration['user_id'];
            $usernameToken['Password']                   = $this->configuration['password'];
            $serviceAccessLicense['AccessLicenseNumber'] = $this->configuration['access'];
            $upss['UsernameToken']                       = $usernameToken;
            $upss['ServiceAccessToken']                  = $serviceAccessLicense;

            fwrite($fw, "Authentication: ".implode(', ', $usernameToken).', using: ' . $this->configuration['access'] . "\n");

            $header = new \SoapHeader(
                    'http://www.ups.com/XMLSchema/XOLTWS/UPSS/v1.0',
                    'UPSSecurity',
                    $upss);

            $client->__setSoapHeaders($header);

            fwrite($fw, "Finished authentication.\n");

            fwrite($fw, "Begin process shippment.\n");

            $processShipment = $this->processShipment();;

            fwrite($fw, json_encode($processShipment) . "\n");

            /* **** 1. Process Shipment **** */

            // gets response
            $resp = $client->__soapCall(
                    'ProcessShipConfirm',
                    array($processShipment));

            // gets status
            //echo "Response confirm status: ".$resp->Response->ResponseStatus->Description."\n";

            $responseXML = $client->__getLastResponse();

            preg_match(
                    '/<ship:ShipmentDigest>(.*)<\/ship:ShipmentDigest>/i',
                    $responseXML,
                    $matchArr);

            $shipmentDigest = $matchArr[1];

            //echo $shipmentDigest;

            // saves soap request and response of ProcessShipConfirm to file
            fwrite($fw, "Request for ProcessShipConfirm: \n".$client->__getLastRequest()."\n");
            fwrite($fw, "Response for ProcessShipConfirm: \n".$client->__getLastResponse()."\n");

            fwrite($fw, "Begin accept shipment.\n");

            /* **** 2. Accept Shipment **** */
            // gets response
            $resp = $client->__soapCall(
                    'ProcessShipAccept',
                    array($this->processShipAccept($shipmentDigest)));

            // gets status
            //echo "Response accept status: ".$resp->Response->ResponseStatus->Description."\n";

            // prepares files
            $cacheFilename = $this->reportDir . '/' . $filename;
            file_put_contents($cacheFilename,
                     base64_decode($resp->ShipmentResults
                             ->PackageResults
                             ->ShippingLabel
                             ->GraphicImage));

            $imObj = new \Imagick($cacheFilename);
            $imObj->setImageFormat($this->getImageFormat());
            $imObj->writeimage($this->printLabelPath);

            fclose($fw);

            // saves soap request and response of ProcessShipAccept to file
            $fw = fopen($this->outputFilenameReport, 'a');
            fwrite($fw, "Request for ProcessShipAccept: \n".$client->__getLastRequest() . "\n");
            fwrite($fw, "Response for ProcessShipAccept: \n".$client->__getLastResponse() . "\n");
            fclose($fw);

            $this->transportNumber = $resp->ShipmentResults->ShipmentIdentificationNumber;
            return $this->filename;

        } catch (\Exception $e) {
            throw new \Exception('Error: ' . $e->getMessage() . '. Please check log file: ' . $this->outputFilenameReport);
        }
    }

    /**
     * Prepares shipment request array.
     *
     * @return array
     */
    protected function processShipment()
    {
        // create soap request
        $request['Request'] = array(
            'RequestOption' => 'validate');

        $shipper = array(
            'Name' => $this->shippingInformation->convertCharacters($this->configuration['shipper_name']),
            'AttentionName' => $this->shippingInformation->convertCharacters($this->configuration['shipper_attention_name']),
            'TaxIdentificationNumber' => $this->configuration['shipper_tax_identification_number'],
            'ShipperNumber' => $this->configuration['shipper_shipper_number'],
            'Address' => array(
                'AddressLine' => $this->convertAddressLine($this->configuration['shipper_address_address_line']),
                'City' => $this->shippingInformation->convertCharacters($this->configuration['shipper_address_city']),
                'PostalCode' => $this->configuration['shipper_address_postal_code'],
                'CountryCode' => $this->configuration['shipper_address_country_code']
            )
        );

        $shipment['Shipper'] = $shipper;

        $shipfrom = array(
            'Name' => $this->shippingInformation->convertCharacters($this->configuration['shipper_name']),
            'AttentionName' => $this->shippingInformation->convertCharacters($this->configuration['shipper_attention_name']),
            'Address' => array(
                'AddressLine' => $this->convertAddressLine($this->configuration['shipper_address_address_line']),
                'City' => $this->shippingInformation->convertCharacters($this->configuration['shipper_address_city']),
                'PostalCode' => $this->configuration['shipper_address_postal_code'],
                'CountryCode' => $this->configuration['shipper_address_country_code']
            ));

        $shipment['ShipFrom'] = $shipfrom;

        $shipto = array(
            'Name' => $this->shippingInformation->convertCharacters($this->shippingInformation->getClientName()),
            'AttentionName' => $this->shippingInformation->convertCharacters($this->shippingInformation->getName()),
            'Address' => array(
                'AddressLine' => $this->convertAddressLine($this->shippingInformation->getStreet()),
                'City' => $this->shippingInformation->convertCharacters($this->shippingInformation->getCity()),
                'PostalCode' => $this->shippingInformation->getPostcode(),
                'CountryCode' => $this->shippingInformation->getCountryCode(),
            ));

        if ($this->shippingInformation->getPhoneNumber()) {
            $shipto['Address']['Phone'] = array(
                'Number' => $this->shippingInformation->getPhoneNumber()
            );
        }

        $shipment['ShipTo'] = $shipto;

        $paymentinformation = array(
            'ShipmentCharge' => array(
                'Type' => '01', // 02 does not work
                'BillShipper' => array(
                    'AccountNumber' => $this->configuration['shipper_shipper_number']
                )
            )
        );
        $shipment['PaymentInformation'] = $paymentinformation;

        $shipment['Service'] = array(
            'Code' => $this->configuration['service_code']);

        $package = array(
            'Description' => $this->shippingInformation->getDescription(),
            'Packaging' => array(
                'Code' => $this->configuration['package_packaging_code']
            ),
            'PackageWeight' => array(
                'UnitOfMeasurement' => array(
                    'Code' => $this->configuration['package_package_weight_unit_of_measurement_code']
                ),
                'Weight' => $this->configuration['package_package_weight_weight']),
                'Dimenstions' => array(
                'UnitOfMeasurement' => array(
                    'Code' => $this->configuration['package_dimensions_unit_of_measurement_code']
                ),
                'Length' => $this->configuration['package_dimensions_length'],
                'Width' => $this->configuration['package_dimensions_width'],
                'Height' => $this->configuration['package_dimensions_height']
            )
        );

        $shipment['Package'] = $package;

        $labelspecification = array(
            'LabelImageFormat' => array(
                'Code' => 'GIF',
                'Description' => 'GIF'
            ),
            'HTTPUserAgent' => 'Mozilla/4.5'
        );

        $shipment['LabelSpecification'] = $labelspecification;
        $request['Shipment'] = $shipment;

        return $request;
    }

    /**
     * Cutting text every 35 characters while maintaining whole words.
     * And return 3 array items
     *
     * @param  string $value
     * @param int $maxLineLength
     * @return array
     */
    protected function convertAddressLine($value, $maxLineLength = 35)
    {
        $value = $this->shippingInformation->convertCharacters($value);
        return $this->shippingInformation->convertAddressLine($value, $maxLineLength);
    }
    

    /**
     * Prepares request for ProcessShipAccept.
     *
     * @param  string $shipmentDigest
     * @return array
     */
    protected function processShipAccept($shipmentDigest)
    {
        $request = $this->processShipment();
        $request['ShipmentDigest'] = $shipmentDigest;

        // creates soap request
        return $request;
    }

    /**
     * Returns image filename.
     *
     * @throws \Exception
     * @return string
     */
    protected function generateFilename()
    {
        if (!$this->shippingInformation->getPackageId()) {
            throw new \Exception('Package\'s id needed for generating filename.');
        }

        $this->filename = $this->shippingInformation->getPackageId() .
                '_UPS_' . date('Y_m_d_G_i_s') .
                '_' . md5(rand(0, 99999));

        return $this->filename;
    }

    /**
     * Returns image format for print label file
     * @return string
     */
    public function getImageFormat()
    {
        return $this->imageFormat;
    }

    /**
     * @return string
     */
    public function getTransportNumber()
    {
        return $this->transportNumber;
    }
}
