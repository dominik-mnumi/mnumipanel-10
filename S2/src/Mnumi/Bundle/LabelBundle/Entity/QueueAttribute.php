<?php

namespace Mnumi\Bundle\LabelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * FieldItem
 *
 * @ORM\Table(name="printlabel_queue_attribute")
 * @ORM\Entity
 */
class QueueAttribute {
    
    public static $TYPE_IMAGE = array('bmp', 'jpg', 'png');
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10, nullable=false)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="width", type="integer", nullable=true)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $width;

    /**
     * @var integer
     *
     * @ORM\Column(name="height", type="integer", nullable=true)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $height;

    /**
     * @var integer
     *
     * @ORM\Column(name="pos_x", type="integer", nullable=false)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $posX;

    /**
     * @var integer
     *
     * @ORM\Column(name="pos_y", type="integer", nullable=false)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $posY;

    /**
     * @var \Mnumi\Bundle\LabelBundle\Entity\Queue
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\LabelBundle\Entity\Queue", inversedBy="attributes", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="printlabel_queue_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"PdfQueue"})
     */
    private $printLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="text_type", type="string", length=255, nullable=false)
     */
    private $textType;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     * @return Queue
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     * @return Queue
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosX()
    {
        return $this->posX;
    }

    /**
     * @param int $posX
     * @return Queue
     */
    public function setPosX($posX)
    {
        $this->posX = $posX;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosY()
    {
        return $this->posY;
    }

    /**
     * @param int $posY
     * @return Queue
     */
    public function setPosY($posY)
    {
        $this->posY = $posY;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Queue
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Queue
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return Queue
     */
    public function getPrintLabel()
    {
        return $this->printLabel;
    }

    /**
     * @param Queue $printLabel
     * @return Queue
     */
    public function setPrintLabel($printLabel)
    {
        $this->printLabel = $printLabel;

        return $this;
    }

    /**
     * @return string
     */
    public function getTextType()
    {
        return $this->textType;
    }

    /**
     * @param string $textType
     * @return Queue
     */
    public function setTextType($textType)
    {
        $this->textType = $textType;

        return $this;
    }
}
