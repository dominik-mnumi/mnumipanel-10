<?php

namespace Mnumi\Bundle\LabelBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * FieldItem
 *
 * @ORM\Table(name="printlabel_queue")
 * @ORM\Entity
 */
class Queue {
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"PdfQueue"})
     */
    private $id;

    /**
     * @var string
     *
     * @Serializer\SerializedName("printer")
     * @ORM\Column(name="printer_name", type="string", length=45, nullable=true)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $printerName;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var integer
     *
     * @Serializer\SerializedName("paperWidth")
     * @ORM\Column(name="paper_width", type="integer", nullable=true)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $paperWidth;

    /**
     * @var integer
     *
     * @Serializer\SerializedName("paperHeight")
     * @ORM\Column(name="paper_height", type="integer", nullable=true)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $paperHeight;

    /**
     * @var string
     *
     * @ORM\Column(name="orientation", type="string", length=100, nullable=true)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $orientation;

    /**
     * @var string
     *
     * @ORM\Column(name="related_class", type="string", length=150, nullable=true)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $relatedClass;

    /**
     * @var string
     *
     * @ORM\Column(name="related_id", type="integer", length=11, nullable=true)
     * @Serializer\Groups({"PdfQueue"})
     */
    private $relatedId;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="\Mnumi\Bundle\LabelBundle\Entity\QueueAttribute", mappedBy="printLabel", fetch="EAGER", cascade={"persist"})
     * @ORM\JoinColumn(name="printlabel_queue_id", referencedColumnName="id")
     *
     */
    private $attributes = null;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getOrientation()
    {
        return $this->orientation;
    }

    /**
     * @param string $orientation
     * @return Queue
     */
    public function setOrientation($orientation)
    {
        $this->orientation = $orientation;

        return $this;
    }

    /**
     * @return int
     */
    public function getPaperHeight()
    {
        return $this->paperHeight;
    }

    /**
     * @param int $paperHeight
     * @return Queue
     */
    public function setPaperHeight($paperHeight)
    {
        $this->paperHeight = $paperHeight;

        return $this;
    }

    /**
     * @return int
     */
    public function getPaperWidth()
    {
        return $this->paperWidth;
    }

    /**
     * @param int $paperWidth
     * @return Queue
     */
    public function setPaperWidth($paperWidth)
    {
        $this->paperWidth = $paperWidth;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrinterName()
    {
        return $this->printerName;
    }

    /**
     * @param string $printerName
     * @return Queue
     */
    public function setPrinterName($printerName)
    {
        $this->printerName = $printerName;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Queue
     */
    public function setStatus($status)
    {
        $this->status = $status;
        
        return $this;
    }

    /**
     * @param null|string|array $type
     * @return ArrayCollection
     */
    public function getAttributes($type = null)
    {
        if($type) {
            $items = new ArrayCollection();
            /** @var QueueAttribute $item */
            foreach ($this->attributes as $item) {
                if(is_array($type))
                {
                    if(in_array($item->getType(), $type))
                    {
                        $items->add($item);
                    }
                } else if(is_string($type))
                {
                    if($item->getType() == "pdf")
                    {
                        $items->add($item);
                    }
                }
            }
        } else {
            $items = $this->attributes;
        }
        return $items;
    }

    /**
     * @param QueueAttribute $attribute
     * @return Queue
     */
    public function addAttributes($attribute)
    {
        $this->attributes->add($attribute);
        
        return $this;
    }

    /**
     * @param QueueAttribute $attribute
     * @return Queue
     */
    public function removeAttributes($attribute)
    {
        $this->attributes->removeElement($attribute);

        return $this;
    }

    /**
     * Check label queue has PDF file
     *
     * @return bool
     */
    public function hasPdf()
    {
        return !$this->getAttributes("pdf")->isEmpty();
    }

    /**
     * Check label queue has image file
     *
     * @return bool
     */
    public function hasImage()
    {
        return !$this->getAttributes(QueueAttribute::$TYPE_IMAGE)->isEmpty();
    }

    /**
     * @return bool|QueueAttribute
     */
    public function getImage()
    {
        $attribute = $this->getAttributes(QueueAttribute::$TYPE_IMAGE);
        if(!$attribute) {
            return false;
        }
        return $attribute->first();
    }

    /**
     * Clear all attributes for queue
     */
    public function cleanAttributes()
    {
        $this->attributes = new ArrayCollection();
    }

    /**
     * Get related class
     *
     * @return string
     */
    public function getRelatedClass()
    {
        return $this->relatedClass;
    }

    /**
     * Set related class
     *
     * @param string $relatedClass
     * @return \Mnumi\Bundle\LabelBundle\Entity\Queue
     */
    public function setRelatedClass($relatedClass)
    {
        $this->relatedClass = $relatedClass;

        return $this;
    }

    /**
     * Get related id
     *
     * @return integer
     */
    public function getRelatedId()
    {
        return $this->relatedId;
    }

    /**
     * Set related id
     *
     * @param integer $relatedId
     * @return \Mnumi\Bundle\LabelBundle\Entity\Queue
     */
    public function setRelatedId($relatedId)
    {
        $this->relatedId = $relatedId;

        return $this;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("data")
     * @Serializer\Groups({"PdfQueue"})
     * 
     * @return ArrayCollection
     */
    public function getPdfAttributes()
    {
        return $this->getAttributes('pdf');
    }
}
