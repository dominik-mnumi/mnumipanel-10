<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.

 * Date: 27.10.15
 * Time: 10:10
 * Filename: LabelController.php
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
namespace Mnumi\Bundle\LabelBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\LazyCriteriaCollection;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use Mnumi\Bundle\LabelBundle\Entity\Queue;
use Mnumi\Bundle\LabelBundle\Entity\QueueAttribute;
use Mnumi\Bundle\LabelBundle\Library\PdfExt;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotations\NamePrefix("api_label_")
 */
class LabelController extends FOSRestController
{
    /**
     * Change status for label queue item
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = false,
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      500 = "Returned when error",
     *  }
     * )
     *
     * @Annotations\View(
     *  serializerGroups="PdfQueue"
     * )
     * @Annotations\Post("/label/queue")
     * @Method({"POST"})
     *
     * @param Request $request the request object
     *
     * @return Queue|null
     */
    public function postLabelQueueAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $queue = $em->getRepository('MnumiLabelBundle:Queue')->find($request->request->get('id'));
        /**
         * Change status if queue exist
         */
        if ($queue instanceof Queue) {
            $queue->setStatus($request->request->get('state'));
            $em->persist($queue);
            $em->flush($queue);
        }
        /**
         * Create view based on group
         */
        return $queue;
    }

    /**
     * Get Label printer queue
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = false,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @Annotations\View(
     *  serializerGroups="PdfQueue"
     * )
     *
     * @Method({"GET"})
     *
     * @param Request $request the request object
     * 
     * @return \FOS\RestBundle\View\View
     */
    public function getLabelQueueAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $printerParam = $request->get('printer');
        if ($printerParam == "any" || empty($printerParam)) {
            $printerParam = null;
        }
        $criteria = Criteria::create();
        $criteria->andWhere(Criteria::expr()->eq('status', 'pending'));
        if($printerParam !== null) {
            $criteria->andWhere(Criteria::expr()->in('printerName', explode(",", $printerParam)));
        }
        /** @var LazyCriteriaCollection $queue */
        $queue = $em->getRepository('MnumiLabelBundle:Queue')->matching($criteria);

        /** @var Queue $item */
        foreach ($queue as $item) {
            if ($item->hasPdf() === false) {
                /**
                 * Generate pdf
                 */
                $item = $this->get('label.pdf')->generate($item);
                $em->merge($item);
                $em->flush($item);
            }
        }

        /**
         * Create view based on group
         */
        return new ArrayCollection($queue->getValues());
    }
}
