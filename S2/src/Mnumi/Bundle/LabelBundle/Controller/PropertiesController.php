<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 
 * Date: 27.10.15
 * Time: 10:50
 * Filename: Properties.php
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
namespace Mnumi\Bundle\LabelBundle\Controller;

use Mnumi\Bundle\RestServerBundle\Entity\WebapiKey;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Exception\InvalidConfigurationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\NoResultException;

/**
 * Class Properties
 * @package Mnumi\Bundle\LabelBundle\Controller
 */
class PropertiesController extends Controller {

    public function getPropertiesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $restServer = $request->getSchemeAndHttpHost().'/app.php/api';

        try {
            $connectionKey = $em->getRepository('MnumiRestServerBundle:WebapiKey')->findFirstUsableKey();
        }
        catch(NoResultException $e) {
            $connectionKey = null;
        }
        if($connectionKey == null)
        {
            $connectionKey = new WebapiKey();
            $connectionKey->setHost($request->getSchemeAndHttpHost());
            $connectionKey->setName(hash('adler32', uniqid()));
            $connectionKey->setType('labelprinter');
            $connectionKey->setValidDate(new \DateTime('2099-12-30'));
            $connectionKey->setDescription('Label printer key');
            $em->persist($connectionKey);
            $em->flush($connectionKey);
        }
        $content = sprintf("[server]\nrest_server=%s\nconnection_key=%s\n", $restServer, $connectionKey->getName());
        $response = new Response('text/plain', 200, array('Content-Disposition' => "attachment; filename=properties.ini"));
        $response->setContent($content);
        
        return $response;
    }
    
} 