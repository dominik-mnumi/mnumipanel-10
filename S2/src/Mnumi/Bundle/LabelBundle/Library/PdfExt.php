<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.

 * Date: 27.10.15
 * Time: 10:10
 * Filename: PdfExt.php
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
namespace Mnumi\Bundle\LabelBundle\Library;

use fpdf\FPDF;

/**
 * Class PdfExt
 * @package Mnumi\Bundle\LabelBundle\Library
 * @see http://www.fpdf.org/~~V/phorum/read.php?f=1&i=47465&t=47388&v=t
 */
class PdfExt extends FPDF {
    private  $angle=0;

    /**
     * @param int $angle
     * @param int $x
     * @param int $y
     * @return void
     */
    function Rotate($angle,$x=-1,$y=-1)
    {
        if($x==-1)
            $x=$this->x;
        if($y==-1)
            $y=$this->y;
        if($this->angle!=0)
            $this->_out('Q');
        $this->angle=$angle;
        if($angle!=0)
        {
            $angle*=M_PI/180;
            $c=cos($angle);
            $s=sin($angle);
            $cx=$x*$this->k;
            $cy=($this->h-$y)*$this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
        }
    }

    /**
     * @return void
     */
    function _endpage()
    {
        if($this->angle!=0)
        {
            $this->angle=0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

    /**
     * @param int $w
     * @param int $h
     * @param string $txt
     * @param int $border
     * @param string $align
     * @param bool $fill
     * @param int $x
     * @param int $y
     * @param int $angle
     * @return void
     */
    function RotatedMultiCell($w, $h, $txt, $border=0, $align='J', $fill=false, $x, $y, $angle)
    {
        //Text rotated around its origin
        $this->Rotate($angle,$x,$y);
        $this->MultiCell($w, $h, $txt, $border=0, $align='J', $fill);
        $this->Rotate(0);
    }
}