<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 
 * Date: 27.10.15
 * Time: 10:10
 * Filename: LabelPdfService.php
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
namespace Mnumi\Bundle\LabelBundle\Service;

use Mnumi\Bundle\LabelBundle\Entity\Queue;
use Mnumi\Bundle\LabelBundle\Entity\QueueAttribute;
use Mnumi\Bundle\LabelBundle\Library\PdfExt;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class LabelPdfService
 * @package Mnumi\Bundle\LabelBundle\Service
 */
class LabelPdfService
{
    /**
     * @var string
     */
    private $dataDir;
    /**
     * @var Request
     */
    private $request;

    /**
     * @param string $rootDir
     * @param Request $request
     */
    public function __construct($rootDir, Request $request)
    {
        $this->dataDir = realpath($rootDir.'/../../data');
        $this->request = $request;
    }

    /**
     * @param Queue $queue
     * @return Queue
     */
    public function generate(Queue $queue)
    {
        $height = $queue->getPaperHeight() / 10;
        $width = $queue->getPaperWidth() / 10;
        $pdf = new PdfExt('P', 'mm', array($width, $height));
        $pdf->SetMargins(5, 5, 5, 5);
        $pdf->SetAutoPageBreak(false);

        $pdf->AddPage();
        $pdf->SetY($width);

        if ($queue->hasImage() === true) {
            $pdf->SetMargins(0, 0, 0, 0);
            $outputImage = tempnam(sys_get_temp_dir(), "labelPrinter") . '.jpg';
            $inputImage = tempnam(sys_get_temp_dir(), "labelPrinter") . '.bmp';

            file_put_contents($inputImage, file_get_contents($queue->getImage()->getValue()));
            $command = sprintf('convert %s %s', $inputImage, $outputImage);
            exec($command);

            $pdf->Image($outputImage, 0, 0, $width);

        } else {
            /** @var \Mnumi\Bundle\LabelBundle\Entity\QueueAttribute $line */
            foreach ($queue->getAttributes() as $line) {
                $text = iconv('UTF-8', 'iso-8859-2//TRANSLIT//IGNORE', $line->getValue());
                if ($line->getTextType() == 'deliveryName') {
                    $pdf->SetFont('Arial', 'B', 12);
                    $pdf->RotatedMultiCell($width - 10, 3.5, $text, '', '', '', 3, $width, 90);
                    $pdf->Ln(2);
                } else {
                    if ($line->getTextType() == 'Package') {
                        $pdf->SetFont('Arial', 'I', 10);
                        $pdf->RotatedMultiCell(
                            $width - 10,
                            $height - $pdf->GetX() * 3,
                            $text,
                            '',
                            '',
                            '',
                            3,
                            $width,
                            90
                        );
                    } else {
                        $pdf->SetFont('Arial', '', 12);
                        $pdf->RotatedMultiCell($width - 10, 4, $text, '', '', '', 3, $width, 90);
                    }
                }
            }

        }

        $filePath = '/printLabel/' . 'Label' . date('Y_m_d_G_i_s') . '_' . md5(rand(0, 99999)) . '.pdf';
        $pdfFile = $this->dataDir . $filePath;
        $pdf->Output($pdfFile);

        $queueAttribute = new QueueAttribute();
        $queueAttribute->setPrintLabel($queue);
        $queueAttribute->setType('pdf');

        $url = $this->request->getSchemeAndHttpHost().'/files'.$filePath;
        $queueAttribute->setValue($url);
        $queueAttribute->setPosX(0);
        $queueAttribute->setPosY(0);
        $queueAttribute->setWidth($queue->getPaperWidth());
        $queueAttribute->setHeight($queue->getPaperHeight());

        $queue->cleanAttributes();
        $queue->addAttributes($queueAttribute);

        return $queue;
    }
}
