<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\PaymentBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Mnumi\Bundle\CarrierBundle\Entity\Carrier;
use Mnumi\Bundle\OrderBundle\Entity\Order;

/**
 * Payment class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
class Payment extends EntityRepository
{
    /**
     * Get payments for given Carrier
     *
     * @param Carrier|integer $carrier
     * @return array
     */
    public function getPaymentsForCarrier($carrier)
    {
        return $this->createQueryBuilder('p')
                ->leftJoin('p.carrierPayments', 'cp')
                ->leftJoin('cp.carrier', 'c')
                ->where('cp.carrier = :carrier')
                ->setParameter('carrier', $carrier)
                ->orWhere('p.applyOnAllCarriers = 1')
                ->andWhere('p.active = 1')
                ->getQuery()
                ->getResult();
    }

    /**
     * Get available payments for order
     *
     * @param \Mnumi\Bundle\PaymentBundle\Entity\Repository\Order $order
     * @param boolean $forOffice
     *
     * @return Array
     */
    public function getAvailablePaymentsForOrder(Order $order, $forOffice = false)
    {
        $client = $order->getClient();

        if(empty($client)) {

            throw new \Exception('Order\'s client is not set');
        }

        $totalAmountGross = $order->calculateTotalAmountGross();

        $requireCreditLimit =
            ($client->getInvoiceCostBalance() == 0)
            &&
            ($client->getCreditLimitAmount() != 0)
            &&
            ($client->getBalance() + $client->getCreditLimitAmount() - $totalAmountGross >= 0)
        ;

        $priceList = $client->getPricelist();
        $qb = $this->createQueryBuilder('p')
                ->leftJoin('p.pricelists', 'pp')
                ->andWhere('pp.id = :priceList OR p.applyOnAllPricelist = 1')
                ->setParameter('priceList', $priceList)
                ->andWhere('p.active = 1')
                ->andWhere('p.requiredCreditLimit = 0 OR p.requiredCreditLimit = :requiredCreditLimit')
                ->setParameter('requiredCreditLimit', $requireCreditLimit);

        $carrier = $order->getCarrier();
        if($carrier) {
            $qb
                ->leftJoin('p.carrierPayments', 'cp')
                ->leftJoin('cp.carrier', 'c')
                ->andWhere('cp.carrier = :carrier')
                ->setParameter('carrier', $carrier);
        }

        if($forOffice) {
            $qb->andWhere('p.forOffice = 1');
        }

        return $qb
                ->getQuery()
                ->getResult();
    }
}
