<?php

namespace Mnumi\Bundle\PaymentBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PaymentType extends AbstractType implements FormTypeInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
                ->add('name', 'text', array('description' => 'Payment name'))
                ->add('label', 'text', array('description' => 'Payment label'))
        ;
    }
    /**
     * @return string
     */
    public function getName()
    {
        return 'mnumi_bundle_paymentbundle_payment';
    }

}
