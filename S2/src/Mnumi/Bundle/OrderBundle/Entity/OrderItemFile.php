<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Mnumi\Bundle\OrderBundle\Exception\FileMetadataBadNumberOfCharacteristicsException;

/**
 * OrderItemFile (Old File)
 *
 * @ORM\Table(name="file", indexes={@ORM\Index(name="fk_file_order1_idx", columns={"order_id"})})
 * @ORM\Entity(repositoryClass="\Mnumi\Bundle\OrderBundle\Entity\Repository\OrderItemFile")
 */
class OrderItemFile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="notice", type="text", nullable=true)
     */
    private $notice;

    /**
     * @var string
     *
     * @ORM\Column(name="metadata", type="text", nullable=true)
     */
    private $metadata;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="text", nullable=false, options={"default": "image"})
     */
    private $type = 'image';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     * @Serializer\Exclude
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="update")
     * @Serializer\Exclude
     */
    private $updatedAt;

    /**
     * @var OrderItem
     *
     * @ORM\ManyToOne(targetEntity="OrderItem", inversedBy="files")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude
     */
    private $orderItem;

    /**
     * @var OrderItem
     *
     * @ORM\ManyToOne(targetEntity="OrderItemFile", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     * @Serializer\Exclude
     */
    private $parent;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param  string $filename
     * @return File
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set notice
     *
     * @param  string $notice
     * @return File
     */
    public function setNotice($notice)
    {
        $this->notice = $notice;

        return $this;
    }

    /**
     * Get notice
     *
     * @return string
     */
    public function getNotice()
    {
        return $this->notice;
    }

    /**
     * Set metadata
     *
     * @param  string $metadata
     * @return File
     */
    protected function setMetadata($metadata)
    {
        $this->metadata = $metadata;

        return $this;
    }

    /**
     * Get metadata
     *
     * @return string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return File
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime $updatedAt
     * @return File
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set type
     *
     * @param  string $type
     * @return File
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set order
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem
     * @return File
     */
    public function setOrderItem(\Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem = null)
    {
        $this->orderItem = $orderItem;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Mnumi\Bundle\OrderBundle\Entity\OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * Set parent
     *
     * @param \Mnumi\Bundle\OrderBundle\Entity\OrderItemFile $file
     */
    public function setParent(OrderItemFile $file)
    {
        $this->parent = $file;
    }

    /**
     * Return parent
     *
     * @return \Mnumi\Bundle\OrderBundle\Entity\OrderItemFile
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Checks if metadata attribute exists.
     *
     * @param  string  $name name of the metadata attribute to check for
     * @return boolean
     */
    public function hasMetadataAttribute($name)
    {
        try {
            $this->getMetadataAttribute($name);

            return true;
        } catch (\Exception $e) {
            // Do nothing, return false
        }

        return false;
    }

    /**
     * Gets current attribute value.
     *
     * @param  string $name The name of the metadata attribute to change
     * @return string
     */
    public function getMetadataAttribute($name)
    {
        $metadata = json_decode($this->getMetadata(), true);

        if (!$metadata || !array_key_exists($name, $metadata)) {
            return false;
        }

        return $metadata[$name];
    }

    /**
     * Replaces or adds new attribute to metadata (in yaml format).
     *
     * @param string $name  The name of the metadata attribute to change
     * @param string $value
     */
    public function setMetadataAttribute($name, $value)
    {
        $metadata = json_decode($this->getMetadata(), true);

        if (!$metadata) {
            $metadata = array();
        }

        $metadata[$name] = $value;

        $this->setMetadata(
            json_encode($metadata)
        );
    }

    /**
     * Deletes current attribute.
     *
     * @param string $name The name of the metadata attribute to change
     */
    public function deleteMetadataAttribute($name)
    {
        $metadata = json_decode($this->getMetadata(), true);

        if (is_array($metadata) && !array_key_exists($name, $metadata)) {
            unset($metadata[$name]);

            $this->setMetadata(
                json_encode($metadata)
            );
        }
    }

    /**
     * Sets OrderItemFile metadata based on a given File.
     * Metadata are stored in yml format.
     *
     * @param \Symfony\Component\HttpFoundation\File\File $file
     *
     * @throws ProcessFailedException                          when identifying file metadata couldn't be executed successfully
     * @throws FileMetadataBadNumberOfCharacteristicsException when returned number of metadatas is different than expected
     */
    public function setMetadataFromFile(\Symfony\Component\HttpFoundation\File\File $file)
    {
        $filepath = $file->getRealPath();

        $process = new Process(sprintf('identify -format "%s" %s[0]', '%m:%[colorspace]:%w:%h', $filepath));
        $process->mustRun();

        $metadatas = explode(':', $process->getOutput());

        if (count($metadatas) < 4) {
            throw new FileMetadataBadNumberOfCharacteristicsException(sprintf("There must be 4 metadatas. Returned: %d", count($metadatas)));
        }

        $this->setMetadataAttribute('format', $metadatas[0]);
        $this->setMetadataAttribute('colorspace', $metadatas[1]);
        $this->setMetadataAttribute('widthPx', $metadatas[2]);
        $this->setMetadataAttribute('heightPx', $metadatas[3]);
        $this->setMetadataAttribute('mimeType', \FileManager::getMimeType($filepath));

        $nrOfPages = 1;
        if ($metadatas[0] == 'PDF') {
            $nrOfPages = trim(exec('pdfinfo '.$filepath.' | grep Pages | cut -d ":" -f 2'));
        }

        $this->setMetadataAttribute('nrOfPages', $nrOfPages);
    }

    /**
     * Returns file path
     *
     * @return string
     */
    public function getFilePath($dataDir)
    {
        return $dataDir . '/' . $this->getOrderItem()->getFilePath();
    }

    /**
     * Returns full file path
     *
     * @return string
     */
    public function getFullFilePath($dataDir)
    {
        return $this->getFilePath($dataDir) . $this->getFilename();
    }

    /**
     * Return cache file path
     *
     * @param string $dataDir
     * @param integer $subPreview
     * @return string
     */
    public function getCacheFullPath($dataDir, $subPreview = 0)
    {
        $fullPath = realpath($this->getFullFilePath($dataDir));
        $md5_filepath = md5($fullPath);
        return $this->getFilePath($dataDir) . $md5_filepath.'_'.$subPreview.'.jpg';
    }
}
