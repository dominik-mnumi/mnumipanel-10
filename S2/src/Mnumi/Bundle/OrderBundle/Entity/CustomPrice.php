<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * CustomPrice
 *
 * @ORM\Table(name="custom_price", indexes={@ORM\Index(columns={"order_id"})})
 * @ORM\Entity
 */
class CustomPrice
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Exclude
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=18, scale=2, nullable=false)
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="cost", type="float", precision=18, scale=2, nullable=false)
     */
    private $cost;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="OrderItem", inversedBy="customSize")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     *
     */
    private $orderItem;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", nullable=false)
     */
    private $label;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return CustomPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set cost
     *
     * @param float $cost
     *
     * @return CustomPrice
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return CustomPrice
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return CustomPrice
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set orderItem
     *
     * @param \Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem
     *
     * @return CustomPrice
     */
    public function setOrderItem(\Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem = null)
    {
        $this->orderItem = $orderItem;

        return $this;
    }

    /**
     * Get orderItem
     *
     * @return \Mnumi\Bundle\OrderBundle\Entity\OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }
}
