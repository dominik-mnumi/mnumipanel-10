<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Mnumi\Bundle\OrderBundle\Validator\Constraints\OrderClientAddress;
use Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus;
use Mnumi\Bundle\OrderBundle\Entity\Repository\Order as OrderRepository;

/**
 * Order (Old Package)
 *
 * @ORM\Table(name="order_packages", indexes={@ORM\Index(columns={"payment_id"}), @ORM\Index(columns={"client_address_id"}), @ORM\Index(columns={"payment_status_name"}), @ORM\Index(columns={"client_id"}), @ORM\Index(columns={"order_package_status_name"}), @ORM\Index(columns={"carrier_id"}), @ORM\Index(columns={"rest_session_id"}), @ORM\Index(columns={"report_despatch_id"}), @ORM\Index(columns={"coupon_name"}), @ORM\Index(columns={"user_id"})})
 * @ORM\Entity(repositoryClass="\Mnumi\Bundle\OrderBundle\Entity\Repository\Order")
 */
class Order
{
    private $orderBaseAmount = false;

    /**
     * Unique autoincrement ID
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"public", "OrdersList", "StoreFront", "Simple"})
     */
    private $id;

    /**
     * Name of delivery (eg. London Office)
     * @var string
     *
     * @ORM\Column(name="delivery_name", type="string", length=255, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $deliveryName;

    /**
     * Country for delivery
     * @var string
     *
     * @ORM\Column(name="delivery_country", type="string", length=150, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $deliveryCountry;

    /**
     * Street for delivery
     * @var string
     *
     * @ORM\Column(name="delivery_street", type="string", length=180, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $deliveryStreet;

    /**
     * City for delivery
     * @var string
     *
     * @ORM\Column(name="delivery_city", type="string", length=80, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $deliveryCity;

    /**
     * Post code for delivery
     * @var string
     *
     * @ORM\Column(name="delivery_postcode", type="string", length=10, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $deliveryPostcode;

    /**
     * Order description
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $description;

    /**
     * Name of invoice (eg. Mnumi sp. z o.o.)
     * @var string
     *
     * @Assert\NotBlank(message="Invoice's name is required", groups={"StoreFrontInvoice"})
     *
     * @ORM\Column(name="invoice_name", type="string", length=255, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $invoiceName;

    /**
     * Streef for invoice
     * @var string
     *
     * @Assert\NotBlank(message="Street is required", groups={"StoreFrontInvoice"})s
     *
     * @ORM\Column(name="invoice_street", type="string", length=180, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $invoiceStreet;

    /**
     * City for invoice
     * @var string
     *
     * @Assert\NotBlank(message="City is required", groups={"StoreFrontInvoice"})
     *
     * @ORM\Column(name="invoice_city", type="string", length=80, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $invoiceCity;

    /**
     * Country for invoice
     * @var string
     *
     * @Assert\NotBlank(message="Country is required", groups={"StoreFrontInvoice"})
     *
     * @ORM\Column(name="invoice_country", type="string", length=10, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $invoiceCountry;

    /**
     * Post code for invoice
     * @var string
     *
     * @Assert\NotBlank(message="Postcode is required", groups={"StoreFrontInvoice"})
     *
     * @ORM\Column(name="invoice_postcode", type="string", length=10, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $invoicePostcode;

    /**
     * Tax value (eg. 23, 8)
     * @var string
     *
     * @Assert\NotBlank(message="Tax id is required", groups={"StoreFrontInvoice"})
     *
     * @ORM\Column(name="invoice_tax_id", type="string", length=20, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $invoiceTaxId;

    /**
     * Infoice info
     * @var string
     *
     * @ORM\Column(name="invoice_info", type="string", length=255, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $invoiceInfo;

    /**
     * Shelf ID
     * @var integer
     *
     * @ORM\Column(name="shelf_id", type="integer", nullable=true)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $shelfId;

    /**
     * Is it to be booked?
     * @var boolean
     *
     * @ORM\Column(name="to_book", type="boolean", nullable=false)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $toBook = false;

    /**
     * Booked date
     * @var \DateTime
     *
     * @ORM\Column(name="booked_at", type="datetime", nullable=true)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $bookedAt;

    /**
     * Does the customer want to invoice?
     * @var boolean
     *
     * @ORM\Column(name="want_invoice", type="boolean", nullable=false)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $wantInvoice = false;

    /**
     * Order sent at date
     * @var \DateTime
     *
     * @ORM\Column(name="send_at", type="datetime", nullable=true)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $sendAt;

    /**
     * Transport number
     * @var string
     *
     * @ORM\Column(name="transport_number", type="string", length=255, nullable=true)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $transportNumber;

    /**
     * Hanger number
     * @var integer
     *
     * @ORM\Column(name="hanger_number", type="integer", nullable=true)
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $hangerNumber;

    /**
     * Order create date
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Groups({"public", "OrdersList", "Simple"})
     */
    private $createdAt;

    /**
     * Order update date
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Serializer\Groups({"public", "OrdersList", "Simple"})
     */
    private $updatedAt;

    /**
     * Package status name (eg. closed, waiting)
     * @var OrderPackageStatus
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\OrderBundle\Entity\OrderPackageStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="order_package_status_name", referencedColumnName="name")
     * })
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $orderPackageStatusName;

    /**
     * Carrier object
     * @var \Carrier
     *
     * @Assert\NotBlank(message="Choose delivery type", groups={"StoreFront"})
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\CarrierBundle\Entity\Carrier")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="carrier_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $carrier;

    /**
     * Client address
     * @var integer
     *
     * @Assert\NotBlank(message="Delivery address is required", groups={"StoreFrontDelivery"})
     * @OrderClientAddress(groups={"StoreFrontDelivery"})
     *
     * @ORM\Column(name="client_address_id", type="integer", nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $clientAddress;

    /**
     * Client object
     * @var \Client
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\ClientBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $client;

    /**
     * Coupon name
     * @var string
     *
     * @ORM\Column(name="coupon_name", type="string", length=255, nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $couponName;

    /**
     * Payment object
     * @var \Payment
     *
     * @Assert\NotBlank(message="Payment is required (depends on carrier type)", groups={"StoreFront"})
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\PaymentBundle\Entity\Payment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $payment;

    /**
     * Order payment status
     * @var \PaymentStatus
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="payment_status_name", referencedColumnName="name")
     * })
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $paymentStatusName;

    /**
     * @var integer
     *
     * @ORM\Column(name="report_despatch_id", type="integer", nullable=true)
     */
    private $reportDespatch;

    /**
     * @var string
     *
     * @ORM\Column(name="rest_session_id", type="string", length=255, nullable=true)
     */
    private $restSession;

    /**
     * Order user object
     * @var \Mnumi\Bundle\RestServerBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\RestServerBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $user;

    /**
     * Order items
     * @ORM\OneToMany(targetEntity="\Mnumi\Bundle\OrderBundle\Entity\OrderItem", fetch="EAGER", mappedBy="order", cascade={"all"})
     * @ORM\JoinColumn(name="order_package_id", referencedColumnName="id")
     * @Serializer\Groups({"public", "OrdersList", "StoreFront", "Simple"})
     */
    private $items;

    /**
     * Order create date
     * @var \Date
     *
     * @ORM\Column(name="delivery_at", type="date", nullable=true)
     * @Serializer\Groups({"public", "OrdersList", "StoreFront"})
     */
    private $deliveryAt;

    /**
     * @ORM\OneToMany(targetEntity="Mnumi\Bundle\LoyaltyPointBundle\Entity\LoyaltyPoint", mappedBy="order")
     */
    private $loyaltyPoints;

    /**
     * Set deliveryName
     *
     * @param  string        $deliveryName
     * @return Order
     */
    public function setDeliveryName($deliveryName)
    {
        $this->deliveryName = $deliveryName;

        return $this;
    }

    /**
     * Get deliveryName
     *
     * @return string
     */
    public function getDeliveryName()
    {
        return $this->deliveryName;
    }

    /**
     * Set deliveryCountry
     *
     * @param  string        $deliveryCountry
     * @return Order
     */
    public function setDeliveryCountry($deliveryCountry)
    {
        $this->deliveryCountry = $deliveryCountry;

        return $this;
    }

    /**
     * Get deliveryCountry
     *
     * @return string
     */
    public function getDeliveryCountry()
    {
        return $this->deliveryCountry;
    }

    /**
     * Set deliveryStreet
     *
     * @param  string        $deliveryStreet
     * @return Order
     */
    public function setDeliveryStreet($deliveryStreet)
    {
        $this->deliveryStreet = $deliveryStreet;

        return $this;
    }

    /**
     * Get deliveryStreet
     *
     * @return string
     */
    public function getDeliveryStreet()
    {
        return $this->deliveryStreet;
    }

    /**
     * Set deliveryCity
     *
     * @param  string        $deliveryCity
     * @return Order
     */
    public function setDeliveryCity($deliveryCity)
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    /**
     * Get deliveryCity
     *
     * @return string
     */
    public function getDeliveryCity()
    {
        return $this->deliveryCity;
    }

    /**
     * Set deliveryPostcode
     *
     * @param  string        $deliveryPostcode
     * @return Order
     */
    public function setDeliveryPostcode($deliveryPostcode)
    {
        $this->deliveryPostcode = $deliveryPostcode;

        return $this;
    }

    /**
     * Get deliveryPostcode
     *
     * @return string
     */
    public function getDeliveryPostcode()
    {
        return $this->deliveryPostcode;
    }

    /**
     * Set description
     *
     * @param  string        $description
     * @return Order
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set invoiceName
     *
     * @param  string        $invoiceName
     * @return Order
     */
    public function setInvoiceName($invoiceName)
    {
        $this->invoiceName = $invoiceName;

        return $this;
    }

    /**
     * Get invoiceName
     *
     * @return string
     */
    public function getInvoiceName()
    {
        return $this->invoiceName;
    }

    /**
     * Set invoiceStreet
     *
     * @param  string        $invoiceStreet
     * @return Order
     */
    public function setInvoiceStreet($invoiceStreet)
    {
        $this->invoiceStreet = $invoiceStreet;

        return $this;
    }

    /**
     * Get invoiceStreet
     *
     * @return string
     */
    public function getInvoiceStreet()
    {
        return $this->invoiceStreet;
    }

    /**
     * Set invoiceCity
     *
     * @param  string        $invoiceCity
     * @return Order
     */
    public function setInvoiceCity($invoiceCity)
    {
        $this->invoiceCity = $invoiceCity;

        return $this;
    }

    /**
     * Get invoiceCity
     *
     * @return string
     */
    public function getInvoiceCity()
    {
        return $this->invoiceCity;
    }

    /**
     * Set invoicePostcode
     *
     * @param  string        $invoicePostcode
     * @return Order
     */
    public function setInvoicePostcode($invoicePostcode)
    {
        $this->invoicePostcode = $invoicePostcode;

        return $this;
    }

    /**
     * Get invoicePostcode
     *
     * @return string
     */
    public function getInvoicePostcode()
    {
        return $this->invoicePostcode;
    }

    /**
     * Set invoiceTaxId
     *
     * @param  string        $invoiceTaxId
     * @return Order
     */
    public function setInvoiceTaxId($invoiceTaxId)
    {
        $this->invoiceTaxId = $invoiceTaxId;

        return $this;
    }

    /**
     * Get invoiceTaxId
     *
     * @return string
     */
    public function getInvoiceTaxId()
    {
        return $this->invoiceTaxId;
    }

    /**
     * Set invoiceInfo
     *
     * @param  string        $invoiceInfo
     * @return Order
     */
    public function setInvoiceInfo($invoiceInfo)
    {
        $this->invoiceInfo = $invoiceInfo;

        return $this;
    }

    /**
     * Get invoiceInfo
     *
     * @return string
     */
    public function getInvoiceInfo()
    {
        return $this->invoiceInfo;
    }

    /**
     * Set shelfId
     *
     * @param  integer       $shelfId
     * @return Order
     */
    public function setShelfId($shelfId)
    {
        $this->shelfId = $shelfId;

        return $this;
    }

    /**
     * Get shelfId
     *
     * @return integer
     */
    public function getShelfId()
    {
        return $this->shelfId;
    }

    /**
     * Set toBook
     *
     * @param  boolean       $toBook
     * @return Order
     */
    public function setToBook($toBook)
    {
        $this->toBook = $toBook;

        return $this;
    }

    /**
     * Get toBook
     *
     * @return boolean
     */
    public function getToBook()
    {
        return $this->toBook;
    }

    /**
     * Set bookedAt
     *
     * @param  \DateTime     $bookedAt
     * @return Order
     */
    public function setBookedAt($bookedAt)
    {
        $this->bookedAt = $bookedAt;

        return $this;
    }

    /**
     * Get bookedAt
     *
     * @return \DateTime
     */
    public function getBookedAt()
    {
        return $this->bookedAt;
    }

    /**
     * Set wantInvoice
     *
     * @param  boolean       $wantInvoice
     * @return Order
     */
    public function setWantInvoice($wantInvoice)
    {
        $this->wantInvoice = $wantInvoice;

        return $this;
    }

    /**
     * Get wantInvoice
     *
     * @return boolean
     */
    public function getWantInvoice()
    {
        return $this->wantInvoice;
    }

    /**
     * Set sendAt
     *
     * @param  \DateTime     $sendAt
     * @return Order
     */
    public function setSendAt($sendAt)
    {
        $this->sendAt = $sendAt;

        return $this;
    }

    /**
     * Get sendAt
     *
     * @return \DateTime
     */
    public function getSendAt()
    {
        return $this->sendAt;
    }

    /**
     * Set transportNumber
     *
     * @param  string        $transportNumber
     * @return Order
     */
    public function setTransportNumber($transportNumber)
    {
        $this->transportNumber = $transportNumber;

        return $this;
    }

    /**
     * Get transportNumber
     *
     * @return string
     */
    public function getTransportNumber()
    {
        return $this->transportNumber;
    }

    /**
     * Set hangerNumber
     *
     * @param  integer       $hangerNumber
     * @return Order
     */
    public function setHangerNumber($hangerNumber)
    {
        $this->hangerNumber = $hangerNumber;

        return $this;
    }

    /**
     * Get hangerNumber
     *
     * @return integer
     */
    public function getHangerNumber()
    {
        return $this->hangerNumber;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime     $createdAt
     * @return Order
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime     $updatedAt
     * @return Order
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paymentStatusName
     *
     * @param  PaymentStatus $paymentStatusName
     * @return Order
     */
    public function setPaymentStatusName(PaymentStatus $paymentStatusName = null)
    {
        $this->paymentStatusName = $paymentStatusName;

        return $this;
    }

    /**
     * Get paymentStatusName
     *
     * @return PaymentStatus
     */
    public function getPaymentStatusName()
    {
        return $this->paymentStatusName;
    }

    /**
     * Set reportDespatch
     *
     * @param null $reportDespatch
     * @return Order
     */
    public function setReportDespatch($reportDespatch = null)
    {
        $this->reportDespatch = $reportDespatch;

        return $this;
    }

    /**
     * Get reportDespatch
     *
     */
    public function getReportDespatch()
    {
        return $this->reportDespatch;
    }

    /**
     * Set restSession
     *
     * @param null $restSession
     * @return Order
     */
    public function setRestSession($restSession = null)
    {
        $this->restSession = $restSession;

        return $this;
    }

    /**
     * Get restSession
     *
     */
    public function getRestSession()
    {
        return $this->restSession;
    }

    /**
     * Set user
     *
     * @param $user
     * @return Order
     */
    public function setUser($user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Mnumi\Bundle\RestServerBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set payment
     *
     * @param $payment
     * @return Order
     */
    public function setPayment($payment = null)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return \Mnumi\Bundle\PaymentBundle\Entity\Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set couponName
     *
     * @param null $couponName
     * @return Order
     */
    public function setCouponName($couponName = null)
    {
        $this->couponName = $couponName;

        return $this;
    }

    /**
     * Get couponName
     *
     * @return \Mnumi\Bundle\CouponBundle\Entity\Coupon
     */
    public function getCouponName()
    {
        return $this->couponName;
    }

    /**
     * Set carrier
     *
     * @param $carrier
     * @return Order
     */
    public function setCarrier($carrier = null)
    {
        $this->carrier = $carrier;

        return $this;
    }

    /**
     * Get carrier
     *
     * @return \Mnumi\Bundle\CarrierBundle\Entity\Carrier
     */
    public function getCarrier()
    {
        return $this->carrier;
    }

    /**
     * Set clientAddress
     *
     * @param $clientAddress
     * @return Order
     */
    public function setClientAddress($clientAddress = null)
    {
        $this->clientAddress = $clientAddress;

        return $this;
    }

    /**
     * Get clientAddress
     *
     * @return \Mnumi\Bundle\ClientBundle\Entity\ClientAddress
     */
    public function getClientAddress()
    {
        return $this->clientAddress;
    }

    /**
     * Set client
     *
     * @param $client
     * @return Order
     */
    public function setClient($client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return \Mnumi\Bundle\ClientBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set orderPackageStatusName
     *
     * @param OrderPackageStatus $orderPackageStatusName
     * @return Order
     */
    public function setOrderPackageStatusName($orderPackageStatusName = null)
    {
        $this->orderPackageStatusName = $orderPackageStatusName;

        return $this;
    }

    /**
     * Get orderPackageStatusName
     *
     * @return OrderPackageStatus
     */
    public function getOrderPackageStatusName()
    {
        return $this->orderPackageStatusName;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * Get items
     *
     * @return ArrayCollection|OrderItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @return string
     */
    public function getInvoiceCountry()
    {
        return $this->invoiceCountry;
    }

    /**
     * @param string $invoiceCountry
     * @return Order
     */
    public function setInvoiceCountry($invoiceCountry)
    {
        $this->invoiceCountry = $invoiceCountry;
        
        return $this;
    }

    /**
     * Checks if order is basket
     *
     * @return boolean
     */
    public function isBasket()
    {
        return $this->getOrderPackageStatusName() === OrderRepository::STATUS_BASKET;
    }

    /**
     * Calculate base amount
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("base_amount")
     * @Serializer\Groups({"public", "StoreFront"})
     *
     * @return float
     */
    public function calculateBaseAmount()
    {
        $baseAmount = 0;

        foreach($this->getItems() as $item) {

            $baseAmount += $item->getBaseAmount();
        }

        return $baseAmount;
    }

    /**
     * Calculate total amount
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("total_amount")
     * @Serializer\Groups({"public", "StoreFront"})
     *
     * @return float
     */
    public function calculateTotalAmount()
    {
        $totalAmount = 0;

        foreach($this->getItems() as $item) {

            $totalAmount += $item->getTotalAmount();
        }

        return $totalAmount;
    }

    /**
     * Calculate total amount gross
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("total_amount_gross")
     * @Serializer\Groups({"public", "StoreFront"})
     *
     * @return float
     */
    public function calculateTotalAmountGross()
    {
        $totalAmountGross = 0;

        foreach($this->getItems() as $item) {

            $totalAmountGross += $item->getTotalAmountGross();
        }

        return $totalAmountGross;
    }

    /**
     * Calculate discount value
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("total_amount_gross")
     * @Serializer\Groups({"public", "StoreFront"})
     *
     * @return float
     */
    public function calculateDiscountValue()
    {
        $totalAmountGross = 0;

        foreach($this->getItems() as $item) {

            $totalAmountGross += $item->getTotalAmountGross();
        }

        return $totalAmountGross;
    }

    /**
     * Return information about having free shipping
     *
     * @return boolean
     */
    private function hasFreeShipping()
    {
        $carrier = $this->getCarrier();
        $freeShippingAmount = (float) $carrier->getFreeShippingAmount();
        if ($freeShippingAmount >= $this->calculateBaseAmount()) {
            return false;
        }

        if ($freeShippingAmount <= 0) {
            return false;
        }

        return true;
    }

    /**
     * Calculate price net for delivery
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("delivery_net_price")
     * @Serializer\Groups({"public", "StoreFront"})
     * @return float
     */
    public function calculateDeliveryNetPrice()
    {
        $carrier = $this->getCarrier();
        if($carrier)
        {
            if (!$this->hasFreeShipping())
            {
                return round($carrier->getPrice(), 2);
            }
        }
        return (float) 0;
    }

    /**
     * Get price net for paymeny
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("payment_net_price")
     * @Serializer\Groups({"public", "StoreFront"})
     * @return float
     */
    public function getPaymentNetPrice()
    {
        $payment = $this->getPayment();

        return $payment?
                $payment->getCost():
                0;
    }

    /**
     * Get loyalty points
     *
     * @return ArrayCollection
     */
    public function getLoyaltyPoints()
    {
        return $this->loyaltyPoints;
    }

    /**
     * Cleans delivery data from order.
     */
    public function cleanDeliveryData()
    {
        $this->setClientAddress(NULL)
                ->setDeliveryName(NULL)
                ->setDeliveryCountry(NULL)
                ->setDeliveryStreet(NULL)
                ->setDeliveryCity(NULL)
                ->setDeliveryPostcode(NULL);
    }

    /**
     * Get orders base amount
     *
     * @Serializer\VirtualProperty
     * @Serializer\Groups({"public", "OrdersList"})
     *
     * @return bool|float
     */
    public function getOrdersBaseAmount()
    {
        if($this->orderBaseAmount !== false) {
            return $this->orderBaseAmount;
        }

        $this->orderBaseAmount = $this->calculateOrdersBaseAmount();

        return $this->orderBaseAmount;
    }

    /**
     * Calculate summary base amount for all orders
     * @return float
     */
    private function calculateOrdersBaseAmount()
    {
        $summaryPrice = 0;

        foreach($this->getItems() as $item) {
            if(!$item->hasPropelStatusForBackend()) continue;

            $summaryPrice += (float) $item->getBaseAmount();
        }

        return round($summaryPrice, 2);
    }

    /**
     * Get price gross for delivery
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("delivery_price_gross")
     * @Serializer\Groups({"public", "OrdersList"})
     *
     * @return float
     */
    public function getDeliveryGrossPrice()
    {
        $carrier = $this->getCarrier();
        if($carrier)
        {
            if (!$this->hasFreeShipping())
            {
                return round($carrier->getPriceGross(), 2);
            }
        }
        return (float) 0;
    }

    /**
     * Get price net for delivery
     *
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("delivery_price_net")
     * @Serializer\Groups({"public", "OrdersList"})
     *
     * @return float
     */
    public function getDeliveryNetPrice()
    {
        $carrier = $this->getCarrier();
        if($carrier)
        {
            if (!$this->hasFreeShipping())
            {
                return round($carrier->getPrice(), 2);
            }
        }
        return (float) 0;
    }
}
