<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemWorkflow
 *
 * @ORM\Table(name="order_workflow", indexes={@ORM\Index(name="fk_order_workflow_order_status1_idx", columns={"current_status"}), @ORM\Index(name="fk_order_workflow_order_status2_idx", columns={"next_status"}), @ORM\Index(name="fk_order_workflow_workflow1_idx", columns={"workflow_id"})})
 * @ORM\Entity
 */
class OrderItemWorkflow
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="current_status", type="string", length=255, nullable=false)
     */
    private $currentStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="next_status", type="string", length=255, nullable=true)
     */
    private $nextStatus;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="backward", type="boolean", nullable=true)
     */
    private $backward;

    /**
     * @var integer
     *
     * @ORM\Column(name="workflow_id", type="integer", nullable=false)
     */
    private $workflowId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currentStatus
     *
     * @param string $currentStatus
     *
     * @return OrderItemWorkflow
     */
    public function setCurrentStatus($currentStatus)
    {
        $this->currentStatus = $currentStatus;

        return $this;
    }

    /**
     * Get currentStatus
     *
     * @return string
     */
    public function getCurrentStatus()
    {
        return $this->currentStatus;
    }

    /**
     * Set nextStatus
     *
     * @param string $nextStatus
     *
     * @return OrderItemWorkflow
     */
    public function setNextStatus($nextStatus)
    {
        $this->nextStatus = $nextStatus;

        return $this;
    }

    /**
     * Get nextStatus
     *
     * @return string
     */
    public function getNextStatus()
    {
        return $this->nextStatus;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return OrderItemWorkflow
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set backward
     *
     * @param boolean $backward
     *
     * @return OrderItemWorkflow
     */
    public function setBackward($backward)
    {
        $this->backward = $backward;

        return $this;
    }

    /**
     * Get backward
     *
     * @return boolean
     */
    public function getBackward()
    {
        return $this->backward;
    }

    /**
     * Set workflowId
     *
     * @param integer $workflowId
     *
     * @return OrderItemWorkflow
     */
    public function setWorkflowId($workflowId)
    {
        $this->workflowId = $workflowId;

        return $this;
    }

    /**
     * Get workflowId
     *
     * @return integer
     */
    public function getWorkflowId()
    {
        return $this->workflowId;
    }
}
