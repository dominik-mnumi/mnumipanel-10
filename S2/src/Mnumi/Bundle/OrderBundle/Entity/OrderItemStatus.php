<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemStatus
 *
 * @ORM\Table(name="order_status")
 * @ORM\Entity
 */
class OrderItemStatus
{

    const STATUS_DRAFT = 'draft';
    const STATUS_NEW = 'new';
    const STATUS_CALCULATION = 'calculation';
    const STATUS_REALIZATION = 'realization';
    const STATUS_READY = 'ready';
    const STATUS_BINDERY = 'bindery';
    const STATUS_DELETED = 'deleted';
    const STATUS_CLOSED = 'closed';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="changeable", type="boolean", nullable=false)
     */
    private $changeable = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="string", length=255, nullable=true)
     */
    private $icon;


    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return OrderItemStatus
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set changeable
     *
     * @param boolean $changeable
     *
     * @return OrderItemStatus
     */
    public function setChangeable($changeable)
    {
        $this->changeable = $changeable;

        return $this;
    }

    /**
     * Get changeable
     *
     * @return boolean
     */
    public function getChangeable()
    {
        return $this->changeable;
    }

    /**
     * Set icon
     *
     * @param string $icon
     *
     * @return OrderItemStatus
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
