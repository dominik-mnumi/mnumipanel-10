<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * OrderItemAttribute
 *
 * @ORM\Table(name="order_attribute", indexes={@ORM\Index(columns={"order_id"}), @ORM\Index(columns={"product_field_id"}), @ORM\Index(columns={"editor_id"})})
 * @ORM\Entity
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class OrderItemAttribute
{
    /**
     * Unique autoincrement ID
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="value", type="string", nullable=false)
     * @Serializer\Exclude
     */
    private $value;

    /**
     * Order item
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\OrderBundle\Entity\OrderItem", inversedBy="attributes", cascade={"all"})
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     * @Serializer\Groups({"public", "OrdersList"})
     *
     */
    private $orderItem;

    /**
     * @var integer
     *
     * @ORM\Column(name="editor_id", type="bigint", nullable=true)
     * @Serializer\Exclude
     */
    private $editorId;

    /**
     * Product field
     * @var \Mnumi\Bundle\ProductBundle\Entity\ProductField
     *
     * @ORM\ManyToOne(targetEntity="\Mnumi\Bundle\ProductBundle\Entity\ProductField", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_field_id", referencedColumnName="id")
     * })
     * @Serializer\Groups({"public", "OrdersList"})
     */
    private $field;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     * @Serializer\Exclude
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="deleted_at", type="datetime", nullable=true)
     * @Serializer\Exclude
     */
    private $deletedAt;

    public function __toString()
    {
        return (string)$this->getValue();
    }
    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("fieldset")
     * @Serializer\Groups({"OrdersList"})
     *
     * @return string
     */
    public function getFieldsetName()
    {
        return $this->getField()->getFieldset()->getName();
    }
    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("value")
     * @Serializer\Groups({"public", "OrdersList"})
     *
     * @return int
     */
    public function getCalculatedValue()
    {
        $value = $this->getValue();

        if ($this->getField()->isCalculateAsCardEnabled()) {

            $side = false;
            $sideOrderAttributes = $this->getOrderItem()->getAttributes();
            /** @var \Mnumi\Bundle\OrderBundle\Entity\OrderItemAttribute $sideOrderAttribute  */
            foreach ($sideOrderAttributes as $sideOrderAttribute)
            {
                if($sideOrderAttribute->getField()->getFieldset()->getName() == "SIDES")
                {
                    $side = $sideOrderAttribute;
                }
            }
            if($side !== false) {
                $sideId = (int) $side->getValue();

                if($sideId > 0) {
                    /** @var OrderItemAttribute $side */
                    $sides = $side->getField()->getFieldItems();
                    foreach ($sides as $sideField)
                    {
                        if ($sideField->getFieldItem()->getId() == $sideId)
                        {
                            if ($sideField->getFieldItem()->getName() === 'Double') {
                                $value = (int) $value * 2;
                            }
                        }
                    }


                }
            }
        }

        return $value;
    }

    /**
     * Set value
     *
     * @param  string         $value
     * @return OrderAttribute
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime      $createdAt
     * @return OrderAttribute
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime      $updatedAt
     * @return OrderAttribute
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set deletedAt
     *
     * @param  \DateTime      $deletedAt
     * @return OrderAttribute
     */
    public function setDeletedAt($deletedAt)
    {
        $this->deletedAt = $deletedAt;

        return $this;
    }

    /**
     * Get deletedAt
     *
     * @return \DateTime
     */
    public function getDeletedAt()
    {
        return $this->deletedAt;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\Orders $orderItem
     * @return OrderAttribute
     */
    public function setOrderItem(\Mnumi\Bundle\OrderBundle\Entity\OrderItem $item = null)
    {
        $this->orderItem = $item;

        return $this;
    }

    /**
     * Get order
     *
     * @return \Mnumi\Bundle\OrderBundle\Entity\OrderItem
     */
    public function getOrderItem()
    {
        return $this->orderItem;
    }

    /**
     * Set editor
     *
     * @param  \Mnumi\Bundle\RestServerBundle\Entity\User $editor
     * @return OrderAttribute
     */
    public function setEditor(\Mnumi\Bundle\RestServerBundle\Entity\User $editor = null)
    {
        $this->editor = $editor;

        return $this;
    }

    /**
     * Get editor
     *
     * @return \Mnumi\Bundle\RestServerBundle\Entity\User
     */
    public function getEditor()
    {
        return $this->editor;
    }

    /**
     * Set editorId
     *
     * @param  integer            $editorId
     * @return OrderItemAttribute
     */
    public function setEditorId($editorId)
    {
        $this->editorId = $editorId;

        return $this;
    }

    /**
     * Get editorId
     *
     * @return integer
     */
    public function getEditorId()
    {
        return $this->editorId;
    }

    /**
     * Set field
     *
     * @param  \Mnumi\Bundle\ProductBundle\Entity\ProductField $field
     * @return OrderItemAttribute
     */
    public function setField($field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return \Mnumi\Bundle\ProductBundle\Entity\ProductField
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("required_amount")
     * @Serializer\Groups({"OrdersList"})
     *
     * @return string
     */
    public function getRequiredAmount()
    {
        $default = $this->getField()->getDefaultValue();
        $fields = $this->getField()->getFieldItems();
        /** @var ProductFieldItem #field */
        foreach ($fields as $field)
        {
            if($field->getFieldItem()->getId() == $this->getValue())
            {
                return $field->getRequiredAmount();
            }
        }
        return 1;
    }
}
