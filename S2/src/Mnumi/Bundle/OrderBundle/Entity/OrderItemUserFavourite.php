<?php

namespace Mnumi\Bundle\OrderBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OrderItemUserFavourite
 *
 * @ORM\Table(name="order_user_favourite", indexes={@ORM\Index(name="fk_order_user_favourite_order1_idx", columns={"order_id"}), @ORM\Index(columns={"user_id"})})
 * @ORM\Entity
 */
class OrderItemUserFavourite
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="user_id", type="bigint", nullable=false)
     */
    private $userId;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_id", type="integer", nullable=false)
     */
    private $orderItemId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userId
     *
     * @param integer $userId
     *
     * @return OrderItemUserFavourite
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId
     *
     * @return integer
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set orderItemId
     *
     * @param integer $orderItemId
     *
     * @return OrderItemUserFavourite
     */
    public function setOrderItemId($orderItemId)
    {
        $this->orderItemId = $orderItemId;

        return $this;
    }

    /**
     * Get orderItemId
     *
     * @return integer
     */
    public function getOrderItemId()
    {
        return $this->orderItemId;
    }
}
