<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\OrderBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Mnumi\Bundle\RestServerBundle\Entity\User;
use Mnumi\Bundle\RestServerBundle\Library\Codes;

class Order  extends EntityRepository {
    
    const STATUS_BASKET = 'basket';
    const STATUS_ARCHIVE = 'archive';

    const PERMISSION_WRITABLE = 'writable';
    const PERMISSION_READONLY = 'readonly';

    /**
     * Get order with basket packageStatusName for uUser
     *
     * @param User $user
     * @return null|object
     */
    public function getBasketOrderForUser(User $user)
    {
        return $this->findOneBy(
            array(
                'user' => $user,
                'orderPackageStatusName' => self::STATUS_BASKET
            )
        );
    }

    /**
     * Get basket by rest session id
     *
     * @param string $sessionId
     * @return null|object
     */
    public function getBasketOrderForSession($sessionId)
    {
        return $this->createQueryBuilder('o')
                ->innerJoin('o.user', 'u')
                ->innerJoin('u.restSessions', 'rs')
                ->where('rs.id = :sessionId')
           ->orderBy('o.createdAt', 'DESC')
           ->setMaxResults(1)
                ->setParameter('sessionId', $sessionId)
                ->andWhere('o.orderPackageStatusName = :orderStatusName')
                ->setParameter('orderStatusName', self::STATUS_BASKET)
                ->getQuery()
                ->getOneOrNullResult();
    }
}
