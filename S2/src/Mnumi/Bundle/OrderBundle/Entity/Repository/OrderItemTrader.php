<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\OrderBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Mnumi\Bundle\OrderBundle\Entity\OrderItem as OrderItemEntity;


class OrderItemTrader  extends EntityRepository {
    
    public function getOrderTotalPrice(OrderItemEntity $orderItem)
    {
        $q = $this
            ->createQueryBuilder('t')
            ->select('SUM(t.quantity * t.price)')
            ->where('t.orderItemId = :id')
            ->setParameter('id', $orderItem->getId())
            ->getQuery();
        ;

        $total = $q->getSingleResult();
        if(!empty($total['sum']))
        {
            return $total[0]['sum'];
        }
        return 0;
    }
}