<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\OrderBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Mnumi\Bundle\OrderBundle\Entity\OrderItem as OrderItemEntity;
use Mnumi\Bundle\OrderBundle\Entity\OrderItemFile as OrderItemFileEntity;

class OrderItemFile  extends EntityRepository
{
    const TYPE_IMAGE = 'image';
    const TYPE_FOTOLIA = 'fotolia';

    /**
     * Try to find OrderItemFile for givent OrderItem, filename and type
     * If does not exist, creates it
     *
     * @param OrderItemEntity $orderItem
     * @param string $filename
     * @param string $type
     * @return OrderItemFileEntity
     */
    public function findOrCreate(OrderItemEntity $orderItem, $filename, $type)
    {
        $em = $this->getEntityManager();
        $orderItemFile = $this->findOneBy(array(
                    'filename' => $filename,
                    'orderItem' => $orderItem,
                    'type' => $type
                    ));

        if (!$orderItemFile) {

            $orderItemFile = new OrderItemFileEntity();
            $orderItemFile->setOrderItem($orderItem);
            $orderItemFile->setFilename($filename);
            $orderItemFile->setType($type);
            $em->persist($orderItemFile);
        }

        return $orderItemFile;
    }
}
