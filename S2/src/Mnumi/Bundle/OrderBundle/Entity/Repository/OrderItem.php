<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\OrderBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Mnumi\Bundle\CalculationBundle\Library\CalculationTool;
use Mnumi\Bundle\CalculationBundle\Library\ProductCalculationProductData;
use Mnumi\Bundle\ClientBundle\Entity\Client;
use Mnumi\Bundle\OrderBundle\Entity\CustomPrice;
use Mnumi\Bundle\OrderBundle\Entity\Order as OrderEntity;
use Mnumi\Bundle\OrderBundle\Entity\OrderItem as OrderItemEntity;
use Mnumi\Bundle\OrderBundle\Entity\OrderItemAttribute;
use Mnumi\Bundle\ProductBundle\Entity\Coupon;
use Mnumi\Bundle\ProductBundle\Entity\FieldItem;
use Mnumi\Bundle\RestServerBundle\Entity\User;

class OrderItem  extends EntityRepository
{
    public function reCalculate(User $user, Client $client, OrderEntity $order)
    {
        $em = $this->getEntityManager();

        // foreach order in new basket
        /** @var OrderItemEntity $item */
        foreach ($order->getItems() as $item) {
            $item->setClient($client);
            $item->setUser($user);
            $item->setEditorId($user->getId());

            // calculate order price for new client

            $priceCalculate = $this->calculate($item, null, $client->getPricelist());
            // update price value
            $this->setPrices($item, $priceCalculate['priceItems']);

            // update tax if client is
            if ($item->getClient()->getWntUe() === true) {
                $item->setTaxValue(0);
            }
            $this->updatePrices($item);

            $em->persist($item);

            /** @var OrderItemAttribute $attribute */
            foreach ($item->getAttributes() as $attribute) {
                $attribute->setEditor($user);
                $em->persist($attribute);
            }
        }
        $em->flush();

    }

    /**
     * Update order prices (used after add or save)
     *
     * @param  OrderItemEntity $orderItem
     * @return OrderItemEntity
     */
    public function updatePrices(OrderItemEntity $orderItem)
    {
        $orderItemTrader = $this->getEntityManager()->getRepository('MnumiOrderBundle:OrderItemTrader');
        $orderItem->setTradersAmount(
            (float) $orderItemTrader->getOrderTotalPrice($orderItem)
        );

        $orderItem->setPriceNet(
            $orderItem->getBaseAmount() + $orderItem->getTradersAmount()
        );

        $orderItem->setDiscountAmount($this->prepareDiscountAmount($orderItem));

        $orderItem->setTotalAmount(
            $orderItem->getPriceNet() - $orderItem->getDiscountAmount()
        );

        $orderItem->setTotalCost($orderItem->getBaseCost() + $orderItem->getCustomCost());
        $orderItem->setCustomCost($this->prepareCustomCost($orderItem));

        return $orderItem;
    }

    /**
     * Calculate amount of discount
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem
     * @throws \Exception
     * @return float                                      Amount
     */
    public function prepareDiscountAmount(OrderItemEntity $orderItem)
    {
        $amount = 0;

        // no discount available
        if ($orderItem->hasDiscount()) {
            switch ($orderItem->getDiscountType()) {
                case Coupon::PERCENT:
                    $amount = $orderItem->getBaseAmount() * ((float) $orderItem->getDiscountValue() / 100);
                    break;
                case Coupon::AMOUNT:
                    $amount = $orderItem->getDiscountValue();
                    break;
                default:
                    throw new \Exception('Unsupported discount type');
            }
        }

        return $amount;
    }

    /**
     * Calculate Custom costs for Order
     *
     * return float Summary amount
     */
    public function prepareCustomCost(OrderItemEntity $orderItem)
    {
        $amount = 0;

        $orderCost = $this->getEntityManager()->getRepository('MnumiOrderBundle:OrderItemCost')->findBy(array('orderItemId' => $orderItem->getId()));

        foreach ($orderCost as $cost) {
            $amount += $cost->getTotalPriceNet();
        }

        return $amount;
    }

    /**
     * @param  OrderItemEntity         $orderItem
     * @param $name
     * @param $label
     * @param $price
     * @param $cost
     * @return CustomPrice|null|object
     */
    private function createOrUpdateCustomPrice(OrderItemEntity $orderItem, $name, $label, $price, $cost)
    {
        // allow ","
        $price = str_replace(',', '.', $price);

        $em = $this->getEntityManager();
        $item = $em->getRepository('MnumiOrderBundle:CustomPrice')->findOneBy(
            array(
                'orderItem' => $orderItem,
                'name' => $name
            )
        );
        if (!$item instanceof CustomPrice) {
            $item = new CustomPrice();
            $item->setOrderItem($orderItem);
            $item->setName($name);
        }
        $item->setLabel($label);
        $item->setPrice($price);
        $item->setCost($cost);
        $em->persist($item);
        $em->flush();

        return $item;

    }

    /**
     * Save order custom prices in CustomPrice table
     *
     * @param  OrderItemEntity $orderItem
     * @param  array           $fields    Array fields loaded from $orderObj->calculate(), from key "priceItems"
     * @throws \Exception
     * @return OrderItemEntity
     */
    public function setPrices(OrderItemEntity $orderItem, $fields)
    {
        $basePrice = 0;
        $baseCost = 0;

        foreach ($fields as $key => $field) {
            if ($key == 'OTHER') {
                continue;
            }
            if (!array_key_exists('label', $field)) {
                throw new \Exception('Wrong field array. Label is required. ');
            }

            $key = sprintf('[%s]', $key);

            $price = (array_key_exists('price', $field)) ? $field['price'] : 0;
            $cost = (array_key_exists('cost', $field)) ? $field['cost'] : 0;

            $price = round($price, 2);
            $cost = round($cost, 2);

            $basePrice += $price;
            $baseCost += $cost;

            $this->createOrUpdateCustomPrice($orderItem, $key, $field['label'], $price, $cost);
        }

        // save others
        if (isset($fields['OTHER'])) {
            foreach ($fields['OTHER'] as $key => $field) {
                $key = sprintf('[OTHER][%s]', $key);

                $price = (array_key_exists('price', $field)) ? $field['price'] : 0;
                $cost = (array_key_exists('cost', $field)) ? $field['cost'] : 0;

                $price = round($price, 2);
                $cost = round($cost, 2);

                $basePrice += $price;
                $baseCost += $cost;

                $this->createOrUpdateCustomPrice($orderItem, $key, $field['label'], $price, $cost);
            }
        }

        $orderItem->setBaseCost($baseCost);
        $orderItem->setBaseAmount($basePrice);

        return $orderItem;
    }

    /**
     * Check if order has calculation as sides enabled
     *
     * @param  \Mnumi\Bundle\OrderBundle\Entity\OrderItem $orderItem
     * @param  FieldItem|null                             $side      FieldItem side object
     * @throws \Exception
     * @return bool
     */
    public function hasCalculationAsCard(OrderItemEntity $orderItem, $side = null)
    {
        $calculateCountAsCard = false;

        if ($orderItem->getProduct()->getCalculateAsCard()) {
            if ($side == null) {
                $side = $this->getEntityManager()->getRepository('MnumiProductBundle:FieldItem')->find($orderItem->getAttribute('SIDES'));
            }

            if (!$side instanceof FieldItem) {
                throw new \Exception('Incorrect side object. ');
            }

            if ($side->getName() === 'Double') {
                $calculateCountAsCard = true;
            }
        }

        return $calculateCountAsCard;
    }

    /**
     * Calculate order
     *
     * @param  OrderItemEntity $orderItem
     * @param  array|null      $postArray   Post data (if not set, use order entity saved fields)
     * @param  integer|null    $pricelistId Pricelist Id (if not set, get order pricelist id)
     * @return array           calculationTool->fetchReport()
     */
    public function calculate(OrderItemEntity $orderItem, $postArray = null, $pricelistId = null)
    {
        if (!$pricelistId) {
            $pricelistId = $orderItem->getPricelistId();
        }

        $orderArray = $this->getCalculationFields($orderItem);

        $sidesValue = ($postArray == null)
            ? $orderArray['SIDES']['value']
            : $postArray['SIDES']['value'];
        $side = $this->getEntityManager()->getRepository('MnumiProductBundle:FieldItem')->find((int) $sidesValue);

        /** @var $side FieldItem */
        $hasCalculationAsCard = $this->hasCalculationAsCard($orderItem, $side);

        if ($hasCalculationAsCard) {
            // change count for default order data
            foreach ($orderArray as $key => $item) {
                if ($key == 'COUNT') {
                    $orderArray[$key]['value'] = (int) ceil($item['value'] / 2);
                }
            }
        }

        if ($postArray !== null) {
            foreach ($postArray as $key => $item) {
                if ($key == 'OTHER') {
                    foreach ($item as $otherId => $otherItem) {
                        $orderArray['OTHER'][$otherId] = $otherItem;
                    }
                } else {
                    if ($hasCalculationAsCard && $key == 'COUNT') {
                        // replace count, adding count from post data
                        $item['value'] = (int) ceil($item['value'] / 2);
                    }
                    $orderArray[$key] = $item;
                }
            }
        }

        $productData = new ProductCalculationProductData($this->getEntityManager(), $orderItem->getProduct(), 0.23);
        $calculationTool = new CalculationTool(
            $productData->getCalculationProductDataObject()
        );

        $calculationTool->initialize($orderArray, $pricelistId);

        $report = $calculationTool->fetchReport();

        if ($orderItem->getClient() && $orderItem->getClient()->getWntUe()) {
            $report['summaryPriceGross'] = $report['summaryPriceNet'];
        }

        return $report;
    }

    public function getCalculationFields(OrderItemEntity $orderItem)
    {
        $attributes = $orderItem->getAttributeValues();

        $fields = array();
        $fields['SIZE'] = array('name' => 'SIZE', 'value' => isset($attributes['size']) ? $attributes['size'] : '');

        // if custom size object does exist
        $customizableObj = $orderItem->getCustomsize()->first();
        if ($customizableObj) {
            $fields['SIZE']['width']['value'] = $customizableObj->getWidth();
            $fields['SIZE']['height']['value'] = $customizableObj->getHeight();
        }

        // gets product object
        $productObj = $orderItem->getProduct();

        if (isset($attributes['material'])) {
            $material = $attributes['material'];
        } else {
            $material = $this->getFieldByName($productObj, "MATERIAL");
        }

        $fields['MATERIAL'] = array('name' => 'MATERIAL', 'value' => $material);

        $fields['COUNT'] = array('name' => 'COUNT', 'value' => isset($attributes['count']) ? $attributes['count'] : $productObj->getProductFieldByFieldsetName('COUNT')
                ->getDefaultValue());
        $fields['QUANTITY'] = array('name' => 'QUANTITY', 'value' => isset($attributes['quantity']) ? $attributes['quantity'] : $productObj->getProductFieldByFieldsetName('QUANTITY')
                ->getDefaultValue());

        if (isset($attributes['print'])) {
            $print = $attributes['print'];
        } else {
            $print = $this->getFieldByName($productObj, "PRINT");
        }

        $fields['PRINT'] = array('name' => 'PRINT', 'value' => isset($attributes['print']) ? $attributes['print'] : $print);

        if (isset($attributes['sides'])) {
            $sides = $attributes['sides'];
        } else {
            $sides = $this->getFieldByName($productObj, "SIDES");
        }

        $fields['SIDES'] = array('name' => 'SIDES', 'value' => $sides);

        if (isset($attributes['other'])) {
            foreach ($attributes['other'] as $key => $other) {
                $fields['OTHER'][$key] = array(
                    'name' => 'OTHER',
                    'value' => $other,
                );
            }
        }

        return $fields;
    }

    /**
     * @param $productObj
     * @param $name
     * @return int
     * @throws Exception
     */
    private function getFieldByName($productObj, $name)
    {
        /** @var \Mnumi\Bundle\ProductBundle\Entity\ProductField $field */
        foreach ($productObj->getFields() as $field) {
            if ($field->getFieldset()->getName() == $name) {
                return $field->getId();
            }

        }
        throw new Exception('Missing product fielset ' . $name);
    }
}
