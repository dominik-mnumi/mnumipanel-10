<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\OrderBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Mnumi\Bundle\OrderBundle\Entity\OrderItemFile;

class CropImageCommand extends ContainerAwareCommand
{
    protected $verbose = false;

    public static $currentTask = null;

    protected function configure()
    {
        $this
            ->setName('mnumi:cropImage')
            ->setDescription('Crop image')
            ->addArgument('fileId', InputArgument::REQUIRED, 'File ID');
    }

    /**
     * Execute the task
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $doctrine = $this->getContainer()->get('doctrine');
        $em = $doctrine->getManager();
        $orderItemFileRepository = $doctrine->getRepository('MnumiOrderBundle:OrderItemFile');
        $fileId = $input->getArgument('fileId');

        /** @var Order $order */
        $orgFile = $orderItemFileRepository->findOneById($fileId);

        if($orgFile &&
            $orgFile->getMetadataAttribute('format') === 'JPEG' &&
            $orgFile->getMetadataAttribute('cropWidth') > 0 &&
            $orgFile->getMetadataAttribute('cropHeight') > 0) {

            $cropX = $orgFile->getMetadataAttribute('cropX');
            $cropY = $orgFile->getMetadataAttribute('cropY');
            $cropWidth = $orgFile->getMetadataAttribute('cropWidth');
            $cropHeight = $orgFile->getMetadataAttribute('cropHeight');

            $dataDir = $this->getContainer()->getParameter('data_dir');

            $filePath = $orgFile->getFullFilePath($dataDir);
            $fileDir = $orgFile->getFilePath($dataDir);

            $imagick = new \Imagick($filePath);
            $imagick->cropimage(
                    $cropWidth,
                    $cropHeight,
                    $cropX,
                    $cropY
                );

            $newFilename = 'crop_' . $orgFile->getFilename();

            $imagick->writeimage($fileDir . $newFilename);

            $croppedFile = $orderItemFileRepository->findOneBy(array(
                'filename' => $newFilename,
                'parent' => $orgFile
            ));

            if(!$croppedFile) {
                $croppedFile = clone $orgFile;
            }

            $croppedFile->setFilename($newFilename);
            $croppedFile->setOrderItem($orgFile->getOrderItem());
            $croppedFile->setType($orgFile->getType());
            $croppedFile->setParent($orgFile);

            $em->persist($croppedFile);
            $em->flush();

            $cacheFile = $croppedFile->getCacheFullPath($dataDir);
var_dump($cacheFile);
            if(file_exists($cacheFile)) {
                unlink($cacheFile);
            }
        }
    }
}
