<?php

namespace Mnumi\Bundle\OrderBundle\Text\Pattern;

use Symfony\Component\DependencyInjection\Container;

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PatternReplace class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class PatternReplace 
{
    private $object;

    /**
     * @param $object
     */
    public function __construct($object)
    {
        $this->object = $object;
    }

    /**
     * Replace string using pattern
     *
     * @param $text
     * @return string
     */
    public function convert($text)
    {
        $object = $this->object;

        $regularExpression = '/\{([0-9a-zA-Z\-\_]+)(\:([0-9a-zA-Z\- ]+))?\}/';

        return preg_replace_callback(
            $regularExpression,
            function($matches) use ($object) {
                $methodName = 'get'.self::camelize($matches[1]);
                $param = (count($matches) > 2) ? $matches[3] : null;

                if (!method_exists($object, $methodName)){
                    throw new PatternReplaceException('Invalid method '.$methodName.' for class object '.get_class($object));
                }

                return $object->$methodName($param);
            },
            $text
        );
    }


    /**
     * Camelizes a string.
     *
     * @param string $id A string to camelize
     *
     * @return string The camelized string
     */
    private static function camelize($id)
    {
        return strtr(ucwords(strtr($id, array('_' => ' ', '.' => '_ ', '\\' => '_ '))), array(' ' => ''));
    }
} 