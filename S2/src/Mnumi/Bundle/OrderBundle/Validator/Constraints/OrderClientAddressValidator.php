<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\OrderBundle\Validator\Constraints;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Validator\RecursiveValidator;

/**
 * ClientAddressValidator class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
class OrderClientAddressValidator extends ConstraintValidator
{
    private $entityManager;
    private $validator;

    public function __construct(EntityManager $entityManager, RecursiveValidator $validator)
    {
        $this->entityManager = $entityManager;
        $this->validator = $validator;
    }

    public function validate($value, Constraint $constraint)
    {
        $isValid = true;

        if($value) {
            $clientAddress = $this->entityManager
                    ->getRepository('MnumiClientBundle:ClientAddress')
                    ->findOneById($value);

            if($clientAddress == null) {

                $isValid = false;
                
            } else {

                $isValid = $this->validator->validate($clientAddress, 'StoreFrontDelivery')->count() == 0;
                
            }

            if(!$isValid) {

                $this->context->buildViolation($constraint->message)
                ->addViolation();
            }
        }
    }
}