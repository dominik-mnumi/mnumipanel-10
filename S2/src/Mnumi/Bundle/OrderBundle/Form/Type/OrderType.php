<?php

namespace Mnumi\Bundle\OrderBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Mnumi\Bundle\OrderBundle\Form\DataTransformer\OrderPackageStatusTransformer;

class OrderType extends AbstractType
{
    private $options;

    public function __construct($options = null)
    {
        $this->options = $options;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('clientAddress', null, array( 'description' => 'Client address'))
            ->add('deliveryName', null, array( 'description' => 'Delivery name'))
            ->add('deliveryStreet', null, array( 'description' => 'Delivery street'))
            ->add('deliveryCity', null, array( 'description' => 'Delivery city'))
            ->add('deliveryPostcode', null, array( 'description' => 'Delivery postcode'))
            ->add('deliveryCountry', null, array( 'description' => 'Delivery country'))
            ->add('description', null, array( 'description' => 'Description'))
            ->add('invoiceName', null, array( 'description' => 'Invoice name'))
            ->add('invoiceStreet', null, array( 'description' => 'Invoice street'))
            ->add('invoiceCity', null, array( 'description' => 'Invoice city'))
            ->add('invoiceCountry', null, array( 'description' => 'Invoice country'))
            ->add('invoicePostcode', null, array( 'description' => 'Invoice postcode'))
            ->add('invoiceTaxId', null, array( 'description' => 'Invoice tax id number'))
            ->add('invoiceInfo', null, array( 'description' => 'Invoice info'))
            ->add('transportNumber', null, array( 'description' => 'Transport number'))
            ->add('client', 'text', array(
                    'description' => 'fullname',
                    'data_class' => 'Mnumi\Bundle\ClientBundle\Entity\Client'
                ))
            ->add('user', 'text', array(
                    'description' => 'username',
                    'data_class' => 'Mnumi\Bundle\RestServerBundle\Entity\User'
                ))
            ->add('orderPackageStatusName', 'transform', array(
                    'description' => 'Status name',
                    'data_class' => 'Mnumi\Bundle\OrderBundle\Entity\OrderPackageStatus',
                    'repository' => 'MnumiOrderBundle:OrderPackageStatus',
                    'transform_name' => 'name',
                ))
            ->add('wantInvoice', 'text', array( 'description' => 'Want invoice?'))

            ->add('paymentStatusName', 'text', array(
                'description' => 'Payment status',
                'required'  => true,
                'data_class' => 'Mnumi\Bundle\PaymentBundle\Entity\PaymentStatus'
            ))
            ->add('payment', 'entity', array(
                'description' => 'Payment',
                'required'  => true,
                'class' => 'Mnumi\Bundle\PaymentBundle\Entity\Payment'
            ))
            ->add('carrier', 'entity', array(
                'description' => 'Carrier',
                'required'  => true,
                'class' => 'Mnumi\Bundle\CarrierBundle\Entity\Carrier'
            ))
            ->add('items', 'collection', array(
                'type'   => new OrderItemType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false,
                'data_class' => 'Doctrine\Common\Collections\Collection'

            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'order';
    }
}
