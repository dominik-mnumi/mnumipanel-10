<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\PluginManagerBundle\Service;


/**
 * ManagerBundle class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ManagerBundle 
{
    private static $configuration;

    public static function has($name)
    {
        return in_array($name, self::$configuration);
    }

    public static function sets($config)
    {
        self::$configuration = $config;
    }

}