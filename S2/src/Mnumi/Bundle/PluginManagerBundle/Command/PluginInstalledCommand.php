<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\PluginManagerBundle\Command;

use Mnumi\Bundle\PluginManagerBundle\Service\ManagerBundle;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


/**
 * InstalledPlugins class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class PluginInstalledCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('mnumi:plugin:installed')
            ->setDescription('List of all installed plugins');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var ManagerBundle $manager */
        $manager = $this->getContainer()->get('plugin.manager');

        $output->writeln('Installed plugin:');

        foreach($manager->getInstalled() as $name) {
            $output->writeln($name);
        }
    }

} 