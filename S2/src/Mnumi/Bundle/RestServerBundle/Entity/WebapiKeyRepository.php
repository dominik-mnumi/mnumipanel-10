<?php

namespace Mnumi\Bundle\RestServerBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * WebapiKeyRepository
 */
class WebapiKeyRepository extends EntityRepository
{
    /**
     * @return int
     */
    public function findFirstUsableKey()
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.type = :type')
            ->andWhere('s.validDate >= :date')
            ->setParameter('type', 'labelprinter')
            ->setParameter('date', date("Y-m-d", time()))
            ->setMaxResults(1)
            ->getQuery()->getSingleResult();
    }
    
}
