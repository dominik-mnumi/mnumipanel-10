<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use JMS\Serializer\Annotation\Groups;

use Doctrine\ORM\PersistentCollection;
use Mnumi\Bundle\ClientBundle\Entity\Client;
use Mnumi\Bundle\ClientBundle\Entity\ClientUser;
use Mnumi\Bundle\RestServerBundle\Entity\GuardGroup;
use Mnumi\Bundle\RestServerBundle\Entity\UserProfile;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Mnumi\Bundle\LoyaltyPointBundle\Entity\LoyaltyPoint;

/**
 * User entity class
 *
 * @ORM\Entity
 * @ORM\Table(name="sf_guard_user")
 * @ORM\Entity(repositoryClass="Mnumi\Bundle\RestServerBundle\Entity\UserRepository")
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class User implements UserInterface, EquatableInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Groups({"public", "UsersList", "ClientsList", "OrdersList"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @Groups({"public", "UsersList", "ClientsList", "OrdersList"})
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @Groups({"public", "UsersList", "ClientsList", "OrdersList"})
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="email_address", type="string", length=255, nullable=false)
     * @Groups({"public", "UsersList", "ClientsList", "OrdersList"})
     */
    private $emailAddress;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     * @Groups({"public", "UsersList", "ClientsList", "OrdersList"})
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=128, nullable=true)
     * @Serializer\Exclude
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=128, nullable=true)
     * @Serializer\Exclude
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="algorithm", type="string", length=128, nullable=true)
     * @Serializer\Exclude
     */
    private $algorithm;

    /**
     * @var GuardGroup[]
     *
     * @ORM\ManyToMany(targetEntity="GuardGroup", inversedBy="users", cascade={"all"})
     * @ORM\JoinTable(name="sf_guard_user_group",
 *              joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *          inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     *          )
     * )
     * @Serializer\Exclude
     **/
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity="\Mnumi\Bundle\ClientBundle\Entity\ClientUser", mappedBy="user", cascade={"all"})
     * @Groups({"public", "UsersList"})
     */
    private $clients;

    /**
     * Order create date
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * Order update date
     * @var \DateTime
     *
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var UserProfile
     *
     * @ORM\OneToMany(targetEntity="Mnumi\Bundle\RestServerBundle\Entity\UserProfile", mappedBy="user")
     */
    private $userProfile;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Mnumi\Bundle\UserBundle\Entity\UserContact", mappedBy="user")
     */
    private $userContact;

    /**
     * @ORM\OneToMany(targetEntity="\Mnumi\Bundle\RestServerBundle\Entity\RestSession", mappedBy="user", cascade={"all"})
     * @Serializer\Exclude
     */
    private $restSessions;

    /**
     * @ORM\OneToMany(targetEntity="Mnumi\Bundle\LoyaltyPointBundle\Entity\LoyaltyPoint", mappedBy="user")
     */
    private $loyaltyPoints;

    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    public function __toString()
    {
        $user = trim(sprintf("%s %s", $this->getFirstName(), $this->getLastName()));

        if ($user != "") {
            return $user;
        }

        return $this->getEmailAddress();
    }

    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * Also implementation should consider that $user instance may implement
     * the extended user interface `AdvancedUserInterface`.
     *
     * @param UserInterface $user
     *
     * @return Boolean
     */
    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof WebserviceUser) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->getSalt() !== $user->getSalt()) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        $roleNames = array();

        foreach ($this->roles as $role) {
            $roleNames[] = "ROLE_" . strtoupper($role->getName());
        }

        return $roleNames;
    }

    /**
     * @param GuardGroup[] $roles
     */
    public function setRoles($roles)
    {
        $this->roles[] = $roles;

        return $this;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmailAddress()
    {
        return $this->emailAddress;
    }

    /**
     * @param string $emailAddress
     */
    public function setEmailAddress($emailAddress)
    {
        $this->emailAddress = $emailAddress;
    }

    /**
     * @return ClientUser
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Find client
     *
     * @param  string $fullname
     * @return mixed
     */
    public function findClient($fullname)
    {
        foreach ($this->getClients() as $item) {
            if ($item->getClient()->getFullname() == $fullname) {
                return $item->getClient();
            }
        }

        return null;
    }

    /**
     * Set createdAt
     *
     * @param  \DateTime $createdAt
     * @return User
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Add role
     *
     * @param GuardGroup $role
     *
     * @return User
     */
    public function addRole(GuardGroup $role)
    {
        $this->roles[] = $role;

        return $this;
    }

    /**
     * Remove role
     *
     * @param GuardGroup $role
     */
    public function removeRole(GuardGroup $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Add client
     *
     * @param ClientUser $client
     *
     * @return User
     */
    public function addClient(ClientUser $client)
    {
        $this->clients[] = $client;

        return $this;
    }

    /**
     * Remove client
     *
     * @param ClientUser $client
     */
    public function removeClient(ClientUser $client)
    {
        $this->clients->removeElement($client);
    }

    /**
     * Set userProfile
     *
     * @param UserProfile $userProfile
     *
     * @return User
     */
    public function setUserProfile(UserProfile $userProfile = null)
    {
        $this->userProfile = $userProfile;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param  \DateTime $updatedAt
     * @return User
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        
        return $this;
    }
    /**
     * Get userProfile
     *
     * @return UserProfile
     */
    public function getUserProfile()
    {
        return $this->userProfile;
    }

    /**
     * Get default Client for sfGuardUser.
     *
     * @return Client
     */
    public function getDefaultClient()
    {
        /** @var PersistentCollection $userProfile */
        $userProfile = $this->getUserProfile()->first();
        // first checks if user profile has defined client id
        $profileClient = $userProfile->getClient();

        if ($profileClient instanceof Client) {
            return $profileClient;
        }

        $firstClient = $this->getFirstClient();
        if ($firstClient instanceof Client) {
            return $firstClient;
        }

        return false;
    }

    /**
     * Get first client connected to user
     *
     * @return null|Client
     */
    private function getFirstClient() {
        $firstClient = null;
        $userClient = $this->getClients()->first();
        if($userClient) {
            $firstClient = $userClient->getClient();
        }
        return $firstClient;
    }

    /**
     * Set algorithm
     *
     * @param string $algorithm
     *
     * @return User
     */
    public function setAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;

        return $this;
    }

    /**
     * Get algorithm
     *
     * @return string
     */
    public function getAlgorithm()
    {
        return $this->algorithm;
    }

    /**
     * Add userProfile
     *
     * @param UserProfile $userProfile
     *
     * @return User
     */
    public function addUserProfile(UserProfile $userProfile)
    {
        $this->userProfile[] = $userProfile;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    
    /**
     * Remove userProfile
     *
     * @param UserProfile $userProfile
     */
    public function removeUserProfile(UserProfile $userProfile)
    {
        $this->userProfile->removeElement($userProfile);
    }

    /**
     * @return ArrayCollection
     */
    public function getUserContact()
    {
        return $this->userContact;
    }

    /**
     * Get loyalty points
     *
     * @return ArrayCollection
     */
    public function getLoyaltyPoints()
    {
        return $this->loyaltyPoints;
    }

    /**
     *   Get available points count
     *
     * @return integer
     */
    public function countAvailableLoyaltyPoints()
    {
        return $this->countLoyaltyPointsByStatus(LoyaltyPoint::STATUS_ACCEPTED) - $this->countLoyaltyPointsByStatus(LoyaltyPoint::STATUS_USED);
    }

    /**
     * Count loyalty points by status
     *
     * @param string $status
     * @return integer
     */
    public function countLoyaltyPointsByStatus($status)
    {
        $count = 0;

        foreach($this->getLoyaltyPoints() as $point)
        {
            if($point->getStatus() == $status) {

                $count += $point->getPoints();
            }
        }

        return $count;
    }
}
