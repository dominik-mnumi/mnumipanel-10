<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Workflow entity class
 *
 * @ORM\Table(name="workflow")
 * @ORM\Entity
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class Workflow
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=30, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=false)
     */
    private $type;

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * Set id
     *
     * @param string $id
     *
     * @return Workflow
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }
}
