<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Mnumi\Bundle\ClientBundle\Entity\Client;

/**
 * Rest session entity class
 *
 * @ORM\Entity
 * @ORM\Table(name="sf_guard_user_profile")
 */
class UserProfile {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_failed_login", type="datetime", nullable=false)
     */
    private $lastFailedLogin;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\ClientBundle\Entity\Client")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Mnumi\Bundle\RestServerBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastFailedLogin
     *
     * @param \DateTime $lastFailedLogin
     *
     * @return UserProfile
     */
    public function setLastFailedLogin($lastFailedLogin)
    {
        $this->lastFailedLogin = $lastFailedLogin;

        return $this;
    }

    /**
     * Get lastFailedLogin
     *
     * @return \DateTime
     */
    public function getLastFailedLogin()
    {
        return $this->lastFailedLogin;
    }

    /**
     * Set client
     *
     * @param Client $client
     *
     * @return UserProfile
     */
    public function setClient(Client $client = null)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * Get client
     *
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserProfile
     */
    public function setUser(User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
