<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;
use Mnumi\Bundle\OrderBundle\Entity\Order;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;


/**
 * UserRepository class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class UserRepository extends EntityRepository implements UserProviderInterface
{

    /**
     * Get user if exist and be Authenticated
     *
     * @param $username
     * @param $password
     * @return User
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationException
     * @throws \Exception
     */
    public function getUserByAuthorization($username, $password)
    {
        $user = $this->loadUserByUsername($username);

        $algorithm = $user->getAlgorithm();
        if (false !== $pos = strpos($algorithm, '::'))
        {
            $algorithm = "\\Mnumi\\Bundle\\RestServerBundle\\Library\\".$algorithm;
            $algorithm = explode('::', $algorithm);
        }
        if (!is_callable($algorithm))
        {
            throw new \Exception(sprintf('The algorithm callable "%s" is not callable.', $algorithm));
        }
        if($user->getPassword() == call_user_func_array($algorithm, array($user->getSalt().$password)))
        {
            return $user;
        }
        throw new AuthenticationException();
    }
    
    /**
     * Move current order to old basket order
     *
     * @param User $user
     * @param Order $order current order (from anonymous session)
     */
    public function moveOrdersToBasketOrder(User $user, Order $order)
    {
        $em = $this->getEntityManager();

        /* Old Order is an order user created before by, e.g. logging-in earlier */
        $oldOrder = $em->getRepository('MnumiOrderBundle:Order')->getBasketOrderForUser($user);

        if($oldOrder && ($oldOrder->getId() !== $order->getId()))
        {
            foreach($order->getItems() as $item)
            {
                $order->getItems()->removeElement($item);
                $item->setOrder($oldOrder);
                $em->persist($item);
            }
            $em->flush();
            $em->remove($order);
        }
        else
        {
            $em->persist($order);
        }
        
        $em->flush();
    }

    /**
     * Loads the user for the given username.
     *
     * This method must throw UsernameNotFoundException if the user is not
     * found.
     *
     * @param string $username The username
     *
     * @return UserInterface
     *
     * @see UsernameNotFoundException
     *
     * @throws UsernameNotFoundException if the user is not found
     */
    public function loadUserByUsername($username)
    {
        $q = $this
            ->createQueryBuilder('u')
            ->where('u.username = :username OR u.emailAddress = :email OR u.id = :id')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->setParameter('id', $username)
            ->getQuery();

        try {
            // The Query::getSingleResult() method throws an exception
            // if there is no record matching the criteria.
            $user = $q->getSingleResult();
        } catch (NoResultException $e) {
            $message = sprintf(
                'Unable to find an active admin RestServerBundle:User object identified by "%s".',
                $username
            );
            throw new UsernameNotFoundException($message, 0, $e);
        }

        return $user;
    }

    /**
     * Refreshes the user for the account interface.
     *
     * It is up to the implementation to decide if the user data should be
     * totally reloaded (e.g. from the database), or if the UserInterface
     * object can just be merged into some internal array of users / identity
     * map.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     *
     * @throws UnsupportedUserException if the account is not supported
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(
                sprintf(
                    'Instances of "%s" are not supported.',
                    $class
                )
            );
        }

        return $this->find($user->getId());
    }

    /**
     * Whether this provider supports the given user class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
        || is_subclass_of($class, $this->getEntityName());
    }
}