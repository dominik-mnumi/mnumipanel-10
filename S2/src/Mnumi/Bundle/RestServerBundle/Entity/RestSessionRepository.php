<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Entity;


use Doctrine\ORM\EntityRepository;
use Mnumi\Bundle\OrderBundle\Entity\Repository\Order as OrderRepository;

class RestSessionRepository extends EntityRepository
{
    /**
     * @param $session
     * @return null|object
     */
    private function getBasketPackageFromSession($session)
    {
        return $this->getEntityManager()->getRepository('MnumiOrderBundle:Order')->findOneBy(
            array(
                'restSession' => $session,
                'orderPackageStatusName' => OrderRepository::STATUS_BASKET
            )
        );
    }
    
    /**
     * Updates rest data with connected objects (sets user_id).
     *
     * @throws MnumiRestServerException
     */
    public function updateRestData(User $user, $session)
    {
        /** @var \Mnumi\Bundle\ClientBundle\Entity\Client $client */
        $client = $user->getDefaultClient();

        /** @var \Mnumi\Bundle\OrderBundle\Entity\Order $basketPackage */
        $basketOrder = $this->getBasketPackageFromSession($session);

        if(!$basketOrder)
        {
            return;
        }

        $basketOrder->setUser($user);
        $basketOrder->setClient($client);
        $em = $this->getEntityManager();

        $em->getRepository('MnumiOrderBundle:OrderItem')
            ->reCalculate(
                $user,
                $client,
                $basketOrder
            );

        /**
         * Move orders to basket if exist
         */
        $em->getRepository('MnumiRestServerBundle:User')->moveOrdersToBasketOrder($user, $basketOrder);

    }
} 