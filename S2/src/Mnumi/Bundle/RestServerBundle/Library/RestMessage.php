<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Bundle\RestServerBundle\Library;

/**
 * Class Codes
 * @package Mnumi\Bundle\RestServerBundle\Library
 */
class RestMessage {

    /**
     * General error.
     */
    const E_MSG_ERROR = 'E_MSG_ERROR';

    const E_MSG_SUCCESS = 'E_MSG_SUCCESS';
} 