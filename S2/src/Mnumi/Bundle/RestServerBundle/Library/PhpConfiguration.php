<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Mnumi\Bundle\RestServerBundle\Library;

/**
 * Class PHPTool
 */
class PhpConfiguration {

    /**
     * Detects max size of file can be uploaded to server
     *
     * @return float|int
     */
    public static function detectMaxUploadFileSize(){
        /**
         * Converts shorthands like "2M" or "512K" to bytes
         *
         * @param $size
         * @return mixed
         */
        $normalize = function($size) {
            if (preg_match('/^([\d\.]+)([KMG])$/i', $size, $match)) {
                $pos = array_search($match[2], array("K", "M", "G"));
                if ($pos !== false) {
                    $size = $match[1] * pow(1024, $pos + 1);
                }
            }
            return $size;
        };
        $max_upload = $normalize(ini_get('upload_max_filesize'));

        $max_post = (ini_get('post_max_size') == 0) ?
            function(){throw new Exception('Check Your php.ini settings');}
            : $normalize(ini_get('post_max_size'));

        if($max_post < $max_upload)
            return $max_post;

        $maxFileSize = min($max_upload, $max_post);
        return round($maxFileSize/1048576, 1)."mb";
    }
} 