<?php

/*
 * This file is part of the MnumiCore3 package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */



namespace Mnumi\Bundle\RestServerBundle\Library;

use Mnumi\Bundle\OrderBundle\Entity\OrderItemFile;

/**
 * PdfComponent class for manipulate PDF files
 */
class PdfComponent {

    /**
     * @var string Filename
     */
    protected $fileName = null;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var string
     */
    protected $dataDir;

    /**
     * @var array
     */
    private $coverPages;

    /**
     * @var string
     */
    protected $hash;

    /**
     * @var int
     */
    protected $pages;

    /**
     * @param string $fileName
     * @param string $path
     * @param string $dataDir
     * @param string $hash
     * @param array $coverPages
     */
    public function __construct($fileName, $path, $dataDir, $hash, $coverPages)
    {
        $this->setFileName($fileName);
        $this->setPath($path);
        $this->setDataDir($dataDir);
        $this->setHash($hash);
        $this->setPages(trim(exec('pdfinfo '.$this->getOriginalFilePath().' | grep Pages | cut -d ":" -f 2')));
        $this->setCoverPages($coverPages);
    }

    private function isPdf()
    {
        $file = $this->getOriginalFilePath();
        $ext = pathinfo($file, PATHINFO_EXTENSION);
        if($ext === "pdf" && $this->getCoverPages())
        {
            return true;
        }
        return false;
    }


    public function getCover()
    {
        if($this->isPdf() === true)
        {
            return $this->getPath().$this->getHash().'/'.$this->get($this->getCoverRange(), 'Cover');
        }
        return false;
    }

    public function getText()
    {
        if($this->isPdf() === true)
        {
            return $this->getPath().$this->getHash().'/'.$this->get($this->getTextRange(), 'Text');
        }
        return false;
    }

    private function get($range, $type)
    {
        $filename = $this->getOriginalFilePath();
        $rangeFilename = $this->getRangeRealFilePath($range, $type);

        if(file_exists($rangeFilename))
        {
            return $this->getRangeFileName($range, $type);
        }

        $exec = sprintf('pdftk %s cat %s output %s', $filename, $range, $rangeFilename);
        exec($exec);

        if (!file_exists($this->getRangeRealFilePath($range, $type))) {
            throw new \Exception('Output file ' . $rangeFilename . ' could not be created. Exec: ' . $exec);
        }
        return $this->getRangeFileName($range, $type);
    }

    /**
     * @param $range
     * @return bool|string
     */
    private function getRangeRealFilePath($range, $type)
    {
        return $this->getDataDir().'/'.$this->getPath().$this->getRangeFileName($range, $type);
    }

    /**
     * @param $type
     * @return bool|string
     */
    private function getRangeFileName($range, $type)
    {
        $range = md5(preg_replace('/\D/', '', $range));
        if($this->getOriginalFilePath() !== false)
        {
            return $type.'_'.$range.'_'.$this->getFilename();
        }

        return false;
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getOriginalFilePath()
    {
        $filename = realpath($this->getDataDir().'/'.$this->getPath().$this->getFilename());

        if (!file_exists($filename)) {
            throw new \Exception('Missing filename: ' . $filename);
        }

        return $filename;
    }

    public function getOriginalHashedFilePath()
    {
        return $this->getPath().$this->getHash().'/'.$this->getFilename();
    }

    /**
     * @return string
     */
    private function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    private function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }


    /**
     * @return mixed
     */
    private function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    private function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    private function getDataDir()
    {
        return $this->dataDir;
    }

    /**
     * @param string $dataDir
     */
    private function setDataDir($dataDir)
    {
        $this->dataDir = $dataDir;
    }

    /**
     * @return string
     */
    private function getCoverRange()
    {
        return implode(" ", $this->coverPages);
    }

    /**
     * @return string
     */
    private function getTextRange()
    {
        $textPages = array();
        for ($i=1;$i<=$this->getPages();$i++)
        {
            if(!in_array($i, $this->coverPages))
            {
                $textPages[] = $i;
            }
        }
        return implode(" ", $textPages);
    }

    /**
     * @param array $coverPages
     */
    private function setCoverPages($coverPages)
    {
        foreach ($coverPages as &$cover)
        {
            if($cover == "penultimate")
            {
                $cover = $this->getPages() - 1;
            } else if($cover == "last")
            {
                $cover = $this->getPages();
            }
        }

        $this->coverPages = $coverPages;
    }

    private function getCoverPages()
    {
        if($this->coverPages)
        {
            return $this->coverPages;
        }
        return false;
    }

    /**
     * @return string
     */
    private function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    private function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return int
     */
    private function getPages()
    {
        return $this->pages;
    }

    /**
     * @param int $pages
     */
    private function setPages($pages)
    {
        $this->pages = intval($pages);
    }

} 