<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * @Annotations\NamePrefix("api_fotolia_")
 */
class FotoliaController extends FOSRestController
{
    /**
     * Get attributes for product.
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @Annotations\View(
     *  templateVar="get"
     * )
     *
     * @Method({"GET"})
     * @Annotations\Get("/fotolia/{phrase}/{limit}/{offset}/{languageId}/search")
     *
     * @param $phrase
     *
     * @return array
     */
    public function getFotoliaSearchAction($phrase, $limit, $offset, $languageId)
    {
        $fotoliaClient = $this->get('fotolia.client.adapter');
        $result = $fotoliaClient->getSearchResults($phrase, $limit, $offset, $languageId);

        return $result;
    }
}
