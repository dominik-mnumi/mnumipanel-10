<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Mnumi\Bundle\RestServerBundle\Exception\UnknownFileExtensionException;
use Mnumi\Bundle\RestServerBundle\Exception\CurlFailedException;
use Symfony\Component\Filesystem\Filesystem;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\Controller\FOSRestController;
use Mnumi\Bundle\OrderBundle\Entity\OrderItemFile;
use Mnumi\Bundle\OrderBundle\Entity\Repository\OrderItemFile as OrderItemFileRepo;
use Mnumi\Bundle\RestServerBundle\Handler\OrderItemHandler;
use Symfony\Component\Validator\Constraints as ValidatorsConstraints;

/**
 * @Annotations\NamePrefix("api_order_item_")
 */
class OrderItemController extends FOSRestController
{
    /**
     * Update order item status
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  parameters = {
     *      {
     *          "name" = "orderStatusName",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "Status name from MnumiCore"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"PATCH"})
     * @Annotations\Patch("/orders/{id}/item/status")
     * @Annotations\Patch("/orders/item/{id}/status")
     *
     * @Annotations\View(templateVar="patch")
     */
    public function patchOrderItemStatusAction($id, Request $request)
    {
        try {
            $data = $request->request->all();
            $order = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:OrderItem')->find($id);
            if ($order !== null && $data !== null) {
                $newObject = $this->getHandler()->patch(
                    $order,
                    $data
                );

                return $newObject;
            }
        } catch (\Exception $exception) {
            return View::create($exception->getMessage(), 400);
        }
    }

    /**
     * Add File to the OrderItem.
     *
     * @ApiDoc(
     *   resource=true,
     *   authentication = true,
     *   authenticationRoles = {
     *      "ROLE_USER"
     *   },
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when the order item is not found"
     *   },
     *   requirements = {
     *      {
     *          "name" = "id",
     *          "required" = true,
     *          "dataType" = "int",
     *          "description" = "OrderItem ID"
     *      }
     *  },
     *  parameters = {
     *      {
     *          "name" = "url",
     *          "required" = true,
     *          "dataType" = "string",
     *          "description" = "URL to fetch file from"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"POST"})
     * @Annotations\Post("/orders/item/{id}/files")
     *
     * @Annotations\View(templateVar="post")
     *
     * @throws NotFoundHttpException
     */
    public function postOrderItemFileAction(Request $request, $id)
    {
        $orderItem = $this->getHandler()->get($id);
        if (!$orderItem) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }

        $url = $request->get('url');

        if ($url === null) {
            throw new NotFoundHttpException(sprintf('The url has not been set.'));
        }

        try {
            $fetchedFile = $this->fetchFileFromUrl($url);
            $extension = $fetchedFile->guessExtension();

            if ($extension === null) {
                throw new UnknownFileExtensionException(
                    sprintf('Extension for file: %s cannot be guessed', $fetchedFile->getFilename())
                );
            }

            $dataDir = $this->container->getParameter('data_dir');

            $em = $this->getDoctrine()->getManager();

            $orderItem->preparePath($dataDir);
            $filePath = $dataDir . '/' . $orderItem->getFilepath();
            $file = $fetchedFile->move($filePath, $fetchedFile->getFilename() . '.' . $extension);

            $orderItemFile = new OrderItemFile();
            $orderItemFile->setOrderItem($orderItem);
            $orderItemFile->setFilename($file->getFilename());
            $orderItemFile->setMetadataFromFile($file);

            $em->persist($orderItemFile);
            $em->flush();
        } catch (\Exception $e) {
            throw new HttpException(400, $e->getMessage(), $e);
        }

        return $orderItemFile;
    }

    /**
     * Add File to the OrderItem.
     *
     * @ApiDoc(
     *   resource=true,
     *   authentication = true,
     *   authenticationRoles = {
     *      "ROLE_USER"
     *   },
     *   statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when the OrderItem is not found"
     *  },
     *  deprecated = true,
     *  parameters = {
     *      {
     *          "name" = "id",
     *          "required" = true,
     *          "dataType" = "int",
     *          "description" = "OrderItem ID"
     *      },
     *      {
     *          "name" = "url",
     *          "required" = true,
     *          "dataType" = "string",
     *          "description" = "URL to fetch file from"
     *      }
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @Annotations\View(templateVar="post")
     *
     * @throws NotFoundHttpException
     */
    public function postOrderItemFileFetchAction(Request $request)
    {
        $id = $request->get('id');

        return $this->postOrderItemFileAction($request, $id);

    }

    /**
     * @param  string $url
     * @return File
     */
    protected function fetchFileFromUrl($url)
    {
        $filepath = tempnam(sys_get_temp_dir(), "RemoteOrderItemFile-");
        $fs = new Filesystem();
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);

        $fp = fopen($filepath, 'w+');

        curl_setopt($ch, CURLOPT_FILE, $fp);

        if (curl_exec($ch) === false) {
            throw new CurlFailedException('Fetching file failed', $ch);
        }

        curl_close($ch);
        fclose($fp);

        return new File($filepath);
    }

    /**
     * Get an order items
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  }
     * )
     *
     * @Annotations\View(
     *  templateVar="get"
     * )
     * @Annotations\Get("/order/item/{order_id}/{fotolia_id}/importfotoliaimage")
     * @Method({"GET"})
     *
     * @return array
     */
    public function getImportFotoliaImageAction($order_id, $fotolia_id)
    {
        $dataDir = $this->container->getParameter('data_dir');
        $orderItemRepo = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:OrderItem');
        $orderItemFileRepo = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:OrderItemFile');

        $orderItem = $orderItemRepo->findOneBy(array('id' => $order_id));

        if($orderItem === null) {
            throw new \Exception(sprintf('OrderItem with given id [%d] does not exits', $order_id));
        }

        $fotoliaClient = $this->get('fotolia.client.adapter');
        $mediaData = $fotoliaClient->getMediaData($fotolia_id, 500);

        $orderItemFile = $orderItemFileRepo->findOrCreate(
                $orderItem,
                $mediaData->getFilename(),
                OrderItemFileRepo::TYPE_FOTOLIA);

        $orderItemFile = $mediaData->updateThumbOrderItemFile($orderItemFile, $dataDir);

        $this->getDoctrine()->getManager()->flush();

        return array('photo_id' => $orderItemFile->getId());
    }

    /**
     * @return OrderItemHandler
     */
    protected function getHandler()
    {
        return $this->container->get('rest.orderitem.handler');
    }
}
