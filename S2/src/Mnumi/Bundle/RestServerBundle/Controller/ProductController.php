<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder;
use Mnumi\Bundle\ProductBundle\Entity\Product;
use Nelmio\ApiDocBundle\DataTypes;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Annotations\NamePrefix("api_product_")
 */
class ProductController extends FOSRestController
{
    /**
     * Get a single product by slug
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when not found"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\ProductBundle\Entity\Product"
     *  },
     *  requirements = {
     *      {
     *          "name"="product",
     *          "dataType"="string",
     *          "requirement"="[\w\d\-\_]+",
     *          "description"="Product slug"
     *      }
     *  }
     * )
     *
     * @Method({"GET"})
     * @Template()
     */
    public function getProductAction($product, Request $request)
    {
        if (!($product = $this->getHandler()->getOneBy(array('slug' => $product)))) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$product));
        }
        $product = $this->filterFields($product, $request);
        return $product;
    }

    /**
     * Get a all products
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when not found"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "array<Mnumi\Bundle\ProductBundle\Entity\Product> as products"
     *  },
     *  filters = {
     *      {
     *          "name"="fields_filter[]",
     *          "dataType"="array",
     *          "required" = false,
     *          "description"="Fields filter. You can use more than one"
     *      },
     *      {
     *          "name"="offset",
     *          "dataType"="number",
     *          "description"="Offset from which to start listing pages."
     *      },
     *      {
     *          "name"="limit",
     *          "dataType"="number",
     *          "default":"null",
     *          "description"="How many pages to return."
     *      },
     *      {
     *          "name"="sort",
     *          "dataType"="string",
     *          "default":"id",
     *          "description"="Sort By field"
     *      },
     *      {
     *          "name"="order",
     *          "dataType"="string",
     *          "default":"ASC",
     *          "description"="Order By ASC|DESC"
     *      },
     *      {
     *          "name" = "simple",
     *          "dataType" = "boolean",
     *          "default" = "0",
     *          "description" = "Show full product data"
     *      }
     *  }
     * )
     *
     * @Method({"GET"})
     * @Annotations\View(serializerGroups={"ProductList"})
     */
    public function getProductsAction(Request $request)
    {
        $simple = (bool)$request->query->get('simple', 0);
        $offset = $request->query->get('offset', 0);
        $limit = $request->query->get('limit', null);
        $sort = $request->query->get('sort', 'id');
        $order = $request->query->get('order', 'ASC');
        try {
            $products = $this->getDoctrine()->getManager()
                ->getRepository('MnumiProductBundle:Product')
                ->findBy(array(), array($sort => strtoupper($order)), $limit, $offset);
        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }

        if (!$products)
        {
            throw new NotFoundHttpException('Not found');
        }

        $products = $this->filterFields($products, $request);
        if($simple === true)
        {
            return View::create($products)->setSerializationContext(
                SerializationContext::create()->setGroups(
                    array('ProductSimpleList')
                )
            );
        }
        return $products;
    }

    /**
     * Create an order
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  }
     * )
     *
     * @Method({"GET"})
     *
     * @Annotations\Get("/product/{slug}/sizes")
     * @Annotations\View(templateVar="get")
     */
    public function getProductSizesAction($slug)
    {
        $productRepository = $this->getDoctrine()->getRepository('MnumiProductBundle:FieldItemSize');
        $sizes = $productRepository->getByProductSlug($slug);

        return $sizes;
    }

    /**
     * Use filter for fields
     *
     * @param array|Product $products
     * @param Request $request
     * @return Product
     */
    private function filterFields($products, Request $request)
    {
        $fieldFilter = array_filter($request->query->get('fields_filter', array()));

        if($products instanceof Product)
        {
            if($fieldFilter) {
                $products = $this->filterProductFields($products, $fieldFilter);
            }
        } else {
            foreach ($products as &$product) {
                $product = $this->filterProductFields($product, $fieldFilter);
            }
        }
        return $products;
    }

    /**
     * Filter fields by $fieldFilter
     *
     * @param Product $product
     * @param $fieldFilter
     * @return Product
     */
    private function filterProductFields(Product $product, $fieldFilter)
    {
        $fields = new ArrayCollection();
        $productFields = $product->getFields();
        /** @var \Mnumi\Bundle\ProductBundle\Entity\ProductField $pf */
        foreach ($productFields as $pf)
        {
            if(in_array($pf->getFieldset()->getName(), $fieldFilter))
            {
                $fields->add($pf);
            }
        }
        /** @var ArrayCollection $fields */
        $product->setFields($fields);
        return $product;
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.product.handler');
    }
}
