<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use Mnumi\Bundle\RestServerBundle\Entity\RestSession;
use Mnumi\Bundle\RestServerBundle\Entity\User;
use FOS\RestBundle\Util\Codes;
use Mnumi\Bundle\RestServerBundle\Library\RestMessage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;


/**
 * @Annotations\NamePrefix("api_user_")
 */
class UserController extends FOSRestController
{
    /**
     * Login action
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful logged in",
     *      400 = "Returned when error",
     *      401 = "Returned when user doesn't hve access",
     *  },
     *  parameters = {
     *      {
     *          "name" = "username",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "Username or email address"
     *      },
     *      {
     *          "name" = "password",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "User password"
     *      },
     *      {
     *          "name" = "session",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "User session"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     * @Method({"POST"})
     * @Annotations\Post("/dologin")
     * @Template()
     */
    public function postDologinAction(Request $request)
    {
        try {
            /** @var User $user */
            $user = $this->getDoctrine()->getManager()
                ->getRepository('MnumiRestServerBundle:User')
                ->getUserByAuthorization($request->get('username'), $request->get('password'));

            $sessionHandle = $this->signInRest($request->get('session'), $user);

        } catch (AuthenticationException $e)
        {
            throw new HttpException(401, 'Problem occured when trying to login user', $e);
        }

        return $this->view(
            array(
                'code' => RestMessage::E_MSG_SUCCESS,
                'session_handle' => $sessionHandle,
                'user_id' => $user->getId(),
                'server_time' => time()
            )
        );
    }

    /**
     * Returns session id. If session was created before, then update it with
     * user id.
     *
     * @param string $sessionHandle
     * @param User $user
     * @return string
     */
    private function signInRest($sessionHandle, User $user)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var RestSession $restSession */
        $restSession = $em->getRepository('MnumiRestServerBundle:RestSession')->findOneBy(array('id' => $sessionHandle));

        // if rest session does exist
        if(!$restSession)
        {
            $restSession = new RestSession($sessionHandle);
            $restSession->setUser($user);

            $em->persist($restSession);
            $em->flush($restSession);

        }
        $em->getRepository('MnumiRestServerBundle:RestSession')->updateRestData($user, $restSession);

        // sets user id
        $restSession->setUser($user);
        $em->persist($restSession);
        $em->flush();
        
        $sessionHandle = $restSession->getId();

        return $sessionHandle;
    }
    
    /**
     * Search user by email address or get all
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when not found"
     *  },
     *  output = {
     *      "class" = "array<Mnumi\Bundle\RestServerBundle\Entity\User> as User",
     *      "groups" = {"UserLists"}
     *  },
     *  parameters = {
     *      {
     *          "name" = "emailAddress",
     *          "dataType" = "string",
     *          "required" = false,
     *          "description" = "User Email address"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     * @Method({"GET"})
     * @Annotations\View(serializerGroups={"UsersList"})
     */
    public function getUsersAction(Request $request)
    {
        try {
            $emailAddress = $request->get('emailAddress', null);
            
            if ($emailAddress) {
                $data = array(
                    'emailAddress' => $emailAddress,
                );
                $return = $this->getHandler()->getOneBy($data);
            } else {
                $return = $this->getDoctrine()->getManager()
                    ->getRepository('MnumiRestServerBundle:USer')->findAll();
            }
        } catch (\Exception $e)
        {
            throw new HttpException(400, $e->getMessage(), $e);
        }

        if($return) {
            return $return;
        }
        throw new NotFoundHttpException("Not found");
    }

    /**
     * Create new user
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when not found"
     *  },
     *  input = {
     *      "class" = "Mnumi\Bundle\RestServerBundle\Form\Type\UserType",
     *      "name" = ""
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\RestServerBundle\Entity\User",
     *      "groups" = {"public"}
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @Annotations\View(templateVar="post")
     */
    public function postUserAction(Request $request)
    {
        try {
            $newObject = $this->getHandler()->post(
                $request->request->all()
            );

            $routeOptions = array(
                'id' => $newObject->getId(),
                '_format' => $request->get('_format')
            );

            return $newObject;
           return $this->routeRedirectView('api_user_post_user', $routeOptions, Codes::HTTP_CREATED);

        } catch (InvalidFormException $exception) {
            return $exception->getForm();
        }
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.user.handler');
    }
}
