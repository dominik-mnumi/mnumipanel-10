<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Util\Codes;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Annotations\NamePrefix("api_carrier_")
 */
class CarrierController extends FOSRestController
{
    /**
     * Get carrier by id/name or all.
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Carrier not found"
     *  },
     *  filters = {
     *      {"name"="id", "dataType"="integer", "description"="ID of Carrier"},
     *      {"name"="name", "dataType"="string", "description"="Name of Carrier"}
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\CarrierBundle\Entity\Carrier",
     *      "groups"={"public"}
     *  }
     * )
     * @Annotations\View(
     *  templateVar="carrier"
     * )
     *
     * @Method({"GET"})
     *
     * @param Request $request the request object
     *
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return array
     */
    public function getCarriersAction(Request $request)
    {
        $result = null;
        $data = array();
        $all = $request->query->all();
        foreach ($all as $key => $value) {
            if ($value !== null) {
                $data[$key] =  $value;
            }
        }
        if ($data || $all) {
            $result = $this->getHandler()->getOneBy(
                $data
            );
        } else {
            $result = $this->getHandler()->all();
        }
        
        if($result === null)
        {
            throw new NotFoundHttpException('Not found.');
        }
        return $result;
    }

    /**
     * Create a carrier
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  authenticationRoles={
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *     200 = "Created",
     *     302 = "Already exists",
     *     406 = "Can't add the carrier"
     *  },
     *  input = {
     *      "class"="Mnumi\Bundle\CarrierBundle\Form\Type\CarrierType",
     *      "name"=""
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\CarrierBundle\Entity\Carrier",
     *      "groups"={"public"}
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @Annotations\View(templateVar="post")
     */
    public function postCarrierAction(Request $request)
    {
        try {
            $carrier = $this->getHandler()->getBy(
                    array('name' => $request->request->get('name'))
            );
            if (!$carrier->isEmpty()) {
                return $this->view($carrier[0], Codes::HTTP_FOUND);
            }
            return $this->getHandler()->post(
                    $request->request->all()
            );
        } catch (\Exception $exception) {
            throw new HttpException(406);
        }
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.carrier.handler');
    }

}
