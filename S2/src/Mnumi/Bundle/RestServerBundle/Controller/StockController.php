<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Mnumi\Bundle\RestServerBundle\Exception\InvalidFormException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use FOS\RestBundle\View\View;

/**
 * @Annotations\NamePrefix("api_stock_")
 */
class StockController extends FOSRestController
{
    /**
     * Get stock value for field item
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  authenticationRoles={
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *     200 = "Requested when success",
     *     404 = "Requested when field item has not been found",
     *  },
     *  requirements = {
     *      {
     *          "name"=  "itemId",
     *          "dataType" = "integer",
     *          "description" = "FieldItem ID"
     *      }
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\ProductBundle\Entity\FieldItem",
     *      "groups"={"fieldItemStock"}
     *  },
     *  tags = {
     *      "alfa" = "#ffa500"
     *  }
     * )
     *
     * @Method({"GET"})
     * @Annotations\Get("/stock/item/{itemId}")
     * @Annotations\View(serializerGroups={"fieldItemStock"})
     */
    public function getStockFieldItemAction($itemId, Request $request)
    {
        try {
            /** @var \Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem */
            $fieldItem = $this->getFieldItem($itemId);
        } catch (NotFoundHttpException $e)
        {
            return View::create(sprintf('Field item \'%s\' was not found.', $itemId), 404);
        }
        
        return $fieldItem;
    }

    /**
     * Set stock value for field item
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  authenticationRoles={
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *     200 = "Requested when success",
     *     302 = "Stock already has that value",
     *     400 = "Requested when error",
     *     404 = "Requested when field item has not been found",
     *     424 = "Requested when missing dependencies"
     *  },
     *  requirements = {
     *      {
     *          "name"=  "itemId",
     *          "dataType" = "integer",
     *          "description" = "FieldItem ID"
     *      }
     *  },
     *  parameters = {
     *      {
     *          "name" = "value",
     *          "dataType" = "integer",
     *          "required" = true,
     *          "description" = "stock availability - must be greater than 0"
     *      }
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\ProductBundle\Entity\FieldItem",
     *      "groups"={"fieldItemStock"}
     *  },
     *  tags = {
     *      "alfa" = "#ffa500"
     *  }
     * )
     *
     * @Method({"PATCH"})
     * @Annotations\Patch("/stock/item/{itemId}")
     * @Annotations\View(serializerGroups={"fieldItemStock"})
     */
    public function patchStockFieldItemAction($itemId, Request $request)
    {

        $value = $request->request->get('value', null);
        if($value === null || !is_numeric($value) || $value < 0)
        {
            return View::create(sprintf('Value "%s" is not valid', $value), 424);
        }

        try {
            /** @var \Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem */
            $fieldItem = $this->getFieldItem($itemId);
        } catch (NotFoundHttpException $e)
        {
            return View::create(sprintf('Field item \'%s\' was not found.', $itemId), 404);
        }

        $previousValue = $fieldItem->getAvailability();
        if(floatval($previousValue) === floatval($value))
        {
            return View::create("Stock already has that value", 302);
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $fieldItem->setAvailability(floatval($value));
            $em->persist($fieldItem);
            $em->flush();
        } catch (\Exception $e)
        {
            return View::create($e->getMessage(), 400);
        }

        return $fieldItem;
    }
    
    private function getFieldItem($itemId)
    {
        $em = $this->getDoctrine()->getManager();
        /** @var \Mnumi\Bundle\ProductBundle\Entity\FieldItem $fieldItem */
        $fieldItem = $em->getRepository('MnumiProductBundle:FieldItem')->find($itemId);
        if (!$fieldItem) {
            throw new NotFoundHttpException(sprintf('Field item \'%s\' was not found.', $itemId));
        }
        return $fieldItem;
    }
}
