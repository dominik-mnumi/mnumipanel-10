<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use Mnumi\Bundle\OrderBundle\Entity\Order;
use Mnumi\Bundle\OrderBundle\Entity\OrderItem;
use Mnumi\Bundle\OrderBundle\Entity\OrderItemFile;
use Mnumi\Bundle\OrderBundle\Form\Type\OrderType;
use Mnumi\Bundle\RestServerBundle\Library\PdfComponent;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Mnumi\Bundle\CalculationBundle\Library\ProductCalculationProductData;
use Mnumi\Bundle\CalculationBundle\Library\CalculationTool;
use Mnumi\Bundle\WizardBundle\Library\WizardManager;
use Symfony\Component\HttpFoundation\JsonResponse;
use Mnumi\Bundle\OrderBundle\Entity\OrderItemStatus;

/**
 * @Annotations\NamePrefix("api_order_")
 */
class OrderController extends FOSRestController
{
    /**
     * List of all orders including the filters
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when orders not found"
     *  },
     *  filters = {
     *      {"name"="offset", "dataType"="number", "description"="Offset from which to start listing pages."},
     *      {"name"="limit", "dataType"="number", "default":"5", "description"="How many pages to return."},
     *      {"name"="sort", "dataType"="string", "default":"id", "description"="Sort By field"},
     *      {"name"="order", "dataType"="string", "default":"asc", "description"="Order By ASC|DESC"}
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  },
     *  output = {
     *      "class" = "array<Mnumi\Bundle\OrderBundle\Entity\Order> as orders",
     *      "groups"={"OrdersList"}
     *  }
     * )
     *
     * @Method({"GET"})
     *
     * @param Request $request the request object
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return array
     *
     * @Annotations\View(templateVar="get", serializerGroups={"OrdersList"})
     */
    public function getOrdersAction(Request $request)
    {
        try {
            $query = $request->query->all();
            $offset = $request->query->get('offset', 0);
            $limit = $request->query->get('limit', 5);
            $sort = $request->query->get('sort', 'id');
            $order = $request->query->get('order', 'ASC');
            unset($query['order']);
            unset($query['limit']);
            unset($query['offset']);
            unset($query['sort']);
            if ($query) {
                $return = $this->getHandler()->allBy($query, $sort, $order, $limit, $offset);
            } else {
                $return = $this->getHandler()->all($sort, $order, $limit, $offset);
            }
        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }
        if ($return) {
            return $return;
        }
        throw new NotFoundHttpException();
    }

    /**
     * Create an order
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  input = {
     *      "class" = "Mnumi\Bundle\OrderBundle\Form\Type\OrderType"
     *  },
     *  output = {
     *      "class" = "Mnumi\Bundle\OrderBundle\Entity\Order",
     *      "groups"={"public"}
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"POST"})
     *
     * @Annotations\View(templateVar="post")
     */
    public function postOrdersAction(Request $request)
    {
        try {
            $data = $request->request->all();
            foreach ($data['order']['items'] as &$attribute) {
                $tmp = array();
                foreach ($attribute['attributes'] as $key => $value) {
                    if ($value['field'] == "OTHER") {
                        $tmp[strtoupper($value['field'])][$key] = array('name' => strtoupper($value['field']), 'value' => $value['value']);
                    } else {
                        $tmp[strtoupper($value['field'])] = array('name' => strtoupper($value['field']), 'value' => $value['value']);
                    }
                }
                if ($attribute['customSize']) {
                    foreach ($attribute['customSize'] as $item) {
                        $tmp['SIZE']['width']['value'] = $item['width'];
                        $tmp['SIZE']['height']['value'] = $item['height'];
                        $tmp['SIZE']['metric']['value'] = isset( $item['metric'] ) ? $item['metric'] : 'mm';
                    }
                }
                $product = $this->getDoctrine()->getManager()->getRepository('MnumiProductBundle:Product')->findOneBy(array('slug' => $attribute['product']));
                $productData = new ProductCalculationProductData($this->getDoctrine()->getManager(), $product, $this->container->getParameter('price_tax')*100);

                $calculationTool = new CalculationTool($productData->getCalculationProductDataObject());
                $calculationTool->initialize($tmp, $productData->getCalculationProductDataObject()->getDefaultPriceListId());

                $priceReportArr = $calculationTool->fetchReport();
                if ($priceReportArr) {
                    $attribute['priceNet'] = $priceReportArr['summaryPriceNet'];
                    $attribute['totalAmount'] = ($priceReportArr['summaryPriceNet']);
                    $attribute['baseAmount'] = $priceReportArr['summaryPriceNet'];
                }
            }
            $data['order']['orderPackageStatusName'] = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:OrderPackageStatus')->findOneBy(array('name' => "waiting"));
            /** @var Order $newObject */
            $newObject = $this->getHandler()->post(
                $data
            );

            return $this->view($newObject);
        } catch (\Exception $exception) {
            throw new HttpException(400);
        }
    }

    /**
     * Get order transport number
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error",
     *      404 = "Returned when not found"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"GET"})
     *
     * @Annotations\View(templateVar="get")
     */
    public function getOrderTransportAction($id)
    {
        $manager = $this->getDoctrine()->getManager();
        /** @var \Mnumi\Bundle\OrderBundle\Entity\Order $order */
        $order = $manager->getRepository('MnumiOrderBundle:Order')->find($id);

        if($order === null)
        {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
        }

        if($order->getTransportNumber() !== null)
        {
            return $this->view($order->getTransportNumber());
        }

        throw new HttpException(400);
    }

    /**
     * Update order status
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  parameters={
     *      {
     *          "name" = "transportNumber",
     *          "dataType" = "string",
     *          "required" = true,
     *          "description" = "Transport number"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"PATCH"})
     *
     * @Annotations\View(templateVar="patch")
     */
    public function patchOrderTransportAction($id, Request $request)
    {
        try {
            $data = $request->request->all();
            $manager = $this->getDoctrine()->getManager();
            $order = $manager->getRepository('MnumiOrderBundle:Order')->find($id);
            if($order === null)
            {
                throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.',$id));
            }
            if($order !== null && $data !== null)
            {
                /** @var Order $newObject */
                $newObject = $this->getHandler()->patch(
                    $order,
                    $data
                );
                $newObject->setSendAt(new \DateTime('now'));
                $manager->persist($newObject);
                $manager->flush($newObject);
                return $newObject;
            }
            throw new HttpException(400, 'Parameters can not be empty');
        } catch (\Exception $exception) {
            throw new HttpException(400);
        }
    }

    /**
     * Get order status
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when not found"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"GET"})
     *
     * @Annotations\View(templateVar="get")
     */
    public function getOrderStatusAction($id)
    {
        /** @var \Mnumi\Bundle\OrderBundle\Entity\Order $order */
        $order = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:Order')->find($id);
        if($order !== null)
        {
            return $this->view($order->getOrderPackageStatusName());
        }
        throw new NotFoundHttpException();
    }

    /**
     * Update order status
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  parameters={
     *      {
     *          "name"="orderPackageStatusName",
     *          "dataType"="string",
     *          "required" = true,
     *          "description"="Status name from MnumiCore"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"PATCH"})
     *
     * @Annotations\View(templateVar="patch")
     */
    public function patchOrderStatusAction($id, Request $request)
    {
        try {
            $data = $request->request->all();
            $order = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:Order')->find($id);
            if($order !== null && $data !== null)
            {
                $newObject = $this->getHandler()->patch(
                    $order,
                    $data
                );
                return $newObject->getOrderPackageStatusName();
            }
        } catch (\Exception $exception) {
            return View::create($exception->getMessage(), 400);
        }
    }

    /**
     * Get all order files
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when error"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     * @Annotations\QueryParam(name="coverPages", requirements="\d+", description="Cover pages count")
     * @Method({"GET"})
     *
     * @Annotations\View(templateVar="get")
     */
    public function getOrderItemsFilesAction($id, Request $request)
    {
        try {
            $orderFiles['status'] = 'empty';
            $coverPages = preg_replace('/\s+/', '', $request->query->get('coverPages'));
            if($coverPages) {
                $coverPages = explode(",", $coverPages);
            } else {
                $coverPages = array();
            }

                /** @var OrderItem $item */
                $item = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:OrderItem')->find($id);
            if($item)
            {
                if(!$item->getFiles()->isEmpty())
                {
                    /** @var OrderItemFile[] $files */
                    $files = $item->getFiles();
                    $path = $item->getFilePath();
                    foreach ($files as $file)
                    {
                            $pdfComponent = new PdfComponent(
                                $file->getFilename(),
                                $path,
                                $this->container->getParameter('data_dir'),
                                md5($item->getCreatedAt()->getTimestamp()),
                                $coverPages
                            );
                            $fileData = array(
                                'all' => array(
                                    'path' => $this->get('request')->getSchemeAndHttpHost() . '/app.php/' . $pdfComponent->getOriginalHashedFilePath(),
                                    'metadata' => array(
                                        'pdfPagesNumber' => $file->getMetadataAttribute('nrOfPages'),
                                        'format' => $file->getMetadataAttribute('format'),
                                    ),
                                )
                            );
                            $cover = $pdfComponent->getCover();
                            $text = $pdfComponent->getText();
                            if ($cover !== false) {
                                $fileData['cover'] = array(
                                    'path' => $this->get('request')->getSchemeAndHttpHost() . '/app.php/' . $cover
                                );
                            }
                            if ($text !== false) {
                                $fileData['text'] = array(
                                    'path' => $this->get('request')->getSchemeAndHttpHost() . '/app.php/' . $text
                                );
                            }
                            $orderFiles['status'] = 'ok';
                            $orderFiles['files'][] = $fileData;

                    }
                }
                $attributes = $item->getAttributes();
                if(!$attributes->isEmpty()) {
                    foreach ($attributes as $attribute) {
                        if ($attribute->getField()->getFieldset()->getName() == "WIZARD") {
                            $statusData = json_decode(
                                file_get_contents(
                                    WizardManager::getInstance($this->container, $request)->generatePdfQueueAddUrl(
                                        $attribute->getValue(),
                                        $item->getId()
                                    )
                                ),
                                true
                            );
                            $orderFiles['status'] = 'waiting';
                            if ($statusData['result']['status']['code'] == "ready") {
                                $orderFiles['status'] = 'ok';
                                $link = WizardManager::getInstance(
                                    $this->container,
                                    $request
                                )->generatePdfQueueStatusUrl($attribute->getValue(), $item->getId(), null, null, '1');

                                $orderFiles['cover']['path'] = $link;

                                $link = WizardManager::getInstance(
                                    $this->container,
                                    $request
                                )->generatePdfQueueStatusUrl($attribute->getValue(), $item->getId(), null, null, '2-end');
                                $orderFiles['text']['path'] = $link;

                                $link = WizardManager::getInstance(
                                    $this->container,
                                    $request
                                )->generatePdfQueueStatusUrl($attribute->getValue(), $item->getId(), null, null);
                                $orderFiles['all']['path'] = $link;
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e)
        {
            throw new HttpException(400);
        }
        return $this->view($orderFiles);
    }

    /**
     * Get an order items
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when the page is not found"
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     *
     * @Method({"GET"})
     *
     * @Annotations\View(serializerGroups={"OrdersList"})
     */
    public function getOrderAction($id, Request $request)
    {
        /** @var Order $order */
        $order = $this->getHandler()->get($id);
        if (!$order) {
            throw new NotFoundHttpException(sprintf('The resource \'%s\' was not found.', $id));
        }
	    $items = $order->getItems();
        /** @var OrderItem $item */
        foreach ($items as $item)
        {
            if(in_array(
                $item->getOrderStatusName()->getName(), 
                array(OrderItemStatus::STATUS_DELETED, OrderItemStatus::STATUS_CLOSED, OrderItemStatus::STATUS_CALCULATION, ""))
            )
            {
                $items->removeElement($item);
            }
        }
        return $order;
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.order.handler');
    }
}
