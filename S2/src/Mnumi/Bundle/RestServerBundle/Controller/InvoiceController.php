<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */


namespace Mnumi\Bundle\RestServerBundle\Controller;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use FOS\RestBundle\View\View;
use Mnumi\Bundle\InvoiceBundle\Entity\Invoice;
use Mnumi\Bundle\InvoiceBundle\Entity\InvoiceItem;
use Mnumi\Bundle\InvoiceBundle\Library\InvoiceFormat;
use Mnumi\Bundle\InvoiceBundle\Library\InvoiceService;
use Mnumi\Bundle\OrderBundle\Entity\Order;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Annotations\NamePrefix("api_invoice_")
 */
class InvoiceController extends FOSRestController
{
    /**
     * Get attributes for product.
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      404 = "Returned when not found",
     *  },
     *  requirements = {
     *      {
     *          "name" = "id",
     *          "required" = "false",
     *          "dataType" = "integer",
     *          "description" = "Invoice ID"
     *      }
     *  },
     *  tags = {
     *      "stable"
     *  }
     * )
     *
     * @param $id
     *
     * @return array
     */
    public function getInvoiceAction($id)
    {
        return $this->view(
            $this->getHandler()->get($id)
        );
    }

    /**
     * Generate Invoice for Order
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  authenticationRoles={
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *     200 = "Requested when success",
     *     302 = "Requested when the order have invoice",
     *     400 = "Requested when error",
     *     404 = "Requested when order has not been found"
     *  },
     *  requirements = {
     *      {
     *          "required" = true,
     *          "name" = "order",
     *          "dataType" = "number",
     *          "description" = "Order ID"
     *      }
     *  },
     *  parameters = {
     *      {
     *          "required" = true,
     *          "name" = "send",
     *          "dataType" = "boolean",
     *          "description" = "Send invoice"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     */
    public function putOrderInvoiceAction($order, Request $request)
    {
        $mustSend = (bool)$request->get('send', 0);
        /** @var Order $order */
        $order = $this->getOrder($order);

        try {
            /** @var \Doctrine\ORM\PersistentCollection $items */
            $items = $order->getItems();

            $itemIds = array();
            /** @var InvoiceItem $item */
            foreach ($items as $item) {
                array_push($itemIds, $item->getId());
            }
            $criteria = Criteria::create();
            $criteria->where(Criteria::expr()->in('orderItem', $itemIds));
            
            /** @var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            /** @var \Doctrine\ORM\LazyCriteriaCollection $invoiceItemTmp */
            $invoiceItemTmp = $em->getRepository(
                'MnumiInvoiceBundle:InvoiceItem'
            )->matching($criteria);
            if ($invoiceItemTmp->isEmpty()) {

                /** @var \Mnumi\Bundle\InvoiceBundle\Library\InvoiceService $invoiceService */
                $invoiceService = $this->get('invoice.service');
                /** @var \Mnumi\Bundle\InvoiceBundle\Entity\Invoice $invoice */
                $invoice = $invoiceService->initialize(
                    $order,
                    $this->container->get('invoice.repository.invoice')
                );

                $em->persist($invoice);
                $em->flush();
                $return = array(
                    'id' => $invoice->getId(),
                    'invoice' => $invoice->getName()
                );

                if($mustSend === true)
                {
                    try {
                        /** @var \Panel\NotificationBundle\Notification\Email $notification */
                        $notification = $this->get('notification.email');
                        $notification->initialize(
                            $invoice,
                            'invoice',
                            ($invoice->getLanguage() == null)?'pl':$invoice->getLanguage(),
                            $invoice->getClient(),
                            $order->getUser()
                        );
                    } catch (\Exception $e)
                    {
                        $return = array(
                            'id' => $invoice->getId(),
                            'invoice' => $invoice->getName(),
                            'error' => sprintf('The invoice was not sent due to an error: %s', $e->getMessage())
                        );
                    }
                }
                return View::create($return, 200);
            }
        } catch (\Exception $e)
        {
            throw new HttpException(400, $e->getMessage());
            //return View::create($e->getMessage(), 400);
        }
        throw new HttpException(302);
    }

    /**
     * Send invoice PDF file
     *
     * @ApiDoc(
     *  resource=true,
     *  authentication=true,
     *  authenticationRoles={
     *      "ROLE_ADMIN"
     *  },
     *  statusCodes = {
     *     200 = "Requested when success",
     *     302 = "Requested when same file already exist",
     *     400 = "Requested when error",
     *     405 = "Requested when method not allowed",
     *     404 = "Requested when order has not been found",
     *     415 = "Requested when the uploaded file is not in PDF format",
     *     424 = "Requested when missing dependencies"
     *  },
     *  parameters = {
     *      {
     *          "required" = true,
     *          "name" = "number",
     *          "dataType" = "string",
     *          "description" = "Invoice number"
     *      },
     *      {
     *          "required" = true,
     *          "name" = "filename",
     *          "dataType" = "string",
     *          "description" = "Invoice file name"
     *      },
     *      {
     *          "required" = true,
     *          "name"="file",
     *          "dataType"="file",
     *          "description"="Invoice file content. It may be in base64"
     *      }
     *  },
     *  tags = {
     *      "stable" = "#14892c"
     *  }
     * )
     */
    public function postInvoiceAction($order, Request $request)
    {
        if ((0 === strpos($request->headers->get('Content-Type'), 'application/json')) ||
           (0 === strpos($request->headers->get('Content-Type'), 'application/xml'))) {
            $data = $request->request->all();
        } else {
            $data = $this->parseMultipartContent($request->getContent());
        }

        $number = $data['number'];
        $filename = $data['filename'];
        $file = $data['file'];
        if($this->isBase64($file))
        {
            $file = base64_decode($file);
        }
        if(!$filename || !$number || (strpos($filename, '.pdf') === false) || empty($file))
        {
            throw new HttpException(424);
        }

        $order = $this->getOrder($order);

        $invoicePath = $this->container->getParameter('kernel.root_dir') . '/../../data/invoice/' . $order->getId();

        /**
         * Accept oly PDF file
         */
        $f = finfo_open();
        $mimeType = finfo_buffer($f, $file, PATHINFO_EXTENSION);
        $filesystem = new Filesystem();

        if ($filesystem->exists($invoicePath)) {
            $finder = new Finder();
            $finder->files()->in($invoicePath);
            /** @var \Symfony\Component\Finder\SplFileInfo $item */
            foreach ($finder as $item) {
                if (md5($item->getContents()) == md5($file)) {
                    throw new HttpException(302);
                }
            }
            if (strpos(strtolower($mimeType), 'pdf') === false) {
                throw new HttpException(415);
            }
        }
        /**
         * Create directory and file for order
         */
        try {
            $filesystem->mkdir($invoicePath);
            $filesystem->dumpFile($invoicePath . '/' . $filename, $file);
        } catch (IOException $e)
        {
            throw new HttpException(400);
        }
        return View::create('File uploaded', 200);
    }

    /**
     * @param int $id
     * @return Order
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     */
    private function getOrder($id)
    {
        /** @var Order $order */
        $order = $this->getDoctrine()->getManager()->getRepository('MnumiOrderBundle:Order')->find($id);
        if (!$order) {
            throw new HttpException(404, 'Order not found');
        }
        return $order;
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.invoice.handler');
    }

    private function isBase64($str)
    {
        return (bool)preg_match('`^[a-zA-Z0-9+/]+={0,2}$`', $str);
    }

    protected function parseMultipartContent($raw_data)
    {
        $boundary = substr($raw_data, 0, strpos($raw_data, "\r\n"));

        // Fetch each part
        $parts = array_slice(explode($boundary, $raw_data), 1);
        $data = array();

        foreach ($parts as $part) {
            // If this is the last part, break
            if ($part == "--\r\n") break;

            // Separate content from headers
            $part = ltrim($part, "\r\n");
            list($raw_headers, $body) = explode("\r\n\r\n", $part, 2);

            // Parse the headers list
            $raw_headers = explode("\r\n", $raw_headers);
            $headers = array();
            foreach ($raw_headers as $header) {
                list($name, $value) = explode(':', $header);
                $headers[strtolower($name)] = ltrim($value, ' ');
            }

            // Parse the Content-Disposition to get the field name, etc.
            if (isset($headers['content-disposition'])) {
                $filename = null;
                preg_match(
                    '/^(.+); *name="([^"]+)"(; *filename="([^"]+)")?/',
                    $headers['content-disposition'],
                    $matches
                );
                list(, $type, $name) = $matches;
                isset($matches[4]) and $filename = $matches[4];

                // handle your fields here
                switch ($name) {
                    // this is a file upload
                    case 'userfile':
                        file_put_contents($filename, $body);
                        break;

                    // default for all other files is to populate $data
                    default:
                        $data[$name] = substr($body, 0, strlen($body) - 2);
                        break;
                }
            }

        }
        return $data;
    }
}
