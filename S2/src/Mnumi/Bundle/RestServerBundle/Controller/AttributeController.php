<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotations\NamePrefix("api_attribute_")
 */
class AttributeController extends FOSRestController
{
    /**
     * Get attributes for product.
     *
     * @ApiDoc(
     *  resource = true,
     *  deprecated = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  }
     * )
     *
     * @Annotations\QueryParam(name="id", nullable=true, description="ID of Attribute")
     * @Annotations\QueryParam(name="name", nullable=true, description="Name of Attribute")
     *
     * @Annotations\View(
     *  templateVar="attribute"
     * )
     *
     * @Method({"GET"})
     *
     * @param $product
     * @param Request $request the request object
     * @param ParamFetcherInterface $paramFetcher param fetcher service
     *
     * @return array
     */
    public function getProductAttributesAction($product, Request $request, ParamFetcherInterface $paramFetcher)
    {
        $data = $paramFetcher->all();
        $data['productId'] = $product;

        return $this->getHandler()->getBy(
           $data
        );
    }

    /**
     * @return object
     */
    protected function getHandler()
    {
        return $this->container->get('rest.attribute.handler');
    }

}
