<?php

namespace Mnumi\Bundle\RestServerBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Mnumi\Bundle\OrderBundle\Command\CropImageCommand;
use Mnumi\Bundle\OrderBundle\Entity\Repository\OrderItemFile;

/**
 * @Annotations\NamePrefix("api_order_item_file_")
 */
class OrderItemFileController extends FOSRestController
{
    /**
     * Update file
     *
     * @ApiDoc(
     *  resource = true,
     *  authentication = true,
     *  statusCodes = {
     *      200 = "Returned when successful"
     *  },
     * parameters = {
     *      {
     *          "name" = "cropX",
     *          "required" = false,
     *          "dataType" = "integer",
     *          "description" = "X crop coordinate"
     *      },
     *      {
     *          "name" = "cropY",
     *          "required" = false,
     *          "dataType" = "integer",
     *          "description" = "Y crop coordinate"
     *      },
     *      {
     *          "name" = "cropWidth",
     *          "required" = false,
     *          "dataType" = "integer",
     *          "description" = "Crop width"
     *      },
     *      {
     *          "name" = "cropHeight",
     *          "required" = false,
     *          "dataType" = "integer",
     *          "description" = "Crop height"
     *      },
     *      {
     *          "name" = "printsNumber",
     *          "required" = false,
     *          "dataType" = "integer",
     *          "description" = "Prints number"
     *      }
     * 
     *  }
     * )
     *
     * @Annotations\View(
     *  templateVar="get"
     * )
     *
     * @Method({"POST"})
     * @Annotations\Post("/file/{fileId}/update")
     *
     * @param integer $fileId Cropped file ID
     *
     * @return OrderItemFile
     */
    public function postUpdateAction($fileId, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $orderItemFileRepo = $em->getRepository('MnumiOrderBundle:OrderItemFile');
        $file = $orderItemFileRepo->findOneById($fileId);

        $cropX = $request->get('cropX');
        $cropY = $request->get('cropY');
        $cropWidth = $request->get('cropWidth');
        $cropHeight = $request->get('cropHeight');
        $printsNumber = $request->get('printsNumber');

        $file->setMetadataAttribute('cropX', $cropX);
        $file->setMetadataAttribute('cropY', $cropY);
        $file->setMetadataAttribute('cropWidth', $cropWidth);
        $file->setMetadataAttribute('cropHeight', $cropHeight);
        $file->setMetadataAttribute('printsNumber', $printsNumber);

        $em->flush();

        if($cropX) {
            $this->cropImage($file->getId());
        }

        return array($file);
    }

    private function cropImage($fileId)
    {
        $command = new CropImageCommand();
        $command->setContainer($this->container);
        $input = new ArrayInput(array(
            'fileId' => $fileId,
            ));

        $output = new NullOutput();
        $command->run($input, $output);
    }
}
