<?php

/*
 * This file is part of the MnumiPanel package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Exception;

/**
 * FileDoesNotExistsException class
 *
 * This exception should be used when given file does not exist and its absence
 * can not be handled in the current scope of code.
 *
 * @author Rafał Długołęcki <rafal.dlugolecki@mnumi.com>
 */
class FileDoesNotExistsException extends \RuntimeException
{
    /**
     * FileDoesNotExistsException constructor
     *
     * @param $filepath Filepath of file which has not been found
     */
    public function __construct($filepath) {
        $this->message = sprintf('File: %s does not exists. Thrown in: %s:%d', $filepath, $this->getFile(), $this->getLine());

        parent::__construct($this->message);
    }
}