<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Handler;

use Mnumi\Bundle\RestServerBundle\Exception\InvalidFormException;

/**
 * OrderHandler class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ClientAddressHandler extends ObjectHandler
{

    /**
     * Create a new ClientAddress.
     *
     * @param array $parameters
     *
     * @return Object
     */
    public function post(array $parameters, $validationGroups = array())
    {
        $object = $this->createEntity();
        $entityType = $this->entityType;
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->formFactory->create(
            $entityType, $object, array(
                'method' => 'POST',
                'validation_groups' => $validationGroups
            )
        );

        $form->submit($parameters);

        if ($form->isValid()) {
            $object = $form->getData();
            $this->om->persist($object);

            $this->om->flush();

            return $object;
        }

        throw new InvalidFormException('Invalid submitted data: ' . (string) $form->getErrors(true, false), $form);
    }
}
