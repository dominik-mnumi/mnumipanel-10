<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\RestServerBundle\Exception\InvalidFormException;
use Symfony\Component\Form\FormFactoryInterface;

/**
 * ObjectHandler class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
abstract class ObjectHandler
{
    public $om;
    public $entityClass;
    public $repository;
    public $formFactory;
    public $entityType;
    public $defaultTax;
    /**
     * @var float
     */
    private $tax;

    /**
     * @param ObjectManager        $om
     * @param string               $entityClass
     * @param string               $entityType
     * @param FormFactoryInterface $formFactory
     * @param float                $tax
     */
    public function __construct(
        ObjectManager $om,
        $entityClass,
        $entityType,
        FormFactoryInterface $formFactory,
        $tax
    )
    {
        $this->om = $om;

        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository($this->entityClass);

        $this->entityType = $entityType;

        $this->formFactory = $formFactory;
        $this->tax = $tax * 100;
    }

    /**
     * Get an Object.
     *
     * @param mixed $id
     *
     * @return Object
     */
    public function get($id)
    {
        return $this->getRepository()->find($id);
    }

    /**
     * Get an Object by criteria.
     *
     * @param mixed $id
     *
     * @return Object
     */
    public function getBy(array $parameters)
    {
        $criteria = \Doctrine\Common\Collections\Criteria::create();
        foreach ($parameters as $field => $parameter) {
            if ($parameter != null) {
                $criteria->andWhere(\Doctrine\Common\Collections\Criteria::expr()->eq($field, $parameter));
            }
        }

        return $this->getRepository()->matching($criteria);
    }

    /**
     * Get one Object by criteria.
     *
     * @param mixed $id
     *
     * @return Object
     */
    public function getOneBy(array $parameters)
    {
        return $this->getRepository()->findOneBy($parameters);
    }

    /**
     * Get a list of Objects.
     *
     * @param string $sort
     * @param string $order
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function all($sort = "id", $order = "ASC", $limit = 5, $offset = 0)
    {
        return $this->allBy(array(), $sort, $order, $limit, $offset);
    }

    /**
     * Get a list of Objects.
     *
     * @param $findBy
     * @param string $sort
     * @param string $order
     * @param int $limit
     * @param int $offset
     * @return array
     */
    public function allBy($findBy, $sort = "id", $order = "ASC", $limit = 5, $offset = 0)
    {
        return $this->getRepository()->findBy($findBy, array($sort => strtoupper($order)), $limit, $offset);
    }

    /**
     * Create a new Object.
     *
     * @param array $parameters
     *
     * @return Object
     */
    public function post(array $parameters)
    {
        $object = $this->createEntity();

        return $this->processForm($object, $parameters, 'POST');
    }

    /**
     * Edit a Object.
     *
     * @param Object $object
     * @param array  $parameters
     *
     * @return Object
     */
    public function put($object, array $parameters)
    {
        return $this->processForm($object, $parameters, 'PUT');
    }

    /**
     * Partially update a Object.
     *
     * @param Object $object
     * @param array  $parameters
     *
     * @return Object
     */
    public function patch($object, array $parameters)
    {
        return $this->processForm($object, $parameters, 'PATCH');
    }

    /**
     * Processes the form.
     *
     * @param Object $object
     * @param array  $parameters
     * @param String $method
     *
     * @return Object
     *
     * @throws InvalidFormException
     */
    protected function processForm($object, array $parameters, $method = "PUT")
    {
        
        $entityType = $this->entityType;
        /** @var \Symfony\Component\Form\Form $form */
        $form = $this->formFactory->create(
            $entityType,
            $object,
            array(
                'method' => $method
            )
        );
        if (isset($parameters[$form->getName()])) {
            $parameters = $parameters[$form->getName()];
        }
        $form->submit($parameters, false);
        if ($form->isValid()) {
            $object = $form->getData();
            $this->om->persist($object);
            $this->om->flush($object);

            return $object;
        }

        throw new InvalidFormException('Invalid submitted data: ' . (string) $form->getErrors(true, false));
    }

    protected function createEntity()
    {
        return new $this->entityClass();
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectRepository
     */
    protected function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return float
     */
    public function getTax()
    {
        return $this->tax;
    }
}
