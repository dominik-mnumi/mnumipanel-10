<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\RestServerBundle\Entity\WebapiKey;

class LoadApiData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        $webapikey = new WebapiKey();
        $webapikey->setName('9LDAvTuD7i53Ef5yXEOabAvt');
        $webapikey->setDescription('Test WebApi Key');
        $webapikey->setValidDate(new \DateTime());
        $webapikey->setType('other');
        $webapikey->setHost('localhost');
        $manager->persist($webapikey);

        $manager->flush();
    }

    public function getOrder()
    {
        return 0;
    }
}
