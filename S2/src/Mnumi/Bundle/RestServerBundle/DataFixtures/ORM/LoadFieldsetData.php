<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Mnumi\Bundle\RestServerBundle\DataFixtures\ORM;

use \Doctrine\Common\DataFixtures\AbstractFixture;
use \Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFieldsetData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        //$em = $this->container->get('doctrine')->getEntityManager('default');

        $manager->clear();
        gc_collect_cycles(); // Could be useful if you have a lot of fixtures

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('GENERAL');
        $field->setLabel('GENERAL');
        $manager->persist($field);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('- Customizable -');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('COUNT');
        $field->setLabel('COUNT');
        $manager->persist($field);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('QUANTITY');
        $field->setLabel('QUANTITY');
        $manager->persist($field);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('SIZE');
        $field->setLabel('SIZE');
        $manager->persist($field);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('Large format');
        $field->setLabel('Large format');
        $manager->persist($field);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('A0');
        $fieldItem->setCost('5.000');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('Small size');
        $field->setLabel('Small size');
        $manager->persist($field);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('A3');
        $fieldItem->setCost('10.000');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('A4');
        $fieldItem->setCost('10.000');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Business card - 10x16');
        $fieldItem->setCost('7.000');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Business card - 9x5');
        $fieldItem->setCost('7.000');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('MATERIAL');
        $field->setLabel('MATERIAL');
        $manager->persist($field);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Paper X');
        $fieldItem->setCost('12.000');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Paper Y');
        $fieldItem->setCost('12.000');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('Large format');
        $field->setLabel('Large format');
        $manager->persist($field);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Full color');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Two color');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Single color');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Large color');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('Small size');
        $field->setLabel('Small size');
        $manager->persist($field);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Print Y');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Print X');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('PRINT');
        $field->setLabel('PRINT');
        $manager->persist($field);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('SIDES');
        $field->setLabel('SIDES');
        $manager->persist($field);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Double');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Single');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('FIXEDPRICE');
        $field->setLabel('FIXEDPRICE');
        $manager->persist($field);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('OTHER');
        $field->setLabel('OTHER');
        $manager->persist($field);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Bindery');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Cutting');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('Something per item');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        $fieldItem = new \Mnumi\Bundle\ProductBundle\Entity\FieldItem();
        $fieldItem->setName('--------');
        $fieldItem->setField($field);
        $manager->persist($fieldItem);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('WIZARD');
        $field->setLabel('WIZARD');
        $manager->persist($field);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('Bindery');
        $field->setLabel('Bindery');
        $manager->persist($field);

        /* FIELD */
        $field = new \Mnumi\Bundle\ProductBundle\Entity\Fieldset();
        $field->setName('can_delete');
        $field->setLabel('can_delete');
        $manager->persist($field);

        $manager->flush();
    }

    public function getOrder()
    {
        return 400;
    }
}
