<?php

namespace Panel\WorkingTimeBundle\Controller;

use Mnumi\Bundle\RestServerBundle\Entity\User;
use Mnumi\Bundle\UserBundle\Exception\NotFoundUserException;
use Panel\BarcodeBundle\Library\UserBarcode;
use Panel\WorkingTimeBundle\Entity\Worklog;
use Panel\WorkingTimeBundle\Exception\WorkStateException;
use Panel\WorkingTimeBundle\Library\Work;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class DefaultController extends Controller
{
    /**
     * Return csv report
     *
     * @param Request $request
     * @return StreamedResponse
     */
    public function reportAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $results = $em->getRepository('PanelWorkingTimeBundle:Worklog')->findAll();

        $response = new StreamedResponse(
            function () use ($results) {
                $handle = fopen('php://output', 'r+');

                fputcsv($handle, array('User', 'Start date', 'End date', 'Minutes'));

                /** @var Worklog $result */
                foreach ($results as $result) {

                    $name = $result->getUser()->getUsername();
                    if($result->getUser()->getFirstName() && $result->getUser()->getLastName())
                    {
                        $name = sprintf('%s %s', $result->getUser()->getFirstName(), $result->getUser()->getLastName());
                    }
                    $line = array(
                        'name' => $name,
                        'startTime' => $result->getStartTime()->format("Y-m-d H:i:s"),
                        'endTime' => ($result->getEndTime()) ? $result->getEndTime()->format("Y-m-d H:i:s") : null,
                    );
                    if ($result->getEndTime()) {
                        $sum = $result->getEndTime()->getTimestamp() - $result->getStartTime()->getTimestamp();
                        $line['summary'] = $this->parseTime($sum);

                    }
                    // add a line in the csv file. You need to implement a toArray() method
                    // to transform your object into an array
                    fputcsv($handle, $line);
                }

                fclose($handle);
            }
        );

        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }

    /**
     * Parse seconds to human time
     *
     * @param $seconds
     * @return mixed
     */
    private function parseTime($seconds)
    {
        /**
         * Convert number of seconds into years, days, hours, minutes and seconds
         * and return an string containing those values
         *
         * @param integer $seconds Number of seconds to parse
         * @return string
         */
        $s = $seconds%60;
        $m = floor(($seconds%3600)/60);
        $h = floor(($seconds%86400)/3600);
        $d = floor(($seconds%2592000)/86400);

        $string = '';
        if ($d > 0) {
            $dw = $d > 1 ? ' days ' : ' day ';
            $string .= $d . $dw;
        }
        if ($h > 0) {
            $hw = $h > 1 ? ' hours ' : ' hour ';
            $string .= $h . $hw;
        }
        if ($m > 0) {
            $mw = $m > 1 ? ' minutes ' : ' minute ';
            $string .= $m . $mw;
        }
        if ($s > 0) {
            $sw = $s > 1 ? ' seconds ' : ' second ';
            $string .= $s . $sw;
        }
        return preg_replace('/\s+/', ' ', $string);
    }

    /**
     * Main barcode reader action
     * 
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('PanelWorkingTimeBundle:Default:index.html.twig');
    }

    /**
     * Ajax action
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response|static
     */
    public function ajaxAction(Request $request)
    {
        $code = $request->request->get('code');

        /** @var ObjectManager $em */
        $em = $this->getDoctrine()->getManager();

        try {
            $barcode = new UserBarcode($em);
            $barcode->setBarcode($code);

            $work = new Work($em, $barcode);
            $work->changeState();
            $data = array(
                'code' => $work->getCode(),
                'state' => $work->getState(),
                'user' => $work->getUser()
            );
        } catch (NotFoundUserException $e)
        {
            $data = array(
                'code' => 500,
                'state' => $e->getMessage()
            );
            
        }
        return JsonResponse::create($data);
    }
}
