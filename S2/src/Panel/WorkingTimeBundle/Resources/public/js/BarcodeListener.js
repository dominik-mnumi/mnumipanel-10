(function($) {
    jQuery.fn.BarcodeListener = function(callback, options) {

        var settings = $.extend({
            // These are the defaults.
            prefix: "^",
            sufix: "#"
        }, options );

        var match = new RegExp("[." + settings.prefix + "](.+)[." + settings.sufix + "]");
        var keys = [];
        $(window).keypress(function (e) {
            keys.push(String.fromCharCode(e.which));
            var code = keys.join('').toString();
            var number = code.match(match);
            if(number !== null)
            {
                keys = [];
                code = number[1];
                if (typeof(callback) == "function") {
                    callback(code);
                }

                return false;
            }
        });
    }
})(jQuery);