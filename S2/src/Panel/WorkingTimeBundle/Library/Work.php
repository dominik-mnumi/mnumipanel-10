<?php
/**
 * Created by IntelliJ IDEA.
 * User: dominik
 * Date: 16.07.15
 * Time: 12:35
 */

namespace Panel\WorkingTimeBundle\Library;


use Doctrine\Common\Persistence\ObjectManager;
use Mnumi\Bundle\UserBundle\Exception\NotFoundUserException;
use Panel\BarcodeBundle\Library\Barcode;
use Panel\WorkingTimeBundle\Entity\Worklog;
use Panel\WorkingTimeBundle\Exception\WorkStateException;
use Mnumi\Bundle\RestServerBundle\Entity\User;

class Work {

    const NEW_WORK = "Getting started.";
    const LAUNCHED_WORK = "Work started, you can not finish work before 1 minute.";
    const FINISHED_WORK = "End of of work.";
    const NEW_WORK_CODE = 1;
    const LAUNCHED_WORK_CODE = 1;
    const FINISHED_WORK_CODE = 0;

    private $code;
    /**
     * @var Worklog
     */
    private $worklog;
    /**
     * @var ObjectManager
     */
    private $em;

    /**
     * @var User
     */
    private $user;

    /**
     * @param ObjectManager $em
     * @param Barcode $barcode
     * @throws \Symfony\Component\Security\Core\Exception\UsernameNotFoundException
     */
    public function __construct(ObjectManager $em, Barcode $barcode)
    {
        $this->user = $barcode->getUser();
        if(!$this->user instanceof User)
        {
            throw new NotFoundUserException('User not found');
            
        }
        $this->em = $em;
        
        /** @var Worklog $userWorkLog */
        $worklog = $em->getRepository('PanelWorkingTimeBundle:Worklog')->findOneBy(array('user' => $this->user, 'endTime' => null));
        if(!$worklog instanceof Worklog)
        {
            $worklog = new Worklog();
            $worklog->setUser($this->user);
        }
        $this->worklog = $worklog;
        
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        if($this->user->getFirstName() && $this->user->getLastName()) {
            return $this->user->getFirstName() . ' ' . $this->user->getLastName();
        }
        return $this->user->getUsername();
    }

    /**
     * @return void
     */
    public function changeState()
    {
        $nowDateTime = new \DateTime('now');
        if($this->getState() == static::NEW_WORK)
        {
            $this->setCode(static::NEW_WORK_CODE);
            $this->worklog->setStartTime($nowDateTime);
            $this->em->persist($this->worklog);
            $this->em->flush($this->worklog);
        } else if($this->getState() == static::LAUNCHED_WORK)
        {
            $this->setCode(static::LAUNCHED_WORK_CODE);
            $endTime = $this->worklog->getEndTime();
            if($endTime === null)
            {
                $endTime = $nowDateTime;
            }
            
            if($this->worklog->getStartTime()
                    ->modify("+1 minute")
                    ->getTimestamp()
                <= $endTime->getTimestamp()
            )
            {
                $this->worklog->setEndTime($nowDateTime);
                $this->em->persist($this->worklog);
                $this->em->flush($this->worklog);
                $this->setCode(static::FINISHED_WORK_CODE);
            }
        }
    }

    /**
     * @return string
     */
    public function getState()
    {
        if($this->worklog->getStartTime() === null)
        {
            return static::NEW_WORK;
        } else if($this->worklog->getEndTime() === null)
        {
            return static::LAUNCHED_WORK;
        }

        return static::FINISHED_WORK;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     * @return Work
     */
    private function setCode($code)
    {
        $this->code = $code;

        return $this;
    }
} 