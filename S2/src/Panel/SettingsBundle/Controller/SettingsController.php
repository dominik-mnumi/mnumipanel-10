<?php

namespace Panel\SettingsBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use FOS\RestBundle\Controller\Annotations;
use Mnumi\Bundle\RestServerBundle\Entity\WebapiKey;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Annotations\NamePrefix("api_settings_")
 */
class SettingsController extends FOSRestController
{
    /**
     * Get attributes for product.
     *
     *  @ApiDoc(
     *  resource = true,
     *  deprecated = true,
     *  authentication = true,
     *  authenticationRoles = {
     *      "ROLE_USER"
     *  },
     *  statusCodes = {
     *      200 = "Returned when successful",
     *      400 = "Returned when invalid url or api key",
     *      406 = "Returned when remote layout update is not supported by MnumiShop application"
     *  }
     * )
     *
     * @Annotations\View(
     *  templateVar="attribute"
     * )
     *
     * @Method({"POST"})
     * @Annotations\Post("/settings/stores/update/{key}")
     *
     * @param string $key
     * @param Request $request the request object
     *
     * @return array
     */
    public function postSettingsStoresUpdateAction($key, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var WebapiKey $shop */
        $shop = $em->getRepository('MnumiRestServerBundle:WebapiKey')->find($key);

        if($shop instanceof WebapiKey)
        {
            if($shop->getHost() && $shop->getName())
            {
                try {
                    $data = file_get_contents(
                        sprintf('%s/updateLayout?api_key=%s', $shop->getHost(), substr($shop->getName(), 10, 10))
                    );
                    if ($data !== false && $data === "ok") {
                        return new JsonResponse(array(
                            'status' => 'success',
                            'message' => 'Store layout has been updated successfuly',
                        ));
                    }
                }
                catch (ContextErrorException $ex) {
                    return new JsonResponse(array(
                            'status' => 'error',
                            'message' => 'Store does not accept remote update',
                        ),
                        JsonResponse::HTTP_NOT_ACCEPTABLE
                    );
                }
            }
        }

        return new JsonResponse(array(
                'status' => 'error',
                'message' => 'Can not update store layout. Store does not have valid url or key',
            ),
            JsonResponse::HTTP_BAD_REQUEST
        );
    }
}
