<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Panel\NotificationBundle\Exception;

/**
 * Class NotFoundNotificationParameterException
 * @package Panel\NotificationBundle\Exception
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
class NotFoundNotificationParameterException extends \Exception {

} 