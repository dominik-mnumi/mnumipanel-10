<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Panel\NotificationBundle\Formatter;

/**
 * Interface FormatterInterface
 * Each notification must used initialize to add object and parse
 *
 * @package Panel\NotificationBundle\Formatter
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
interface FormatterInterface {

    /**
     * Initialize notification
     * Set object and parse data for notification
     *
     * @param $object
     * @return mixed
     */
    public function initialize($object);
} 