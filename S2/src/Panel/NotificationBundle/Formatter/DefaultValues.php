<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Panel\NotificationBundle\Formatter;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class DefaultParameters
 * @package Panel\NotificationBundle\Formatter
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
class DefaultValues
{
    /**
     * @var array
     */
    private $configurationData = array();


    /**
     * Add configuration data
     * If $value is not array set $key
     *
     * @param $value
     * @param null $key
     * @return $this
     */
    public function add($value, $key = null)
    {
        if(!is_array($value))
        {
            $value = array($key => $value);
        }
        $this->configurationData = array_merge($this->configurationData, $value);

        return $this;
    }

    /**
     * Get configuration value
     * 
     * @param $key
     * @return null
     */
    private function get($key)
    {
        if(isset($this->configurationData[$key]))
        {
            return $this->configurationData[$key];
        }
        return null;
    }

    /**
     * @return ArrayCollection
     */
    public function getValues()
    {
        return new ArrayCollection(array(
            'companyName' => new Value($this->get('seller_name'), 'company name'),
            'companyAddress' => new Value($this->get('seller_address'), 'company address'),
            'companyPostcode' => new Value($this->get('seller_postcode'), 'company postcode'),
            'companyCity' => new Value($this->get('seller_city'), 'company city'),
            'companyTaxId' => new Value($this->get('tax_id'), 'company TIN'),
            'companyBankName' => new Value($this->get('bank_name'), 'company bank name'),
            'companyBankAccount' => new Value($this->get('bank_account'), 'company bank account'),
            'shopHost' => new Value($this->get('shop_host'), 'shop url'),
            'shopLoginUrl' => new Value($this->get('shop_host').$this->get('shop_url_login'), 'shop login url'),
            'date' => new Value(new \DateTime(), 'current date'),
            'shopDescription' => new Value(null, 'shop description'),
            'currencySymbol' => new Value(null, 'currency symbol'),
            'sender' => new Value($this->get('sender'), null),
            'sender_name' => new Value($this->get('sender_name'), null),
        ));
    }
} 