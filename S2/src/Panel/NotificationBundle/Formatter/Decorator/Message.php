<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Panel\NotificationBundle\Formatter\Decorator;

use Panel\NotificationBundle\Entity\Notification;

/**
 * Class Message - decorator
 *
 * @package Panel\NotificationBundle\Formatter\Decorator
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
class Message
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $bcc;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $sender;

    /**
     * @var string
     */
    private $senderName;

    /**
     * @param $title
     * @param $content
     * @param $bcc
     * @param $sender
     * @param $senderName
     * @internal param \Panel\NotificationBundle\Entity\Notification $notification
     * @internal param $values
     */
    function __construct($title, $content, $bcc, $sender, $senderName)
    {
        $this->bcc = $bcc;
        $this->content = $content;
        $this->title = $title;

        $this->sender = $sender;
        $this->senderName = $senderName;
    }

    /**
     * @return string
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return string
     */
    public function getSenderName()
    {
        return $this->senderName;
    }
    
    
} 