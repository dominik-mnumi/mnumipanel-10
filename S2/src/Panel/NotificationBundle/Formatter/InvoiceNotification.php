<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Panel\NotificationBundle\Formatter;

use Mnumi\Bundle\InvoiceBundle\Entity\Invoice;
use Panel\NotificationBundle\Exception\InitializeObjectException;

/**
 * Class InvoiceNotification
 * Build parameters for invoice notification
 *
 * @package Panel\NotificationBundle\Formatter
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
class InvoiceNotification extends Notification implements FormatterInterface
{
    /**
     * @param $object
     * @return void
     * @throws InitializeObjectException
     */
    public function initialize($object)
    {
        if ($object && !$object instanceof Invoice) {
            throw new InitializeObjectException('Passed object is not instance of Invoice class.');
        }

        $this->addValue(
            'invoiceUrl',
            new Value($this->getValue('shopHost') . 'invoice/' . $object->getId(), 'invoice')
        );
    }
} 