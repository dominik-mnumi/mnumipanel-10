<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Panel\NotificationBundle\Formatter;

/**
 * Container for values, stored value, and description.
 * @todo implement all methods user on old MnumiCore
 *
 * @package Panel\NotificationBundle\Formatter
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
class Value {

    /**
     * @var string|integer
     */
    private $value;

    /**
     * @var string
     */
    private $description;

    /**
     * @param $value mixed
     * @param $description string
     */
    function __construct($value, $description)
    {
        $this->description = $description;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->value;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        
        return $this;
    }
    
    
} 