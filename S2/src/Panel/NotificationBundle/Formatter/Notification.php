<?php
/*
 * This file is part of the MnumiPanel package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Panel\NotificationBundle\Formatter;

use Doctrine\Common\Collections\ArrayCollection;
use Panel\NotificationBundle\Formatter\Decorator\Message;
use Panel\NotificationBundle\Entity\Notification as NotificationEntity;

/**
 * Class Notification
 *
 * @package Panel\NotificationBundle\Formatter
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
abstract class Notification implements FormatterInterface {

    /**
     * @var object
     */
    protected $object;

    /**
     * @var ArrayCollection
     */
    private $values;
    
    public function __construct(DefaultValues $defaultParameters)
    {
        /** @var ArrayCollection values */
        $this->values = $defaultParameters->getValues();
    }

    /**
     * Add value if exist param
     *
     * @param string $name
     * @param Value $value
     * @return Notification
     */
    public function addValue($name, Value $value)
    {
        $this->values->set($name, $value);

        return $this;
    }

    /**
     * Get all values
     *
     * @return array
     */
    public function getValues()
    {
        return $this->values->toArray();
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function getValue($name)
    {
        return $this->values->get($name)->getValue();
    }

    /**
     * Create email object from NotificationEntity
     * 
     * @param NotificationEntity $notification
     * @return Message
     */
    public function toMail(NotificationEntity $notification)
    {
        $loader = new \Twig_Loader_String();
        $twig = new \Twig_Environment($loader);
        $values = $this->getValues();
        $content = $twig->render($notification->getContent(), $values);
        return new Message($notification->getTitle(), $content, $notification->getBcc(), $values['sender'], $values['sender_name']);
    }
    
} 