<?php
/**
 * Created by IntelliJ IDEA.
 * User: dominik
 * Date: 20.05.15
 * Time: 15:10
 */

namespace Panel\NotificationBundle\Notification;


use Doctrine\ORM\EntityManager;
use Mnumi\Bundle\ClientBundle\Entity\Client;
use Mnumi\Bundle\RestServerBundle\Entity\User;
use Panel\NotificationBundle\Entity\Notification\Message;
use Panel\NotificationBundle\Formatter\DefaultValues;
use Panel\NotificationBundle\Formatter\InvoiceNotification;

class Email {
    
    private $em;
    private $oldParameters;

    /**
     * @param EntityManager $em
     * @param DefaultValues $oldParameters
     */
    public function __construct(EntityManager $em, DefaultValues $oldParameters)
    {
        $this->em = $em;
        $this->oldParameters = $oldParameters;
    }

    /**
     * @param object $entity
     * @param string $type
     * @param string $lang
     * @param \Mnumi\Bundle\ClientBundle\Entity\Client $client
     * @param \Mnumi\Bundle\RestServerBundle\Entity\User $user
     */
    public function initialize($entity, $type, $lang = 'pl', Client $client, User $user)
    {

        /* @var \Panel\NotificationBundle\Entity\Notification $article */
        $notification = $this->em->getRepository('PanelNotificationBundle:Notification')->findOneByTypeAndLang($type, $lang);
        
        $object = new InvoiceNotification($this->oldParameters);
        $object->initialize($entity);
        
        $mail = $object->toMail($notification);
        
        $message = new Message();
        $message->setSender($mail->getSender());
        $message->setSenderName($mail->getSenderName());
        $message->setBcc($mail->getBcc());
        $message->setTitle($mail->getTitle());
        $message->setMessageHtml($mail->getContent());
        $message->setMessagePlain($this->convertToPlainMessage($mail->getContent()));
        $message->setNotificationType($this->em->getRepository('PanelNotificationBundle:Notification\Type')->find(1));
        
        $message->setRecipient($user->getEmailAddress());
        $message->setRecipientName($client->getFullname());
        $message->setUser($user);

        $this->em->persist($message);
        $this->em->flush($message);
    }

    /**
     * Convert content to plain message
     *
     * @param string $content
     * @return string
     */
    function convertToPlainMessage($content) {
        return strip_tags(preg_replace("=<br */?>=i", "\n", $content));
    }
} 