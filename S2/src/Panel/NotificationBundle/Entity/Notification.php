<?php

namespace Panel\NotificationBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use \Panel\NotificationBundle\Entity\Translation\Notification as NotificationTranslation;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Translatable\Translatable;

/**
 * Notification
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="Panel\NotificationBundle\Entity\Repository\Notification")
 */
class Notification implements Translatable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Panel\NotificationBundle\Entity\Notification\Template
     *
     * @ORM\ManyToOne(targetEntity="Panel\NotificationBundle\Entity\Notification\Template")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notification_template_id", referencedColumnName="id")
     * })
     */
    private $notificationTemplate;

    /**
     * @var \Panel\NotificationBundle\Entity\Notification\Type
     *
     * @ORM\ManyToOne(targetEntity="Panel\NotificationBundle\Entity\Notification\Type")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notification_type_id", referencedColumnName="id")
     * })
     */
    private $notificationType;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=500, nullable=false)
     * @Gedmo\Translatable
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", nullable=false)
     * @Gedmo\Translatable
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="bcc", type="string", length=45, nullable=false)
     */
    private $bcc;

    /**
     * @ORM\OneToMany(
     *   targetEntity="Panel\NotificationBundle\Entity\Translation\Notification",
     *   mappedBy="notification", indexBy="lang",
     *   cascade={"persist", "remove"}
     * )
     */
    private $translations;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Notification
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->getTranslations()->first()->getTitle();
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Notification
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->getTranslations()->first()->getContent();
    }

    /**
     * Set bcc
     *
     * @param string $bcc
     *
     * @return Notification
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;

        return $this;
    }

    /**
     * Get bcc
     *
     * @return string
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * Set notificationTemplate
     *
     * @param \Panel\NotificationBundle\Entity\Notification\Template $notificationTemplate
     *
     * @return Notification
     */
    public function setNotificationTemplate(\Panel\NotificationBundle\Entity\Notification\Template $notificationTemplate = null)
    {
        $this->notificationTemplate = $notificationTemplate;

        return $this;
    }

    /**
     * Get notificationTemplate
     *
     * @return \Panel\NotificationBundle\Entity\Notification\Template
     */
    public function getNotificationTemplate()
    {
        return $this->notificationTemplate;
    }

    /**
     * Set notificationType
     *
     * @param \Panel\NotificationBundle\Entity\Notification\Type $notificationType
     *
     * @return Notification
     */
    public function setNotificationType(\Panel\NotificationBundle\Entity\Notification\Type $notificationType = null)
    {
        $this->notificationType = $notificationType;

        return $this;
    }

    /**
     * Get notificationType
     *
     * @return \Panel\NotificationBundle\Entity\Notification\Type
     */
    public function getNotificationType()
    {
        return $this->notificationType;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->translations = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add translation
     *
     * @param \Panel\NotificationBundle\Entity\Translation\Notification $translation
     *
     * @return Notification
     */
    public function addTranslation(\Panel\NotificationBundle\Entity\Translation\Notification $translation)
    {
        $this->translations[] = $translation;

        return $this;
    }

    /**
     * Remove translation
     *
     * @param \Panel\NotificationBundle\Entity\Translation\Notification $translation
     */
    public function removeTranslation(\Panel\NotificationBundle\Entity\Translation\Notification $translation)
    {
        $this->translations->removeElement($translation);
    }

    /**
     * Get translations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTranslations()
    {
        return $this->translations;
    }
}
