<?php

namespace Panel\NotificationBundle\Entity\Repository;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NoResultException;

/**
 * Notification
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class Notification extends EntityRepository
{
    /**
     * Get notification by type and lang
     *
     * @param $type
     * @param string $lang
     * @return mixed
     */
    public function findOneByTypeAndLang($type, $lang = "pl")
    {
        try {
            /** @var \Doctrine\ORM\QueryBuilder $query */
            return $this->getEntityManager()->createQuery(
                'SELECT n FROM PanelNotificationBundle:Notification n
                 LEFT JOIN n.translations t JOIN n.notificationTemplate nt
                 WHERE nt.name = :name
                 AND t.lang = :lang'
            )
                ->setParameter('name', ucfirst($type))
                ->setParameter('lang', $lang)
                ->getSingleResult(AbstractQuery::HYDRATE_OBJECT);
        } catch (NoResultException $e)
        {
            return null;
        }
    }
}
