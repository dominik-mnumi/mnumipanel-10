<?php

namespace Panel\NotificationBundle\Entity\Notification;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;

/**
 * Message
 *
 * @ORM\Table(name="notification_template")
 * @ORM\Entity
 */
class Template
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="help_information", type="string", length=100, nullable=false)
     */
    private $helpInformation;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Template
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set helpInformation
     *
     * @param string $helpInformation
     *
     * @return Template
     */
    public function setHelpInformation($helpInformation)
    {
        $this->helpInformation = $helpInformation;

        return $this;
    }

    /**
     * Get helpInformation
     *
     * @return string
     */
    public function getHelpInformation()
    {
        return $this->helpInformation;
    }
}
