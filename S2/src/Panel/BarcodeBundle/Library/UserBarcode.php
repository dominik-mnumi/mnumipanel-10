<?php
namespace Panel\BarcodeBundle\Library;

use Mnumi\Bundle\RestServerBundle\Entity\User;
use Mnumi\Bundle\UserBundle\Exception\NotFoundUserException;
use Doctrine\Common\Persistence\ObjectManager;

class UserBarcode extends Barcode {

    /**
     * @var \Doctrine\Common\Persistence\ObjectManager
     */
    private $em;

    /**
     * Constructor
     *
     * @param ObjectManager $em
     */
    function __construct(ObjectManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return User
     * @throws \Mnumi\Bundle\UserBundle\Exception\NotFoundUserException
     */
    public function getUser()
    {
        $user = $this->em->getRepository('MnumiRestServerBundle:User')->find($this->getId());
        if($user instanceof User)
        {
            return $user;
        }

        throw new NotFoundUserException("User not found");
    }
} 