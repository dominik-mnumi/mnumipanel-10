<?php

namespace Panel\BarcodeBundle\Library;

/**
 * BarcodeDecoder class for decoding barcode from given string
 *
 * @package Panel\BarcodeBundle
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
abstract class Barcode {

    /**
     * @var string
     */
    protected $barcode;

    /**
     * @param $barcode
     */
    public function setBarcode($barcode)
    {
        $this->barcode = $barcode;
    }

    /**
     * Get prefix from barcode
     *
     * @return string
     */
    protected function getPrefix()
    {
        return substr($this->barcode,0,1);
    }

    /**
     * Get type from barcode
     *
     * @return integer
     */
    protected function getType()
    {
        return (int)substr($this->barcode,1,2);
    }

    /**
     * Get id from barcode
     *
     * @return integer
     */
    public function getId()
    {
        /**
         * Id is from 3 to 7
         */
        return (int)ltrim(substr($this->barcode,3,7), '0');
    }

    /**
     * Get additional number from barcode
     *
     * @return integer
     */
    public function getAdditionalNumber()
    {
        return substr($this->barcode,11, -1);
    }
} 