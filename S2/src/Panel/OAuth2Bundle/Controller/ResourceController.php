<?php
/*
 * This file is part of the FOSOAuthServerBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace Panel\OAuth2Bundle\Controller;

use FOS\OAuthServerBundle\Entity\AccessToken;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Request as GlobalRequest;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class ResourceController extends Controller
{
    /**
     * Get user data by access_token
     *
     * @param Request $request
     * @return JsonResponse
     * @throws \Symfony\Component\Security\Core\Exception\AccessDeniedException
     */
    public function getAction(Request $request)
    {
        $request = GlobalRequest::createFromGlobals();
        if($request->get('access_token'))
        {
            /** @var \Panel\OAuth2Bundle\Entity\OAuth2AccessToken $token */
            $token = $this->getDoctrine()->getRepository('Panel\OAuth2Bundle\Entity\OAuth2AccessToken')->findOneByToken($request->get('access_token'));

            if (!$token) {
                throw new AccessDeniedException('Token is required');
            }
            
            if ($token->getExpiresAt() < time())
            {
                throw new AccessDeniedException('Token expired');
            }

            $user = $token->getUser();
            if (!$user instanceof UserInterface) {
                throw new AccessDeniedException('This user does not have access to this section.');
            }

            $json = array();
            if($user)
            {
                $json = array (
                    'name' => $user->getFirstName().' '.$user->getLastName(),
                    'email' => $user->getEmailAddress()
                );
            }
        } else {
            $json = array("error" => "token is required");
            
        }
        return new JsonResponse($json);
    }
}