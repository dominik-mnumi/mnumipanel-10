<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 
 * Date: 28.10.15
 * Time: 14:06
 * Filename: Text.php
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
namespace Panel\DoctrineBundle\Form\Transformer;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

/**
 * Class Text
 * @package Panel\SettingsBundle\Form\Transformer
 */
class TextTransform implements DataTransformerInterface
{
    private $manager;
    /**
     * @var array
     */
    private $options;

    public function __construct(ObjectManager $manager, array $options)
    {
        $this->manager = $manager;
        $this->options = $options;
    }

    /**
     * Transforms an object to a string.
     *
     * @param  null $data
     * @return string
     */
    public function transform($data)
    {
        if (null === $data || !$data instanceof $this->options['data_class']) {
            return '';
        }

        return $data;
    }

    /**
     * Transforms a string to an object.
     *
     * @param  string $text
     * @return null
     * @throws TransformationFailedException if object  is not found.
     */
    public function reverseTransform($text)
    {
        if (!$text) {
            return;
        }
        $repository = $this->manager->getRepository($this->options['repository'])->findOneBy(array($this->options['transform_name'] => $text));

        if (null === $repository) {
            // causes a validation error
            // this message is not shown to the user
            // see the invalid_message option
            throw new TransformationFailedException(sprintf(
                'An object with name "%s" does not exist!',
                $text
            ));
        }

        return $repository;
    }
}