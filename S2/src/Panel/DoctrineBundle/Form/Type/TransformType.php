<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 
 * Date: 28.10.15
 * Time: 13:29
 * Filename: Transform.php
 * @author Dominik Labudzinski <dominik.labudzinski@mnumi.com>
 */
namespace Panel\DoctrineBundle\Form\Type;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Panel\DoctrineBundle\Form\Transformer\TextTransform;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Class Transform
 * @package Panel\SettingsBundle\Form\Type
 */
class TransformType extends AbstractType {

    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $transformer = new TextTransform($this->manager, $options);
        $builder->addModelTransformer($transformer);
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'transform';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'repository' => null,
                'transform_name' => 'name',
            )
        );
    }

    /**
     * Configures the options for this type.
     *
     * @param OptionsResolver $resolver The resolver for the options.
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->addAllowedTypes('repository');
        $resolver->addAllowedTypes('transform_name');
        $resolver->addAllowedTypes('data_class');

    }

    /**
     * @return null|string|\Symfony\Component\Form\FormTypeInterface
     */
    public function getParent()
    {
        return 'text';
    }
}