<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

$baseAppDir = (isset($_SERVER["MNUMICORE_ROOT"]))
    ? $_SERVER["MNUMICORE_ROOT"].'/../S2/app'
    : __DIR__
;
/**
 * @var ClassLoader $loader
 */
$loader = require $baseAppDir.'/../vendor/autoload.php';

AnnotationRegistry::registerLoader(array($loader, 'loadClass'));

return $loader;
