<?php

$baseAppDir = (isset($_SERVER["MNUMICORE_ROOT"]))
    ? $_SERVER["MNUMICORE_ROOT"].'/../S2/app'
    : __DIR__
;

require_once $baseAppDir.'/AppKernel.php';

use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;

class AppCache extends HttpCache
{
}
