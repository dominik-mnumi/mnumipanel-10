<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Convert Symfony 1.4 database configuration
 */

use Symfony\Component\Yaml\Parser;

$baseDir = (isset($_SERVER["MNUMICORE_ROOT"]))
    ? $_SERVER["MNUMICORE_ROOT"].'/..'
    : __DIR__.'/../../..'
;

$configurationFile = $baseDir . "/config/databases.yml";

$yaml = new Parser();
$configuration = $yaml->parse(file_get_contents($configurationFile));

$dsn = $configuration['all']['doctrine']['param']['dsn'];

list($type, $data) = preg_split('/:/', $dsn);


$params = preg_split('/;/', $data);

if (count($params) == 3) {
    list($hostParam, $dbParam, $encParam) = preg_split('/;/', $data);
} else {
    list($hostParam, $dbParam) = preg_split('/;/', $data);
    $encParam = 'utf8';
}

$container->setParameter('database_driver', 'pdo_' . $type);

list($key, $host) = preg_split('/=/', $hostParam);
$container->setParameter('database_host', $host);

list($key, $db) = preg_split('/=/', $dbParam);
$container->setParameter('database_name', $db);

$container->setParameter('database_user', $configuration['all']['doctrine']['param']['username']);
$container->setParameter('database_password', $configuration['all']['doctrine']['param']['password']);
$container->setParameter('database_port', null);

$appFile = $baseDir . "/apps/mnumicore/config/app.yml";
$appConfig = $yaml->parse(file_get_contents($appFile));
$container->setParameter('data_dir', $baseDir . "/data");
$container->setParameter('data_cache', $baseDir . "/cache");

$availableParameters = array(
    'price_tax',
    'wizard',
    'company_data',
    'notification',
    'invoice',
    'shop_host',
    'shop_url_login',
    'fotolia_api_key',
    'fotolia_language_id',
    'default_currency',
    'loyalty_points_reverted_scale'
    );

foreach($availableParameters as $main_parameter) {
    if(isset($appConfig['all'][$main_parameter])) {
        $container->setParameter('old_'.$main_parameter, $appConfig['all'][$main_parameter]);
        continue;
    }
    
    list($prefix, $parameter) = explode('_', $main_parameter, 2);
    if(isset($appConfig['all'][$prefix][$parameter]))
    {
        $prefixName = 'old_'.str_replace(".", "", $prefix).'_'.$parameter;
        $container->setParameter($prefixName, $appConfig['all'][$prefix][$parameter]);
        continue;
    }

    $exploded = explode('_', $main_parameter);
    $prefix = implode('_', array_slice($exploded, 0, 2));
    $parameter = implode('_', array_slice($exploded, 2));
    if(isset($appConfig['all'][$prefix][$parameter]))
    {
        $prefixName = 'old_'.str_replace(".", "", $prefix).'_'.$parameter;
        $container->setParameter($prefixName, $appConfig['all'][$prefix][$parameter]);
    }
}
