<?php

$rootDir = dirname(dirname($_SERVER["SCRIPT_FILENAME"]));
require_once($rootDir.'/config/ProjectConfiguration.class.php');

$configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'prod', false, $rootDir);
sfContext::createInstance($configuration)->dispatch();
