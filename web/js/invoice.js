/**
 * Standarizes price to specific format.
 */
function standarizeRegionalAmount(value)
{   
    return replaceCommaToDot(value);
}
    
/**
 * Formats value to fixed float.
 */
function replaceCommaToDot(value)
{
    if(value == '')
    {
        value = 0;
    }

    var value = parseFloat(value
        .toString()
        .replace(/\,/i, '.')).fixToFixed(2);
        
    // some fixes
    if(value == '-0.00')
    {
        value = '0.00';
    }    
        
    return value;
}
    
/**
 * Calculate No item number.
 */
function recalculateNoItems()
{
    var i = 1;
    $(".invoice_item_row:visible").each(
        function()
        {
            $(this).children(".item-no-td").html(i);
            i++;
        });
}
    
function getOneItemPriceNet(selector)
{       
    return standarizeRegionalAmount($(selector).find(".item_price_net").val());
}
    
/**
 * Returns tax. 
 */
function getTax(selector)
{
    var tax = $(selector)
    .find(".item_tax")
    .val();
        
    return tax;
}
    
/**
 * Returns quantity. 
 */
function getQuantity(selector)
{
    var quantity = $(selector)
    .find(".item_quantity")
    .val();
        
    return quantity;
}
    
/**
 * Returns amount of discount. 
 */
function getPriceDiscount(selector)
{
    return standarizeRegionalAmount($(selector).find(".item_price_discount").val());
}
    
/**
 * Calculates item price net. 
 * 
 * @return itemPriceNet * quantity
 */
function getItemPriceNet(selector)
{       
    var oneItemPriceNet = getOneItemPriceNet(selector);
    var quantity = getQuantity(selector);

    return standarizeRegionalAmount(oneItemPriceNet * quantity);
}
    
/**
 * Calculates item tax amount after discount. 
 * 
 * @return itemPriceNet - itemPriceDiscount
 */
function getItemTaxAmount(selector)
{
    var itemPriceNetAfterDiscount = getItemPriceNetAfterDiscount(selector);
    var tax = getTax(selector);
        
    return standarizeRegionalAmount(itemPriceNetAfterDiscount * (tax / 100));        
}
    
function getItemPriceNetAfterDiscount(selector)
{
    var itemPriceNet = getItemPriceNet(selector);
    var itemPriceDiscount = getPriceDiscount(selector);
        
    return standarizeRegionalAmount(itemPriceNet - itemPriceDiscount);        
}

/**
 * Calculate item price gross.
 */
function getItemPriceGross(selector)
{      
    var itemPriceNetAfterDiscount = getItemPriceNetAfterDiscount(selector);   
    var tax = getTax(selector);

    var itemCalculatePriceGross = standarizeRegionalAmount(itemPriceNetAfterDiscount * (1 + (tax / 100)));

    return itemCalculatePriceGross;
}
    
/**
 * Calculate total net price.
 *
 * @return float
 */
function getTotalNetPrice()
{
    var totalNetPrice = 0;
    var itemPrice = 0;
    
    // gets all items higher then 0 and summarizes
    $(".invoice_item_row:visible").each(function()
    {     
        itemPrice = parseFloat(getItemPriceNetAfterDiscount(this));
        if(itemPrice > 0)
        { 
            totalNetPrice += itemPrice;
        }
    });

    if(isCorrection)
    {
        totalNetPrice -= parentNetAmount;
    }

    return totalNetPrice;
}
    
/**
 * Calculate total gross price.
 *
 * @return float
 */
function getTotalGrossPrice()
{
    return getTotalNetPrice()+getTotalTaxAmount();
}
    
/**
 * Calculate total tax price.
 *
 * @return float
 */
function getTotalTaxAmount()
{
    var totalTaxPrice = 0;        
    var itemPrice = 0;

    // gets all items and summarizes
    $(".invoice_item_row:visible").each(function()
    {    
        itemPrice = parseFloat(getItemTaxAmount(this));
        if(itemPrice > 0)
        {
            totalTaxPrice += itemPrice;
        }
    });
    
    if(isCorrection)
    {
        totalTaxPrice -= parentVatAmount;
    }
 
    return totalTaxPrice;
}
    
function renderCalculations()
{
    // renders every price gross item
    $(".invoice_item_row:visible").each(function()
    {   
        // gets item price net
        var priceNet = getItemPriceNet(this);
        
        if(!allowZeroedPrices && !invoiceIsPaid) {
            var priceDiscount = getPriceDiscount(this);
            if(parseFloat(priceDiscount) >= parseFloat(priceNet) && parseFloat(priceNet) > 0.01) {
                $(this).find('.item_price_discount').val(parseFloat(priceNet) - 0.01);
                renderCalculations();
            }
        }
        
        $(this).find(".item_calculate_price_net").html(priceNet);

        // gets price set after discount
        var priceNetAfterDiscount = getItemPriceNetAfterDiscount(this);
        $(this).find(".item_calculate_price_net_after_discount").html(priceNetAfterDiscount);

        // gets tax amount
        var taxAmount = getItemTaxAmount(this);
        $(this).find(".item_calculate_tax_amount").html(taxAmount);        

        // gets price gross        
        var priceGross = getItemPriceGross(this);
        $(this).find(".item_calculate_price_gross").html(priceGross);
    });
        
    // gets total prices
    var totalPriceNet = standarizeRegionalAmount(getTotalNetPrice());
    var totalPriceGross = standarizeRegionalAmount(getTotalGrossPrice());
    var totalTaxAmount = standarizeRegionalAmount(getTotalTaxAmount());


    // injects values to inputs
    $(".invoice_price_net_total").html(totalPriceNet);
    $(".invoice_price_gross_total").html(totalPriceGross);
    $(".invoice_price_tax_total").html(totalTaxAmount);
}
   
/**
 * If no visible item then add one (for new invoice).
 */
function addFirstItem()
{
    var length = $(".invoice_item_row:visible").length;

    if(!length)
    {
        addNewItem();
        recalculateNoItems();
    }
}

function disableInvoiceFormInputs()
{
    // disables all inputs 
    $("#invoice-form input:not(#invoice_payed_at), #invoice-form select, #invoice-form textarea")
    .attr("readonly", "readonly").css({ 'background-image': 'none', 'background-color': '#DDD' });
    
    $("#invoice-form select, #invoice-form input:not(#invoice_payed_at), #invoice-form textarea")
    .each(function(index) 
    {
        var name = $(this).attr('name');
        var value;
        
        if($(this).attr('id') == 'invoice_full_address')
        {   
             value = $(this).html();
        }
        else
        {
            value = $(this).val();
        }
        
        $("#invoice-form").prepend('<input type="hidden" name="' 
            + name + '" value="' + value + '" />');
    });

    // disables custom buttons
    $("#item-add-button").attr("disabled", "disabled");
    
    $(".item_actions img").remove();
    $("#payment-date-at-span .ui-datepicker-trigger").hide();
}

jQuery(document).ready(function($)
{
    /**
     * Calculate at start.
     */
    recalculateNoItems();

    // calculate
    $(".item_unit_of_measure, .item_quantity, .item_calculate_price_net_1_item, .item_price_discount, .item_tax").change(
        function()
        {
            // renders calculations
            renderCalculations();      
        }
        );
           
    // add one item if no exist
    addFirstItem();
           
    // renders prices at start
    renderCalculations();   
});