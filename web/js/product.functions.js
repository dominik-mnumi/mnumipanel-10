/**
 * Renders (show or hide empty field in default value)
 * @param selector multiselect selector
 *
 */
function serviceEmptyOption(selector)
{
    var values = [];

    selector.find(".multiselect :selected").each(
        function(i, selected)
        {
            values[i] = new Array();

            values[i][0] = $(selected).val();
            values[i][1] = $(selected).text();
        });

    var defaultValueSelector = selector.children(".default_value_p").children("span").children("select");
    var visibleCheckboxValue = selector.find(".visible_form_content_checkbox").is(':checked');
    var requiredCheckboxValue = selector.find(".requiredCheckbox").is(':checked');
    var html = '';
    var emptyArray = new Array();

    emptyArray[0] = emptyValueId;
    emptyArray[1] = '--------';

    var hasEmpty = false;
    for(var key in values)
    {
        if(values[key][0] == emptyValueId)
        {
            hasEmpty = true;
            break;
        }
    }

    if(!hasEmpty)
    {
        values.unshift(emptyArray);
    }

    // if one or more selected options add empty
    if(((visibleCheckboxValue && requiredCheckboxValue)
        || !visibleCheckboxValue
        || (values.length == 1 && values[0][0] == emptyValueId))
        && selector.hasClass('tab_other'))
    {
        for(var key in values)
        {
            if(values[key][0] == emptyValueId)
            {
                values.splice(key, 1);
            }
        }
    }

    var defaultValue = defaultValueSelector.val();
    var selected;
    for(i in values)
    {
        if(values[i][0] == defaultValue)
        {
            selected = ' selected="selected" ';
        }
        else
        {
            selected = ' ';
        }

        html =  html + '<option' + selected + 'value="' + values[i][0] + '">' + values[i][1] + '</option>\n';
    }

    //insert elements
    defaultValueSelector.html(html);
}

$(function()
{
    // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
    $("#dialog:ui-dialog").dialog("destroy");

    //category delete form
    $("#dialog-delete-category-form").dialog({
        autoOpen: false,
        resizable: false,
        height: 170,
        modal: true,
        buttons: {
            deleteCancel: function()
            {
                $(this).dialog("close");
            },
            deleteOk: function()
            {
                $('#deleteCategorySubmitButton').trigger('click');
                $(this).dialog("close");
            }
        }
    });

    //form
    //label
    $(".change_checkbox_product_form").click(
        function()
        {
            $(this).parent().next("p.change_label_product_form").toggle();
            $(this).parent().next("p.change_label_product_form").children("span").children("input").val("");
        }
    );

    $('input.package_order_checkbox_product_form').click(
        function()
        {
            var input1 = $('input[name="product_0[is_static_quantity]"]').is(':checked');
            var input2 = $('input[name="product_0[simple_calculation]"]').is(':checked');

            $('.package_order_product_form').toggle(input1 || input2);
        }
    );

    // visible form content
    $(".visible_form_content_checkbox").click(
        function()
        {
            $(this).parents(".tabs-content")
                .children(".visible_form_content")
                .toggle();

            //gets current select node
            var selectNode = $(this).parents(".tabs-content")
                .children(".visible_form_content")
                .children("p")
                .children("span")
                .children("select");

            //if hidden then check all options
            if($(this).parents(".tabs-content").children(".visible_form_content").is(":hidden"))
            {
                //select all options
                selectNode.find("option")
                    .each(function()
                    {
                        $(this).attr("selected", "selected");
                    });
            }
            else
            {
                var defaultValue = $(this).parents(".tabs-content").find(".default_value_p select").val();

                //unselect all options
                selectNode.find("option")
                    .each(
                    function()
                    {
                        // if select option has value different 
                        // than default value then unselect
                        if($(this).val() != defaultValue)
                        {
                            $(this).removeAttr('selected')
                        }
                    });
            }

            //update default values
            selectNode.trigger("change");
        }
    );

    // default values select list
    $(".multiselect, .requiredCheckbox").change(
        function()
        {
            var selector = $(this).parents(".tabs-content");
            serviceEmptyOption(selector);
        });

    //AJAX SECTION


    //add new tab
    $("#new_tab").click(
        function()
        {
            var tabName = $(".tab_other_li:hidden").first().children("a").attr("href").replace("#", "");

            //sets true to other tab
            $("#" + tabName).children(".valid_hidden_field").val("1");

            //makes it visible
            $(".tab_other_li:hidden").first().show();
        }
    );

    $(".delete_tab_other").click(
        function()
        {
            //set valid input value and hide
            $(this).parents(".tab_other").children(".valid_hidden_field").val("");
            $(this).parents(".tab_other").hide();
            $(".current").hide();

            //remove class
            $(".current").removeClass("current");

            //add class current for first class and make visible
            $(".side-tabs li").first().addClass("current");
            $(".col200pxL-right div").show();
        }
    );

    //changes button text
    //$('div.ui-dialog-buttonpane button:contains(selectPriceCancel)').empty().html(toJsSelectFixpriceList['selectPriceCancel']);
    //$('div.ui-dialog-buttonpane button:contains(selectPriceOk)').empty().html(toJsSelectFixpriceList['selectPriceOk']);

    $("#dialog-fixprice-selectlist-click").click(function() {
        $.modal({
            content: $("#dialog-fixprice-selectlist-form"),
            title: toJsSelectFixpriceList['selectPricelist'],
            maxWidth: 300,
            resizable: false,
            buttons: {
                'Close': function(win) {
                    win.closeModal();
                }
            }
        });
    });

    //add subtab of fixed price
    $("#selectFixpriceList").bind("change",
        function()
        {
            if($(this).val() > 0)
            {
                $('#modal').closeModal();

                pricelistId = $(this).val();
                productId = $("#productId").val();

                var pricelistName = $(this).children("option[value='" + pricelistId + "']").html();

                //sets added href list: title, var pricelistId and shows
                $("[href=#tab-fix-" + nb + "]").html(pricelistName)
                $("[href=#tab-fix-" + nb + "]").attr("pricelistId", pricelistId);
                $("[href=#tab-fix-" + nb + "]").parent().show();

                //deletes this option fron context
                $(this).children("option[value='" + pricelistId + "']").hide();
                $(this).val("");
                $("#tab-fix-" + nb).html(addFixedPriceTab());
                $(".add_new_fixprice_row").on('click', add_new_fixprice_row);
                nb++;
            }
        }
    );

    //delete subtab of fixed price
    $(".delete_fixedprice_subtab").live("click",
        function()
        {
            var tabKey = $(this).parents(".tab_fixprice_subcontent").attr("id");

            //gets tab name
            var pricelistName = $("[href=#" + tabKey + "]").html();
            var pricelistId = $("[href=#" + tabKey + "]").attr("pricelistId");


            //makes hidden
            $("[href=#" + tabKey + "]").parent("li").removeClass("current").hide();

            $(this).parents(".tab_fixprice_subcontent").html("");

            //add option back to select list

            $("#selectFixpriceList").children("option[value='" + pricelistId + "']").show();
            $("a[href=#tab-fix-3]").trigger('click');
        }
    );

    var add_new_fixprice_row = function() {
        $(this).parent('.tab_fixprice_subcontent').find('table').find('tbody').find('tr:hidden:first').show();
    };

    //fixprice interior mechanism
    $(".add_new_fixprice_row").on('click', add_new_fixprice_row);

    $(".del_fixedprice_row").live('click',
        function()
        {
            $(this).parents("tr").remove();
        });

    $(".fixpriceInput").live('click, keyup',
        function()
        {
            var tmp = true;

            $(this).parents("table").children("tbody").children("tr").children("td").children(".fixpriceInput:visible").each(
                function(index, object)
                {
                    if($(object).val() == '')
                    {
                        tmp = false;
                    }
                }
            );
            if(tmp)
            {
                $(this).parents(".tab_fixprice_subcontent").children(".add_new_fixprice_row").trigger('click');
            }
        });

});

jQuery(document).ready(function($)
{
    $("#uploader_available").change(function()
    {
        var isChecked = $(this).attr('checked');
        $(".file_required, .file_multiprint").toggleClass('hidden', !isChecked);
    });

    var tagsRange = [];

    for(var i=1;i<11;i++) tagsRange.push("" + i);
    for(var i=15;i<50;i=i+5) tagsRange.push("" + i);
    for(var i=60;i<100;i=i+10) tagsRange.push("" + i);
    for(var i=100;i<500;i=i+50) tagsRange.push("" + i);
    for(var i=500;i<1000;i=i+100) tagsRange.push("" + i);
    for(var i=1000;i<5000;i=i+500) tagsRange.push("" + i);
    for(var i=5000;i<10000;i=i+1000) tagsRange.push("" + i);
    for(var i=10000;i<100000;i=i+5000) tagsRange.push("" + i);
    for(var i=500000;i<1000000;i=i+10000) tagsRange.push("" + i);

    $("select.select2").select2();
    $("#product_0_static_quantity_options").select2({
        tags:tagsRange,
        tokenSeparators: [",", " "]
    });
});


