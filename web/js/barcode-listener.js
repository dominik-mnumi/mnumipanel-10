var checkAdditionalNedded = checkAdditionalNedded || false;

var intervalID;
var intervalID2;
// global variable determines if any barcode is readed now
var barcodeInProgress = false;

var lastQueue = null;

function showPrintlabelQueue(printlabels)
{
    $('#allPrintlabels tbody').html('');
    $.each(printlabels, function() 
    {
        $('#allPrintlabels tbody').append('<tr><td>' + this.id + '</td><td>' + this.printer_name+
                    '</td><td>' + this.status + '</td></tr>');
    });
}

/**
 * Shows button loader and adds disabled.
 */
function showButtonLoader()
{
    $('.print-again-button').prepend('<img src="/images/loader.gif" width="18" height="18" style="margin-right: 5px;" />');  
    $('.print-again-button').attr('disabled', 'disabled');
}

/**
 * Hides button loader and removes disabled.
 */
function hideButtonLoader()
{
    $('.print-again-button img').remove();      
    $('.print-again-button').removeAttr('disabled');
}

$(function($)
{
    // focusout input if first sign is barcode prefix
    $('#worklog-form').on('keypress','input', function(e)
    {
        if(String.fromCharCode(e.which) == barcodePrefix)
        {
            $(this).blur();
            return false;
        }
    });
	
    function clearFormElements(form)
    {
        $(form).find(':input').each(function() {
            switch(this.type) {
                case 'password':
                case 'select-multiple':
                case 'select-one':
                case 'text':
                case 'textarea':
                    $(this).val('');
                    break;
                case 'checkbox':
                case 'radio':
                    this.checked = false;
            }
        });
    }
	
    //clears all data and lock page
    function lockPage()
    {
        $("#worklog-form").hide();
        $('#page-authorized').hide();
        $('#worklog-wait').show();
        $('#page-locked').show();
        //clear previous form elements
        clearFormElements('#global-message');
        // send request to remove authorization
        $.get(removeAuthorization, function() {});
        $('#global-message').html('');
    }
	
    //save worklog in database 
    //if quiet is set we dont want to view form save result - for example when we save on page lock
    //if lockPage is set it means that we want to lock page AFTER save
    function saveWorklog(quiet, lock)
    {
        // cancel if form is not visible or form data is not set
        if(!$("#worklog-form").is(":visible") || !$("#worklog-form #OrderWorklogForm_id").val())
        {
            if(lock)
            {
                lockPage();
            }
            return;
        }
        var form = $("#worklog-form");
        var uri =  form.attr('action');
        var method = form.attr('method');
        var dataInput = form.serialize();

        $.ajax(
        {
            url: uri,
            type: method,
            data: dataInput,
            dataType: "json",
            success: function(formResult)
            {
                if(lock)
                {
                    lockPage();
                }
                if(!quiet)
                {
                    // clear previous error messages
                    $('#global-message').html('').show().stop().removeClass('success error');
                    if(formResult.errors)
                    {
                        $('#global-message').addClass('error');
                        $.each(formResult.errors, function() 
                        {
                            $('#global-message').append('<li>' + this + '</li>');
                        });
                    }
                    else
                    {
                        $('#global-message').addClass('success');
                        $('#global-message').append('<li>'+formResult.success+'</li>');

                        $('#previousWorklogs tbody').html('');
                        $.each(formResult.previousWorklogs, function() 
                        {                                        
                            $('#previousWorklogs tbody').append('<tr><td>'+this.username+'</td><td>'+this.value+
                                        '</td><td>'+this.notice+'</td><td>'+this.createdAt+'</td></tr>');
                        });
                    }

                    $('#global-message').stop().delay(5000).fadeOut('slow');
                }
            }
        });
    }
    
    $(document.body).on('keypress',function(e)
    {
        addKey(String.fromCharCode(e.which));
    });
    
    var code = '';
    var timerCount = 60;

    function addKey(key) 
    {
        if(barcodePrefix == key || code != '')
        {
            code += key;
        }

        if(code.length >= 10 && key == barcodeSufix)
        {
            if(barcodeInProgress)
            {
                return;
            }
            barcodeInProgress = true;
            $("#progressAndWait").show();
            console.log(code);

            // save Worklog if is current edited
            saveWorklog(true);
            var tmpCode = code;

            if(checkAdditionalNedded === false) {

                sendBarcode(tmpCode);

            } else {

                $.ajax(
                {
                    type: 'GET',
                    url: checkAdditionalNedded,
                    data: "barcode="+encodeURIComponent(tmpCode),
                    dataType: "json",
                    success: function(data) {

                        if(data.isNedded) {

                            var form = $(data.formContent);
                            var title = form.find('#modal-title').attr('title');
                            form.show();

                            $("#progressAndWait").hide();

                            $.modal({
                                content: form,
                                title: title,
                                buttons: {
                                    'OK': function(win) {
                                        var dataForm = $('.modal-content').find('form');
                                        var sendData = dataForm.serialize();
                                        var url = form.attr('action');

                                        $.ajax({
                                            type: 'POST',
                                            url: url,
                                            data: sendData,
                                            dataType: 'json',
                                            success: function(data) {

                                                if('success' == data.result.status)
                                                {
                                                    if(0 != data.result.error.errorCount)
                                                    {
                                                        var errorlist = renderError(data.result.error.errorArray);

                                                        $(".modal-content .validateTips").html(errorlist);
                                                    }
                                                    else
                                                    {
                                                        win.closeModal();
                                                        $("#progressAndWait").show();
                                                        sendBarcode(tmpCode);
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });

                        } else {

                            sendBarcode(tmpCode);
                        }
                    }
                });
            }

            code = '';
        }

        if(code.length > 20)
        {
            code = '';
        }
    } 

    $('#printAgain').on('click', function()
    {
        if(lastQueue)
        {
            showButtonLoader();
            $.post(printAgain, { queueId: lastQueue } );
        } else {
            if ($('#orderPackageId')[0]) {
                var _packageId = $('#orderPackageId').val();
            }
            else {
                var _packageId = packageId;
            }
            showButtonLoader();
            $.post(printAgain, { id: _packageId } );
        }


    });
    
    // save form on enter without normal request
    $('#worklog-form').keydown(function(event)
    {
        if(event.keyCode == 13) 
        {
            event.preventDefault();
            saveWorklog();
            return false;
        }
    });

    function sendBarcode(code) {
    // send barcode to action

        $.ajax(
        {
            type: 'GET',
            url: barcodeRead,
            data: "barcode="+encodeURIComponent(code),
            dataType: "json",
            error: function()
            {
                $("#progressAndWait").hide();
                barcodeInProgress = false;
            },
            success: function(result)
            {
                // redirecting to system resources (clients ,users, orders etc)
                if(result.redirect)
                {
                    window.location.replace(result.redirect);
                }

                // viewing messages for user
                if(result.message)
                {
                    $('#global-message').html('').show().stop().removeClass('success error');
                    if(result.message.type == 'error')
                    {
                        var message = result.message.text + "<br/>" + result.message.printInfo;
                        $('#global-message').addClass('error').append('<li>'+message+'</li>');
                    }
                    else
                    {
                        var message = result.message.text + "<br/>" + result.message.printInfo;
                        $('#global-message').addClass('success').append('<li>' + message + '</li>');
                    }

                    $('#global-message').stop().delay(5000).fadeOut('slow');
                }

                // worklog was saved (now we want to edit it)
                if(result.worklog)
                {
                    // fill worklog orm with newly created worklog object
                    $('#OrderWorklogForm_value').val(result.worklog.value);
                    $('#OrderWorklogForm_id').val(result.worklog.id);
                    $('#OrderWorklogForm_material').val(result.worklog.material);

                    // submit form on select change
                    $('#worklog-form').on('change','select', function()
                    {
                        saveWorklog();
                    });

                    // submit form on input focusout
                    $('#worklog-form').on('blur','input', function()
                    {
                        saveWorklog();
                    });

                    // hide informations to scan code and show edit form
                    $('#worklog-wait').hide();
                    $('#worklog-form').show();

                    //show previous worklogs
                    if(result.worklog.previousWorklogs)
                    {
                        $('#previousWorklogs tbody').html('');
                        $.each(result.worklog.previousWorklogs, function()
                        {
                            $('#previousWorklogs tbody').append('<tr><td>' + this.username + '</td><td>' + this.value +
                            '</td><td>' + this.notice + '</td><td>' + this.createdAt + '</td></tr>');
                        });
                    }
                }

                // show other all orders in table
                if(result.allOrders)
                {
                    $('#allOrders tbody').html('');
                    $('#allOrders').show();
                    $('#orderPackageId').val(result.orderPackageId);

                    $.each(result.allOrders, function()
                    {
                        var shelfId = (this.shelf_id == null) ? '-' : this.shelf_id;

                        $('#allOrders tbody').append('<tr><td>'+this.id+'</td><td>'+this.name+
                                    '</td><td>'+shelfId+'</td><td>'+this.price_net+'</td></tr>');
                    });
                }

                // show other printlabel queue
                if(result.allPrintlabels)
                {
                    lastQueue = result.allPrintlabels.length > 0 ? result.allPrintlabels[result.allPrintlabels.length-1].id : null;
                    console.log(lastQueue);
                    $('#allPrintlabels').show();
                    $('#printlabelTitle').show();

                    intervalID2 = setInterval(function()
                    {
                        $.ajax({
                            url: printlabelQueue,
                            type: 'post',
                            dataType: "json",
                            success: function(printlabels)
                            {

                                showPrintlabelQueue(printlabels);
                                hideButtonLoader();
                            }
                        });
                    }, 5000);

                    showPrintlabelQueue(result.allPrintlabels);
                }

                // printer authorization
                if(result.auth)
                {
                    // resets print label interval
                    if(intervalID2)
                    {
                        clearInterval(intervalID2);
                    }

                    $('#page-locked').hide();
                    $('#page-authorized').show();

                    var resetTimer = function()
                    {
                        var timerCount = activeTime;
                        if(intervalID)
                        {
                            clearInterval(intervalID);
                        }
                        intervalID = setInterval(function()
                        {
                            $("#countdown").html(timerCount);
                            if (timerCount == 0)
                            {
                                    // reset default html
                                    $("#countdown").html(activeTime);
                                    saveWorklog(true, true);
                                    clearInterval(intervalID);
                            }
                            timerCount--;
                        }, 1000);
                    };

                    resetTimer();
                    $('#worklog-form').on('change', function()
                    {
                        resetTimer();
                    });

                    $('#printlabelTitle').hide();
                    $('#allOrders').hide();
                    $('#allPrintlabels').hide();
                }

                $("#progressAndWait").hide();

                barcodeInProgress = false;
            }
        });
    }
});

