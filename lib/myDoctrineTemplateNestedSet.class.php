<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Doctrine template which implements the external custom NestedSet implementation
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class myDoctrineTemplateNestedSet extends Doctrine_Template
{    
    /**
     * Returns true if current category is subcategory of branch, otherwise false
     *
     * @param integer $currentCatId
     * @return boolean
     */
    public function hasDescandant($currentCatId = null)
    {
        $node = $this->getInvoker();
   
        if($node->getId() == $currentCatId)
        {
            return true;
        }
        
        foreach($node->__children as $rec)
        {
            if($rec->hasDescandant($currentCatId))
            {
                return true;
            }
        }   
    }   
}

