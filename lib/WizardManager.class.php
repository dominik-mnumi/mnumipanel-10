<?php

/**
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author Marek Balicki
 */
class WizardManager
{
    public static $pdfStatusReady = 'ready';
    
    private static $_wizardManagerInstance;
    private $_signatureManager;
    
    /**
     * Returns WizardManager instance.
     * 
     * @return WizardManager 
     */
    public static function getInstance()
    {
        if(!self::$_wizardManagerInstance)
        {
            self::$_wizardManagerInstance = new WizardManager();
        }
        
        return self::$_wizardManagerInstance;
    }
    
    /**
     * Returns init url (new wizard action).
     * 
     * @return string 
     */
    public static function getNewUrl()
    {     
        return self::getWizardHostUrl().'initE';
    }
    
    /**
     * Returns init url (edit wizard action).
     * 
     * @return string 
     */
    public static function getEditUrl()
    {     
        return self::getWizardHostUrl().'editE';
    }
      
    /**
     * Returns init order url (new wizard clone action).
     * 
     * @return string 
     */
    public static function getNewOrderUrl()
    {     
        return self::getWizardHostUrl().'initOrder';
    }
    
    /**
     * Returns pdf url.
     *
     * @return string
     */
    public static function getPdfUrl()
    {
        return self::getWizardHostUrl().'pdfE';
    }
    
    /**
     * Returns pdf add queue url.
     *
     * @return string
     */
    public static function getPdfQueueAddUrl()
    {
        return self::getWizardHostUrl().'pdfcheck';
    }
    
    /**
     * Returns pdf status queue url.
     *
     * @return string
     */
    public static function getPdfQueueStatusUrl()
    {
        return self::getWizardHostUrl().'pdfstatus';
    }
    
    /**
     * Returns pdf url.
     *
     * @return string
     */
    public static function getPreviewUrl()
    {
        return self::getWizardHostUrl().'previewE';
    }
    
    /**
     * Returns back url (new wizard action).
     * 
     * @return string 
     */
    public static function getNewBackUrl()
    {   
        return self::getHostUrl().'/addWizard/%s';
    }
    
    /**
     * Returns back url (edit wizard action).
     * 
     * @return string 
     */
    public static function getEditBackUrl()
    {   
        return self::getHostUrl().'/editWizard';
    }
    
    /**
     * Returns copy url.
     * 
     * @return string 
     */
    public static function getCopyUrl()
    {   
        return self::getWizardHostUrl().'copy';
    }
      
    /**
     * Returns prepared array with parameters.
     * 
     * @param string $productName
     * @param string $backUrl
     * @param string $action
     * @param string $orderId
     * @param integer $pageNr
     * @param integer $width
     * @param integer $height
     * @param string $countChange
     * @param integer $count
     * @param integer $id
     * @param string $barcode
     * @param string $wizards
     * @return array 
     */
    public static function getPreparedArrayParameter($productName = null, 
            $backUrl = '', $action = 'initOrder', $orderId = null,
            $pageNr = null, $width = null, $height = null, $countChange = null,
            $count = null, $id = null, $barcode = null, $wizards = null)
    {
        // mandatory
        $parameterArray = array(
            'action' => $action,
            'backUrl' => $backUrl);
        
        if(isset($productName))
        {
            $parameterArray['productName'] = $productName;
        }
        
        if(isset($orderId))
        {
            $parameterArray['orderId'] = $orderId;
        }
        
        if(isset($pageNr))
        {
            $parameterArray['pageNr'] = $pageNr;
        }
        
        if(isset($width))
        {
            $parameterArray['width'] = $width;
        }
        
        if(isset($height))
        {
            $parameterArray['height'] = $height;
        }
        
        if(isset($countChange))
        {
            $parameterArray['countChange'] = $countChange;
        }
        
        if(isset($count))
        {
            $parameterArray['count'] = $count;
        }
        
        if(isset($id))
        {
            $parameterArray['id'] = $id;
        }
        
        if(isset($barcode))
        {
            $parameterArray['barcode'] = $barcode;
        }

        if(isset($wizards))
        {
            $parameterArray['wizards'] = $wizards;
        }
        
        return $parameterArray;
    }
 
    /**
     * Returns secret key.
     * 
     * @return string 
     */
    public static function getSecretKey()
    {
        return sfConfig::get('app_wizard_secret_key', '27a1c4124c5f05a98f37a8886c54c8f7463e02dc');
    }
    
    /**
     * Returns application host url.
     * 
     * @return string 
     */
    private static function getHostUrl()
    {
        $request = sfContext::getInstance()->getRequest();
        
        // please keep in mind, that: $request->getHost() != $_SERVER['SERVER_NAME']
        $serverName = $_SERVER['SERVER_NAME']; 
        
        $url = ($request->isSecure()) 
                   ? 'https://'.$serverName 
                   : 'http://'.$serverName;
                
        return $url;
    }
    
    /**
     * Returns wizard id (if defined)
     * 
     * @return string|null
     */
    public static function getId()
    {
        return sfConfig::get('app_wizard_id');
    }
    
    /**
     * Returns application wizard host url.
     * 
     * @return string 
     */
    private static function getWizardHostUrl()
    {
        return sfConfig::get('app_wizard_host', 'http://wizard.tests.mnumi.com/');
    }

    public function __construct()
    {
        $this->_signatureManager = new SignatureManager(
                        sfConfig::get('app_wizard_secret_key',
                                '27a1c4124c5f05a98f37a8886c54c8f7463e02dc'));
    }

    /**
     * Returns SignatureManager object.
     * 
     * @return SignatureManager 
     */
    public function getSignatureManager()
    {
        return $this->_signatureManager;
    }
    
    /**
     * Returns parameter and signature for new wizard.
     * based on productName and count.
     * 
     * @param string $productName
     * @param integer $countChange
     * @param integer $count
     * @param integer $orderId
     * @param string $wizards
     * @return string 
     */
    public function generateNewWizardEncodedParamAndSignature($productName, 
            $countChange, $count, $orderId = null, $barcode = null,
            $wizards = null)
    {
        // gets prepared parameter
        $parameterDecoded = self::getPreparedArrayParameter(
                $productName, 
                self::getNewBackUrl(), 
                null, 
                null, 
                null, 
                null, 
                null, 
                $countChange, 
                $count,
                $orderId, 
                $barcode,
                $wizards);

        return array('encodedParameter' => $this->_signatureManager->encodeParameter($parameterDecoded),
            'signature' => $this->_signatureManager->generateSignature($parameterDecoded));
    }

    /**
     * Returns parameter and signature for new wizard.  
     * based on productName and count.
     * 
     * @param string $projectName
     * @param string $productName
     * @param integer $orderId
     * @param string $barcode
     * @param string $wizards
     * @return array 
     */
    public function generateEditWizardEncodedParamAndSignature($projectName, 
            $productName, $orderId = null, $barcode = null,
            $wizards = null)
    {
        // gets prepared parameter
        $parameterDecoded = self::getPreparedArrayParameter(
                $productName, 
                self::getEditBackUrl(), 
                'editWizard', 
                $projectName,
                null,
                null,
                null,
                null,
                null,
                $orderId,
                $barcode,
                $wizards);

        return array('encodedParameter' => $this->_signatureManager->encodeParameter($parameterDecoded),
            'signature' => $this->_signatureManager->generateSignature($parameterDecoded));
    }
    
    /**
     * Returns parameter and signature for preview.
     * 
     * @param string $filename
     * @param integer $width
     * @param integer $height
     * @param integer $pageNr
     * @param integer $id
     * @return string 
     */
    public function generatePreviewEncodedParamAndSignature($filename = null, 
            $width = 400, $height = 400, $pageNr = 1, $id = null, $barcode = null)
    {
        // gets prepared parameter  
        // generate encoded parameter and signature (for preview)    
        $parameterDecoded = WizardManager::getPreparedArrayParameter(
                        null, 
                        null, 
                        'previewE',
                        basename($filename, '.pdf'),
                        $pageNr, 
                        $width, 
                        $height,
                        null,
                        null,
                        $id,
                        $barcode);

        return array('encodedParameter' => $this->_signatureManager->encodeParameter($parameterDecoded),
            'signature' => $this->_signatureManager->generateSignature($parameterDecoded));
    }
    
    /**
     * Returns ready url for wizard preview.
     * 
     * @param string $filename
     * @param integer $width
     * @param integer $height
     * @param integer $pageNr
     * @param integer $wizardId
     * @return string 
     */
    public function generatePreviewUrl($filename = null, $width = 100, 
            $height = 100, $pageNr = 1)
    {
        // generate encoded parameter and signature (for preview)    
        $resultArr = $this->generatePreviewEncodedParamAndSignature($filename, 
                $width, $height, $pageNr);
        
        $url = self::getPreviewUrl().'?parameter='.$resultArr['encodedParameter'].'&signature='.$resultArr['signature'];
        
        $wizardId = self::getId();
        
        // if wizardId is defined
        if($wizardId)
        {
            $url .= '&id='.$wizardId;
        }

        return $url;
    }

    /**
     * Returns parameter and signature for pdf.
     * 
     * @param string $productName
     * @param integer $orderId
     * @return array 
     */
    public function generatePdfEncodedParamAndSignature($projectName, $orderId = null, $count = 1, $barcode = null)
    {
        // gets prepared parameter  
        $parameterDecoded = self::getPreparedArrayParameter(
                null, 
                null, 
                'pdfE', 
                $projectName,
                null,
                null,
                null,
                null,
                $count,
                $orderId,
                $barcode);

        return array('encodedParameter' => $this->_signatureManager->encodeParameter($parameterDecoded),
            'signature' => $this->_signatureManager->generateSignature($parameterDecoded));
    }
    
    /**
     * Returns ready url for wizard pdf.
     * 
     * @param string $projectName
     * @param integer $orderId
     * @param string $barcode
     * @param integer $wizardId
     * @return string 
     */
    public function generatePdfUrl($projectName, $orderId = null, $barcode = null,
            $wizardId = null)
    {
        // generate encoded parameter and signature (for preview)    
        $resultArr = $this->generatePdfEncodedParamAndSignature($projectName, $orderId, $barcode);
        
        $url = WizardManager::getPdfUrl().'?parameter='.$resultArr['encodedParameter'].'&signature='.$resultArr['signature'];
        
        // if wizardId is defined
        if($wizardId)
        {
            $url .= '&id='.$wizardId;
        }
        
        return $url;
    }
    
    /**
     * Returns ready url for wizard queue check pdf.
     * 
     * @param string $projectName
     * @param integer $orderId
     * @param string $barcode
     * @param integer $wizardId
     * @return string 
     */
    public function generatePdfQueueAddUrl($projectName, $orderId = null, 
            $barcode = null, $wizardId = null)
    {
        // generate encoded parameter and signature (for preview)    
        $resultArr = $this->generatePdfEncodedParamAndSignature($projectName, $orderId, $barcode);
        
        $url = WizardManager::getPdfQueueAddUrl().'?parameter='.$resultArr['encodedParameter'].'&signature='.$resultArr['signature'];
        
        // if wizardId is defined
        if($wizardId)
        {
            $url .= '&id='.$wizardId;
        }
        
        return $url;
    }
    
    /**
     * Returns ready url for wizard queue status pdf.
     * 
     * @param string $projectName
     * @param integer $orderId
     * @param string $barcode
     * @param integer $wizardId
     * @return string 
     */
    public function generatePdfQueueStatusUrl($projectName, $orderId = null, 
            $barcode = null, $wizardId = null)
    {
        // generate encoded parameter and signature (for preview)    
        $resultArr = $this->generatePdfEncodedParamAndSignature($projectName, $orderId, $barcode);
        
        $url = WizardManager::getPdfQueueStatusUrl().'?parameter='.$resultArr['encodedParameter'].'&signature='.$resultArr['signature'];
        
        // if wizardId is defined
        if($wizardId)
        {
            $url .= '&id='.$wizardId;
        }
        
        return $url;
    }
    
    /**
     * Returns parameter and signature for wizard copy.
     * 
     * @param string $projectName
     * @return array 
     */
    public function generateCopyEncodedParamAndSignature($projectName)
    {
        // gets prepared parameter  
        $parameterDecoded = WizardManager::getPreparedArrayParameter(
                        null, null, 'copy', $projectName);
        
        return array('encodedParameter' => $this->_signatureManager->encodeParameter($parameterDecoded),
            'signature' => $this->_signatureManager->generateSignature($parameterDecoded));
    }
    
    /**
     * Returns copy url for wizard.
     * 
     * @param string $projectName
     * @param string $parameter
     * @param string $signature
     * @return string 
     */
    public function generateCopyUrl($projectName = null, $parameter = null, $signature = null)
    {
        if($parameter && $signature)
        {
            $resultArr = array('encodedParameter' => $parameter,
                'signature' => $signature);
        }
        // gets prepared parameter  
        else
        {   
            $resultArr = $this->generateCopyEncodedParamAndSignature($projectName);
        }

        return WizardManager::getCopyUrl().'?parameter='.$resultArr['encodedParameter'].'&signature='.$resultArr['signature'];
    }

    /**
     * Returns response from wizard server after copy action.
     *
     * @param $projectName
     * @throws Exception
     * @return int New Order Id
     */
    public function copyWizard($projectName)
    {
        $url = $this->getCopyUrl();
    	$resultArr = $this->generateCopyEncodedParamAndSignature($projectName);

        // WIZARD SERVER SECTION
        // creates curl resource 
        $ch = curl_init(); 

        // sets url 

        curl_setopt($ch, CURLOPT_URL, $url);

        // returns the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 

        // post method
        curl_setopt($ch, CURLOPT_POST, 1);
        
        // sets parameters
        $postData = array(
        		'parameter' => $resultArr['encodedParameter'],
        		'signature' => $resultArr['signature'],
        );
        
        // id (optional)
        $wizardId = WizardManager::getId(); 
        if($wizardId)
        {
            $postData['id'] = $wizardId;
        }
        
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

        $output = curl_exec($ch);
        
        if(!$output)
        {
            throw new Exception(sprintf('Wizard server not response with success. Url: %s, Post data: %s',
                $url, var_export($postData, true))
            );
        }
            	
        // contains the array output
        $results = json_decode($output, true);

        // close curl resource to free up system resources 
        curl_close($ch); 
        
        if($results === NULL)
        {
            throw new Exception('JSON response from Wizard could not be decoded.');
        }

        // checks if order id exists
        if(!array_key_exists('order_id', $results))
        {
            throw new Exception('Invalid response order id at');
        }

        return $results['order_id'];
    }
    
    
}