<?php

/**
 * OrderAttribute
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    mnumicore
 * @subpackage model
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class OrderAttribute extends BaseOrderAttribute
{

    /**
     * Event invoked before saving
     */
    public function preSave($event)
    {
        $invoker = $event->getInvoker();

        if(!$invoker->isNew())
        {
            $this->recordChanges();
        }

        $orderObj = $invoker->getOrder();
        
        if(sfContext::hasInstance())
        {
            $instance = sfContext::getInstance();
        }
        else
        {
            $instance = false;
        }
        
        if(!$instance || $instance->getConfiguration()->getEnvironment() == 'test')
        {
            // for test enviroment
            $userObj = sfGuardUserTable::getInstance()->findOneByUsername('admin');
            
            if(!$userObj)
            {
                throw new \Exception('There is no "admin" username to run tests. Please check your fixtures data. ');
            }
            
            $userId = $userObj->getId();
        }
        elseif($instance->getUser()->isAuthenticated())
        {
            // for normal use (we are logged in panel)
            $userObj = $instance->getUser()->getGuardUser();
            
            if(!$userObj)
            {
                throw new \Exception('Could not get sfGuardUser object. ');
            }
            
            $userId = $userObj->getId();            
        }
        elseif($orderObj->getUserId() != null)
        {
            // if still no user then get user object directly from order
            $userId = $orderObj->getUserId();
        }
        else
        {
            // if still no user - there is anonymouse user (using MnumiShop)
            $userId = false;
        }

        if($userId)
        {
            $invoker->setEditorId($userId);
        }
    }

    /**
     *
     */
    public function preDelete($event)
    {
        $quantityAttribute = $this->getOrder()->getAttribute('QUANTITY');
        $countAttribute = $this->getOrder()->getAttribute('COUNT');

        $quantityStockState = $quantityAttribute->createStockState($quantityAttribute->getValue(), 0);
        $countStockState = $countAttribute->createStockState($countAttribute->getValue(), 0);

        $this->changeStockAvailability($quantityStockState, $countStockState);
    }

    /**
     * Returns label for fieldset (field item)
     * 
     * @return string
     */
    public function getValueLabel()
    {
        return $this->getProductField()->getFieldsetLabel();
    }

    /**
     * Returns hotfolder filename.
     * 
     * @param string $orderStatus
     * @return string 
     */
    public function getHotfolderFilename($orderStatus = null)
    {
        $orderObj = $this->getOrder();
        return $orderObj->getHotfolderPath($orderStatus).$orderObj->getId().'.'
                .$this->getValue().'.pdf';
    }

    /**
     * Record all changes
     */
    protected function recordChanges()
    {
        $order = $this->getOrder();

        $changes = $this->getModified(true);

        if (count($changes) > 0 && $this->getEditorId() != null) {
            $changeGroup = ChangeGroupTable::getInstance()->addOrder($order);

            $productField = $this->getProductField();

            foreach ($changes as $field => $oldValue) {
                $methodName = 'get' . ucfirst($field);
                $newValue = $this->$methodName();

                /** @var ProductFieldItem $oldValueObject */
                $oldValueObject = $productField->getProductFieldItemById($oldValue);

                if ($oldValueObject) {
                    $oldValue = $oldValueObject->getFieldItem()->getName();
                }

                /** @var ProductFieldItem $oldValueObject */
                $newValueObject = $productField->getProductFieldItemById($newValue);

                if ($newValueObject) {
                    $newValue = $newValueObject->getFieldItem()->getName();
                }

                if ($newValue == $oldValue) {
                    continue;
                }

                $fieldValue = $this->getValueLabel();
                $changeGroup->addItem($fieldValue, $newValue, $oldValue);
            }

            $changeGroup->save();
        }
    }

    /**
     * Load value, replacing page count, when Product
     * has enabled "calculate_as_card" parameter
     *
     * @return string
     */
    public function getCalculatedValue()
    {
        $value = $this->getValue();

        if ($this->getProductField()->isCalculateAsCardEnabled()) {
            $sideOrderAttribute = $this->getOrder()->getAttribute('SIDES');

            if($sideOrderAttribute !== false) {
                $sideId = (int) $sideOrderAttribute->getValue();

                if($sideId > 0) {
                    /** @var FieldItem $side */
                    $side = FieldItemTable::getInstance()->find($sideId);

                    if ($side->getName() === 'Double') {
                        $value = (int) $value * 2;
                    }
                }
            }
        }

        return $value;
    }

    /**
     * Fix if exists cloned attribute, if not, we set first exists value.
     */
    public function fixNotExistAttribute()
    {
        $require = array();
        foreach ($this->getProductField()->getProductFieldItems() as $item)
        {
            $require[] = $item->getFieldItemId();
        }
        if(!in_array($this->getValue(), $require) && $require)
        {
            $this->setValue($require[0]);
        }
    }

    /**
     * Changes OrderAttribute stock availability based on changes in Quantity and Count
     */
    public function changeStockAvailability($quantityStockState, $countStockState)
    {
        if ($this->isStockField() === false)
        {
            return;
        }

        $modified = $this->getModified(true);
        if (!isset($modified['value']) || $modified['value'] === null)
        {
            $oldStock = $this->getValue();
        } else {
            $oldStock = $modified['value'];
        }
        $newStock = $this->getValue();

        $oldFieldItem = FieldItemTable::getInstance()->findOneById($oldStock);
        $newFieldItem = FieldItemTable::getInstance()->findOneById($newStock);

        $calculationToolOldStock = $this->getCalculation($oldFieldItem);
        $calculationToolNewStock = $this->getCalculation($newFieldItem);

        $stockDifference = $this->getStockChange($calculationToolOldStock, $calculationToolNewStock, $oldStock, $newStock);
        $stockChanges = $stockDifference->getStockChanges($quantityStockState, $countStockState)->getChanges();

        foreach ($stockChanges as $stockChange) {
            $fields = FieldItemTable::getInstance()->getFieldsUsedInStock(array($stockChange->getId()));
            if ($fields->count() > 0) {
                /** @var FieldItem $field */
                foreach ($fields as $field) {
                    /**
                     * Change stock availability
                     */
                    $requiredAmount = $this->getRequiredAmount($field->getId());
                    $field->changeAvailableStock($stockChange->getAmountDifference() * $requiredAmount);

                    $field->save();
                }
            }
        }
    }

    /**
     * Get required amount for current field
     *
     * @param int $id A FieldItem id
     * @return float|int
     */
    public function getRequiredAmount($id)
    {
        $productField = $this->getProductField();
        /** @var ProductFieldItem $productFieldItem */
        $productFieldItem = $productField->getProductFieldItemById($id);
        if($productFieldItem instanceof ProductFieldItem)
        {
            return $productFieldItem->getRequiredAmount();
        }
        return 1;
    }

    /**
     * Gets instance of StockChangeableInterface allowing to calculate
     * collection of stock changes which needs to be applied in Depository.
     *
     * @param StockCalculation calculation tool used before applying change
     * @param StockCalculation calculation tool used after applying change
     * @param int $oldStock OrderAttribute::value used before applying change
     * @param int $newStock OrderAttribute::value used after applying change
     *
     * @return \Mnumi\Bundle\DepositoryBundle\Library\StockChangeableInterface
     */
    public function getStockChange($calculationToolOldStock, $calculationToolNewStock, $oldStock, $newStock) {
        $stockType = $this->getProductField()->getFieldset()->getName();
        switch($stockType) {
        case "MATERIAL":
            return new \Mnumi\Bundle\DepositoryBundle\Library\StockChange\MaterialStockChange(
                $calculationToolOldStock,
                $calculationToolNewStock,
                $oldStock,
                $newStock
            );
        case "OTHER":
            return new \Mnumi\Bundle\DepositoryBundle\Library\StockChange\OtherStockChange(
                $calculationToolOldStock,
                $calculationToolNewStock,
                $oldStock,
                $newStock
            );
        }

        return new \Mnumi\Bundle\DepositoryBundle\Library\StockChange\DummyStockChange(
            $oldStock,
            $newStock
        );
    }

    /**
     * Helper method for checking if entry has been modified and its value changed
     *
     * @param boolean $deep     whether to process also the relations for changes
     * @return boolean
     */
    public function isChanged($deep = false) {
        return $this->isModified($deep) &&
            (count(array_diff($this->getModified(true), $this->getModified())) > 0);
    }

    /**
     * @return \Mnumi\Bundle\DepositoryBundle\Library\StockState
     */
    public function getStockState()
    {
        $modified = $this->getModified(true);
        if (!isset($modified['value']) || $modified['value'] === null)
        {
            $old = $this->getValue();
        } else {
            $old = $modified['value'];
        }
        $new = $this->getValue();

        return $this->createStockState($old, $new);
    }

    /**
     * @return \Mnumi\Bundle\DepositoryBundle\Library\StockState
     */
    public function createStockState($old, $new)
    {
        $fieldsetName = $this->getProductField()->getFieldset()->getName();
        if ($fieldsetName == "QUANTITY") {
            return new Mnumi\Bundle\DepositoryBundle\Library\StockState\QuantityStockState($old, $new);
        }
        else if ($fieldsetName == "COUNT") {
            return new Mnumi\Bundle\DepositoryBundle\Library\StockState\CountStockState($old, $new);
        }

        throw new \Exception(sprintf(
            'This method should be invoked only for QUANTITY or COUNT attributes, but was invoked for OrderAttribute:id=%d, from Fieldset::name=%s',
            $this->getId(), $fieldsetName
        ));
    }

    /**
     * Returns calculator class name for the given measure type of material
     *
     * @see CalculationToolReport::getMeasureOfUnit
     *
     * @param string $measureType available values: price_linear_metre & price_square_metre.
     *  If different one than the above is passed, Sheets calculator class name is returned.
     * @param \Mnumi\Bundle\CalculationBundle\Library\Size $sheetSize
     * @param \Mnumi\Bundle\CalculationBundle\Library\Size $useSize
     *
     * @return correct material calculator instance for given measureType
     */
    public function getMaterialCalculator($measureType, $sheetSize, $useSize)
    {
        $measureUnit = \Mnumi\Bundle\CalculationBundle\Library\Material\MeasureUnit::getUnit(sfConfig::get('app_size_metric', 'mm'));

        switch($measureType) {
        case "price_linear_metre":
            return new \Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation\LinearMetreCalculator($sheetSize, $useSize, $measureUnit);
        case "price_square_metre":
            return new \Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation\SquareMetreCalculator($sheetSize, $useSize, $measureUnit);
        }

        return new \Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\MaterialStockCalculation\SheetsCalculator($sheetSize, $useSize, $measureUnit);
    }

    /**
     * Gets correct StockCalculation for given FieldItem
     *
     * @param FieldItem $fieldItem for which StockCalculation will be returned
     *
     * @throws \Exception if FieldItem is expected to be of Material type, but
     *  FieldItemMaterial was not found
     */
    public function getCalculation(FieldItem $fieldItem) {
        if(!$fieldItem->isUsedInStock()) {
            return new \Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\NotUsedStockCalculation();
        }

        $stockType = $this->getProductField()->getFieldset()->getName();
        switch($stockType) {
        case "MATERIAL":
            if($fieldItem->getFieldset()->getName() == 'MATERIAL') {
                $material = $fieldItem->getMaterial();
                $sheetSize = new \Mnumi\Bundle\CalculationBundle\Library\Size($material->getPrintsize()->getWidth(), $material->getPrintsize()->getHeight());

                $sizeId = $this->getOrder()->getAttribute('SIZE')->getValue();
                $size = FieldItemSizeTable::getInstance()->findOneByFieldItem($sizeId);

                $useSize = new \Mnumi\Bundle\CalculationBundle\Library\Size($size->getWidth(), $size->getHeight());// Handle different Use sizes before and after change

                return $this->getMaterialCalculator($fieldItem->getMeasureType(), $sheetSize, $useSize);
            }
            break;
        case "OTHER":
            return new \Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\QuantityStockCalculation();
        }

        return new \Mnumi\Bundle\DepositoryBundle\Library\StockCalculation\NotUsedStockCalculation();
    }

    /**
     * Indicates if Fieldset of current OrderAttribute is used in Depository
     *
     * @return bool true if current OrderAttribute Fieldset is used in Depository
     */
    public function isStockField() {
        return in_array($this->getProductField()->getFieldset()->getName(), FieldItemTable::$stockFields);
    }
}
