<?php

/**
 * JmsJobTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class JmsJobTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object JmsJobTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('JmsJob');
    }
}