<?php

/**
 * FileTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class FileTable extends Doctrine_Table
{
    public static $displayTableFields = array('filename' => 'Filename',
        'notice' => 'Notice');

    const TYPE_IMAGE = 'image';
    const TYPE_FOTOLIA = 'fotolia';

    public static $fotolia_laguages = array(
            Mnumi\Bundle\FotoliaBundle\Client\Client::LANGUAGE_ID_PL_PL => 'pl',
            Mnumi\Bundle\FotoliaBundle\Client\Client::LANGUAGE_ID_EN_US => 'en',
            Mnumi\Bundle\FotoliaBundle\Client\Client::LANGUAGE_ID_FR_FR => 'fr'
        );

    /**
     * Returns an instance of this class.
     *
     * @return object FileTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('File');
    }
    
    /**
     * Returns array with column aliases or methods to use if defined.
     * 
     * @return array
     */
    public function getDisplayTableFields()
    {
        $fieldArr = array();
        foreach(self::$displayTableFields as $key => $rec)
        {
            $fieldArr[$this->getClassnameToReturn().'.'.$key] = $rec;
        }
        
        return $fieldArr;
    }
}