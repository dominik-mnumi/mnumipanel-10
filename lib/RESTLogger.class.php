<?php

/**
 * Logger class
 */
class RESTLogger
{
    private $timer;
    private $callerClass;
    private $callerMethod;
    
    public function __construct($callerClass, $callerMethod)
    {
        $this->callerClass = $callerClass;
        $this->callerMethod = $callerMethod;
    }
    
    /**
     * Executes before REST action request.
     */
    public function preRest()
    {
        if (sfConfig::get('sf_logging_enabled'))
        {
            $this->timer = sfTimerManager::getTimer('REST: '.$this->callerMethod);
        }
    }

    /**
     * Executes after REST action request. Returns REST response array.
     * 
     * @param array $returnREST 
     */
    public function postRest($returnREST)
    {
        if (sfConfig::get('sf_logging_enabled'))
        {
            $this->timer->addTime();

            ob_start();
            print_r($returnREST);
            $output = ob_get_clean();

            sfContext::getInstance()->getEventDispatcher()->notify(
                    new sfEvent($this,
                            'application.log',
                            array(sprintf('Executing REST: "%s -> %s",
                                 Return: "%s"', $this->callerClass, $this->callerMethod, $output))));
        }
    }

    /**
     *
     * @param string $function
     * @param array $use_stack
     * @return string  
     */
    private function getCaller($function = NULL, $use_stack = NULL)
    {
        if (is_array($use_stack))
        {
            // if a function stack has been provided
            $stack = $use_stack;
        }
        else
        {
            $stack = debug_backtrace();
        }

        if ($function == NULL)
        {
            $function = $this->getCaller(__FUNCTION__, $stack);
        }

        if (is_string($function) && $function != "")
        {
            // if we are given a function name as a string, go through the function stack and find
            // it's caller.
            for ($i = 0; $i < count($stack); $i++)
            {
                $curr_function = $stack[$i];
                
                // make sure that a caller exists, a function being called within the main script
                // won't have a caller.
                if ($curr_function["function"] == $function && ($i + 1) < count($stack))
                {
                    return $stack[$i + 1]["function"];
                }
            }
        }

        // at this stage, no caller has been found
        return "";
    }
}
