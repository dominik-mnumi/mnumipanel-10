<?php

class OrderPackageView
{
    private $orderPackage;

    /**
     * @param OrderPackage $orderPackage
     */
    public function __construct($orderPackage)
    {
        $this->orderPackage = $orderPackage;
    }

    public function getCreated()
    {
        return format_date(
            $this->orderPackage->getCreatedAt(),
            OrderPackageTable::$packageTimeFormat
        );
    }

    public function getId()
    {
        return $this->orderPackage->getId();
    }
}

/**
 * @param OrderPackage $orderPackage
 *
 * @return string
 */
function order_package_name($orderPackage)
{
    $packageNameFormat = sfConfig::get('app_package_format', '{created}');

    $orderPackageView = new OrderPackageView($orderPackage);

    $patternReplace = new \Mnumi\Bundle\OrderBundle\Text\Pattern\PatternReplace($orderPackageView);
    return $patternReplace->convert($packageNameFormat);
}
