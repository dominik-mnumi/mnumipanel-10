<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Import layout from external site.
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class configureShopHostTask extends sfBaseTask
{
    /**
     * @see sfTask
     */
    protected function configure()
    {
        $this->namespace = 'mnumi';
        $this->name = 'configure-shop-host';
        $this->briefDescription = 'Configure HOST for MnumiShop application';

        $this->detailedDescription = '
            The [mnumi:configure-key|INFO] configure MnumiPanel with MnumiShop application. 
            
            Usage example: 
              [./symfony mnumi:configure-shop-host application hostname |INFO]
        ';
        
        $this->addArguments(array(
            new sfCommandArgument('hostname', sfCommandArgument::REQUIRED, 'The application host name'),
        ));
        
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore')
        ));

    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {
        $hostname = $arguments['hostname'];
        
        if(substr($hostname, 0, 7) != 'http://' && substr($hostname, 0, 8) != 'https://')
        {
            $this->logBlock('Hostname must have "http(s)://" at the begin.', 'ERROR');
            exit(1);
        }
        
        if(substr($hostname, -1) != '/')
        {
            $this->logBlock('Hostname must have slash "/" at the end.', 'ERROR');
            exit(1);
        }
        $appConfiguration = sfConfig::get('sf_app_config_dir') .'/app.yml';
        
        
        $config = sfYaml::load($appConfiguration);
        $config['all']['shop']['host'] = $hostname;
        
        $result = file_put_contents($appConfiguration, sfYaml::dump($config, 4)); 
        if($result === false)
        {
            $this->logBlock(sprintf('Could not save (%s) file.', $appConfiguration), 'ERROR');
            exit(1);
        }
        $this->clearCache();
        $this->logBlock('Saved new API configuration (shop host).', 'INFO');
        exit(0);
    }
    
    private function clearCache()
    {
        foreach(array('prod', 'dev') as $env)
        {
            $cacheDir = sfConfig::get('sf_cache_dir') . '/' . sfConfig::get('sf_app') . '/'.$env.'/';
            $cache = new sfFileCache(array('cache_dir' => $cacheDir));
            $cache->clean();
        }
        
    }

}

