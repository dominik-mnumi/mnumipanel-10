<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of mnumiCreateWebapi
 *
 * @author jupeter
 */
class wizardRemoveTask extends sfDoctrineBaseTask
{
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));
    
    $this->addArguments(array(
         new sfCommandArgument('productName', sfCommandArgument::REQUIRED, 'Product name'),
         new sfCommandArgument('wizards', sfCommandArgument::REQUIRED, 'Comma separated wizards'),
    ));

    $this->namespace        = 'wizard';
    $this->name             = 'remove';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [wizard:add|INFO] un-relate wizards from product.
Call it with:

  [php symfony wizard:remove "product-slug" wizard1,wizard2,wizard3|INFO]
EOF;
  }
  
  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    
    // check if product exist
    if(! ($product = ProductTable::getInstance()->findOneBySlug($arguments['productName'])))
    {
    	$this->logBlock('Could not find "'. $arguments['productName'] .'" product.', 'ERROR');
    	exit(1);
    }
    
    $wizards = split(',', $arguments['wizards']);
    
    $i = 0;
    foreach($wizards as $wizard)
    {
    	// verify if ProductWizard already exist
    	$obj = ProductWizardTable::getInstance()->findOneByProductIdAndWizardName(
    			$product->getId(), $wizard);
    	
    	if(!$obj)
    	{
    		continue;
    	}
    	
    	$obj->delete();
    	$i++;
    }
    
    // if all projects already imported
    if($i == 0)
    {
    	$this->logBlock('No projects related. Probably none projects reladed to product "'. $arguments['productName'] .'".', 'INFO');
    	exit(0);
    }
    
	$this->logBlock($i . ' projects unrelate successfull from product "'. $arguments['productName'] .'".', 'INFO');
    exit(0);
  }
}

?>
