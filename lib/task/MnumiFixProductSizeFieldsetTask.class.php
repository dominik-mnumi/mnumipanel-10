<?php

class MnumiFixProductSizeFieldsetTask extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            // add your own options here
        ));

        $this->namespace        = 'mnumi';
        $this->name             = 'fix-product-size-fieldset';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [mnumi:fix-product-size-fieldset|INFO] task fixes polish SIZE label in fieldset table.
This task has been introduced due the: PAN-977
Call it with:

  [php symfony mnumi:fix-product-size-fieldset|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        $settings = sfYAML::Load(sfConfig::get('sf_app_config_dir').'/settings.yml');

        if ($settings['all']['.settings']['default_culture'] !== 'pl') {
            $this->logSection('No action', 'Task can be used only when default culture is set to be equal to PL, as it fixes polish value only');
            return;
        }

        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

        try {

            $connection
                ->prepare("UPDATE `fieldset` SET `label` = 'Rozmiar' WHERE `fieldset`.`id` = 4;")
                ->execute();

            $this->logSection('Fixed', 'Polish product size fieldset label has been set correctly');

        }
        catch(Exception $e)
        {
            $this->logSection('No action', 'There was a problem when trying to fix product size fieldset label');
        }
    }
}
