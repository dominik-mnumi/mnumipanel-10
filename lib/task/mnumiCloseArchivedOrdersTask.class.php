<?php

class mnumiCloseArchivedOrdersTask extends sfBaseTask
{
  protected function configure()
  {    
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'mnumi';
    $this->name             = 'close-archived-orders';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [mnumi:close-archived-orders|INFO] task close orders in archived packages.
Call it with:

  [php symfony mnumi:close-archived-orders|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {    
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
    $archivedPackages = OrderPackageTable::getInstance()->createQuery('op')
                    ->where('op.order_package_status_name = ?', OrderPackageStatusTable::$archive)
                    ->count();
    
    $ordersInArchivedPackages = OrderTable::getInstance()->createQuery('o')
                                ->innerJoin('o.OrderPackage op')
                                ->where('op.order_package_status_name = ?', OrderPackageStatusTable::$archive)
                                ->andWhere('o.order_status_name != ?', OrderStatusTable::$closed)
                                ->execute();
    
    foreach($ordersInArchivedPackages as $order) {
        $order->setOrderStatusName(OrderStatusTable::$closed);
        $order->save();
    }
    
    $this->logSection('Closed', count($ordersInArchivedPackages) . ' Orders in ' . $archivedPackages . ' archived packages');
    
  }
}
