<?php

class mnumiMigrateTask extends sfDoctrineBaseTask
{
  //backups dir
  private $dir = 'data/backup/';
  
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'mnumi';
    $this->name             = 'migrate';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [mnumi:migrate|INFO] task does things.
Call it with:

  [php symfony mnumi:migrate|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    
    $database = $databaseManager->getDatabase($options['connection']);

    $this->logSection('MNUMI Migration', 'Starting migrate task');
    
    $fileName = 'migrate-'.time().'.sql';
    if(!is_dir($this->dir))
    {
        $this->logSection('MNUMI Migration', 'Creating migration directory : '.$this->dir);
        if(mkdir($this->dir, 0777))
        {
            $this->logSection('MNUMI Migration', 'Migration directory created successfull.');
        }
        else
        {
            $this->logSection('MNUMI Migration', 'Cannot create directory: '.$this->dir.'. Check permissions.', null, 'ERROR');
            return;
        }
    }

    $dbUsername = $database->getParameter('username');
    $dbPassword = $database->getParameter('password');
    $dsn = $database->getParameter('dsn');

    $dsnParam = $database->getDoctrineConnection()->getManager()->parsePdoDsn($dsn);
    $dbName = $dsnParam['dbname'];
    $dbHost = $dsnParam['host'];
    
    if($dsnParam['scheme'] != 'mysql')
    {
        $this->logSection('MNUMI Migration', 'The Migration work only with MySQL scheme. ', null, 'ERROR');
        return false;
    }
    
    $this->logSection('MNUMI Migration', 'Dumping database ...');
    $cmd = 'mysqldump '.$dbName.' --user='.$dbUsername.' --password='.$dbPassword.' --host='.$dbHost.' > '.$this->dir.$fileName;

    @system($cmd, $hasError);
    if($hasError || !file_exists($this->dir.$fileName) || filesize($this->dir.$fileName) < 1024)
    {
        $this->logSection('MNUMI Migration', 'Unable to create dump file - check your connection parameters and permission to output directory', null, 'ERROR');
        return false;
    }
    $this->logSection('MNUMI Migration', 'Backup saved successfull');
    
    if(!$this->doctrineMigration($arguments, $options))
    {
        $this->logSection('doctrine', 'Restoring database to previous version');
        
        // delete all tables from database
        $dbh = $database->getDoctrineConnection()->getDbh();
        $dbh->query(sprintf('SET FOREIGN_KEY_CHECKS = 0;'));
        $tables = $database->getDoctrineConnection()->import->listTables();
        foreach ($tables as $table)
        {
            $dbh->query(sprintf('DROP TABLE %s', $table));
        }
        $dbh->query(sprintf('SET FOREIGN_KEY_CHECKS = 1;'));
        
        if(count($database->getDoctrineConnection()->import->listTables()) > 0)
        {
            $this->logSection('MNUMI Migration', 'Unable to delete all tables. Please restore database from dump manually.', null, 'ERROR');
        }
        // end of deleting tables
        
        //restore database
        $passCmd = '';
        
        if($dbPassword)
        {
            $passCmd = '-p'.$dbPassword;
        }
        
        $cmd = 'mysql -h '.$dbHost.' -u '.$dbUsername.' '.$passCmd.' '.$dbName.' < '.$this->dir.$fileName;
        
        @system($cmd, $hasError);
        if($hasError)
        {
            $this->logSection('MNUMI Migration', 'Unable to restore database from dump file. Please do it manually.', null, 'ERROR');
            return;
        }
        $this->logSection('doctrine', 'Database restored succesfully');
        return;
    }
    
    $this->logSection('MNUMI Migration', 'CONGRATULATIONS - MIGRATIONS FINISHED SUCCESFULLY !!');
    
    // add your code here
  }
  
  /**
   *   Method from original doctrine migrations + our changes
   */
  protected function doctrineMigration($arguments, $options)
  {
      $config = $this->getCliConfig();
      $migration = new Doctrine_Migration($config['migrations_path']);
      $from = $migration->getCurrentVersion();
  
      if (is_numeric(@$arguments['version']))
      {
          $version = $arguments['version'];
      }
      else if (@$options['up'])
      {
          $version = $from + 1;
      }
      else if (@$options['down'])
      {
          $version = $from - 1;
      }
      else
      {
          $version = $migration->getLatestVersion();
      }
  
      if ($from == $version)
      {
          $this->logSection('doctrine', sprintf('Already at migration version %s', $version));
          return true;
      }
  
      $this->logSection('doctrine', sprintf('Migrating from version %s to %s%s', $from, $version, @$options['dry-run'] ? ' (dry run)' : ''));
      try
      {
          $migration->migrate($version, @$options['dry-run']);
      }
      catch (Exception $e)
      {
      }
  
      // render errors
      if ($migration->hasErrors())
      {
          
          $errors = '';
          foreach ($migration->getErrors() as $error)
          {
            $errors  .= $error.'
            ';
          }
          
          echo $errors;
          
          file_put_contents($this->dir.'errors.txt', $errors);
          
          $this->logSection('doctrine','Errors occured during migration! Check '.$this->dir.'errors.txt'.' for more informations.', null, 'ERROR');
          return false;
      }

      $this->logSection('doctrine', 'Migration complete');
      return true;
  }
}
