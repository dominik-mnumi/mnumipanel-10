<?php

class MnumiHotfolderUpdateTask extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            new sfCommandOption('linuxuser', null, sfCommandOption::PARAMETER_OPTIONAL, 'The linux user name'),
            new sfCommandOption('linuxgroup', null, sfCommandOption::PARAMETER_OPTIONAL, 'The linux group name')
        ));
        
        $this->namespace = 'mnumi';
        $this->name = 'hotfolderUpdate';
        $this->briefDescription = 'Updates hotfolder of all orders images.';
        $this->detailedDescription = <<<EOF
The [mnumi:hotfolderUpdate|INFO] task does things.
Call it with:
  [php symfony mnumi:hotfolderUpdate|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

        $this->logBlock('Start updating hotfolder', 'INFO');

        // COPY SECTION
        // gets empty order packages
        $date = new DateTime();
        $date->modify('-90 days');

        /** @var Order[] $orderColl */
        $orderColl = OrderTable::getInstance()
            ->createQuery()
            ->andWhere('updated_at > ?', $date->format('Y-m-d H:i:s'))
            ->orderBy('updated_at DESC')
            ->execute();

        $success = 0;
        $failed = 0;
        $statuses = OrderStatusTable::getHotfolderStatuses();

        $this->logBlock(
            ' - copy order files to hotfolder (accept statuses: '.implode(', ', $statuses).')',
            'INFO'
        );

        foreach($orderColl as $order)
        {
            // if status is properly for hotfolder
            if(!in_array($order->getOrderStatusName(), $statuses))
            {
                continue;
            }

            try
            {
                if($this->moveFilesToHotfolder($order)) {
                    $this->logBlock(
                        '  * add order id: ' . $order->getId() . ' status: ' . $order->getOrderStatusName(),
                        'INFO'
                    );
                }
                $success++;
            }
            catch(FileManagerException $e)
            {
                $this->logBlock($e->getMessage(), 'ERROR');
                $failed++;
            }
            catch(Exception $e)
            {
                $failed++;
            }           
        }
  
        // DELETE SECTION
        // gets product collection
        $productColl = ProductTable::getInstance()->getActive();

        $this->logBlock(' - delete old files from hotfolder', 'INFO');

        foreach($productColl as $rec)
        {
            // gets all files from database
            $filenameFromDbArr = $this->getProductDbFileHotfolderArr($rec);

            // gets current files from hotfolder
            $currentHotfolderFileArr = $this->dirScan($rec->getHotfolderDir().'*');

            // prepare files to delete (diff current to database)
            $toDeleteArr = array_diff($currentHotfolderFileArr, $filenameFromDbArr);           
            foreach($toDeleteArr as $rec2)
            {
                if(is_dir($rec2))
                {
                    continue;
                }

                if(!is_writable($rec2))
                {
                    $this->logBlock('Insufficient permissions to delete file: ' . $rec2, 'ERROR');
                    continue;
                }

                unlink($rec2);
            }
        }

        // UPLOAD SECTION
        $this->logBlock(' - upload files from /hotfolder/upload', 'INFO');

        if ($this->configuration instanceof sfApplicationConfiguration)
        {
            sfContext::createInstance($this->configuration);
        }

        if (sfContext::hasInstance())
        {
            $context = sfContext::getInstance();
        }

        $uploadReportArr = $this->importFilesFromUploadHotfolder();

        // sets user and group owner
        // if linuxuser is defined
        if(!empty($options['linuxuser']))
        {
            // if linuxuser is defined and linuxgroup is not defined
            if(empty($options['linuxgroup']))
            {
                $options['linuxgroup'] = $options['linuxuser'];
            }
            exec('chown '.$options['linuxuser'].':'.$options['linuxgroup'].' data/hotfolder/* -R');
        }
        
        // report
        $this->log('Summary report:');
        $this->log(' - Orders: success '.$success.', failed '.$failed);

        if($uploadReportArr)
        {
            $this->log(' - Import: success '.$uploadReportArr['success'].', failed '.$uploadReportArr['failed']);
        }
        
        $this->log('End Hotfolder updating.');
    }

    /**
     * Get product files
     *
     * @param Product $product
     * @return array
     */
    private function getProductDbFileHotfolderArr($product)
    {
        $fileArr = array();

        foreach($product->getHotfolderOrders() as $order)
        {
            $originalArr = $this->getOriginalSourceAndDestHotfolderArr($order);

            foreach($originalArr as $rec2)
            {
                $fileArr[] = $rec2['dest'];
            }
        }

        return $fileArr;
    }


    /**
     * 
     * @param string $folder
     * @return array
     */
    private function dirScan($folder)
    {
        $fileArr = glob($folder);
        foreach($fileArr as $key => $f) 
        {
            if(is_dir($f)) 
            {
                //unset($fileArr[$key]);            
                $fileArr = array_merge($fileArr, $this->dirScan($f.'/*')); // scan subfolder               
            }
        }
        return $fileArr;
    }

    /**
     * Imports files from hotfolder/uploads directory.
     * 
     * @return array
     */
    private function importFilesFromUploadHotfolder()
    {
        if((sfContext::hasInstance()) && (sfContext::getInstance()->getConfiguration()->getEnvironment() == 'test'))
        {
            $uploadDir = sfConfig::get('sf_data_dir').'/../test/phpunit/data/uploads/';
        }
        else
        {
            $uploadDir = sfConfig::get('sf_data_dir').'/hotfolder/uploads/';
        }

        if(!is_dir($uploadDir))
        {
            $basedir = dirname($uploadDir);

            if(!is_writable($basedir))
            {
                $this->logBlock('Insufficient permissions to create directory: ' . $uploadDir, 'ERROR');
                return false;
            }

            mkdir($uploadDir);
        }

        // gets all files from upload dir
        $uploadFileArr = $this->dirScan($uploadDir.'*');
        $successNr = 0;
        $failedNr = 0;
        foreach($uploadFileArr as $rec)
        {
            if(!is_dir($rec))
            {
                preg_match('/\/([0-9]+)\.([\w\.\(\)-_\s]+)$/i', $rec, $matchedArr);
                
                $orderId = $matchedArr[1];
                $fileBasename = $matchedArr[2];

                // gets order object
                /** @var Order $orderObj */
                $orderObj = OrderTable::getInstance()->find($orderId);
                if($orderObj)
                {      
                    try
                    {
                        $this->logBlock(
                            '  * add ' . $fileBasename . ' for order ' . $orderId,
                            'INFO'
                        );

                        $orderObj->importFile($rec, $fileBasename);
                        $orderObj->save();
                        $this->moveFilesToHotfolder($orderObj);

                        // for test do NOT remove upload files
                        if(!sfContext::hasInstance() 
                                ||(sfContext::hasInstance() 
                                && (sfContext::getInstance()->getConfiguration()->getEnvironment() != 'test')))
                        {
                            unlink($rec);
                        }
                            
                        $successNr++;
                    }
                    catch(Exception $e)
                    {
                        $this->logBlock('Could not upload file: ' . $rec . '. Message: ' . $e->getMessage(), 'ERROR');
                        $failedNr++;
                    }
                }
                else
                {
                    $failedNr++;
                }    
            }
        }
    
        return array(
            'success' => $successNr,
            'failed' => $failedNr);
    }

    /**
     * Moves all files from one location to another.
     *
     * @param Order $order
     * @return bool True, then file was moved
     */
    private function moveFilesToHotfolder($order)
    {
        $result = false;

        // gets filenames of current status directory
        $filenameSourceDestArr = FileManager::findFilenamesToCopyHotfolder($order->getHotfolderDir(),
            $order->getId(), OrderStatusTable::getAllStatuses(),
            $order->getOrderStatusName());

        // if status is properly for hotfolder
        if(in_array($order->getOrderStatusName(),
            OrderStatusTable::getHotfolderStatuses()))
        {
            // gets original filenames basing on database
            $fileArr = $this->getOriginalSourceAndDestHotfolderArr($order);

            // foreach file
            foreach($fileArr as $rec)
            {
                // if file already does not exist
                if(!file_exists($rec['dest']))
                {
                    // if file does not exist check in hotfolder
                    if($this->isFileAlreadyInHotfolder($filenameSourceDestArr, $rec['dest']))
                    {
                        $hotfolderFilenameArr = $this->getFilenameFromHotfolder($filenameSourceDestArr, $rec['dest']);
                        FileManager::copyImage(
                            $hotfolderFilenameArr['source'],
                            $hotfolderFilenameArr['dest']);

                        $this->deleteFileFromHotfolder($order, $hotfolderFilenameArr['source']);

                        $result = true;
                    }
                    else
                    {
                        // if valid url or source
                        if(filter_var($rec['source'], FILTER_VALIDATE_URL)
                            || file_exists($rec['source']))
                        {
                            FileManager::copyImage(
                                $rec['source'],
                                $rec['dest']
                            );

                            $result = true;
                        }
                    }
                }
            }
        }
        else
        {
            $this->deleteFilesFromHotfolder(
                $order,
                Cast::getChoiceArr($filenameSourceDestArr, 'source')
            );
        }

        return $result;
    }

    /**
     * Deletes files from hotfolder.
     *
     * @param Order $order
     * @param array $filenameToDeleteArr
     */
    private function deleteFilesFromHotfolder($order, $filenameToDeleteArr)
    {
        // foreach filename
        foreach($filenameToDeleteArr as $rec)
        {
            $this->deleteFileFromHotfolder($order, $rec);
        }
    }

    /**
     * Deletes file from hotfolder.
     *
     * @param Order $order
     * @param array $filename
     */
    public function deleteFileFromHotfolder($order, $filename)
    {
        // prepares statuses for file deleting (just in case look at all files
        // excluding files in current status directory)
        $statusToDeleteArr = OrderStatusTable::getAllStatuses();

        // unsets current status directory
        unset($statusToDeleteArr[array_search($order->getOrderStatusName(), $statusToDeleteArr)]);

        // gets basename
        $fileBasename = basename($filename);

        // checks fo all statuses except current status
        foreach($statusToDeleteArr as $rec2)
        {
            // create file name and if exists then delete
            $filename = $order->getHotfolderDir().$rec2.'/'.$fileBasename;

            if(file_exists($filename))
            {
                unlink($filename);
            }
        }
    }

    /**
     * Returns true if file is in hotfolder.
     *
     * @param array $filenameSourceDestArr
     * @param type $filename
     * @return boolean
     */
    public function isFileAlreadyInHotfolder($filenameSourceDestArr, $filename)
    {
        // file does not exist check in hotfolder
        foreach($filenameSourceDestArr as $rec)
        {
            // means that file is already in hotfolder but not in current dir
            if($rec['dest'] == $filename)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns array with source and destination parameters.
     *
     * @param array $filenameSourceDestArr
     * @param type $filename
     * @return mixed
     */
    public function getFilenameFromHotfolder($filenameSourceDestArr, $filename)
    {
        // file does not exist check in hotfolder
        foreach($filenameSourceDestArr as $key => $rec)
        {
            // means that file is already in hotfolder but not in current dir
            if($rec['dest'] == $filename)
            {
                return $filenameSourceDestArr[$key];
            }
        }

        return false;
    }

    /**
     * Returns array with source and dest keys.
     *
     * @param Order $order
     * @return array
     */
    public function getOriginalSourceAndDestHotfolderArr($order)
    {
        $fileArr = array();

        // moves files to new hotfolder
        foreach($order->getFiles() as $rec)
        {
            // prepares filenames
            $sourceFilename = $rec->getFullFilePath();
            $destinationFilename = $rec->getHotfolderFilename();

            $fileArr[] = array(
                'source' => $sourceFilename,
                'dest' => $destinationFilename
            );
        }

        // moves wizards to new hotfolder
        foreach($order->getWizardOrderAttributes() as $rec)
        {
            // first checks if pdf is generated already
            $pdfCheckUrl = WizardManager::getInstance()
                ->generatePdfQueueAddUrl(
                    $rec->getValue(),
                    $order->getId(),
                    $order->getBarcode(),
                    sfConfig::get('app_wizard_id'));

            $checkQueueArr = $order->checkWizardQueueCurl($pdfCheckUrl);

            // if pdf is not already generated
            if($checkQueueArr['result']['status']['code'] != WizardManager::$pdfStatusReady)
            {
                continue;
            }

            // prepares filenames
            $sourceUrl = WizardManager::getInstance()
                ->generatePdfQueueStatusUrl(
                    $rec->getValue(),
                    $order->getId(),
                    $order->getBarcode());

            $destinationFilename = $rec->getHotfolderFilename();

            $fileArr[] = array(
                'source' => $sourceUrl,
                'dest' => $destinationFilename
            );
        }

        return $fileArr;
    }
}
