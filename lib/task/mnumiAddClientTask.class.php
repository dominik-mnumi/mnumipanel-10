<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of mnumiSetLocale
 *
 * @author jupeter
 */
class mnumiAddClientTask extends sfBaseTask
{

    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
                // add your own options here
        ));

        $this->addArguments(array(
            new sfCommandArgument('id', sfCommandArgument::REQUIRED, 'Client id'),
            new sfCommandArgument('username', sfCommandArgument::REQUIRED, 'Username'),
        ));

        $this->namespace           = 'mnumi';
        $this->name                = 'add-client';
        $this->briefDescription    = '';
        $this->detailedDescription = <<<EOF
The [mnumi:add-client|INFO] task save is add client to user.
Call example:

  [php symfony mnumi:add-client 1 admin"|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);

        $clientId = $arguments['id'];
        $username = $arguments['username'];

        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);

        /** @var Client $client */
        $client = ClientTable::getInstance()->find($clientId);

        if(!$client instanceof Client) {
            $this->log('Invalid client id: ' . $clientId);
            exit(1);
        }

        $user = sfGuardUserTable::getInstance()->findOneByUsername($username);

        if(!$user instanceof sfGuardUser) {
            $this->log('Invalid username: ' . $username);
            exit(1);
        }

        $clientUserAdmin = ClientUserPermissionTable::getInstance()->findOneByName(
            ClientUserPermissionTable::$admin
        );

        $clientUser = new ClientUser();
        $clientUser->setClient($client);
        $clientUser->setUser($user);
        $clientUser->setClientUserPermission($clientUserAdmin);

        try {
            $clientUser->save();
        } catch (Doctrine_Exception $e) {
            $this->log('Could not add username "' . $username . '" into client "' . $client->getShortenedFullName() .'". Relation already exists.');
            exit(0);
        }

        $this->log('Successful added username "' . $username . '" into client "' . $client->getShortenedFullName().'"');
        exit(0);
    }
}
