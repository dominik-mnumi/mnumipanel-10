<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Cleans up data/files dir.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class mnumiCropImageTask extends sfBaseTask
{
    /**
     * @see sfTask
     */
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine')
        ));

        $this->addArguments(array(
                new sfCommandArgument('imageId', sfCommandArgument::REQUIRED, 'ID of image to crop'),
                new sfCommandArgument('cropX', sfCommandArgument::REQUIRED, 'Cropping X coordinate'),
                new sfCommandArgument('cropY', sfCommandArgument::REQUIRED, 'Cropping Y coordinate'),
                new sfCommandArgument('cropWidth', sfCommandArgument::REQUIRED, 'Width'),
                new sfCommandArgument('cropHeight', sfCommandArgument::REQUIRED, 'Height'),
            ));

        $this->namespace = 'mnumi';
        $this->name = 'cropImage';
        $this->briefDescription = 'Crop image';

        $this->detailedDescription = '
            The [mnumi:cropImage 1 10 10 100 100|INFO] cropping image with given parameters.
        ';

    }

    /**
     * @see sfTask
     */
    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $databaseManager->getDatabase($options['connection'])->getConnection();

        $file = FileTable::getInstance()->findOneById($arguments['imageId']);
        var_dump($file);
        
    }
}

