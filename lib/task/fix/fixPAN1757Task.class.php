<?php

class fixPAN1757Task extends sfBaseTask
{
    protected function configure()
    {
        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
        ));
        
        $this->namespace = 'fix';
        $this->name = 'PAN-1757';
        $this->briefDescription = 'Fix file metadata.';
        $this->detailedDescription = <<<EOF
The [mnumi:fix-file-metadata|INFO] fix file metadata.
EOF;
    }

    protected function execute($arguments = array(), $options = array())
    {
        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $databaseManager->getDatabase($options['connection'])->getConnection();

        $query = "select id from file f WHERE f.metadata NOT LIKE '{%' AND f.metadata NOT LIKE '[]';";

        $statement = Doctrine_Manager::getInstance()->connection();
        $results = $statement->execute($query)->fetchAll();

        $this->log('Found '.count($results).' objects.');

        foreach($results as $row)
        {
            // gets order object
            $file = FileTable::getInstance()->find($row['id']);

            $metadata = preg_split('/\n/', $file->getMetadata());

            if(count($metadata) == 0) {
                continue;
            }

            $attributes = array();

            foreach($metadata as $line) {
                if(preg_match('/^([\wa-z]+):[\s]?\'(.*)\'/', $line, $matches)) {
                    $attributes[$matches[1]] = $matches[2];
                }
            }

            $file->setMetadata(
                json_encode($attributes)
            );

            $file->save();
            $file = null;
        }

        $configDir = '/etc/mnumi/mnumicore3';
        exec('touch ' . $configDir . '/PAN-1757');
        $this->log('Finished.');
    }

}
