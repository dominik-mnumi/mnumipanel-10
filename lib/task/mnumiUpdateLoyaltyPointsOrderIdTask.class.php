<?php

class mnumiUpdateLoyaltyPointsOrderIdTask extends sfBaseTask
{
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = 'mnumi';
    $this->name             = 'update-loyaltypoints-orderid';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [mnumi:update-loyaltypoints-orderid|INFO] task update order_id if id null.
Call it with:

  [php symfony mnumi:update-loyalitypoints-orderid|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    $query =
            'SELECT lp.id lpID, o.id oID FROM loyalty_points lp '.
            'INNER JOIN invoice_item ii ON lp.invoice_item_id = ii.id '.
            'INNER JOIN orders o ON ii.order_id = o.id '.
            'WHERE lp.order_id IS NULL';
    $stmt = $connection->prepare($query);

    $stmt->execute();
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $updateQuery = 'UPDATE loyalty_points SET order_id = :oID WHERE id = :lpID';
    foreach($result as $params) {
        $stmt = $connection->prepare($updateQuery);
        $stmt->execute($params);
    }

    $this->logSection('UPDATED', count($result) . ' LoyaltyPoints');

  }
}
