<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of update twitter task
 *
 * @author jupeter
 */
class updateTwitterTask extends sfDoctrineBaseTask
{
  protected function configure()
  {
    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'mnumicore'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));
    
    $this->namespace        = 'cron';
    $this->name             = 'update-news';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [cron:update-news|INFO] update twitter news.
Call it with:

  [php symfony cron:update-news|INFO]
EOF;
  }
  
  protected function execute($arguments = array(), $options = array())
  {
      $url = 'http://security.mnumi.com/twitter.json';
      $content = file_get_contents($url);;

      if($content === false) {
          $this->logBlock('Could not load data from URL: '.$url, 'INFO');
          exit(1);
      }

      $cacheFile = mnumicoreConfiguration::getTwitterFile();

      if(file_put_contents($cacheFile, $content) === false) {
          $this->logBlock('Could not save data to file: '.$cacheFile, 'INFO');
          exit(1);
      }

      $this->logBlock('Saved successfully to: '.$cacheFile, 'INFO');
      exit(0);
  }
}

