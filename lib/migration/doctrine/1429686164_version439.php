<?php
/**
 * PAN-1398 Add missing OrderWorkflows which has been discovered due: SH-460, and PAN-1397
 */
class Version439 extends Doctrine_Migration_Base
{
    public function up()
    {
        $configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'prod', true);
        $trans = sfContext::createInstance($configuration)->getI18N();

        $createDraft = $trans->__('Create draft');

        $stmt = Doctrine_Manager::getInstance()->connection();
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(33, 'calculation', 'draft', '". $createDraft . "', '0', '1', '0');");
    }

    public function down()
    {

    }
}