<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version413 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->removeIndex('pricelist', 'name_UNIQUE', array(
             'fields' => 
             array(
              0 => 'name',
             ),
             'type' => 'unique',
             ));

        $this->changeColumn('pricelist', 'currency_id', 'integer', '4', array(
             'notnull' => '1',
             ));

        $this->createForeignKey('pricelist', 'pricelist_currency_id_currency_id', array(
             'name' => 'pricelist_currency_id_currency_id',
             'local' => 'currency_id',
             'foreign' => 'id',
             'foreignTable' => 'currency',
             'onUpdate' => 'restrict',
             'onDelete' => 'restrict',
             ));
        $this->addIndex('pricelist', 'pricelist_currency_id', array(
             'fields' => 
             array(
              0 => 'currency_id',
             ),
             ));
        $this->addIndex('pricelist', 'fk_pricelist_currency_idx', array(
             'fields' => 
             array(
              0 => 'currency_id',
             ),
             ));
        $this->addIndex('pricelist', 'name_currency_id', array(
             'fields' => 
             array(
              0 => 'name',
              1 => 'currency_id',
             ),
             'type' => 'unique',
             ));
    }

    public function down()
    {
        $this->addIndex('pricelist', 'name_UNIQUE', array(
             'fields' => 
             array(
              0 => 'name',
             ),
             'type' => 'unique',
             ));
        $this->dropForeignKey('pricelist', 'pricelist_currency_id_currency_id');
        $this->removeIndex('pricelist', 'pricelist_currency_id', array(
             'fields' => 
             array(
              0 => 'currency_id',
             ),
             ));
        $this->removeIndex('pricelist', 'fk_pricelist_currency_idx', array(
             'fields' => 
             array(
              0 => 'currency_id',
             ),
             ));
        $this->removeIndex('pricelist', 'name_currency_id', array(
             'fields' => 
             array(
              0 => 'name',
              1 => 'currency_id',
             ),
             'type' => 'unique',
             ));
        $this->changeColumn('pricelist', 'currency_id', 'integer', '4', array(
             'notnull' => '0',
             ));
    }
}