<?php

class Version440 extends Doctrine_Migration_Base
{
    public function up()
    {
        $stmt = Doctrine_Manager::getInstance()->connection();

        $stmt->execute('UPDATE order_packages op '
                . 'INNER JOIN client c ON c.id = op.client_id '
                . 'SET op.invoice_country = c.country '
                . 'WHERE op.invoice_country IS NULL '
                . 'AND op.invoice_street = c.street');

        $stmt->execute('UPDATE order_packages op '
                . 'INNER JOIN client_address ca '
                . 'ON ca.client_id = op.client_id '
                . 'SET op.invoice_country = ca.country '
                . 'WHERE op.invoice_country IS NULL '
                . 'AND op.invoice_street = ca.street');

        $stmt->execute('UPDATE order_packages op '
                . 'INNER JOIN client c '
                . 'ON c.id = op.client_id '
                . 'SET op.invoice_country = c.country '
                . 'WHERE op.invoice_country IS NULL ');

    }

    public function down()
    {

    }
}