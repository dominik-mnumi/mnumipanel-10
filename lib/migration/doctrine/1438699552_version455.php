<?php
/**
 * PAN-1743 - allow storing up to 5 decimal places
 */
class Version455 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->changeColumn('field_item', 'cost', 'float', 18, array('scale' => '5'));
        $this->changeColumn('field_item', 'availability', 'float', 18, array('scale' => '5'));
        $this->changeColumn('field_item', 'availability_alert', 'float', 18, array('scale' => '5'));

        $this->changeColumn('field_item_price', 'minimal_price', 'float', 18, array('scale' => '5'));
        $this->changeColumn('field_item_price', 'maximal_price', 'float', 18, array('scale' => '5'));
        $this->changeColumn('field_item_price', 'plus_price', 'float', 18, array('scale' => '5'));
        $this->changeColumn('field_item_price', 'plus_price_double', 'float', 18, array('scale' => '5'));
        $this->changeColumn('field_item_price', 'cost', 'float', 18, array('scale' => '5'));

        $this->changeColumn('field_item_price_quantity', 'quantity', 'float', 18, array('scale' => '5'));
        $this->changeColumn('field_item_price_quantity', 'price', 'float', 18, array('scale' => '5'));
        $this->changeColumn('field_item_price_quantity', 'cost', 'float', 18, array('scale' => '5'));

        $this->changeColumn('product_fixprice', 'price', 'float', 18, array('scale' => '5'));
        $this->changeColumn('product_fixprice', 'cost', 'float', 18, array('scale' => '5'));
        $this->changeColumn('product_fixprice', 'price_page', 'float', 18, array('scale' => '5'));
    }

    public function down()
    {
        $this->changeColumn('field_item', 'cost', 'float', 18, array('scale' => '3'));
        $this->changeColumn('field_item', 'availability', 'float', 18, array('scale' => '3'));
        $this->changeColumn('field_item', 'availability_alert', 'float', 18, array('scale' => '3'));

        $this->changeColumn('field_item_price', 'minimal_price', 'float', 18, array('scale' => '3'));
        $this->changeColumn('field_item_price', 'maximal_price', 'float', 18, array('scale' => '3'));
        $this->changeColumn('field_item_price', 'plus_price', 'float', 18, array('scale' => '3'));
        $this->changeColumn('field_item_price', 'plus_price_double', 'float', 18, array('scale' => '3'));
        $this->changeColumn('field_item_price', 'cost', 'float', 18, array('scale' => '3'));

        $this->changeColumn('field_item_price_quantity', 'quantity', 'float', 18, array('scale' => '4'));
        $this->changeColumn('field_item_price_quantity', 'price', 'float', 18, array('scale' => '3'));
        $this->changeColumn('field_item_price_quantity', 'cost', 'float', 18, array('scale' => '3'));

        $this->changeColumn('product_fixprice', 'price', 'float', 18, array('scale' => '3'));
        $this->changeColumn('product_fixprice', 'cost', 'float', 18, array('scale' => '3'));
        $this->changeColumn('product_fixprice', 'price_page', 'float', 18, array('scale' => '3'));
    }
}