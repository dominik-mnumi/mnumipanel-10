<?php
/**
 * PAN-1397 Add missing OrderWorkflows which has been discovered due: SH-460
 */
class Version405 extends Doctrine_Migration_Base
{
    public function up()
    {
        $configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'prod', true);
        $trans = sfContext::createInstance($configuration)->getI18N();

        $delete = $trans->__('Delete');

        $stmt = Doctrine_Manager::getInstance()->connection();

        // Add/Remove from Stock
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(19, 'realization', 'deleted', '". $delete . "', '1', '1', '1');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(20, 'bindery', 'deleted', '". $delete . "', '1', '1', '1');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(21, 'calculation', 'deleted', '". $delete . "', '1', '1', '0');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(22, 'ready', 'deleted', '". $delete . "', '1', '1', '1');");
    }

    public function down()
    {
    }
}