<?php
/**
 * PAN-1398 Add missing OrderWorkflows which has been discovered due: SH-460, and PAN-1397
 */
class Version410 extends Doctrine_Migration_Base
{
    public function up()
    {
        $configuration = ProjectConfiguration::getApplicationConfiguration('mnumicore', 'prod', true);
        $trans = sfContext::createInstance($configuration)->getI18N();

        $toBindery = $trans->__('Move to bindery');
        $toReady = $trans->__('Set as ready');
        $backToNew = $trans->__('Back to new');
        $backToRealization = $trans->__('Back to working');

        $stmt = Doctrine_Manager::getInstance()->connection();
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(23, 'new', 'bindery', '". $toBindery . "', '0', '1', '0');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(24, 'new', 'ready', '". $toReady . "', '0', '1', '0');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(25, 'bindery', 'new', '". $backToNew . "', '1', '1', '0');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(26, 'ready', 'new', '". $backToNew . "', '1', '1', '0');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(27, 'ready', 'realization', '". $backToRealization . "', '1', '1', '0');");
        $stmt->execute("INSERT INTO order_workflow (id, current_status, next_status, title, backward, workflow_id, stock_change) VALUES(28, 'realization', 'ready', '". $toReady . "', '0', '1', '0');");
    }

    public function down()
    {

    }
}