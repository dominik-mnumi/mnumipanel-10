<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version445 extends Doctrine_Migration_Base
{
    public function up()
    {
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $upsColl = $conn->execute("SELECT id, configuration FROM carrier WHERE  type = '" . CarrierTable::$ups . "'")
                ->fetchAll();

        foreach($upsColl as $ups) {

            if($ups['configuration']) {

                $upsConfArr = json_decode($ups['configuration'], true);
                $upsConfiguration = addslashes(json_encode($this->flatten($upsConfArr)));
                $q = "UPDATE carrier SET configuration = '" . $upsConfiguration ."'"
                        ." WHERE id = " . $ups['id'];

                $conn->execute($q);
            }
        }
    }

    public function down()
    {
    }

    /**
     * Flatten multidimenional associative array
     *
     * @param array $array
     * @param string $prefix
     * @return array
     */
    public function flatten($array, $prefix = '') {

        $result = array();

        foreach($array as $key => $value) {

            if(is_array($value)) {

                $result = $result + $this->flatten($value, $prefix . $key . '_');

            } else {

                $result[$prefix . $key] = $value;
            }
        }
        return $result;
    }
}
