<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version82 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createForeignKey('invoice_item', 'invoice_item_invoice_id_invoice_id', array(
             'name' => 'invoice_item_invoice_id_invoice_id',
             'local' => 'invoice_id',
             'foreign' => 'id',
             'foreignTable' => 'invoice',
             'onUpdate' => 'cascade',
             'onDelete' => 'cascade',
             ));
        $this->createForeignKey('invoice_item', 'invoice_item_order_id_orders_id', array(
             'name' => 'invoice_item_order_id_orders_id',
             'local' => 'order_id',
             'foreign' => 'id',
             'foreignTable' => 'orders',
             'onUpdate' => 'set null',
             'onDelete' => 'set null',
             ));
        $this->addIndex('invoice_item', 'invoice_item_invoice_id', array(
             'fields' => 
             array(
              0 => 'invoice_id',
             ),
             ));
        $this->addIndex('invoice_item', 'invoice_item_order_id', array(
             'fields' => 
             array(
              0 => 'order_id',
             ),
             ));
    }

    public function down()
    {
        $this->dropForeignKey('invoice_item', 'invoice_item_invoice_id_invoice_id');
        $this->dropForeignKey('invoice_item', 'invoice_item_order_id_orders_id');
        $this->removeIndex('invoice_item', 'invoice_item_invoice_id', array(
             'fields' => 
             array(
              0 => 'invoice_id',
             ),
             ));
        $this->removeIndex('invoice_item', 'invoice_item_order_id', array(
             'fields' => 
             array(
              0 => 'order_id',
             ),
             ));
    }
}