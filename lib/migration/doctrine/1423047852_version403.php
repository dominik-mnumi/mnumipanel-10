<?php
/**
 * Adds defaults to stock_change column created in last migration.
 */
class Version403 extends Doctrine_Migration_Base
{
    public function up()
    {
        $stmt = Doctrine_Manager::getInstance()->connection();

        // Add/Remove from Stock
        $stmt->execute("UPDATE order_workflow set stock_change = 1 WHERE current_status = 'draft' AND next_status = 'new';");
        $stmt->execute("UPDATE order_workflow set stock_change = 1 WHERE current_status = 'calculation' AND next_status = 'new';");
        $stmt->execute("UPDATE order_workflow set stock_change = 1 WHERE current_status = 'deleted' AND next_status = 'new';");
        $stmt->execute("UPDATE order_workflow set stock_change = 1 WHERE current_status = 'new' AND next_status = 'deleted';");

        // Stock doesn't change its state
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'draft' AND next_status = 'deleted';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'new' AND next_status = 'realization';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'new' AND next_status = 'ready';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'realization' AND next_status = 'new';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'realization' AND next_status = 'bindery';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'bindery' AND next_status = 'ready';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'bindery' AND next_status = 'realization';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'ready' AND next_status = 'bindery';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'ready' AND next_status = 'new';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'ready' AND next_status = 'closed';");
        $stmt->execute("UPDATE order_workflow set stock_change = 0 WHERE current_status = 'closed' AND next_status = 'ready';");
    }

    public function down()
    {
        
    }
}