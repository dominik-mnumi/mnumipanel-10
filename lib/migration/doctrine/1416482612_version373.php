<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version373 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->changeColumn('invoice', 'invoice_status_name', 'string', '255', array(
             'notnull' => '1',
             'default' => 'unpaid',
             ));
    }

    public function down()
    {
    }
}