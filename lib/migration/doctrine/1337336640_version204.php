<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version204 extends Doctrine_Migration_Base
{ 
    public function up()
    {
        $this->dropForeignKey('cash_desk', 'cash_desk_user_id_sf_guard_user_id');
        $this->createForeignKey('cash_desk', 'cash_desk_user_id_sf_guard_user_id_1', array(
             'name' => 'cash_desk_user_id_sf_guard_user_id_1',
             'local' => 'user_id',
             'foreign' => 'id',
             'foreignTable' => 'sf_guard_user',
             'onUpdate' => 'cascade',
             'onDelete' => 'set null',
             ));
 
    }

    public function down()
    {
        $this->createForeignKey('cash_desk', 'cash_desk_user_id_sf_guard_user_id', array(
             'name' => 'cash_desk_user_id_sf_guard_user_id',
             'local' => 'user_id',
             'foreign' => 'id',
             'foreignTable' => 'sf_guard_user',
             ));
        $this->dropForeignKey('cash_desk', 'cash_desk_user_id_sf_guard_user_id_1');
    
    }
}