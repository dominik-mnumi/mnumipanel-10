<?php
/**
 * This class has been NOT auto-generated by the Doctrine ORM Framework
 */
class Version205 extends Doctrine_Migration_Base
{
    public function up()
    {
        $stmt = Doctrine_Manager::getInstance()->connection();  
  
        // Payments
        $whereNameCond = 'cash';
        $results = $stmt->execute("SELECT * FROM payment WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'cash payment';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");  
            $stmt->execute("UPDATE payment set label = '$setName' WHERE name = '$whereNameCond'");  
        }
        
        $whereNameCond = 'platnosc.pl (Płatności on-line)';
        $results = $stmt->execute("SELECT * FROM payment WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'online payment';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");  
            $stmt->execute("UPDATE payment set label = '$setName' WHERE name = '$whereNameCond'");  
        }

        $whereNameCond = 'karta (płatność kartą - terminal)';
        $results = $stmt->execute("SELECT * FROM payment WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'card payment';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");  
            $stmt->execute("UPDATE payment set label = '$setName' WHERE name = '$whereNameCond'");  
        }

        $whereNameCond = 'przelew (21 dni)';
        $results = $stmt->execute("SELECT * FROM payment WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'transfer (21 days)';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");  
            $stmt->execute("UPDATE payment set label = '$setName' WHERE name = '$whereNameCond'");  
        }

        $whereNameCond = 'kompensata';
        $results = $stmt->execute("SELECT * FROM payment WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'compensation';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");  
            $stmt->execute("UPDATE payment set label = '$setName' WHERE name = '$whereNameCond'");  
        }
        
        $whereNameCond = 'pobranie';
        $results = $stmt->execute("SELECT * FROM payment WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'cash on delivery';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");  
            $stmt->execute("UPDATE payment set label = '$setName' WHERE name = '$whereNameCond'");  
        }
        
        $whereNameCond = 'płatność punktami';
        $results = $stmt->execute("SELECT * FROM payment WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'points payment';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");  
            $stmt->execute("UPDATE payment set label = '$setName' WHERE name = '$whereNameCond'");  
        }
        
        $whereNameCond = 'przedpłata na konto';
        $results = $stmt->execute("SELECT * FROM payment WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'payment on account';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");  
            $stmt->execute("UPDATE payment set label = '$setName' WHERE name = '$whereNameCond'");  
        }
        
        // Carriers
        $whereNameCond = 'Kurier';
        $results = $stmt->execute("SELECT * FROM carrier WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'Courier';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");   
        }
        
        $whereNameCond = 'Poczta Polska - Priorytet';
        $results = $stmt->execute("SELECT * FROM carrier WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'Post office';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");   
        }
        
        $whereNameCond = 'Odbiorę osobiscie';
        $results = $stmt->execute("SELECT * FROM carrier WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'Collection';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");   
        }
        
        $whereNameCond = 'Wysyłka do odbiorcy';
        $results = $stmt->execute("SELECT * FROM carrier WHERE name = '$whereNameCond'");
        if(count($results->fetchAll()))
        {
            $setName = 'Shipping to customers';
            $stmt->execute("UPDATE payment set name = '$setName' WHERE name = '$whereNameCond'");   
        }
    }

    public function down()
    {

    }
}