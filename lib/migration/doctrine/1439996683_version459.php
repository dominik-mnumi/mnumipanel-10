<?php
/**
 * PAN-1746 Set sluggable index on name&deleted_at fields instead of only name
 */
class Version459 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->removeIndex( 'product', 'product_sluggable' );

        $options = array(
            'fields' => array(
                'slug' => array(),
                'deleted_at' => array()
            )
        );

        $this->addIndex( 'product', 'product_sluggable', $options );
    }

    public function down()
    {
        $this->removeIndex( 'product', 'product_sluggable' );

        $options = array(
            'fields' => array(
                'slug' => array(),
            )
        );

        $this->addIndex( 'product', 'product_sluggable', $options );
    }
}