<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Creates singleton instance of dispatcher.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class Mnumi
{

    static protected $instance;
    protected $dispatcher;

    static public function getInstance()
    {
        if(!self::$instance instanceof self)
        {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function setEventDispatcher(sfEventDispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function getEventDispatcher()
    {
        return $this->dispatcher;
    }

}
