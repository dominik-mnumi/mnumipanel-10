<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Company config data extends abstract class.
 * Checks if all neccessary data is defined in app.yml.
 * - 'green' if exist and no default,
 * - 'yellow' if company data is default,
 * - 'red' if no data.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
class ReportCompanyConfigData extends Report
{    
    public function checkStatus($value = null)
    {
        $status = 'green';
       
        // for tests only
        if(!$value)
        {
            $value = sfConfig::get('app_company_data_seller_name');           
        }
        
        if($value == 'My Company Name')
        {
            $status = 'yellow';
        }
        
        if(!$value
        || !sfConfig::get('app_company_data_seller_address')
        || !sfConfig::get('app_company_data_seller_postcode')
        || !sfConfig::get('app_company_data_seller_city')
        || !sfConfig::get('app_company_data_seller_tax_id'))
        {
            $status = 'red';
        }

        return $status;
    }

    public function getValue()
    {
        $status = $this->checkStatus();
        
        switch($status)
        {
            case 'green':
                return 'OK';
            case 'yellow':
                return 'Warning';
            case 'red':
                return 'Error';
        }
      
    }
}