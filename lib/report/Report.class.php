<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Abstract class implements InterfaceReport.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */
abstract class Report implements InterfaceReport
{
    static $config = 'mod_settings_report_class';
    static $statusArray = array('green', 'yellow', 'red');
    static $statusRed = 'red';
    static $statusYellow = 'yellow';
    static $statusGreen = 'green';
    
    public $currentStatus;
    public $name;
    public $info;

    public function __construct()
    {
        $this->currentStatus = $this->checkStatus();
        $config = sfConfig::get(self::$config, array());
        $this->info = array_key_exists(get_class($this), $config) ? $config[get_class($this)] : array();
    }

    public function getName()
    {
        return (array_key_exists('name', $this->info)) ?  $this->info['name']
                                                       :  null;
    }

    public function getDesc()
    {
        return (array_key_exists('desc', $this->info)) ?  $this->info['desc']
                                                       :  null;
    }

    public function getStatus()
    {
        return $this->currentStatus;
    }

    public function getStatusInfo()
    {
        return (array_key_exists('status', $this->info)) ?  $this->info['status'][$this->currentStatus]
                                                         :  null;
    }

    public function getInfo()
    {
        return $this->info;
    }
    
    public static function visible()
    {
        return true;
    }
}