<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Report interface.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Marek Balicki
 */

interface InterfaceReport
{
    //returns green, yellow or red status
    public function checkStatus($value);

    public function getName();
    public function getDesc();
    public function getStatus();
    public function getInfo();
    public function getStatusInfo();

    //returns value of current report, exp. 5.17 or ver. 5.17
    public function getValue();
}