<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Database Migration Version extends abstract class.
 *
 * @package    mnumicore
 * @subpackage report
 * @author     Adam Marchewicz
 */
class ReportMigrationVer extends Report
{
    private $migrationsDir = '/lib/migration/doctrine/';
    
    public function checkStatus($value = null)
    {
        $migration = new Doctrine_Migration(sfConfig::get('sf_root_dir').$this->migrationsDir);
        
        $latestVersion = $migration->getLatestVersion();
        $currentVersion = $migration->getCurrentVersion();

        if($currentVersion == $latestVersion)
        {
            return 'green';
        }
        else
        {
            return 'red';
        }
    }

    public function getValue()
    {
        $migration = new Doctrine_Migration(sfConfig::get('sf_root_dir').$this->migrationsDir);
        $currentVersion = $migration->getCurrentVersion();

        return $currentVersion;
    }
}