<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * OrdersCollectionNotification class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
class OrdersCollectionNotification implements Iterator {

    /** @var OrderNotification[] */
    private $orders;

    /**  @var sfI18NGettextPlural */
    private $i18n;

    /** @var myUser */
    private $user;

    /**
     * @param Order[] $orders
     * @param sfContext $context
     */
    public function __construct($orders, sfContext $context) {

        foreach($orders as $order) {
            $this->orders[] = new OrderNotification($order);
        }

        $this->user = $context->getUser();
        $this->i18n = $context->getI18N();
    }

    public function rewind() {
        reset($this->orders);
    }

    public function current() {
        return current($this->orders);
    }

    public function key() {
        return key($this->orders);
    }

    public function next() {
        return next($this->orders);
    }

    public function valid() {
        $key = key($this->orders);
        $var = ($key !== NULL && $key !== FALSE);
        return $var;
    }

    public function __toString() {

        $str = $this->i18n->__('Ordered products:') . '<br />';
        foreach ($this->orders as $order) {
            $str .= '- ' . $order->getName() . ' - ' . $this->user->formatCurrency($order->getTotalAmountGross()) . '<br />';
        }

        return $str;
    }
}
