<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Decorator for Order class - used in notification
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class OrderNotification 
{
    private $name;
    private $totalAmountGross;
    private $totalAmount;

    public function __construct(Order $order)
    {
        $this->name = $order->getName();
        $this->totalAmount = $order->getTotalAmount();
        $this->totalAmountGross = $order->getTotalAmountGross();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getTotalAmountGross()
    {
        return $this->totalAmountGross;
    }

    /**
     * @return float
     */
    public function getTotalAmount()
    {
        return $this->totalAmount;
    }
} 