<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class BuyNotification extends Notifications
{
    public $notificationTemplateName = 'Buy';

    /**
     * Creates instance of BuyNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @param string $webapiKey
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = null, $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        if($obj) {
            $obj->calculatePrice($obj->getOrdersBasketQuery());
        }

        // sets some delivery and payment prices
        $deliveryGrossPrice = ($obj) ? $this->myUserObj->formatCurrency(OrderTable::getInstance()->addTax($obj->getDeliveryPrice())) : 12.12;

        // values are sample data
        $this->availableLocalShortcodeArr = array(
            'orderColl' => array(
                'value' => $this->getSampleOrderCollection(),
                'desc' => 'orders'
            ),
            'createdAt' => array(
                'value' => '2012-12-12',
                'desc' => 'created at'
            ),
            'paymentType' => array(
                'value' => $this->i18NObj->__('cash payment'),
                'desc' => 'payment type'
            ),
            'deliveryType' => array(
                'value' => $this->i18NObj->__('collection'),
                'desc' => 'delivery type'
            ),
            'deliveryGrossPrice' => array(
                'value' => $deliveryGrossPrice,
                'desc' => 'delivery gross price'
            ),
            'totalGrossPrice' => array(
                'value' => $this->myUserObj->formatCurrency(43.89),
                'desc' => 'total gross price'
            ),
            'deliveryName' => array(
                'value' => $this->i18NObj->__('Delivery name'),
                'desc' => 'delivery name'
            ),
            'deliveryStreet' => array(
                'value' => $this->i18NObj->__('Street'),
                'desc' => 'delivery street'
            ),
            'deliveryCity' => array(
                'value' => $this->i18NObj->__('City'),
                'desc' => 'delivery city'
            ),
            'deliveryPostcode' => array(
                'value' => '00-000',
                'desc' => 'delivery postcode'
            ),
            'packageId' => array(
                'value' => '9999',
                'desc' => 'package id'
            ),
            'orderId' => array(
                'value' => '9999',
                'desc' => 'order id'
            ),
            'shopDescription' => array(
                'value' => 'Shop',
                'desc'  => 'shop description'
            ),
        );

        if($obj && !$obj instanceof OrderPackage)
        {
            throw new Exception('Passed object is not instance of OrderPackage class.');
        }

        $this->setValuesOfAvailableLocalShortcodeArr();
    }

    /**
     * Prepares availableLocalShortcodeArr.
     */
    public function setValuesOfAvailableLocalShortcodeArr($externalArr = null)
    {
        if($this->obj) {
            $this->obj->calculatePrice($this->obj->getOrdersQuery());
        }

        foreach($this->availableLocalShortcodeArr as $key => $rec)
        {
            $methodName = $this->getNotificationShortcodeMethodName($key);

            if($this->obj && method_exists($this->obj, $methodName))
            {
                if(in_array($key, array('deliveryGrossPrice', 'totalGrossPrice')))
                {
                    $this->availableLocalShortcodeArr[$key]['value'] = $this->myUserObj->formatCurrency($this->obj->$methodName());
                }
                elseif($key == 'orderColl')
                {
                    $orderColl = $this->obj->$methodName();
                    $this->availableLocalShortcodeArr[$key]['value'] = new OrdersCollectionNotification($orderColl->getData(), $this->contextObj);
                }
                else
                {
                    $this->availableLocalShortcodeArr[$key]['value'] = $this->obj->$methodName();
                }
            }
        }
    }

    /**
     * Get Sample order collection
     */
    protected function getSampleOrderCollection()
    {
        $sampleOrder = new Order();
        $sampleOrder->setName('Flyers 1page');
        $sampleOrder->setTaxValue(.23);
        $sampleOrder->setTotalAmount(100);
        $orderColl = new OrdersCollectionNotification(
            array(
                $sampleOrder
            ),
            $this->contextObj
        );

        return $orderColl;
    }
}
