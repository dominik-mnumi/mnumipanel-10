<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending
 *
 * @author Bartosz Dembek<bartosz.dembek@mnumi.com>
 */
class PackagePostedNotification extends Notifications
{
    public $notificationTemplateName = 'Package posted';

    /**
     * Creates instance of PackagePostedNotification.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param array $externalArr
     * @param string $webapiKey
     * @throws Exception
     */
    public function __construct(sfContext $contextObj, $obj = null, $externalArr = array(), $webapiKey = null)
    {
        parent::__construct($contextObj, $obj, $webapiKey);

        // values are sample data
        $this->availableLocalShortcodeArr = array(
            'transportNumber' => array(
                'value' => '9999',
                'desc' => 'transport number'
            ),
            'sendAt' => array(
                'value' => '2014-01-01',
                'desc' => 'date of package post'
            ),
            'packageId' => array(
                'value' => '9999',
                'desc' => 'package id'
            ),
            'packageName' => array(
                'value' => '2014-01-01',
                'desc' => 'package name'
            ),
            'shopDescription' => array(
                'value' => 'Shop',
                'desc'  => 'shop description'
            )
        );

        if($obj && !$obj instanceof OrderPackage)
        {
            throw new Exception('Passed object is not instance of OrderPackage class.');
        }

        $this->setValuesOfAvailableLocalShortcodeArr();
    }

}
