<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications class - handling email, sms etc notifications sending
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class Notifications
{
    public $notificationTemplateName = null;

    protected $availableGlobalShortcodeArr = array();
    protected $availableLocalShortcodeArr = array();
    protected $contextObj;
    protected $myUserObj;
    protected $i18NObj;
    protected $userObj;

    /**
     * @var string
     */
    private $webapiKey;

    /**
     * Returns notification template class name based on notification template name.
     *
     * @param string $notificationTemplateName
     * @return string
     */
    public static function getNotificationClassByTemplate($notificationTemplateName)
    {
        return Cast::camelize($notificationTemplateName).'Notification';
    }

    /**
     * Returns user object based on given object.
     *
     * @param Doctrine_Record $obj
     * @return Doctrine_Record
     */
    public static function getUserObject($obj)
    {
        // checks events
        switch(get_class($obj))
        {
            case 'sfGuardUser':
                return $obj;
            case 'Client':
                // gets client user object
                $clientUserObj = $obj->getClientUsers()->getFirst();

                // if client user object exists
                if($clientUserObj)
                {
                    // gets user
                    $userObj = $clientUserObj->getUser();

                    // if user exists
                    if($userObj)
                    {
                        return $userObj;
                    }
                }

                return null;
            default:
                return $obj->getSfGuardUser();
        }
    }

    /**
     * Creates instance of Notifications.
     *
     * @param sfContext $contextObj
     * @param Doctrine_Record $obj
     * @param string|null $webapiKey
     */
    public function __construct(sfContext $contextObj, $obj = null, $webapiKey = null)
    {
        $this->contextObj = $contextObj;
        $this->contextObj->getConfiguration()->loadHelpers(array('Date'));
        $this->myUserObj = $this->contextObj->getUser();
        $this->i18NObj = $this->contextObj->getI18N();
        $this->obj = $obj;
        $this->webapiKey = $webapiKey;

        if($this->obj)
        {
            $this->userObj = self::getUserObject($this->obj);
        }

        $this->availableGlobalShortcodeArr = array(
            'companyName' => array(
                'value' => sfConfig::get('app_company_data_seller_name', 'My Company Name'),
                'desc' => 'company name'
            ),
            'companyAddress' => array(
                'value' => sfConfig::get('app_company_data_seller_address', 'Street Number/Flat'),
                'desc' => 'company address'
            ),
            'companyPostcode' => array(
                'value' => sfConfig::get('app_company_data_seller_postcode', '00-000'),
                'desc' => 'company postcode'
            ),
            'companyCity' => array(
                'value' => sfConfig::get('app_company_data_seller_city', 'City'),
                'desc' => 'company city'
            ),
            'companyTaxId' => array(
                'value' => sfConfig::get('app_company_data_seller_tax_id', '1234567819'),
                'desc' => 'company TIN'
            ),
            'companyBankName' => array(
                'value' => sfConfig::get('app_company_data_seller_bank_name', 'Bank name'),
                'desc' => 'company bank name'
            ),
            'companyBankAccount' => array(
                'value' => sfConfig::get('app_company_data_seller_bank_account', '12 3456 7890 1234 5678 9012 3456'),
                'desc' => 'company bank account'
            ),
            'shopHost' => array(
                'value' => sfConfig::get('app_shop_host', 'http://shop.tests.mnumi.com/'),
                'desc' => 'shop url'
            ),
            'shopDescription' => array(
                'value' => $this->getShop() ? $this->getShop()->getDescription() : '',
                'desc'  => 'shop description'
            ),
            'shopLoginUrl' => array(
                'value' => sfConfig::get('app_shop_host').sfConfig::get('app_shop_url_login', 'user/login'),
                'desc' => 'shop login url'
            ),
            'currencySymbol' => array(
                'value' => $this->myUserObj->getCurrencySymbol(),
                'desc' => 'currency symbol'
            ),
            'date' => array(
                'value' => format_date(time(), 'D'),
                'desc' => 'current date'
            )
        );
    }

    /**
     * Returns list of notifications that will be send.
     *
     * @param integer limit of return notifications
     * @return Doctrine_Collection|Doctrine_Null
     */
    public function getNotificationsToSend($limit = 10)
    {
        return NotificationMessageTable::getInstance()->getNotificationsToSend($limit);
    }

    /**
     * Returns global shortcodes.
     *
     * @return array
     */
    public function getGlobalShortcodes()
    {
        return $this->availableGlobalShortcodeArr;
    }

    /**
     * Returns local shortcodes.
     *
     * @return array
     */
    public function getLocalShortcodes()
    {
        return $this->availableLocalShortcodeArr;
    }

    public function hasLocalShortcodes()
    {
        return (count($this->getLocalShortcodes()) > 0);
    }

    /**
     * Returns array with prepared shortcodes parameters.
     *
     * @return array
     */
    public function getShortcodesAsArgArr()
    {
        $allShortcodeArr = array_merge(
            $this->getGlobalShortcodes(),
            $this->getLocalShortcodes()
        );

        foreach($allShortcodeArr as $key => $rec)
        {
            $shortcodeAsArgArr[$key] = $rec['value'];
        }

        return $shortcodeAsArgArr;
    }

    /**
     * Prepares availableLocalShortcodeArr.
     *
     * @param array $externalArr
     */
    public function setValuesOfAvailableLocalShortcodeArr($externalArr = null)
    {
        foreach($this->availableLocalShortcodeArr as $key => $rec)
        {
            if(isset($externalArr[$key]))
            {
                $this->availableLocalShortcodeArr[$key]['value'] = $externalArr[$key];
            }
            elseif($this->obj)
            {
                $methodName = $this->getNotificationShortcodeMethodName($key);

                if(method_exists($this->obj, $methodName))
                {
                    $this->availableLocalShortcodeArr[$key]['value'] = $this->obj->$methodName();
                }
            }
        }
    }

    /**
     * Prepares and sends notification depending on user, template and type (parameters are prepared in children classes).
     *
     * @return boolean
     */
    public function prepareAndSendNotification()
    {
        return $this->sendNotification(
            $this->userObj,
            null,
            $this->notificationTemplateName,
            $this->getShortcodesAsArgArr());
    }

    /**
     * Returns NotificationTemplate object.
     *
     * @return NotificationTemplate
     * @throws Exception
     */
    public function getTemplateObj()
    {
        if(!$this->notificationTemplateName)
        {
            throw new Exception('Template name is not defined.');
        }

        return NotificationTemplateTable::getInstance()->findOneByName($this->notificationTemplateName);
    }

    /**
     * Returns NotificationTemplate object.
     *
     * @param string $notificationType
     * @return Notification
     * @throws Exception
     */
    public function getNotificationObj($notificationType = 'E-mail')
    {
        if(!$this->notificationTemplateName)
        {
            throw new Exception('Template name is not defined.');
        }

        if(!in_array($notificationType, NotificationTypeTable::$typeArr))
        {
            throw new Exception('Wrong notification type.');
        }

        return NotificationTable::getInstance()->getByTemplateNameAndTypeName(
            $this->notificationTemplateName,
            $notificationType);
    }

    /**
     * Returns class name for NotificationXXXXX based on notification type.
     *
     * @param string $input
     * @return string
     * @throws Exception
     */
    protected static function getNotificationClassName($input)
    {
        if(!in_array($input, NotificationTypeTable::$typeArr))
        {
            throw new Exception('Problem with: '.$input.' channel to send notification.');
        }

        $str = Cast::removeDash($input);
        $str = Cast::camelize($str);

        return 'Notification'.$str;
    }

    /**
     * Returns method name based on notification shortcode name.
     *
     * @param string $shortcodeName
     * @return string
     */
    protected static function getNotificationShortcodeMethodName($shortcodeName)
    {
        return 'get'.Cast::camelize($shortcodeName).'Notification';
    }



    /**
     * Sends notification depending on user, template and type.
     *
     * @param sfGuardUser $userObj
     * @param array $notificationTypeArr
     * @param string $notificationTemplateName
     * @param array $argArr
     * @return boolean
     */
    protected function sendNotification(sfGuardUser $userObj, $notificationTypeArr = array(), $notificationTemplateName = null,
                                        $argArr = array())
    {
        // if null then gets all types for user (where send_notification is true),
        // otherwise types only from parameter
        if(empty($notificationTypeArr))
        {
            $notificationTypeArr = $userObj->getNotificationTypes();
        }
        if(self::getNotificationClassByTemplate($notificationTemplateName) == "LostPasswordNotification")
        {
            $notificationTypeArr = $userObj->getNotificationLostPasswordType();
        }

        // if zero defined types
        if(count($notificationTypeArr) == 0)
        {
            return false;
        }

        // foreach notification send
        foreach($notificationTypeArr as $rec)
        {
            // gets template object
            $notificationTemplateObj = NotificationTemplateTable::getInstance()
                ->findOneByName($notificationTemplateName);

            // gets notification template id
            $notificationTemplateId = $notificationTemplateObj->getId();

            // gets type object
            $notificationTypeObj = NotificationTypeTable::getInstance()
                ->findOneByName($rec);

            // gets notification type id
            $notificationTypeId = $notificationTypeObj->getId();

            // gets notification object
            $notificationObj = NotificationTable::getInstance()
                ->findOneByNotificationTypeIdAndNotificationTemplateId($notificationTypeId,
                    $notificationTemplateId);

            if(!$notificationObj)
            {
                return false;
            }

            // prepares class name for sender
            $className = self::getNotificationClassName($rec);

            // twig template
            $twigContent = TwigNotificationManager::getInstance()
                ->getTwigTemplateObj($notificationTemplateObj->getName(),
                    $notificationTypeObj->getName())
                ->render($argArr);

            /**
             * creates sender object, prepares messages and sends
             * @param NotificationSendType $notificationSend
             */
            $notificationSend = new $className(null, $this->getWebapiKey());
            $notificationSend->composeMessage($userObj,
                $twigContent, $notificationObj->getTitle(), $notificationObj->getBcc());

            /* Send section is moved to task and is run by cron.
             *
            foreach($notificationSend->getNotificationsToSend() as $rec)
            {
                $notificationSendMessage = new $className($rec);
                $notificationSendMessage->send(sfContext::getInstance()->getMailer());
            }
             */
        }

        return true;
    }

    /**
     * @return string
     */
    public function getWebapiKey()
    {
        return $this->webapiKey;
    }

    /**
     * Returns shop object
     *
     * @return Shop
     */
    public function getShop()
    {
        $shop = null;
        if($this->webapiKey) {
            $shop = ShopTable::getInstance()->findOneByName($this->webapiKey);
        }

        return $shop;
    }

}
