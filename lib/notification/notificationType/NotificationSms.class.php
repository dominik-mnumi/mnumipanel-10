<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Notifications SMS
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class NotificationSms extends NotificationSendType
{
    // notification object
    private $notificationType;
    
    // sender name (company name or person name)
    private $senderName = 'Mnumi';
    
    //sender email
    private $sender = '12345678';
    
    /**
     * Compose message to send
     */
    function __construct(NotificationMessage $notification = null, $webapiKey = null)
    {
        parent::__construct($notification, $webapiKey);

        $notificationType = NotificationTypeTable::getInstance()->getSms();
        
        if(!$notificationType)
        {
            throw new Exception('Notification type for sms does not exixst.');
        }
        
        $this->notificationType = $notificationType;
    }

    /**
     * Compose message to send
     *
     * @param $user
     * @param $message
     * @param string $title
     * @return boolean
     */
    public function composeMessage($user, $message, $title = '')
    {
        return $this->composeMessageAndSave($user, $message, $title, $this->notificationType, '', '');
    }

    /**
     * Send email
     *
     * @param null $mailer
     * @return boolean
     */
    public function send($mailer = null)
    {
        $notifications = $this->notification;

        if($notifications && get_class($notifications) == 'NotificationMessage')
        {
            $psms = new SmsNotificationAdapter();
            
            $status = $psms->sendSms($notifications->getRecipient(), $notifications->getMessagePlain());
            $notifications->setSendAt(new Doctrine_Expression('NOW()'));
            
            if(!$status)
            {
                $notifications->setErrorMessage("Sms couldn't be send. Check your configuration.");
                $notifications->save();
                return false;
            }
            $notifications->save();
            return true;
        }
        
        return false;
    }
}

