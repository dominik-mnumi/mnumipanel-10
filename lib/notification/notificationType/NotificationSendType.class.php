<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * AbstractNotificationType class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
abstract class NotificationSendType
{
    // notification type object
    /**
     * @var NotificationMessage
     */
    protected $notification;

    /**
     * @var string
     */
    protected $webapiKey;

    /**
     * Compose message to send
     *
     * @param Notifications $notification
     */
    public function __construct(NotificationMessage $notification = null, $webapiKey = null)
    {
        $this->notification = $notification;
        $this->webapiKey = $webapiKey;
    }

    abstract public function composeMessage($user, $message, $title = '');

    /**
     * Sends email.
     *
     * @param Mailer $mailer
     * @return boolean
     */
    abstract public function send($mailer = null);

    /**
     * Saves message in database
     *
     * @param $user - User object
     * @param $message - message for user
     * @param $title - message title
     * @param $notificationType - Notification type
     * @param $sender sender email / numer etc
     * @param $senderName
     * @return boolean
     */
    protected function composeMessageAndSave($user, $message, $title = '',
                                             $notificationType, $sender, $senderName, $bcc = null)
    {
        $contacts = $user->getUserContacts();

        if($contacts)
        {
            foreach($contacts as $contact)
            {
                if(($contact->getNotificationTypeId() == $notificationType->getId())
                    && ($contact->getSendNotification()))
                {
                    $this->createNotification($contact, $user, $message, $title,
                        $notificationType, $sender, $senderName, $bcc);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Saves message in database
     *
     * @param $user - User object
     * @param $message - message for user
     * @param $title - message title
     * @param $notificationType - Notification type
     * @param $sender sender email / numer etc
     * @param $senderName
     * @return boolean
     */
    protected function composeEmailAndSave($user, $message, $title = '',
        $notificationType, $sender, $senderName, $bcc = null)
    {
        $contacts = $user->getUserContacts();

        if($contacts)
        {
            foreach($contacts as $contact)
            {
                if($contact->getNotificationTypeId() == $notificationType->getId())
                {
                    $this->createNotification($contact, $user, $message, $title,
                        $notificationType, $sender, $senderName, $bcc);
                }
            }
            return true;
        }
        return false;
    }

    /**
     * Create notification
     *
     * @param $contact UserContact
     * @param $user - User object
     * @param $message - message for user
     * @param $title - message title
     * @param $notificationType - Notification type
     * @param $sender sender email / numer etc
     * @param $senderName
     */

    private function createNotification($contact, $user, $message, $title = '',
        $notificationType, $sender, $senderName, $bcc = null)
    {
        $notificationMessage = new NotificationMessage();
        $notificationMessage->setUserId($user->getId());
        $notificationMessage->setNotificationTypeId($notificationType->getId());
        $notificationMessage->setSender($sender);
        $notificationMessage->setSenderName($senderName);
        $notificationMessage->setRecipient($contact->getValue());
        $notificationMessage->setRecipientName($user->getFirstName().' '.$user->getLastName());
        $notificationMessage->setMessagePlain(strip_tags($message));
        $notificationMessage->setMessageHtml($message);
        $notificationMessage->setTitle($title);
        $notificationMessage->setBcc($bcc);
        $notificationMessage->save();
    }
}