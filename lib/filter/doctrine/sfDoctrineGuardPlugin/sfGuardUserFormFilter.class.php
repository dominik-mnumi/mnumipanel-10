<?php

/**
 * sfGuardUser filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserFormFilter extends PluginsfGuardUserFormFilter
{
    // additional fields for request
    private $additionalFields = array('client_name' => 'client_name');
    
    public function configure()
    {
        $this->setWidgets(array(
          'email_address'    => new sfWidgetFormFilterInput(array('with_empty' => false)),
          'username'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
          'client_name'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
        ));

        $this->setValidators(array(
          'email_address'    => new sfValidatorPass(array('required' => false)),
          'username'         => new sfValidatorPass(array('required' => false)),
          'client_name'         => new sfValidatorPass(array('required' => false)),
        ));

        $this->widgetSchema->setNameFormat('sf_guard_user_filters[%s]');      
      
    }
    
    public function addClientNameColumnQuery(Doctrine_Query $query, $field, $values) 
    {
        if ($values['text'])
        {
            $query->leftJoin('r.ClientUsers cu')
                  ->leftJoin('cu.Client c')
                  ->andWhere('c.fullname LIKE ?', '%'.$values['text'].'%');
        }
    }    
    
    /**
     * Merge original fields with additional
     * 
     * @return array
     */
    public function getFields() 
    {
        return array_merge(parent::getFields(), $this->additionalFields);
    }  
}
