<?php

/**
 * InvoiceStatus filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceStatusFormFilter extends BaseInvoiceStatusFormFilter
{
  public function configure()
  {
  }
}
