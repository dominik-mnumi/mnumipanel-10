<?php

/**
 * Coupon filter form.
 *
 * @package    mnumicore
 * @subpackage filter
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CouponFormFilter extends BaseCouponFormFilter
{
    public function configure()
    {
        $this->setWidgets(array(
            'name' => new sfWidgetFormFilterInput(array(
                'with_empty' => false,
                'label' => 'Coupon name')),
            'inactive' => new sfWidgetFormInputCheckbox(array(
                'label' => 'Show inactive coupons')),
            'from_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
            'to_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker'))
        ));
        
        $this->setValidators(array(
            'name' => new sfValidatorPass(),
            'inactive' => new sfValidatorPass(),
            'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')),
            'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59'))
        ));

        // set base filter query
        $this->setQuery(CouponTable::getInstance()->getTableRecordsQuery());
        
        $this->widgetSchema->setNameFormat('coupon_filters[%s]');
    }
    
    /**
     * Builds a Doctrine Query based on the passed values.
     * Logic structure of query must take attention to dependencies between
     * inactive and name fields.
     *
     * @param array An array of parameters to build the Query object
     * @return Doctrine_Query A Doctrine Query object
     */
    public function buildQuery(array $values)
    {
        $query = parent::buildQuery($values);

        // clears where clause
        $query->where('name IS NOT NULL');
   
        if($values['name']['text'])
        {
            $query->andWhere('name LIKE ?', '%'.$values['name']['text'].'%');
        }  
        
        if(!$values['inactive'])
        {
            $query->andWhere('expire_at >= ?', date('Y-m-d'));
        } 

        // from date
        if($values['from_date'])
        {
            $query->andWhere('created_at >= ?', $values['from_date']);
        }
        
        // to date
        if($values['to_date'])
        {
            $query->andWhere('created_at <= ?', $values['to_date']);
        }
        
        return $query;
    }
}
