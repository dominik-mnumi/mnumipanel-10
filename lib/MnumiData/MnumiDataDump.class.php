<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Prepares php data basing on specified database.
 *
 * @author Marek Balicki <marek.balicki@mnumi.com>
 */
class MnumiDataDump
{
    protected $conn;
    protected $i18NObj;
    
    protected $tableArr;
    protected $filename;

    /**
     * Constructor of MnumiDataDump class.
     * 
     * @param string $dbHost     
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbName
     * @param sfI18N $i18NObj
     * @param array $tableArr
     */
    public function __construct($dbHost, $dbUser, $dbPass, $dbName, sfI18N $i18NObj, 
            $tableArr)
    {       
        // gets connection
        $this->conn = $this->getConnection($dbHost, $dbUser, $dbPass, $dbName);

        // sets $i18NObj
        $this->i18NObj = $i18NObj;
        
        // gets tables
        $this->tableArr = $tableArr;

        // prepares filename
        $this->filename = __DIR__.'/../../data/v3MigrationFiles/v3Dump_'.$this->i18NObj->getCulture().'.sql';
    }
  
    public function generateSqlFile()
    {
        $resultText = '';

        // foreach table
        foreach($this->tableArr as $table => $fieldArr)
        {
            // prepares columns and records
            $columnArr = $this->getColumns($table);
            foreach($columnArr as &$column)
            {
                $column = '`'.$column.'`';
            }
            
            $columnsStr = implode(', ', $columnArr);
            $rowArr = $this->getRecords($table, $columnsStr);
            
            // if no records
            if(empty($rowArr))
            {
                continue;
            }
            
            // header section
            $text = 'INSERT INTO `'.$table.'` ('.$columnsStr.') VALUES'."\n";
            
            // values section
            foreach($rowArr as &$row)
            {
                foreach($row as $columnKey => &$columnValue)
                {      
                    if($columnValue == null)
                    {
                        $columnValue = 'NULL';
                    }
                    // some exeptions
                    elseif(($table == 'category' && $columnValue == 'Main')
                        || ($table == 'field_item' && in_array($columnValue,
                                array('Single', 'Double', '- Customizable -')))
                        || ($table == 'pricelist' && $columnValue == 'Default')
                    )
                    {
                        $columnValue = '\''.$columnValue.'\'';
                    }
                    // translate for defined fields
                    elseif(is_array($fieldArr) && in_array($columnKey, $fieldArr))
                    {
                        $columnValue = $this->clean($columnValue);
                        $columnValue = '\''.$this->translate($columnValue).'\'';
                    }  
                    // migration version
                    elseif($table == 'migration_version' && $columnKey == 'version')
                    {
                        $fileMigration = new Doctrine_Migration(sfConfig::get('sf_root_dir').'/lib/migration/doctrine/');
                        $columnValue = '\''.$fileMigration->getLatestVersion().'\'';
                    }
                    // for other fields
                    else
                    {
                        $columnValue = $this->clean($columnValue);
                        $columnValue = '\''.$columnValue.'\'';
                    }
                }
     
                $row = '('.implode(', ', $row).')';
            }
            
            $text .= implode(",\n", $rowArr);

            // end section
            $text .= ";\n\n";

            $resultText .= $text;         
        }
        
        // checks if file exists
        if(!file_exists($this->filename))
        {
            fopen($this->filename, "a");
            chmod($this->filename, 0777);
        }
        
        // saves to file
        file_put_contents($this->filename, $resultText);
    }
    
    /**
     * Returns database connection.
     * 
     * @param string $dbHost
     * @param string $dbUser
     * @param string $dbPass
     * @param string $dbName
     * @return string
     * @throws Exception
     */
    protected function getConnection($dbHost, $dbUser, $dbPass, $dbName)
    {
        $conn = mysql_connect($dbHost, $dbUser, $dbPass);
        if(!$conn) 
        {
            throw new Exception('Could not connect: '.mysql_error());
        }
       
        // selects database
        if(!mysql_select_db($dbName, $conn)) 
        {
            throw new Exception('Could not select database.');
        }
        
        return $conn;
    }
    
    /**
     * Returns tables of databases.
     * 
     * @return array
     */
    protected function getTables()
    {
        return $this->getValues('SHOW TABLES');
    }
    
    /**
     * Returns columns of table.
     * 
     * @param $table $table
     * @return array
     */
    protected function getColumns($table)
    {
        return $this->getValues('SHOW COLUMNS FROM '.$table);
    }
    
    /**
     * Returns records of current table in specific column order.
     * 
     * @param string $table
     * @param array $columns
     * @return array
     */
    protected function getRecords($table, $columns)
    {
        $result = mysql_query('SELECT '.$columns.' FROM '.$table, $this->conn); 

        $arr = array();        
        while($row = mysql_fetch_assoc($result))
        {
            $arr[] = $row;
        }

        return $arr;
    }
    
    /**
     * Returns values of given query (for non standard queries e.g. SHOW TABLES).
     * 
     * @param string $query
     * @return array
     */
    protected function getValues($query)
    {
        // gets tables of database
        $result = mysql_query($query, $this->conn); 
        
        $arr = array();        
        while($row = mysql_fetch_assoc($result))
        {
            $row2 = array_values($row);
            $arr[] = array_shift($row2);
        }

        return $arr;
    }
    
    /**
     * Returns translated text.
     * 
     * @param string $text
     * @return string
     */
    protected function translate($text)
    {
        return $this->i18NObj->__($text);
    }

    /**
     * Clean text (remove new lines, convert (') char
     * @param $text
     * @return mixed
     */
    protected function clean($text)
    {
        $text = preg_replace("/\r\n/", "\\n", $text);
        $text = preg_replace("/\n/", "\\n", $text);
        $text = preg_replace("/'/", "\\'", $text);

        return $text;
    }
}
