<?php

/*
* This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
* This class checks input prices parameter and cleans up if necessary.
*
* @package mnumicore
* @subpackage validator
* @author Adam Marchewicz
*/
class sfValidatorDefaultPricelistPricesRequired extends sfValidatorBase
{
    /**
    * Returns cleaned values and errors.
    *
    * @param array $values
    * @return array
    */
    protected function doClean($pricelist)
    {
        $anyPriceSet = false;
        if(isset($pricelist['type']) && is_array($pricelist['type']))
        {
            // foreach price type in pricelist
            foreach($pricelist['type'] as $typeKey => $type)
            {
                foreach($type as $rowKey => $row)
                {
                    if(isset($row['price']))
                    {
                        if($row['price'] > 0)
                        {
                            $anyPriceSet = true;
                        }
                    }
                }
            }
        }
        
        if(!$anyPriceSet)
        {
            return new sfValidatorError($this, 'At least one price is required for default pricelist');
        }
        
        return false;
    }
}