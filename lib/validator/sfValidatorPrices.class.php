<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * This class checks input prices parameter and cleans up if necessary.
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class sfValidatorPrices extends sfValidatorBase
{
    /**
     * Returns cleaned values and errors.
     *
     * @param array $valueArr
     * @return array
     */
    protected function doClean($valueArr)
    {
        $cleanedValueArr = $valueArr;

        // foreach pricelist
        $errorArr = array();
        $validatorNumberExtended = new sfValidatorNumberExtended();
        foreach($cleanedValueArr as $pricelistKey => $pricelist)
        {
            // gets pricelist object
            $pricelistObj = PricelistTable::getInstance()->find($pricelistKey);

            if(isset($pricelist['type']) && is_array($pricelist['type']))
            {
                // foreach price type in pricelist
                foreach($pricelist['type'] as $typeKey => $type)
                {
                    foreach($type as $rowKey => $row)
                    {
                        // sets price type as amount (default)
                        $priceType = FieldItemPriceQuantityTable::$amountType;

                        if(isset($row['price']))
                        {
                            // if price = '' means that this is last row (for new record)
                            if($row['price'] == '')
                            {
                                unset($cleanedValueArr[$pricelistKey]['type'][$typeKey][$rowKey]);
                                continue;
                            }
                            if(!is_numeric($row['quantity']))
                            {
                                $errorArr[] = new sfValidatorError($this, 'Invalid quantity (format: x or x.xx)');
                            }

                            if((int)$row['quantity'] != $row['quantity']
                                    && in_array($typeKey, array(FieldItemPriceTypeTable::$price_page,
                                            FieldItemPriceTypeTable::$price_copy,
                                            FieldItemPriceTypeTable::$price_item)))
                            {
                                $errorArr[] = new sfValidatorError($this, 'Invalid quantity in "'.MeasureUnitTable::getPrice($typeKey).'" (format: x - only integer)');
                            }

                            // if pricelist is default then price can be only amount
                            if($pricelistObj->isDefault()) {
                                try {
                                    $row['price'] = $validatorNumberExtended->clean($row['price']);
                                }
                                catch(sfValidatorError $e) {
                                    $errorArr[] = new sfValidatorError($this, 'Invalid price format (format: x.xx)');
                                }
                            }

                            // if pricelist is not default then price can be amount (e.g. 10.00) or percent (e.g. 10.5%)
                            if(!$pricelistObj->isDefault())
                            {
                                // if percent type
                                if(substr($row['price'], -1) == '%')
                                {
                                    $row['price'] = substr($row['price'], 0, -1);
                                    try {
                                        $row['price'] = $validatorNumberExtended->clean($row['price']);
                                    }
                                    catch(sfValidatorError $e) {
                                        $errorArr[] = new sfValidatorError($this, 'Invalid price percent format (format: x.xx%, positive)');
                                    }
                                    if ($row['price'] < 0) {
                                        $errorArr[] = new sfValidatorError($this, 'Invalid price percent format (format: x.xx%, positive)');
                                    }

                                    // sets price type as percent
                                    $priceType = FieldItemPriceQuantityTable::$percentType;
                                }
                                // if amount type
                                elseif(!is_numeric($row['price']))
                                {
                                    $errorArr[] = new sfValidatorError($this, 'Invalid price format (format: x.xx)');
                                }
                            }

                            $cleanedValueArr[$pricelistKey]['type'][$typeKey][$rowKey]['price'] = CurrencyTool::formatPrice($row['price']);
                            $cleanedValueArr[$pricelistKey]['type'][$typeKey][$rowKey]['type'] = $priceType;
                        }
                        // validation for simplex and duplex
                        elseif(isset($row['price_simplex']) && isset($row['price_duplex']))
                        {
                            // if price = '' means that this is last row (for new record)
                            if($row['price_simplex'] == '' && $row['price_duplex'] == '')
                            {
                                unset($cleanedValueArr[$pricelistKey]['type'][$typeKey][$rowKey]);
                                continue;
                            }

                            if(!is_numeric($row['quantity']))
                            {
                                $errorArr[] = new sfValidatorError($this, 'Invalid quantity (format: x or x.xx)');
                            }

                            $printArr = array('price_simplex', 'price_duplex');
                            foreach($printArr as $printType)
                            {
                                // if pricelist is default then price can be only amount
                                if ($pricelistObj->isDefault())
                                {
                                    try {
                                        $row[$printType] = $validatorNumberExtended->clean($row[$printType]);
                                    }
                                    catch(sfValidatorError $e) {
                                        $errorArr[] = new sfValidatorError($this, 'Invalid price format (format: x.xx)');
                                    }
                                }

                                // if pricelist is not default then price can be amount (e.g. 10.00) or percent (e.g. 10.5%)
                                if(!$pricelistObj->isDefault())
                                {
                                    // if percent type
                                    if(substr($row[$printType], -1) == '%')
                                    {
                                        // sets price type as percent
                                        $priceType = FieldItemPriceQuantityTable::$percentType;

                                        $row[$printType] = substr($row[$printType], 0, -1);
                                        
                                        try {
                                            $row[$printType] = $validatorNumberExtended->clean($row[$printType]);
                                        }
                                        catch(sfValidatorError $e) {
                                            $errorArr[] = new sfValidatorError($this, 'Invalid price percent format (format: x.xx%, positive)');
                                        }
                                        if ($row[$printType] < 0)
                                        {
                                            $errorArr[] = new sfValidatorError($this, 'Invalid price percent format (format: x.xx%, positive)');
                                        }
                                    }
                                    // if amount price
                                    else
                                    {
                                        // sets price type as percent
                                        $priceType = FieldItemPriceQuantityTable::$amountType;

                                        // if is not numeric then error
                                        if(!is_numeric($row[$printType]))
                                        {
                                            $errorArr[] = new sfValidatorError($this, 'Invalid price format (format: x.xx)');
                                        }
                                    }
                                }

                                $cleanedValueArr[$pricelistKey]['type'][$typeKey][$rowKey][$printType] = CurrencyTool::formatPrice($row[$printType]);
                                $cleanedValueArr[$pricelistKey]['type'][$typeKey][$rowKey][$printType.'_type'] = $priceType;
                            }
                        }
                    }
                }
            }
        }

        return array(
            'values' => $valueArr,
            'cleanedValues' => $cleanedValueArr,
            'errors' => $errorArr);
    }
}
