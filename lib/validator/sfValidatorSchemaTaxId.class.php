<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * sfValidatorSchemaTaxId
 *
 * @package    mnumicore
 * @subpackage validator
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 */

class sfValidatorSchemaTaxId extends sfValidatorSchema
{
    /**
     * @see sfValidatorBase
     */
    public function __construct($field, $options = array(), $messages = array())
    {
        $this->addOption('field', $field);


        return parent::__construct(null, $options, $messages);
    }

    /**
     * @see sfValidatorBase
     */
    protected function configure($options = array(), $messages = array())
    {
        $this->addOption('parseFromFullAddress', false);
        $this->addOption('countryField', 'country');

        return parent::configure($options, $messages);
    }

    /**
     * @see sfValidatorBase
     */
    public function clean($values)
    {
        if (!is_array($values))
        {
            throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
        }

        if(!array_key_exists($this->getOption('field'), $values))
        {
            throw new InvalidArgumentException('Field ' . $this->getOption('field') . ' does not exists.');
        }

        $fieldValue = $values[$this->getOption('field')];

        if(empty($fieldValue))
        {
            return $values;
        }

        if($this->getOption('parseFromFullAddress'))
        {
            $addressValue = $values['full_address'];

            if(empty($addressValue))
            {
                return $values;
            }

            $address = InvoiceTable::getInstance()->parseClientDetails($addressValue);
            $country = $address['country'];
        }
        else
        {
            $country = $this->getOption('countryField')
                ? $values[$this->getOption('countryField')]
                : $values['country'];

        }

        $country = strtolower($country);

        switch ($country)
        {
            case 'be':
                if(!preg_match('/^BE[\d]{10}$/', $fieldValue))
                {
                    throw new sfValidatorErrorSchema($this, array($this->getOption('field') => new sfValidatorError($this, 'invalid')));
                }
                break;
            case 'pl':
                $fieldValue = trim(str_replace(array(',', ' ', '-'), '', $fieldValue));

                if(!Validate_PL::nip($fieldValue))
                {
                    throw new sfValidatorErrorSchema($this, array($this->getOption('field') => new sfValidatorError($this, 'invalid')));
                }
                break;
        }

        return $values;
    }

}