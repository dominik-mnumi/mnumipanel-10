<?php

class sfWidgetFormSchemaFormatterSpecialList extends sfWidgetFormSchemaFormatter {
    protected
      $rowFormat                 = '<p>%error%%label%%field%%help%%hidden_fields%</p>',
      $helpFormat                = '<span class="help">%help%</span>',
      $errorRowFormat            = '<dt class="error">Errors:</dt><dd>%errors%</dd>',
      $errorListFormatInARow     = '<ul class="message error no-margin">%errors%</ul>',
      $errorRowFormatInARow      = '<li>%error%</li>',
      $namedErrorRowFormatInARow = '<li>%name%: %error%</li>',
      $decoratorFormat           = '<dl id="formContainer">%content%</dl>';
}