<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * FieldsetEditForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */

class FieldsetEditForm extends BaseFieldsetForm
{ 
    public function configure()
    {           
        $this->useFields(array('label'));
    }
}