<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SettingsForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */

abstract class SettingsForm extends FieldItemForm
{
    protected static $measureUnits = array('Per page','Square metre');
    public $errorArray = array();

    public function configure()
    {
        $this->setWidget('field_id', new sfWidgetFormInputHidden());

        $this->setValidator('name', new sfValidatorString(
            array(
                'max_length' => 100,
                'required' => true
            ),
            array(
                'max_length' => 'Field "Name" is too long (%max_length%)',
                'required' => 'Field "Name" is required'
            )
        ));

        $this->setValidator('cost', new sfValidatorNumberExtended(
            array(
                'required' => false
            )
        ));

        // measure unit
        $choiceArray = MeasureUnitTable::getInstance()->getChoicesFiltered(self::$measureUnits);
        $this->setWidget('measure_unit_id', new sfWidgetFormChoice(
            array(
               'choices' => $choiceArray,
            )
        ));

        $this->setValidator('measure_unit_id', new sfValidatorDoctrineChoice(
            array(
                'model' => $this->getRelatedModelName('MeasureUnit'),
                'required' => true
            ),
            array(
                'required' => 'Field "Measure unit" is required'
            )
        ));

        $i18n = sfContext::getInstance()->getI18N();
        /*
         * Depository widgets
         */
        $this->setWidget('availability', new sfWidgetFormInputText(
            array(
               'label' => 'Quantity in stock',
            )
        ));
        $this->setValidator('availability', new sfValidatorNumberExtended(
            array(
                'required' => false,
                'min' => 0,
            ),
            array(
                'min' => $i18n->__('"In stock" must be greater or equal to %min%'),
            )
        ));

        $this->setWidget('availability_alert', new sfWidgetFormInputText(
            array(
               'label' => 'Warn when stock level is lower than',
            )
        ));
        $this->setValidator('availability_alert', new sfValidatorNumberExtended(
            array(
                'required' => false,
                'min' => 0,
            ),
            array(
                'min' => $i18n->__('"Warn when stock level is lower than" must be greater or equal to %min%'),
            )
        ));

        $availableUnits = UnitTable::getInstance()->getChoicesContaining(UnitTable::getUnits());
        $this->setWidget('availability_unit_id', new sfWidgetFormChoice(
            array(
               'choices' => $availableUnits,
               'label' => 'Depository unit',
            )
        ));

        $this->setValidator('availability_unit_id', new sfValidatorDoctrineChoice(
            array(
                'model' => $this->getRelatedModelName('Unit'),
                'required' => false,
            ),
            array()
        ));

        $this->setWidget('stock_number', new sfWidgetFormInputText(
            array(
               'label' => 'Stock Number',
            )
        ));
        $this->setValidator('stock_number', new sfValidatorString(
            array(
                'max_length' => 30,
                'required' => false
            ),
            array(
                'max_length' => $i18n->__('Field "Stock Number" is too long (%max_length%)'),
            )
        ));

        $this->getWidgetSchema()->setLabels(array(
            'name' => 'Name',
            'cost' => 'Cost',
        ));
    }

    /**
     * getFormattedErrors - returns formated errors for form and its embed forms 
    */
    public function getFormattedErrors($errorSchema = null)
    {
        if (null === $errorSchema)
        {
            $errorSchema = $this->getErrorSchema();
        }
        if($errorSchema instanceof sfValidatorErrorSchema)
        {            
            foreach($errorSchema->getErrors() as $e)
            {
                if(!($e instanceof sfValidatorErrorSchema))
                {
                    if(!in_array($e->getMessage(), $this->errorArray))
                    {
                        array_push($this->errorArray, sprintf('%s', $e->getMessage()));
                    }
                }
            }
        }  
        foreach ($errorSchema as $field => $error)
        {     
            if ($error instanceof sfValidatorErrorSchema && $nestedErrorSchema = $error->getErrors())
            {
                return $this->getFormattedErrors($nestedErrorSchema);
            }
            else
            {
                if(!in_array($error->getMessage(), $this->errorArray))
                {
                  array_push($this->errorArray, sprintf('%s', $error->getMessage()));
                }
            }
        }
        return $this->errorArray;
    }

    protected function doSave($con = null)
    {
        parent::doSave($con);

        // gets pricelist array from external
        $this->savePricelist();
    }

    public function savePricelist()
    {
        $priceArray = $this->getOption('pricelistArr');

        if(!$priceArray)
        {
            return;
        }

        $tmpArr = $this->validatePrices($priceArray);

        $pricelistArr = $tmpArr['cleanedValues'];


        // if pricelist array does not exist
        if(empty($pricelistArr))
        {
            return;
        }

        // foreach pricelist
        foreach($pricelistArr as $pricelistKey => $pricelist)
        {
            // prepares min and max price
            $minimalPrice = isset($pricelist['minimal_price']) ? $pricelist['minimal_price'] : null;
            $maximalPrice = isset($pricelist['maximal_price']) ? $pricelist['maximal_price'] : null;
            $plusPrice = isset($pricelist['plus_price']) ? $pricelist['plus_price'] : null;
            $plusDoublePrice = isset($pricelist['plus_price_double']) ? $pricelist['plus_price_double'] : null;
            $cost = isset($pricelist['cost']) ? $pricelist['cost'] : null;

            // gets FieldItemPrice object based on FieldItem and Pricelist
            $fieldItemPriceObj = $this->getObject()->getFieldItemPrice($pricelistKey);

            // if FieldItemPrice does not exist object
            // and minimal or maximal or any price/cost exists then create
            if(!$fieldItemPriceObj && ($minimalPrice || $maximalPrice || $plusPrice || $plusDoublePrice || $cost
                || (!empty($pricelist['type'][FieldItemPriceTypeTable::$price_simplex][0][FieldItemPriceTypeTable::$price_simplex])
                    && !empty($pricelist['type'][FieldItemPriceTypeTable::$price_simplex][0][FieldItemPriceTypeTable::$price_duplex]))
                || !empty($pricelist['type'][FieldItemPriceTypeTable::$price_page][0]['price'])
                || !empty($pricelist['type'][FieldItemPriceTypeTable::$price_copy][0]['price'])
                || !empty($pricelist['type'][FieldItemPriceTypeTable::$price_item][0]['price'])
                || !empty($pricelist['type'][FieldItemPriceTypeTable::$price_linear_metre][0]['price'])
                || !empty($pricelist['type'][FieldItemPriceTypeTable::$price_square_metre][0]['price'])))
            {
                $fieldItemPriceObj = new FieldItemPrice();
                $fieldItemPriceObj->setFieldItem($this->getObject());
                $fieldItemPriceObj->setPricelistId($pricelistKey);
            }

            // if FieldItemPrice does not exist, continue
            if(!$fieldItemPriceObj)
            {
                continue;
            }

            // if delete flag is set
            if(isset($pricelist['delete']) && $pricelist['delete'])
            {
                $fieldItemPriceObj->delete();
                continue;
            }

            $fieldItemPriceObj->setMinimalPrice($minimalPrice);
            $fieldItemPriceObj->setMaximalPrice($maximalPrice);
            $fieldItemPriceObj->setPlusPrice($plusPrice);
            $fieldItemPriceObj->setPlusPriceDouble($plusDoublePrice);
            $fieldItemPriceObj->setCost($cost);
            $fieldItemPriceObj->save();

            // if prices not defined, continue
            if(!isset($pricelist['type']))
            {
                continue;
            }

            $this->doSavePrices($fieldItemPriceObj, $pricelist['type'], $con = null);
        }
    }

    /**
     * Synchronize prices in pricelist
     * Loaded from doSave method
     * (used in child classes)
     *
     * @param FieldItemPrice $fieldItemPriceObj
     * @param array $pricelist
     * @param null $conn
     *
     * @throws Exception
     */
    protected function doSavePrices($fieldItemPriceObj, $pricelist, $conn = null)
    {
        throw new Exception('doSavePrice is not defined in child class.');
    }
    
    public function isValid()
    {
        if(!parent::isValid())
        {
            return false;
        }

        // gets pricelist array from external
        $pricelistArr = $this->getOption('pricelistArr', array());

        // if pricelist array does exist
        if(!empty($pricelistArr))
        {
            // validates input values
            $resultArr = $this->validatePrices($pricelistArr);

            $pricelistArr = $resultArr['values'];
            if(!$pricelistArr)
            {
                return false;
            }
        }

        $this->setOption('pricelistArr', $pricelistArr);

        return true;
    }
    
    /**
     * Validates prices and throwns exceptions.
     * 
     * @param array $values
     * @return array
     * @throws sfValidatorErrorSchema 
     */
    public function validatePrices($values)
    {
        $validatorPrices = new sfValidatorPrices();

        $result = $validatorPrices->clean($values);
        
        foreach($result['errors'] as $rec)
        {
            $this->getErrorSchema()->addError($rec);
        }
        
        if(count($result['errors']))
        {
            return false;
        }
        
        return $result;
    }

    /**
     * get Class name of Field Item Form
     *
     * @param Fieldset $fieldset
     * @return string
     */
    public static function getFieldItemFormClassName(Fieldset $fieldset)
    {
        $fieldset = $fieldset->getRoot();
        $fieldset_key = ucfirst($fieldset->name);

        $class_name = 'Settings' . $fieldset_key . 'Form';

        if (class_exists($class_name))
        {
            return $class_name;
        }

        return 'SettingsForm';
    }
}