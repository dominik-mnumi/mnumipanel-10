<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * SettingsOtherForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */

class SettingsOtherForm extends SettingsForm
{   
    public function configure()
    {
        parent::configure();
 
        $this->useFields(array(
            'name', 'field_id',
            'availability', 'availability_alert', 'availability_unit_id', 'stock_number', // depository
        ));
    }

    /**
     * Synchronize prices in pricelist
     * Loaded from doSave method
     *
     * @param FieldItemPrice $fieldItemPriceObj
     * @param array $pricelist
     * @param null $conn
     */
    protected function doSavePrices($fieldItemPriceObj, $pricelist, $conn = null)
    {
// gets all price types excluding simplex and duplex
        $typeArr = FieldItemPriceTypeTable::$priceTypeWithoutSimplexAndDuplexArr;

        // gets all defined in current pricelist price types
        $formTypeArr = array();
        foreach($pricelist as $key => $rec)
        {
            $fieldItemPriceObj->synchronizePricesOfType($key, $rec);
            $formTypeArr[] = $key;
        }

        // difference beetween all available price types
        // and form price types. For all diff array
        // synchronize with empty array (means delete)
        $diffArr = array_diff($typeArr, $formTypeArr);
        foreach($diffArr as $rec)
        {
            $fieldItemPriceObj->synchronizePricesOfType($rec, array());
        }

    }
}