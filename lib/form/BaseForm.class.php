<?php

/**
 * Base project form.
 * 
 * @package    mnumicore
 * @subpackage form
 * @author     Your name here 
 * @version    SVN: $Id: BaseForm.class.php 20147 2009-07-13 11:46:57Z FabianLange $
 */
class BaseForm extends sfFormSymfony
{

    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null)
    {
        parent::__construct($defaults, $options, $CSRFSecret);

        $this->getWidgetSchema()->setDefaultFormFormatterName('specialList');
    }

    public function addCSRFProtection($secret = null)
    {
        parent::addCSRFProtection($secret);
        if (array_key_exists(self::$CSRFFieldName, $this->getValidatorSchema())) {
            $this->getValidator(self::$CSRFFieldName)->setMessage('csrf_attack', 'This session has expired. Please return to the home page and try again.');
        }
    }
    
    /**
     * Sets messages of current type for all fields.
     * 
     * @param string $type 
     * @param string $message
     * @param array $optionalArray
     */
    public function setMessages($type = 'required', $message = 'Field "%label%" is required', $optionalArray = array())
    {    
        // if optional array is empty
        if(!empty($optionalArray))
        {
            foreach($optionalArray as $rec)
            {               
                $this->getValidator($rec)->setMessage($type, $message);
            }
        }   
        // otherwise all validators
        else
        {
            foreach($this as $key => $rec)
            {               
                $this->getValidator($key)->setMessage($type, $message);
            }
        }
    }
    
    /**
     * Sets css classes for all fields.
     * 
     * @param string $class
     */
    public function setCssClasses($class = null)
    {        
        foreach($this as $key => $rec)
        {               
            $this->getWidget($key)->setAttribute('class', $class);
        }
    }
    
    /**
     * Create folder if is needed and sets permissions
     * 
     * @param string $directory
     */
    
    public function setDir($directory)
    {
        if(!is_dir($directory))
        {
            mkdir($directory);
            chmod($directory, 0777);
        }
    }

}
