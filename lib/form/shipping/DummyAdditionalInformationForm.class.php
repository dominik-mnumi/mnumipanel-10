<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * DummyAdditionalInformation form.
 *
 * @author     Bartosz Dembek
 */
class DummyAdditionalInformationForm extends AdditionalInformationForm
{

    public function configure() {
        parent::configure();

        $this->setWidget('whatever', new sfWidgetFormInput());

        $this->setValidator('whatever', new sfValidatorNumber(
                    array('required' => true)));

        $this->setCssClasses('full-width');

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
    }
}