<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * AdditionalInformationForm form.
 *
 * @author     Bartosz Dembek
 */
class AdditionalInformationForm extends BaseFormDoctrine
{
    /**
     * Configure form
     */
    public function configure()
    {
        $additionalInformation = $this->getObject()->getAdditionalShippingInformation();
        $this->setDefaults($additionalInformation);

        $this->setWidgets(array(
            'id' => new sfWidgetFormInputHidden(),
            
        ));

        $this->setValidators(array(
            'id' => new sfValidatorNumber(
                    array('required' => true))));
    }

    /**
     * Save additional shipping information for OrderPackage
     *
     * @return OrderPackage
     */
    public function save($con = NULL) {

        $values = $this->getValues();
        unset($values['id']);

        $this->getObject()->setAdditionalShippingInformation($values);
        $this->getObject()->save();
        return $this->getObject();
    }

    /**
     * Returns model name
     *
     * @return string
     */
    public function getModelName()
    {
        return 'OrderPackage';
    }

    /**
     * Get form for additional shipping information
     *
     * @param OrderPackage $orderPackage
     * @return AdditionalInformationForm|null
     */
    public static function getAdditionalShippingInformationForm(OrderPackage $orderPackage)
    {
        $form = null;
        $carrierType = $orderPackage->getCarrier()->getType();
        if($carrierType) {
            $formName = $carrierType . 'AdditionalInformationForm';

            if(class_exists($formName)) {
                $form = new $formName($orderPackage);
            }
        }

        return $form;
    }
}