<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ShopOptionsForm
 *
 * @package    mnumishop
 * @subpackage form
 * @author     Bartosz Dembek
 */
class ShopOptionsForm extends BaseFormDoctrine
{    
    public function configure()
    {
        $currenciesCodes = CurrencyTable::getInstance()->getCurrencyCodes();
        $this->setWidget('currency_code', new sfWidgetFormChoice(array('label' => 'Currency', 'choices' => array_combine($currenciesCodes, $currenciesCodes),
            'default' => $this->getOption('currency_code', 'PLN'))));

        $this->setValidator('currency_code', new sfValidatorChoice(array('choices' => $currenciesCodes)));

        $this->widgetSchema->setNameFormat('shop_options[%s]');
    }

    public function getModelName()
    {
        return 'Shop';
    }

    public function save($con = NULL) {

        foreach($this->getValues() as $name => $value) {
            $this->getObject()->setOption($name, $value);
        }

        parent::save($con);
    }

    /**
     * Get form's object option by name
     *
     * @param $name
     * @param null $default
     * @return null|string
     */
    public function getOption($name, $default = null)
    {
        /** @var ShopOption $option */
        foreach ($this->getObject()->getShopOptions() as $option)
        {
            if($option->getName() == $name)
            {
                return $option->getValue();
            }
        }
        return $default;
    }
}