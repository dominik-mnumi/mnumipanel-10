<?php
/*
 * This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * RestClientAddressForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class RestClientDeliveryForm extends BaseClientAddressForm
{
    public function configure()
    {
        parent::configure();
        
        $this->disableCSRFProtection();
        
        $this->useFields(array('fullname', 'postcode', 'city', 'street', 
            'client_id', 'country'));
    }   
}
