<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * APaczkaConfigurationForm form.
 *
 * @author     Bartosz Dembek
 */

use Mnumi\Shipping\ElektronicznyNadawcaBundle\API\Api as ElektronicznyNadawcaAPI;

class ElektronicznyNadawcaConfigurationForm extends BaseFormDoctrine
{
    private static $sizes = array(
        'XS' => 'XS',
        'S' => 'S',
        'M' => 'M',
        'L' => 'L',
        'XL' => 'XL',
        'XXL' => 'XXL'
    );

    private static $packageTypes = array(
        'business' => 'business package',
        'trading' => 'trading package'
    );

    private static $apiEnv = array(
        'prod_environment' => 'production',
        'test_environment' => 'test'
    );

    public function configure()
    {

        $this->disableCSRFProtection();
        $contextObj = sfContext::getInstance();
        $i18N = $contextObj->getI18N();

        $this->defaults = $this->getObject()->getConfiguration();

        $this->setWidgets(array(
                'api_login' => new sfWidgetFormInput(),
                'api_password' => new sfWidgetFormInputPassword(
                    array(),
                    array(
                        'value' => empty($this->defaults['api_password']) ? '' : '0000000000'
                    )
                ),
                'api_environment' => new sfWidgetFormChoice(
                    array(
                        'choices' => self::$apiEnv
                    )
                )
            ));

        $this->setWidget('package_weight', new sfWidgetFormInput());

        $this->widgetSchema->setLabels(array(
                'api_login' => 'User ID',
                'api_password' => 'Password',
                'api_environment' => 'API environment',
                'sender_profile' => 'Sender profile',
                'package_size' => 'Package gauge',
                'package_weight' => 'Default package weight (in grams)',
                'package_type' => 'Package type',
            ));

        $this->setValidators(array(
                'api_login' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 255
                    )
                ),
                'api_password' => new sfValidatorPass(
                    array(
                        'required' => true
                    )
                ),
                'api_environment' => new sfValidatorChoice(
                    array(
                        'choices' => array_keys(self::$apiEnv)
                    )
                )
            ));

        if($this->getAPI()->isLogged()) {

            $this->setWidget('sender_profile', new sfWidgetFormChoice(
                    array(
                        'choices' => $this->getAPI()->getSenderProfiles()
                    )
                ));

            $this->setWidget('send_office', new sfWidgetFormChoice(
                    array(
                        'choices' => $this->getAPI()->getSendOffices()
                    )
                ));

            $this->setWidget('package_size', new sfWidgetFormChoice(
                    array(
                        'choices' => self::$sizes
                    )
                ));

            $this->setWidget('package_type', new sfWidgetFormChoice(
                    array(
                        'choices' => self::$packageTypes
                    )
                ));

            $this->setValidator('send_office', new sfValidatorChoice(
                    array(
                        'choices' => array_keys($this->getAPI()->getSendOffices())
                    )
                ));
            $this->setValidator('sender_profile', new sfValidatorChoice(
                    array(
                        'choices' => array_keys($this->getAPI()->getSenderProfiles())
                    )
                ));

            $this->setValidator('package_size', new sfValidatorChoice(
                    array(
                        'choices' => array_keys(self::$sizes)
                    )
                ));

            $this->setValidator('package_type', new sfValidatorChoice(
                    array(
                        'choices' => array_keys(self::$packageTypes)
                    )
                ));

            $requiredPattern = $i18N->__('Field "%label%" is required');
            $invalidPattern = $i18N->__('Field "%label%" is invalid');
            $label = $i18N->__($this->getWidget('package_weight')->getLabel());

            $this->setValidator('package_weight', new sfValidatorNumber(
                array(),
                array(
                    'required' => $i18N->__($requiredPattern, array(
                        '%label%' => $label
                    )),
                    'invalid' => $i18N->__($invalidPattern, array(
                        '%label%' => $label
                    ))
                )
            ));

            $printLabelConfiguration = new PrintLabelConfigurationForm($this->defaults);
            $this->embedForm('PrintLabelConfiguration', $printLabelConfiguration);
            $this->setValidator('PrintLabelConfiguration', $printLabelConfiguration->getValidatorSchema());
        }

        $this->setCssClasses('full-width');
        $this->setDefaults($this->defaults);
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
    }
    
    /**
     * Saves data into app.yml.
     *
     */
    public function save($con = NULL)
    {
        $values = $this->getValues();
        
        if(isset($values['PrintLabelConfiguration'])) {

            $printLabelConf = $values['PrintLabelConfiguration'];
            unset($values['PrintLabelConfiguration']);
            $values += $printLabelConf;
        }

        if($values['api_password'] == '0000000000') {
            $configuration = $this->getObject()->getConfiguration();
            $values['api_password'] = $configuration['api_password'];
        }

        $this->getObject()->setConfiguration($values);
    }

    public function getModelName()
    {
        return 'Carrier';
    }

    /**
     * Get ElektroniczyNadawca API
     *
     * @return ElektronicznyNadawcaAPI
     */
    public function getAPI()
    {
        if(empty($this->api)) {

            $apiEnvironment = !empty($this->defaults['api_environment'])?
                    $this->defaults['api_environment']:
                    ElektronicznyNadawcaAPI::TEST_ENV;

            $this->api = new ElektronicznyNadawcaAPI(
                    $this->defaults['api_login'],
                    $this->defaults['api_password'],
                    $apiEnvironment);
        }

        return $this->api;
    }
}
