<?php
/*
 * This file is part of the MnumiCore package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * APaczkaConfigurationForm form.
 *
 * @author     Bartosz Dembek
 */
class APaczkaConfigurationForm extends BaseFormDoctrine
{
    private static $dictServiceCode = array(
        'UPS_K_STANDARD' => 'UPS Standard',
        'UPS_K_EX_SAV' => 'UPS Express Saver',
        'UPS_K_EX' => 'UPS Express',
        'UPS_K_EXP_PLUS' => 'UPS Express Plus',
        'DPD_CLASSIC' => 'DPD Classic',
        'DHLSTD' => 'DHL Standard',
        'DHL12' => 'DHL Express 12',
        'DHL09' => 'DHL Express 9',
        'DHL1722' => 'DHL 17-22',
        'UPS_Z_STANDARD' => 'UPS Standard - Zagraniczna',
        'UPS_Z_EX_SAV' => 'UPS Express Saver - Zagraniczna',
        'UPS_Z_EX' => 'UPS Express - Zagraniczna',
        'UPS_Z_EXPEDITED' => 'PS Expedited - Zagraniczna',
        'KEX_EXPRESS' => 'KEX Express',
        'POCZTA_POLSKA' => 'Poczta Polska',
        'SIODEMKA_STD' => 'Siódemka Standard',
    );
    private static $dictShipmentTypeCode = array(
        'LIST' => 'List',
        'PACZ' => 'Paczka',
        'PALETA' => 'Paleta'
    );

    public function configure()
    {

        $this->disableCSRFProtection();
        $contextObj = sfContext::getInstance();

        $this->defaults = $this->getObject()->getConfiguration();

        $this->setWidgets(array(
                'api_login' => new sfWidgetFormInput(),
                'api_password' => new sfWidgetFormInputPassword(
                    array(),
                    array(
                        'value' => empty($this->defaults['api_password']) ? '' : '0000000000'
                    )
                ),
                'api_key' => new sfWidgetFormInput(),
                'api_url' => new sfWidgetFormInput(),
                
                'shipper_name' => new sfWidgetFormInput(),
                'shipper_address_street' => new sfWidgetFormInput(),
                'shipper_address_city' => new sfWidgetFormInput(),
                'shipper_phone' => new sfWidgetFormInput(),
                'shipper_email' => new sfWidgetFormInput(),
                'shipper_address_postal_code' => new sfWidgetFormInput(),
                'shipper_address_country_code' => new sfWidgetFormI18nChoiceCountry(
                        array('culture' => $contextObj->getUser()->getCulture())),
                'shipper_tax_id' => new sfWidgetFormInput(),
                'service_code' => new sfWidgetFormChoice(
                    array(
                        'choices' => self::$dictServiceCode
                    )
                ),
                'shipment_type_code' => new sfWidgetFormChoice(
                    array(
                        'choices' => self::$dictShipmentTypeCode
                    )
                ),

                'package_weight' => new sfWidgetFormInput(),
                'package_dimensions_length' => new sfWidgetFormInput(),
                'package_dimensions_width' => new sfWidgetFormInput(),
                'package_dimensions_height' => new sfWidgetFormInput(),
            ));

        $this->widgetSchema->setLabels(array(
                'api_key' => 'Access key',
                'api_login' => 'User ID',
                'api_password' => 'Password',
                'api_url' => 'API url',

                'shipper_name' => 'Sender name',
                'shipper_tax_id' => 'Taxpayer Identification Number (TIN)',
                'shipper_phone' => 'Sender phone number',
                'shipper_email' => 'Sender email',
                'shipper_address_street' => 'Sender address',
                'shipper_address_city' => 'Sender city',
                'shipper_address_postal_code' => 'Sender postcode',
                'shipper_address_country_code' => 'Sender country',
                'service_code' => 'Service type',
                'shipment_type_code' => 'Package type',
                'package_weight' => 'Default package weight (in kgs)',
                'package_dimensions_length' => 'Default package length (in unit above)',
                'package_dimensions_width' => 'Default package width (in unit above)',
                'package_dimensions_height' => 'Default package height (in unit above)',
            ));

        $this->setValidators(array(
                'api_key' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 255
                    )
                ),
                'api_login' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 255
                    )
                ),
                'api_password' => new sfValidatorPass(
                    array(
                        'required' => true
                    )
                ),
                'api_url' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 255
                    )
                ),
                'shipper_name' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 255
                    )
                ),
                'shipper_phone' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 255
                    )
                ),
                'shipper_tax_id' =>  new sfValidatorString(
                    array(
                        'required' => true
                    )
                ),
                'shipper_email' => new sfValidatorEmail(
                    array(
                        'required' => true
                    )
                ),
                'shipper_address_street' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 255
                    )
                ),
                'shipper_address_city' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 255
                    )
                ),
                'shipper_address_postal_code' => new sfValidatorString(
                    array(
                        'required' => true,
                        'max_length' => 20
                    )
                ),
                'shipper_address_country_code' => new sfValidatorI18nChoiceCountry(
                    array(
                        'required' => true
                    )
                ),
                'service_code' => new sfValidatorChoice(
                    array(
                        'choices' => array_keys(self::$dictServiceCode)
                    )
                ),
                'shipment_type_code' => new sfValidatorChoice(
                    array(
                        'choices' => array_keys(self::$dictShipmentTypeCode)
                    )
                ),
                'package_weight' =>new sfValidatorNumber(
                    array(
                        'required' => false
                    ),
                    array(
                        'invalid' => 'Field "%label%" is invalid'
                    )
                ),
                'package_dimensions_length' => new sfValidatorNumber(
                    array(
                        'required' => false
                    ),
                    array(
                        'invalid' => 'Field "%label%" is invalid'
                    )
                ),
                'package_dimensions_width' => new sfValidatorNumber(
                    array(
                        'required' => false
                    ),
                    array(
                        'invalid' => 'Field "%label%" is invalid'
                    )
                ),
                'package_dimensions_height' => new sfValidatorNumber(
                    array(
                        'required' => false
                    ),
                    array(
                        'invalid' => 'Field "%label%" is invalid'
                    )
                )
            ));

        $this->widgetSchema->setHelps(array(
                'shipper_name' => 'e.g. Sample Company',
            ));

        $this->setCssClasses('full-width');
        $this->setDefaults($this->defaults);
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $printLabelConfiguration = new PrintLabelConfigurationForm($this->defaults);
        $this->embedForm('PrintLabelConfiguration', $printLabelConfiguration);
        $this->setValidator('PrintLabelConfiguration', $printLabelConfiguration->getValidatorSchema());
    }
    
    /**
     * Saves data into app.yml.
     *
     */
    public function save($con = NULL)
    {
        $values = $this->getValues();
        
        if(isset($values['PrintLabelConfiguration'])) {

            $printLabelConf = $values['PrintLabelConfiguration'];
            unset($values['PrintLabelConfiguration']);
            $values += $printLabelConf;
        }

        if($values['api_password'] == '0000000000') {
            $configuration = $this->getObject()->getConfiguration();
            $values['api_password'] = $configuration['api_password'];
        }

        $this->getObject()->setConfiguration($values);
    }

    public function getModelName()
    {
        return 'Carrier';
    }
}
