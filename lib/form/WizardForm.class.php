<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * WizardForm
 *
 * @package    mnumishop
 * @subpackage form
 * @author     Marek Balicki
 */
class WizardForm extends BaseForm
{    
    public function configure()
    {
        $this->setWidget('parameter', new sfWidgetFormInputHidden());
        $this->setWidget('signature', new sfWidgetFormInputHidden());
        
        // validators
        $this->setValidator('parameter', new sfValidatorPass());
        $this->setValidator('signature', new sfValidatorPass());
    }  
}