<?php

/**
 * Client form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class ClientForm extends BaseClientForm
{
    public function configure()
    {
        // these data must be set here (not as external parameter) 
        // because of ajax requests and automatic mechanism of forms 
        $userObj = sfContext::getInstance()->getUser();
        $this->culture = $userObj->getCulture();
        $this->country = $this->isNew()
            ? $userObj->getCountryCode()
            : $this->getObject()->getCountry();

        // sets country because of model default country which overrides widget default
        $this->getObject()->setCountry($this->country);
        
        $this->setWidget('pricelist_id', new sfWidgetFormChoice(
                array('choices' => PricelistTable::getInstance()->asArray()),
                array('class' => 'full-width')));


        $this->getWidget('credit_limit_amount')->setAttribute('class', 'full-width');
        $this->getWidget('credit_limit_overdue')->setAttribute('class', 'full-width');

        $this->setWidget('postcodeAndCity', new sfWidgetFormInputText(
            array('default' => $this->getObject()->get('postcodeAndCity')),
            array('class' => 'full-width')
        ));

        // country widget       
        $this->setWidget('country', new sfWidgetFormI18nChoiceCountry(
                        array('culture' => $this->culture,
                            'label' => 'Country'),
                        array('class' => 'full-width')));           

        $this->getWidget('tax_id')->setAttribute('class', 'full-width');
        $this->getWidget('account')->setAttribute('class', 'full-width');
        
        //validators
        $this->setValidator('credit_limit_amount', new sfValidatorNumberExtended(
                array(),
                array('invalid' => 'Field "Credit limit amount" is not a number',
                      'required' => 'Field "Credit limit amount" is required')));

        // pass this validators, becouse moved to postvalidator
        $this->setValidator('postcodeAndCity', new sfValidatorPass());
        $this->setValidator('tax_id', new sfValidatorPass());
        
        // country validator
        $this->setValidator('country', new sfValidatorI18nChoiceCountry());

        // validators messages
        $this->setMessages('required');

        $this->getValidator('credit_limit_overdue')
                ->setMessage('invalid', 'Field "Overdue limit" is not a integer');

        // labels
        $this->getWidgetSchema()->setLabels(array('fullname' => 'Customer fullname',
                    'last_address_id' => 'Current address',
                    'pricelist_id' => 'Pricelist',
                    'credit_limit_amount' => 'Credit limit',
                    'credit_limit_overdue' => 'Overdue limit',
                    'tax_id' => 'Tax ID',
                    'account' => 'Bank account',
                    'wnt_ue' => 'Intra-Community acquisition of goods'
            )
        );
               
    }   

}
