<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Carrier form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CarrierForm extends BaseCarrierForm
{

    public function configure()
    {
        $this->getWidget('name')->setAttribute('class', 'full-width');
        $this->getWidget('label')->setAttribute('class', 'full-width');
        $this->getWidget('delivery_time')->setAttribute('class', 'full-width');
        
        $this->getWidget('restrict_for_cities')->setAttribute('class', 'full-width');
        $this->getWidget('cost')->setAttribute('class', 'full-width');
        $this->getWidget('price')->setAttribute('class', 'full-width');
        $this->getWidget('free_shipping_amount')->setAttribute('class', 'full-width');

        $taxChoices = sfConfig::get('app_tax_options');

        $this->setWidget('tax', new sfWidgetFormChoice(array('choices' => $taxChoices)));

        $carrierTypes = array_keys(CarrierTable::$types);
        $this->setWidget('type', new sfWidgetFormChoice(array('choices' => array(''=>'') + array_combine($carrierTypes, $carrierTypes))));

        $pricelistChoiceArray = array('__ALL__' => 'Apply on all pricelists') + PricelistTable::getInstance()->asArray();
        $this->setWidget('custom_pricelists', new sfWidgetFormChoice(
                array('multiple' => true,
                    'choices' => $pricelistChoiceArray,
                    'expanded' => true)
        ));

        //sets apply on all pricelist
        if($this->getObject()->getApplyOnAllPricelist())
        {
            $this->getWidget('custom_pricelists')->
                setDefault(array_keys(array('__ALL__' => '') + CarrierTable::getPricelistArray($this->getObject()->getCarrierPricelists())));
        }
        else
        {
            $this->getWidget('custom_pricelists')->
                setDefault(array_keys(CarrierTable::getPricelistArray($this->getObject()->getCarrierPricelists())));
        }

        $this->setValidator('custom_pricelists', new sfValidatorChoice(array('choices' => array_keys($pricelistChoiceArray), 'required' => false, 'multiple' => true)));
        $this->setValidator('name', new sfValidatorString(array('max_length' => 255),
                array('required' => 'Field "Name" is required',
                    'max_length' => 'Field "Name" is too long (%max_length%)'))
        );
        $this->setValidator('delivery_time', new sfValidatorString(
                array('max_length' => 255),
                array('required' => 'Field "Delivery time" is required',
                    'max_length' => 'Field "Delivery time" is too long (%max_length%)'))
        );

        $this->setValidator('cost', new sfValidatorNumberExtended(
            array('required' => false),
            array('invalid' => 'Field "Cost" must be a number')));
        
        $this->setValidator('price', new sfValidatorNumberExtended(
            array('required' => false),
            array('invalid' => 'Field "Price" must be a number')));
        
        $this->setValidator('free_shipping_amount', new sfValidatorNumberExtended(
            array('required' => false),
            array('invalid' => 'Field "Free shipping amount" must be a number')));
        
        $this->getWidgetSchema()->setLabels(array(
            'name' => 'Name',
            'delivery_time' => 'Delivery time',
            'active' => 'Active',
            'apply_on_all_pricelist' => 'Apply on all pricelist',
            'tax' => 'Tax',
            'restrict_for_cities' => 'Restrict for city',
            'require_shipment_data' => 'Require user to set shipment fields',
            'cost' => 'Cost',
            'price' => 'Price',
            'free_shipping_amount' => 'Free shipping amount'
        ));
        
        if(!$this->isNew())
        {
            $this->getWidget('name')->setAttribute('readonly', 'readonly');
        }

        $this->useFields(array(
            'name',
            'type',
            'label',
            'delivery_time',
            'active', 'tax',
            'custom_pricelists',
            'restrict_for_cities',
            'require_shipment_data',
            'cost',
            'price',
            'free_shipping_amount'
            ));

        $configurationForm = $this->getConfigurationForm();
        if($configurationForm) {

            $formName = $configurationForm->getName();
            $this->embedForm($formName, $configurationForm);
            $this->setValidator($formName, $configurationForm->getValidatorSchema());
        }
    }

    public function saveEmbeddedForms($con = null, $forms = null) {

        if($this->getObject()->getType()) {
            foreach($this->getEmbeddedForms() as $form) {
                $form->save();
            }
            parent::saveEmbeddedForms($con, $forms);
        }
    }

    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);
        
        $carrierArrayForm = ($this->getValue('custom_pricelists')) ? $this->getValue('custom_pricelists') : array();
        $object->setApplyOnAllPricelist(in_array('__ALL__', $carrierArrayForm));
        
        return $object;
    }

    /**
     * Returns name of configuration form for shipper connected with carrier
     *
     * @return string
     */
    public function getConfigurationFormName() {

        $configurationFormName = null;
        $obj = $this->getObject();
        if($obj->getType() &&
                in_array($obj->getType(), array_keys(CarrierTable::$types))) {

            $configurationFormName = $obj->getType() . "ConfigurationForm";
        }

        return $configurationFormName;
    }

    /**
     * Returns instance of configuration form for shipper connected with carrier
     *
     * @return BaseFormDoctrine|null
     */
    public function getConfigurationForm() {

        $configurationForm = null;
        $configurationFormName = $this->getConfigurationFormName();

        if($configurationFormName &&
                class_exists($configurationFormName)) {

            $configurationForm = new $configurationFormName($this->getObject());
        }

        return $configurationForm;
    }

    public function doBind(array $values) {

        if($this->getConfigurationFormName() !== null &&
                empty($values['type'])) {

            unset($values[$this->getConfigurationFormName()]);
            unset($this->embeddedForms[$this->getConfigurationFormName()]);
            unset($this->validatorSchema[$this->getConfigurationFormName()]);
        }

        foreach($this->getEmbeddedForms() as $form) {
            $form->bind($values[$form->getName()]);
        }

        parent::doBind($values);
    }
}
