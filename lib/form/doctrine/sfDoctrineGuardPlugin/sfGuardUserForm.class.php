<?php

/**
 * sfGuardUser form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserForm extends PluginsfGuardUserForm 
{
    public function configure() 
    {
        $this->setWidget('password', new sfWidgetFormInputPassword(
                array(),
                array('class' => 'full-width')));
        
        $this->setWidget('password_again', new sfWidgetFormInputPassword(
                array(),
                array('class' => 'full-width')));
    
        $this->setWidget('password_old', new sfWidgetFormInputPassword(
                array(),
                array('class' => 'full-width')));
     
        $this->getWidget('username')->setAttribute('class', 'full-width');
        $this->getWidget('first_name')->setAttribute('class', 'full-width');
        $this->getWidget('last_name')->setAttribute('class', 'full-width');
        $this->getWidget('email_address')->setAttribute('class', 'full-width');

        //validators        
        $this->setValidator('email_address', new sfValidatorEmail(
                array('required' => true),
                array('invalid' => 'Field "Email address" is invalid')));
        
        $this->setValidator('password', new sfValidatorString(
                array('required' => true)));
        
        $this->setValidator('password_again', new sfValidatorString(
                array('required' => true)));
    
        $this->setValidator('password_old', new sfValidatorString(
                array('required' => true)));
        
        $this->getValidator('username')->setOption('required', true);        
        $this->getValidator('first_name')->setOption('required', true);       
        $this->getValidator('last_name')->setOption('required', true);
        
        // validators messages
        $this->setMessages('required');
        
        // labels
        $this->getWidgetSchema()->setLabels(array(
            'username' => 'Username',
            'first_name' => 'First name',
            'last_name' => 'Surname',
            'email_address' => 'Email address',
            'password' => 'Password',
            'password_again' => 'Confirm password',
            'password_old' => 'Current password'
        ));           
    }
    
    public function save($con = null)
    {
        $obj = parent::save($con);

        return $obj;
    }

}
