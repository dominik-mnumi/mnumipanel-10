<?php

/**
 * ClientUser form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ClientUserForm extends BaseClientUserForm
{

    public function configure()
    {

        $this->getWidget('client_user_permission_id')->setAttribute('class', 'full-width');
        $this->getWidget('client_user_permission_id')->setLabel('Customer permission');

        // text field for user autocomplete
        $this->setWidget('user', new sfWidgetFormInputText(
                        array('label' => 'User'),
                        array('class' => 'full-width')));
               
        // text field for client autocomplete
        $this->setWidget('client', new sfWidgetFormInputText(
                        array('label' => 'Customer (enter name or NIP)'),
                        array('class' => 'full-width')));
        
        $this->setWidget('user_id', new sfWidgetFormInputHidden());
        $this->setWidget('client_id', new sfWidgetFormInputHidden());
    
        // validators
        $this->setValidator('user', new sfValidatorString(
                        array('required' => false)));    
   
        $this->setValidator('client', new sfValidatorString(
                        array('required' => false)));
        
        // post validators
        $this->validatorSchema->getPostValidator()->setMessage('invalid', 
                sprintf('Field "%s" and "%s" must be unique', 'User', 'Customer'));
       
        // validators messages
        $this->setMessages('required');

        // sets static message because no label in hidden (label is nessesary in setMessages())
        $i18N = sfContext::getInstance()->getI18N();
        $this->getValidator('user_id')->setMessage('required', 
                $i18N->__('Field "%label%" is required. Choose from available user list', array('%label%' => $i18N->__('User'))));
    
        $this->getValidator('client_id')->setMessage('required', 
                $i18N->__('Field "%label%" is required. Choose from available customer list', array('%label%' => $i18N->__('Customer'))));
    
        
        $this->useFields(array('client_user_permission_id', 'user_id', 'client_id', 'user', 'client'));
    }

}
