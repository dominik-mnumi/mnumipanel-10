<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Category form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CategoryForm extends BaseCategoryForm
{
    public function configure()
    {
        $this->getWidget('name')->setLabel('Category name');
        $this->getValidator('name')->setMessage('required',
                'Category name is required');

        //photo
        $this->setWidget('photo',
                new sfWidgetFormInputFileEditable(
                        array('file_src' => $this->getObject()->getImage(190),
                            'is_image' => true,
                            'edit_mode' => $this->getObject()->getPhoto() ,
                            'delete_label' => 'Delete image')));
        $this->setValidator('photo',
                new sfValidatorFile(array(
                    'required' => false,
                    'mime_types' => 'web_images'
                )));
        $this->setValidator('photo_delete', new sfValidatorBoolean());
        $this->getWidget('photo')->setLabel('Photo');
    }

    protected function doSave($con = null)
    {
        $file = $this->getValue('photo');
        if($file)
        {
            // gets absolute path to dir
            $fileDir = CategoryTable::getDefaultImagePath(true);
            
            // if dir does not exist - create it
            if(!is_dir($fileDir))
            {
                mkdir($fileDir);
                chmod($fileDir, 0777);
            }
            
            $filename = sha1($file->getOriginalName().microtime().rand());
            $extension = $file->getExtension();
            $file->save($fileDir.$filename.$extension);           
        }

        //save object
        parent::doSave($con);
    }
    
    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);
        $object->setPhoto(str_replace(CategoryTable::getDefaultImagePath(true), '',
                        $object->getPhoto()));
        return $object;
    }
}
