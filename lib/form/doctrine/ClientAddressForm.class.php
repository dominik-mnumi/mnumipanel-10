<?php

/**
 * ClientAddress form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ClientAddressForm extends BaseClientAddressForm
{
    public function configure()
    {          
        // these data must be set here (not as external parameter) 
        // because of ajax requests and automatic mechanism of forms 
        $userObj = sfContext::getInstance()->getUser();
        $this->culture = $userObj->getCulture();
        $this->country = $this->isNew()
            ? $userObj->getCountryCode()
            : $this->getObject()->getCountry();
        
        $postcode = $this->getObject()->getPostcode();
        $city = $this->getObject()->getCity();
        $postcodeAndCityDefault = $postcode.' '.$city;
        $postcodeAndCityDefault = trim($postcodeAndCityDefault);
        
        $this->setWidget('postcodeAndCity', new sfWidgetFormInputText(
                array('default' => $postcodeAndCityDefault), 
                array('class' => 'full-width')));
                
        $this->getWidget('fullname')->setAttribute('class', 'full-width');
        $this->getWidget('street')->setAttribute('class', 'full-width');
        
        // country widget       
        $this->setWidget('country', new sfWidgetFormI18nChoiceCountry(
                        array('culture' => $this->culture,
                            'label' => 'Country'),
                        array('class' => 'full-width')));     
        
        //validators and messages
        $this->setValidator('postcodeAndCity', new sfValidatorPass());
   
        // country validator
        $this->setValidator('country', new sfValidatorI18nChoiceCountry());
        
        // validators messages
        $this->setMessages('required');
        
        // sets post validators
        $this->validatorSchema->setPostValidator(new sfValidatorCallback(
                array('callback' => array($this, 'postcodeAndCity'))));
        
        // labels
        $this->getWidgetSchema()->setLabels(
                array('fullname' => 'Delivery fullname',
                    'postcodeAndCity' => 'Postcode and city',
                    'street' => 'Street',
                    'client_id' => 'Client'
                    ));
        
        $this->useFields(array('fullname', 'postcodeAndCity', 'street',
            'country'));
    }
    
    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);      
        $clientId = sfContext::getInstance()->getUser()->getAttribute('id', null, 'client');
        
        if(!$clientId)
        {
            throw new Exception('Invalid client id. Client id should be on session in client namespace.');
        }

        if($this->isNew())
        {
            $object->setClientId($clientId);
        }

        $object->setPostcodeAndCity($this->getValue('postcodeAndCity'));
   
        return $object;
    }

    /**
     * Validates postcode and city depending on country.
     * 
     * @param array $validator
     * @param array $values
     */
    public function postcodeAndCity($validator, $values)
    {
        // gets error schema
        $errorSchema = new sfValidatorErrorSchema($validator);

        // prepares validator
        $postcodeAndValidatorClass = 'sfValidatorPostcodeAndCity'.$values['country'];
        
        // checks if class exists (if not then sets default class)
        if(!class_exists($postcodeAndValidatorClass))
        {
            $postcodeAndValidatorClass = 'sfValidatorPostcodeAndCityGB';
        }
        
        // sets validator
        $this->setValidator('postcodeAndCity', new $postcodeAndValidatorClass(
                        array('max_length' => 150,
                            'required' => true)));

        // do validator clean
        try
        {
            $values['postcodeAndCity'] =  $this->validatorSchema['postcodeAndCity']
                    ->clean($values['postcodeAndCity']);
        }
        catch(sfValidatorError $e)
        {
            $errorSchema->addError($e, 'postcodeAndCity');
        }

        // if some errors
        if($errorSchema->count())
        {
            throw $errorSchema;   
        }
        
        return $values;
    }
}
