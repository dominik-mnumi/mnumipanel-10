<?php

/**
 * Coupon form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CouponForm extends BaseCouponForm
{
    public function configure()
    {
        $this->setWidget('name', new sfWidgetFormInput());
        $this->setCssClasses('full-width');
        $this->setWidget('expire_at', new sfWidgetFormInput(array(), array('class' => 'datepicker')));
        $this->setDefault('one_time_usage', false);
        $this->setDefault('used', false);
        
        $this->setValidator('name', new sfValidatorRegex(
            array('pattern' => '/^[a-z0-9\-\_]+$/', 'required' => true),
            array('invalid' => 'Field "name" can contain only lowercase letters, numbers, "-" and "_" (no special characters)')));
        $this->setValidator('value', new sfValidatorNumber(array('required' => true), array('required' => 'Required', 'invalid' => 'Field "Value" must be a number')));
        $this->setValidator('expire_at',new sfValidatorDate(array('required' => true)));
        
        // pricelist widget configuration
        $pricelistChoiceArray = array('__ALL__' => 'Apply on all pricelists') + PricelistTable::getInstance()->asArray();
        $this->setWidget('custom_pricelists', new sfWidgetFormChoice(
            array(
                'choices' => $pricelistChoiceArray,
                'expanded' => true,
                'multiple' => true)
        ));
        
        if($this->getObject()->getApplyForAllPricelist())
        {
            $this->getWidget('custom_pricelists')->setDefault(array_keys(array('__ALL__' => '') + CouponTable::getPricelistArray($this->getObject()->getCouponPricelists())));
        }
        else
        {
            $this->getWidget('custom_pricelists')->setDefault(array_keys(CouponTable::getPricelistArray($this->getObject()->getCouponPricelists())));
        }
        
        $this->setValidator('custom_pricelists', new sfValidatorChoice(array('choices' => array_keys($pricelistChoiceArray), 'required' => false, 'multiple' => true)));
        
        // product widget configuration
        $productChoiceArray = array('__ALL__' => 'Apply for all products') + ProductTable::getInstance()->asArray();
        $this->setWidget('custom_products', new sfWidgetFormChoice(
            array(
                'choices' => $productChoiceArray,
                'expanded' => true,
                'multiple' => true)
        ));
        
        if($this->getObject()->getApplyForAllProducts())
        {
            $this->getWidget('custom_products')->setDefault(array_keys(array('__ALL__' => '') + CouponTable::getProductArray($this->getObject()->getCouponProducts())));
        }
        else
        {
            $this->getWidget('custom_products')->setDefault(array_keys(CouponTable::getProductArray($this->getObject()->getCouponProducts())));
        }
        
        $this->setValidator('custom_products', new sfValidatorChoice(array('choices' => array_keys($productChoiceArray), 'required' => false, 'multiple' => true)));
        
        
        $this->widgetSchema->setNameFormat(get_class($this).'[%s]');
        
        $this->widgetSchema->setLabels(array(
            'custom_products'         => 'Products',
            'custom_pricelists' => 'Pricelists',
            'expire_at'        => 'Expiry date',
            'one_time_usage'   => 'One time usage',
        ));
        
        // post validator
        $this->validatorSchema->setPostValidator(new sfValidatorCallback(array('callback' => array($this, 'validPrimaryKey'))));
    
        $this->useFields(array(
            'name',
            'type',
            'value',
            'used',
            'custom_products',
            'custom_pricelists',
            'expire_at',
            'one_time_usage'
        ));
    }
    
    /**
     * Checks if primary key is unique
     *
     * @param array $validator
     * @param array $values
     * @return array
     */
    public function validPrimaryKey($validator, $values)
    {
        $coupon = CouponTable::getInstance()->find($values['name']);
        
        // if there is no coupon with this same name
        if(!$coupon)
        {
            return $values;
        }
        
        // if there is coupon and this is same coupon that is edited now
        if($coupon && !$this->isNew() && ($this->getObject()->getName() == $coupon->getName()))
        {
            return $values;
        }
        
        $error = new sfValidatorError($validator, 'Coupon name already exists in database');
        throw new sfValidatorErrorSchema($validator, array('name' => $error));
        
    }
    
    /**
     * Updates object
     * 
     * @param array $values
     */
    public function updateObject($values = null)
    {
        $object = parent::updateObject($values);
        
        $couponArrayPricelistForm = ($this->getValue('custom_pricelists')) ? $this->getValue('custom_pricelists') : array();
        $object->setApplyForAllPricelist(in_array('__ALL__', $couponArrayPricelistForm));
          
        $couponArrayProductForm = ($this->getValue('custom_products')) ? $this->getValue('custom_products') : array();
        $object->setApplyForAllProducts(in_array('__ALL__', $couponArrayProductForm));
      
        return $object;
    }
}
