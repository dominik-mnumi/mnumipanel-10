<?php

/**
 * ProductWizard form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductWizardForm extends BaseProductWizardForm
{
    public function configure()
    {
        $this->setWidget('sort_order', new sfWidgetFormInputHidden(
                array(),
                array('class' => 'wizard-order')));
        
        $this->setWidget('wizard_name', new sfWidgetFormInputHidden(array(), array('class' => 'wizard-name-input')));
        $this->setWidget('width', new sfWidgetFormInputHidden());
        $this->setWidget('height', new sfWidgetFormInputHidden());
        $this->setWidget('product_id', new sfWidgetFormInputHidden());   
        $this->setWidget('valid', new sfWidgetFormInputHidden(array(), array('class' => 'wizard-valid-input')));
        $this->setWidget('active', new sfWidgetFormInputCheckbox());       
             
        // validators
        $this->getValidator('sort_order')->setOption('required', false);
        $this->getValidator('wizard_name')->setOption('required', false);
        $this->getValidator('width')->setOption('required', false);
        $this->getValidator('height')->setOption('required', false);
        $this->getValidator('product_id')->setOption('required', false);
        $this->setValidator('valid', new sfValidatorPass());
        $this->setValidator('active', new sfValidatorBoolean());
    }   
}
