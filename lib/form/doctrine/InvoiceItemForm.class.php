<?php

/**
 * InvoiceItem form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class InvoiceItemForm extends BaseInvoiceItemForm
{
    public function configure()
    {
        // widgets
        $this->setWidget('valid', new sfWidgetFormInputHidden(
                array('default' => 0), 
                array('class' => 'invoice_item_valid')));
        $this->setWidget('order_id', new sfWidgetFormInputHidden(
                array('default' => null)));
        $this->setWidget('unit_of_measure',
            new sfWidgetFormDoctrineChoiceI18N(
                array(
                    'model' => $this->getRelatedModelName('Unit'),
                    'add_empty' => false)));
        
        $this->getWidget('description')->setAttribute('class', 'full-width item_description');
        $this->getWidget('unit_of_measure')->setAttribute('class', 'full-width item_unit_of_measure');
        $this->getWidget('quantity')->setAttribute('class', 'full-width item_quantity');
        $this->getWidget('price_net')->setAttribute('class', 'full-width item_price_net');
        $this->getWidget('price_discount')->setAttribute('class', 'full-width item_price_discount');
        $this->getWidget('tax_value')->setAttribute('class', 'full-width item_tax');
        
        // validators
        $this->setValidator('valid', new sfValidatorPass());
        $this->setValidator('quantity', new sfValidatorNumberExtended());
        $this->setValidator('price_net', new sfValidatorNumberExtended());
        $this->setValidator('price_discount', new sfValidatorNumberExtended(array('required' => false)));
       
        // messages
        $this->setMessages();

        $invoiceItem = $this->getObject();

        //disable fields for invoice with correction
        if(!$this->isNew())
        {
            if($invoiceItem->getInvoice()->hasChildren())
            {
                foreach ($this->getWidgetSchema()->getFields() as $field)
                {
                  $field->setAttribute('disabled', 'disabled');
                }
              
            }
        }

        /*
         * In InvoiceItem::loadDataFrom[Order/Trader]() we get correct tax value for this
         * invoice item. Therefore:
         *
         *      $defaultTax = round(sfConfig::get('app_price_tax', 0.13) * 100));
         *
         * is not needed anymore.
         */
        $defaultTax = $invoiceItem->getTaxValue();

        $this->setDefault('tax_value', $defaultTax);
        $this->useFields(array('description', 'unit_of_measure', 'quantity',
            'price_net', 'price_discount', 'tax_value',
            'valid',
            'order_id'));
    }

}
