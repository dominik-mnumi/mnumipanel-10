<?php

/**
 * Pricelist form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PricelistForm extends BasePricelistForm
{
  public function configure()
  {
      $this->getWidget('name')->setAttribute('class', 'full-width');
      $this->getWidget('name')->setLabel('Pricelist name');
      
      unset($this['changeable']);
  }
}
