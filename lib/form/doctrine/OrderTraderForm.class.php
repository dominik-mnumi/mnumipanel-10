<?php

/**
 * OrderTrader form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class OrderTraderForm extends BaseOrderTraderForm
{
    public function configure()
    {
        $this->disableCSRFProtection();
        
        $this->useFields(array('quantity', 'price', 'order_id', 'user_id', 'description'));
    }

}
