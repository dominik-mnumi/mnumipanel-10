<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PerPageForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Adam Marchewicz
 */
class PerPageForm extends BaseForm
{
    protected static $selectOptions = array(10 => '10',20 => '20',50 => '50',100 => '100');
    
    public function configure()
    {
        $this->setWidgets(array(
            'options'        => new sfWidgetFormSelect(array('choices' => self::$selectOptions))
        ));
        $this->widgetSchema->setNameFormat('perPage[%s]');
     
        $this->setValidators(array(
            'options'       => new sfValidatorChoice(array('choices' => array_keys(self::$selectOptions)))
        ));
    }
}