<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas(); ?>
        <?php include_metas(); ?>
        <?php include_javascripts(); ?>
        <link rel="shortcut icon" type="image/x-icon"  href="/images/icons/favicon.ico" />

        <title><?php echo __('Mnumi system - Your browser version is not supported'); ?></title>
        <link href="/css/oldBrowser.css" rel="stylesheet" type="text/css">
    </head>
    <body class="dark">
        <?php echo $sf_content; ?>
    </body>
</html>
