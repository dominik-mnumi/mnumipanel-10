<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <?php include_title() ?>
        <link rel="shortcut icon" type="image/x-icon"  href="/images/icons/favicon.ico" />
        <?php include_stylesheets() ?>
        <?php include_javascripts() ?>

        <?php if(has_slot('actionJavascript')): ?>
        <?php include_slot('actionJavascript') ?>
        <?php endif; ?>
    </head>
    <body>
    <?php if($sf_user->isAuthenticated()): ?>
    <script type="text/javascript">
        var barcodePrefix = '<?php echo sfConfig::get('app_barcode_prefix', '^') ?>';
        var barcodeSufix = '<?php echo sfConfig::get('app_barcode_sufix', '#') ?>';
        var barcodeSize = <?php echo sfConfig::get('app_barcode_size', '8') ?>;
        var barcodeRead = '<?php echo url_for('@barcodeRead') ?>';

        var devenv =  <?php echo (sfConfig::get('sf_environment') == 'dev') ? 'true' : 'false'; ?>;

        /**
         * Pings to server.
         */
        function ping()
        {
            $.ajax(
            {
                type: 'GET',
                url: '<?php echo url_for('ping'); ?>',
                error: function(xhr, ajaxOptions, thrownError)
                {
                    alert('<?php echo __('Failed attempt to connect to the server. Please check your internet connection.'); ?>');
                }
            });
        }

        setInterval(ping, 5 * 60000);
    </script>
    <script type="text/javascript" src="/js/barcode-listener.js"></script>

    <?php if(substr($sf_user->getCulture(), 0, 2) != 'en'): ?>
        <script type="text/javascript" src="/js/jquery-datepicker-<?php echo substr($sf_user->getCulture(), 0, 2) ?>.js"></script>
    <?php endif; ?>
    
    <?php endif ?>
        <!--[if lt IE 9]><div class="ie"><![endif]-->
        <!--[if lt IE 8]><div class="ie7"><![endif]-->

        <!-- Server status -->
        <header>
            <div class="container_12">
                <p id="skin-name"><small>Mnumisystem<br /> Admin PANEL</small> <strong title="<?php echo ProjectConfiguration::getRepositoryVersion(); ?>"><?php echo ProjectConfiguration::getVersion(); ?></strong></p>
            </div>
        </header>
        <!-- End server status -->
        <!-- Main nav -->
        <nav id="main-nav">
            <?php echo include_component('dashboard', 'mainNavi'); ?>
        </nav>
        <!-- End main nav -->

        <!-- Sub nav -->
        <div id="sub-nav">
            <div class="container_12">
                &nbsp;
                <?php echo include_component('search', 'form'); ?>
            </div>
        </div>
        <!-- End sub nav -->

        <?php echo include_component('dashboard', 'statusBar'); ?>

        <div id="sf_content">
            <?php echo $sf_content ?>
        </div>

        <!--[if lt IE 8]></div><![endif]-->
        <!--[if lt IE 9]></div><![endif]-->
        <footer>
            <div class="float-right">
                <a class="button" href="#top">
                    <?php echo image_tag('icons/fugue/navigation-090.png', array('style' => 'width: 16px; height: 16px;')) ?> <?php echo __('Page top') ?>
                </a>
            </div>
        </footer>
    </body>
</html>
