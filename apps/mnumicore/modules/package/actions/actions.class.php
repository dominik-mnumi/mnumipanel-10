<?php

/**
 * package actions.
 *
 * @package    mnumicore
 * @subpackage package
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class packageActions extends tablesActions
{

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    { 
        // view objects
        $this->client = $this->getRoute()->getObject();
        $this->filtered = $request->getParameter('filtered', 1);
        $this->packages = $this->client->getFilteredOrderPackagesWithExclusion($this->filtered);
        $this->defaultDisableAdvancedAccountancy = sfConfig::get('app_default_disable_advanced_accountancy', 0);
    }

    /**
     * Details of client packages. eg url: /client/d+/packages/d+
     *
     * @param sfWebRequest $request 
     */
    public function executeShow(sfWebRequest $request)
    {
        $packageObj = $this->getRoute()->getObject();
        $packageObj->calculatePrice($packageObj->getOrdersDefaultQuery());
        $filtered = $request->getParameter('filtered', 1);

        // prepares form for delivery and payment modal
        $formArray[0] = new EditPackageForm($packageObj);
        $formArray[0]->setDefault('delivery_country', sfConfig::get('app_default_country'));
        $formArray[1] = new SendPackageForm($packageObj); 
        $formArray['defaultPaymentId'] = PaymentTable::getInstance()->getDefaultOfficePayment()->getId();
        
        $this->setTemplate('index');
        
        // view objects
        $this->shipping = null;
        try {
            $this->shipping = $packageObj->getCarrier()->getShipping();
        }
        catch(\Symfony\Component\Config\Definition\Exception\InvalidConfigurationException $e) {
            $this->getUser()->setFlash('info_data', array(
                'messageType' => 'msg_error',
                'message' => $e->getMessage()
            ));
        }
        $this->shippingAdditionalInformationForm = AdditionalInformationForm::getAdditionalShippingInformationForm($packageObj);

        $this->client = $packageObj->getClient();
        $this->packageInDetails = $packageObj;
        $this->personalCollection = $packageObj->requiresPersonalCollection();
        $this->packages = $packageObj->getClient()->getFilteredOrderPackagesWithExclusion($filtered, $packageObj);
        $this->formArray = $formArray;    
        $this->filtered = $filtered;
        $this->defaultPaymentId = PaymentTable::getInstance()->getDefaultOfficePayment()->getId();
        $this->defaultDisableAdvancedAccountancy = sfConfig::get('app_default_disable_advanced_accountancy', 0);
    }

    /**
     * Saves form through ajax and return json response.
     * 
     * @param sfWebRequest $request
     * @return json 
     */
    public function executeReqEditPackageForm(sfWebRequest $request)
    {
        $requestArray = $request->getParameter('EditPackageForm');

        $packageId = isset($requestArray['id']) ? $requestArray['id'] : 0;

        $package = OrderPackageTable::getInstance()->find($packageId);
        $this->forward404Unless($package instanceof OrderPackage);

        $form = new EditPackageForm($package);

        $form = $this->processAjaxForm($form,
            $request->getParameter($form->getName()), $request->getFiles($form->getName())
        );

        $id = null;

        $errorArray = array();
        if(!$form->isValid())
        {
            foreach($form as $field)
            {
                if($field->hasError())
                {
                    $sfI18N = $this->getContext()->getI18N();
                    $fieldName = $sfI18N->__($field->renderLabelName());
                    $fieldMessage = $field->getError()->__toString();

                    $errorArray[] = $fieldName . ': ' . $sfI18N->__(
                            $fieldMessage, array('%label%' => $sfI18N->__($field->renderLabelName()))
                    );
                }
            }

            $this->getUser()->setFlash('info_data', array(
                'messageType' => 'msg_success',
                'message' => 'Saved successfully.',
                'messageParam' => array()));

        }
        else
        {
            $obj = $form->save();
            $id = $obj->getId();
        }

        //prepares returnArray
        $returnArray['result']['status'] = 'success';
        $returnArray['result']['id'] = $id;
        $returnArray['result']['error']['errorArray'] = $errorArray;
        $returnArray['result']['error']['errorCount'] = count($errorArray);

        return $this->renderText(json_encode($returnArray));

    }
    
    /**
     * Show shelf list
     * 
     * @param sfWebRequest $request
     */
     public function executeShelfList(sfWebRequest $request)
     {
        $this->displayTableFields = array(
            'ShelfId' => array('label' => 'Shelf number', 'partial' => 'shelf'),
            'Orders' => array('label' => 'Orders', 'partial' => 'orders'),
        );

        $this->modelObject = 'OrderPackage';
        $this->actions = array();
        $this->customQuery = OrderPackageTable::getInstance()->createQuery('op')
                        ->leftJoin('op.Orders o ON o.shelf_id = op.shelf_id')
                        ->where('op.shelf_id IS NOT NULL')
                        ->orderBy('op.shelf_id');

        $this->availableViews = array('table');

        $this->tableOptions = $this->executeTable();

        if ($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }         

     } 
     
     /**
     * Execute toBook action.
     *
     * @param sfRequest $request A request object
     */
    public function executeToBook(sfWebRequest $request)
    {
        $packageObj = $this->getRoute()->getObject();
        
        $packageObj->setToBook(true);
        $packageObj->save();
        
        if(!$request->isXmlHttpRequest())
        {
            $this->redirect('clientPackageDetails', $packageObj);
        }
        return sfView::NONE;
    }
     
     /**
      * Executes list action
      *
      * @param sfWebRequest $request
      */
     public function executeList(sfWebRequest $request)
     {
         $this->displayTableFields = array(
             'Package' => array('label' => 'Package', 'partial' => 'package/list/package'),
             'Client' => array('label' => 'Client', 'partial' => 'package/list/client'),
             'OrderPackageStatusName' => array('label' => 'Status', 'partial' => 'package/list/status'),
             'Payment' => array('label' => 'Payment', 'partial' => 'package/list/payment'),
             'Delivery' => array('label' => 'Delivery address', 'partial' => 'package/list/delivery'),
         );
         $this->displayGridFields = array(
             'Client' => array('destination' => 'name', 'label' => 'Client'),
             'OrderPackageStatusName' => array('destination' => 'keywords', 'label' => 'Status')
         );
         $this->sortableFields = array('order_package_status_name');
         $this->filterForm = new OrderPackageFormFilter();
         $this->modelObject = 'OrderPackage';
         
         $this->customQuery = OrderPackageTable::getInstance()
                 ->getNotEmptyPackagesListQuery();
         $this->availableViews = array('table');

         $this->filterForm->setQuery($this->customQuery);
         
         $this->actions = array(
            'show' => array('label' => 'Show',
                        'route' => 'client_package_details',
                        'icon' => '/images/icons/fugue/magnifier.png',
                        'attributes' => array('title' => 'Show')),
                        
         );
         
         $this->tableOptions = $this->executeTable();
         
         if($request->isXmlHttpRequest())
         {
             return $this->renderPartial('global/tableHtml', $this->tableOptions);
         }
     }
     
     /**
      * Executes form process for send package
      *
      * @param sfRequest $request A request object
      */
     public function executeSendPackageForm(sfWebRequest $request)
     {
         $returnArray = parent::executeRequestAjaxProcessForm($request, true);

         if($returnArray['result']['error']['errorCount'] == 0) {

             // triggers the event
             $this->dispatcher->notify(new sfEvent($this,
                     'package.posted',
                     array('obj' => $this->ajaxForm->getObject())));

         }

         return $this->renderText(json_encode($returnArray));
     }

     /**
      * Executes form process for print label and send package with additional shipping data
      *
      * @param sfWebRequest $request
      */
     public function executePrintLabelAndSendPackageAdditional(sfWebRequest $request)
     {
         $returnArray = parent::executeRequestAjaxProcessForm($request, true);

         if($returnArray['result']['error']['errorCount'] == 0) {

            $packageObj = $this->ajaxForm->getObject();

            try {

               $this->printLabelAndSendPackage($packageObj);

            } catch (Exception $e) {
                $returnArray['result']['status'] = 'error';
                $returnArray['result']['error']['errorCount'] = 1;
                $returnArray['result']['error']['errorArray'] = array($e->getMessage());
            }
         }

         return $this->renderText(json_encode($returnArray));
     }

     /**
      * Executes cancel package shipping
      *
      * @param sfWebRequest $request
      */
     public function executeCancelPackageShipping(sfWebRequest $request)
     {
        $packageObj = $this->getRoute()->getObject();
        $packageObj->setSendAt(NULL);
        $packageObj->setTransportNumber(NULL);
        $packageObj->setOrderPackageStatusName(OrderPackageStatusTable::$completed);
        $packageObj->save();

         return sfView::NONE;
     }

     /**
      * Executes form process for print label and send package
      *
      * @param sfWebRequest $request
      */
     public function executePrintLabelAndSendPackage(sfWebRequest $request)
     {
        $packageObj = $this->getRoute()->getObject();

        try {

           $this->printLabelAndSendPackage($packageObj);
           $returnArray['result']['status'] = 'success';

        } catch (Exception $e) {
            $returnArray = array();
            $returnArray['result']['status'] = 'error';
            $returnArray['result']['error']['errorCount'] = 1;
            $returnArray['result']['error']['errorArray'] = array($e->getMessage());
        }


         return $this->renderText(json_encode($returnArray));
     }

     /**
      * Print order package's label and send package
      *
      * @param OrderPackage $orderPackage
      */
     protected function printLabelAndSendPackage(OrderPackage $orderPackage)
     {
        PrintlabelQueueTable::getInstance()
               ->saveDefaultPrintlabelQueueWithAttributes($orderPackage);

        $this->dispatcher->notify(new sfEvent($this,
                     'package.posted',
                     array('obj' => $orderPackage)));
     }

     /**
      * Execute payWithLoyaltyPoints action.
      *
      * @param sfWebRequest $request A request object
      */
     public function executePayWithLoyaltyPoints(sfWebRequest $request)
     {
         $packageObj = $this->getRoute()->getObject();

         if($packageObj->isPaidWithLoyaltyPoints())
         {
             $packageObj->unpayLoyaltyPoints();
            $packageObj->setPaymentStatusName(PaymentStatusTable::$waiting);
            $packageObj->save();
         }
         else
         {
             $loyaltyPoint = $packageObj->payWithLoyaltyPoints();
             $amountPaidByLoyaltyPoints = $loyaltyPoint->getPointsValue();
             $amountToPay = $packageObj->getSummaryTotalAmount();

            /**
             * Allow setting OrderPackage::PaymentStatusName as paid, only if
             * *entire* package can be paid by LoyaltyPoints.
             * Loyalty points can not be fully pay delivery price. So if there
             * is one set, user have to pay for it and we shouldn't set package
             * as paid.
             */
            if ($amountPaidByLoyaltyPoints >= $amountToPay) {
                $packageObj->setPaymentStatusName(PaymentStatusTable::$paid);
                $packageObj->save();
            }
        }

         $this->redirect('clientPackageDetails', $packageObj);
     }

    /**
     * Execute pay package action.
     *
     * @param sfWebRequest $request A request object
     * @throws Exception
     */
    public function executePay(sfWebRequest $request)
    {
        $packageObj = $this->getRoute()->getObject();

        if(!$packageObj->canPay())
        {
            throw new Exception('This package cannot be paid.');
        }

        $price = $packageObj->getRealPrice();
        $userObj = $this->getUser()->getGuardUser();
        $description = 'auto generated';

        if($packageObj->isPaid())
        {
            $packageObj->unpay($price, $userObj, $description);
        }
        else
        {
            $packageObj->pay($price, $userObj, $description);
        }

        $this->redirect('clientPackageDetails', $packageObj);
    }
}
