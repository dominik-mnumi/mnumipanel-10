<div class="task with-legend">
    <div class="legend package-header">
        <?php echo link_to(order_package_name($package), url_for('clientPackageDetails', $package), "class=order-package-title"); ?>
    </div>

    <ul class="floating-tags">

        <?php if($package->getDeliveryAt()): ?>
            <li class="tag-delivery-at">
                <?php echo __('Delivery date'); ?>:
                <?php echo format_date($package->getDeliveryAt(), 'i'); ?>
            </li>
        <?php endif; ?>
        <li class="tag-time"><?php echo order_package_name($package); ?></li>
        <li class="tag-info"><?php echo __($package->getOrderPackageStatusName()); ?></li>

        <?php if($package->getInvoice()): ?>
        <li class="tag-info"><?php echo link_to($package->getInvoice(), 
                'clientInvoiceEdit', $package->getInvoice()); ?>
        </li>
        <?php endif; ?>
        
        <!-- payment status -->
        <?php include_partial('package/bookAndPaymentStatus', 
                array('packageObj' => $package)); ?>
  
        <!-- if package sent -->
        <?php if($package->isSent() && $personalCollection): ?>
        <li class="tag-info">
            <div>
                <?php echo __('Personally collected'); ?>
            </div>
            <?php if($package->getTransportNumber()): ?>
                <div>
                    <?php echo __('Collection\'s information'); ?>:
                </div>
                <div>
                    <?php echo $package->getTransportNumber(); ?>
                </div>
            <?php endif; ?>
        </li>
        <?php elseif($package->isSent()): ?>
        <li class="tag-info height-40px">
            <div>
                <?php echo __('Package sent'); ?>
            </div>
            <div>
                <?php echo __('Transport number'); ?>: 
            </div>
            <div>
                <?php echo $package->getTransportNumber(); ?>
            </div>
        </li>
        <?php endif; ?>
    </ul>
 
    <div class="order-package-content">
        <?php if(!$package->isPaid() && !$package->isEmpty()): ?>
            <?php if($package->getsfGuardUser()->getAvailablePointsCount() > 0 && !$package->isPaidWithLoyaltyPoints()): ?>
            <br/>
            <div class="order-package-button">
                <a href="<?php echo url_for('packagePayWithLoyaltyPoints', $package) ?>">
                    <button type="button">
                    <?php echo __('Pay with loyalty points'); ?>
                    </button>
                </a>
            </div>
            <?php endif; ?>

            <?php if($package->isPaidWithLoyaltyPoints()): ?>
            <br/>
            <div class="order-package-button">
                <a href="<?php echo url_for('packagePayWithLoyaltyPoints', $package) ?>">
                    <button type="button">
                        <?php echo __('Unpay loyalty points'); ?>
                    </button>
                </a>
            </div>
            <?php endif; ?>
        <?php endif; ?>

        <!-- order pay -->
        <?php if($defaultDisableAdvancedAccountancy && !$package->isEmpty()): ?>
        <br />
        <div class="order-package-button">
            <?php include_partial('package/payButton', array('package' => $package)); ?>
        </div>
        <?php endif; ?>

        <br/>
        <?php if($package->isSent() === false && !$package->isEmpty()): ?>
        <div class="order-package-button">
            <button id="dialog-edit-package-click" type="button">
                <?php echo __('Delivery and Payment') ?>
            </button>   
        </div>
        <?php endif; ?>
        <?php if($package->getInvoice() == null
                && !$package->isEmpty()
                && !$defaultDisableAdvancedAccountancy): ?>
        <div class="order-package-button">
            <form action="<?php echo url_for('clientInvoiceAdd', $package->getClient()); ?>" method="post">
                <input type="hidden" name="packageId[]" value="<?php echo $package->getId(); ?>" />
                <input type="hidden" name="type" value="preliminary invoice" />
                <button id="generate-preliminary-invoice" type="submit">
                    <?php echo __('Create preliminary invoice') ?>
                </button>
            </form>
        </div>
        <?php endif; ?>

        <!-- if not already booked and advanced accountancy isn't disabled then show button -->
        <?php if(!$package->getToBook()
                && !$package->isEmpty()
                && !$defaultDisableAdvancedAccountancy): ?>
        <div class="order-package-button">
            <?php include_partial('package/invoicingButton', array('package' => $package)); ?>
        </div>
        <?php endif; ?>

        <?php if(!$package->isSent()): ?>

            <?php if(!$package->isEmpty()): ?>

                <?php if($shipping): ?>

                    <div class="order-package-button">
                        <button id="dialog-send-package-click" type="button"
                                class="<?php if(!$package->canSend()){ echo 'disabled'; } ?>">
                            <?php echo __('Print label and send package'); ?>
                        </button>
                    </div>

                    <?php if($shippingAdditionalInformationForm): ?>
                    <div class="hidden">
                        <?php include_partial('package/additionalInformationForm', array(
                            'form' => $shippingAdditionalInformationForm,
                            'action' => url_for('printLabelAndSendPackageAdditional',
                                    array('model' => 'OrderPackage',
                                        'type' => 'edit')))); ?>
                    </div>
                    <?php endif; ?>

                <?php else: ?>

                    <div class="order-package-button">
                        <button id="dialog-send-package-click" type="button"
                                class="<?php if(!$package->canSend()){ echo 'disabled'; } ?>">

                            <?php echo $personalCollection ?
                                    __('Personally collected'):
                                    __('Print label and send package'); ?>

                        </button>
                    </div>

                    <?php include_partial('sendForm', array(
                        'form' => $formArray[1],
                        'personalCollection' => $personalCollection));?>

                <?php endif; ?>

            <?php endif; ?>
        
        <?php elseif($personalCollection): ?>
            <div class="package-block">
                <h3><?php echo __('Personally collected'); ?></h3>
                <?php echo $package->getSendAt(); ?>
                <?php if($package->getTransportNumber()): ?>
                <h4><?php echo __('Collection\'s information'); ?>:</h4><?php echo $package->getTransportNumber(); ?>
                <?php endif; ?>
            </div>
        <?php else: ?>
        <div class="package-block">
            <h3><?php echo __('Send on'); ?></h3>
            <?php echo order_package_name($package); ?>
            <h4><?php echo __('Transport number'); ?>: <?php echo $package->getTransportNumber(); ?></h4>
        </div>
        <?php endif; ?>

        <?php if($package->isSent() && !$personalCollection): ?>
            <div class="order-package-button">
                <button id="cancel-shipping" type="button" class="red">
                    <?php echo __('Cancel shipping') ?>
                </button>
            </div>
        <?php endif; ?>

        <div class="package-block">
            <?php if($package->hasCarrier()): ?>
                <h3><?php echo __('Delivery address') ?></h3>
                <span class='package-big-span'><?php echo $package->getCarrier(); ?></span>
                <?php if($package->getCarrier()->getRequireShipmentData()): ?><?php echo $package->getFullDeliveryAddress(); ?><?php endif; ?>
            <?php endif; ?>
        </div>
        <?php if($package->getDescription()): ?>
        <div class="package-block">
                <h3><?php echo __('Additional informations') ?></h3>
                <span class='package-big-span'><?php echo $package->getDescription(); ?></span>
        </div>
        <?php endif; ?>
        <?php if($package->getHangerNumber() != "") : ?>
            <div class="package-block">
                <?php echo __('Numbered hanger tag') ?>:
                <b><?php echo $package->getHangerNumber() ?></b>
            </div>
        <?php endif ?>

        <div class="package-block">
            <h3><?php echo __('Payment') ?></h3>
            <span class='package-big-span'><?php echo $package->getPayment()->getLabel(); ?></span>
            <?php echo ($package->isPaid()) ? __('paid') : __('not paid'); ?>
        </div>

        <div class="package-block">
            <h3><?php echo __('Invoice') ?></h3>
            <span class='package-big-span'><?php echo $package->getWantInvoice() ? __('Yes') : __('No'); ?></span>
            <?php echo $package->getFullInvoiceAddress(); ?>
        </div>

        <?php if($package->getCouponName() != ""): ?>
            <?php $couponValue = $package->getCoupon()->getValue() . (($package->getCoupon()->getType() == CouponTable::$amount) ? ' ' . $sf_user->getCurrency() : "%"); ?>
        <div class="package-block">
            <h3><?php echo __('Coupon') ?></h3>
            "<?php echo $package->getCoupon(); ?>" - <?php echo $couponValue; ?>
        </div>
        <?php endif ?>
        
        <!-- if package is booked -->
        <?php if($package->getToBook()): ?>
        <div class="package-block">
            <h3><?php echo __('Package has been moved to invoicing'); ?></h3>        
        </div>
        <?php endif; ?>
        
        <div class="package-block no-margin">
            <table class="table" id="extender_order_list" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th scope="col" style="width: 15%;">
                            <?php echo __('Preview'); ?>
                        </th>
                        <th scope="col" class="sorting" style="width: 40%;">
                            <?php echo __('Number') ?>, <?php echo __('Name') ?>
                        </th>
                        <th scope="col" class="sorting" style="width: 10%;">
                            <?php echo __('Status') ?>
                        </th>
                        <th scope="col" style="width: 10%;">
                            <?php echo __('Shelf') ?>
                        </th>
                        <th scope="col" class="sorting" style="width: 10%;">
                            <?php echo __('Price') ?>
                        </th>
                        <th scope="col" class="table-actions" style="width: 5%; ">
                            <?php echo __('Actions'); ?>
                        </th>
                    </tr>
                </thead>
                <?php $extraClass = ($package->isPaid() || $package->isCompleted()) ? '' : 'draggable '; ?>

                <tbody>
                    <?php foreach($package->getNotDeletedOrders() as $order): ?>
                    <tr class="odd <?php echo $extraClass; ?><?php echo $order->getRowClassStatus(); ?>" orderid="<?php echo $order->getId(); ?>">
                        <td class="align-center">
                            <?php if($order->getPhotoWithPath()): ?>                            
                            <img alt="" src="<?php echo $order->getPhotoWithPath(); ?>" />
                            <?php else: ?>  
                                -
                            <?php endif ?>
                        </td>                      
                        <td class="package-name">
                        	<a href="<?php echo url_for('orderListEdit', array('id' => $order->getId())); ?>" title="<?php echo __('Edit order'); ?>" class="with-tip">
                        	<?php echo $order->getId() ?>.
                        	<?php echo $order->getName() ?>
                        	</a>
                        </td>
                        <td><?php echo __($order->getOrderStatusName()); ?></td>
                        <td class="align-center"><?php echo $order->getShelfId(); ?></td>
                        <td>
                            <?php if($order->getPriceNet() <> $order->getTotalAmount()): ?>
                                <span title="<?php echo $sf_user->formatCurrency($order->getPriceNet()); ?> - <?php echo $couponValue; ?> = <?php echo $sf_user->formatCurrency($order->getTotalAmount()); ?>">
                                    <?php echo $sf_user->formatCurrency($order->getTotalAmount()); ?>
                                    <del><?php echo $sf_user->formatCurrency($order->getPriceNet()); ?></del>
                                </span>
                            <?php else: ?>
                                <?php echo $sf_user->formatCurrency($order->getPriceNet()); ?>
                            <?php endif; ?>
                        </td>
                        <td class="table-actions">
                            <?php if($order->canMoveToShelf()): ?>
                            <a href="<?php echo url_for('orderToShelf', $order); ?>" title="<?php echo __('Move to shelf'); ?>" class="with-tip">
                                <img src="/images/icons/finefiles/32/_Favorites.png" width="16" height="16">
                            </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <div class="message no-margin">
                <?php if($package->isCompleted()): ?>
                    <?php echo __('You can not move orders, when package is completed.'); ?>
                <?php elseif($package->isPaid()): ?>
                    <?php echo __('You can not move orders, when package is set as paid.'); ?>
                <?php elseif($package->hasUnmovableOrders()): ?>
                    <?php echo __('You can not move orders, when package is set as: ') . __($package->getOrderPackageStatusName()); ?>
                <?php else: ?>
                    <?php echo __('To move the order to another package, drag and drop over another package.'); ?>
                <?php endif; ?>
            </div>
        </div>
        <table class="package-summary">
        <tr>
            <td>
                <?php echo __(
                    ($package->getOrderPackageStatusName() == OrderPackageStatusTable::$basket)
                        ? 'Cart value'
                        : 'Orders value'
                ); ?>:
            </td>
            <td class="amount">
                <?php if(($package->getOrderBaseAmount() + $package->getTraderTotalAmount()) <> $package->getOrderTotalAmount()): ?>
                    <?php
                        $titleDescription = "";

                        if($couponValue)
                            $titleDescription .= "(";

                        $titleDescription .= $sf_user->formatCurrency($package->getOrderBaseAmount());

                        if($couponValue)
                            $titleDescription .= ' - ' . $couponValue . ')';


                    if($package->getTraderTotalAmount() > 0)
                            $titleDescription .= ' + ' . $sf_user->formatCurrency($package->getTraderTotalAmount());

                        $titleDescription .= ' = ' . $sf_user->formatCurrency($package->getOrderTotalAmount());
                    ?>
                    <span title="<?php echo $titleDescription; ?>">
                        <?php echo $sf_user->formatCurrency($package->getOrderTotalAmount()) ?>
                        <del><?php echo $sf_user->formatCurrency($package->getOrderBaseAmount()) ?></del>
                    </span>
                <?php else: ?>
                    <?php echo $sf_user->formatCurrency($package->getOrderTotalAmount()) ?>
                <?php endif; ?>
                <br/>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo __('Delivery and payment'); ?>:
            </td>
            <td class="amount">
                <?php echo $sf_user->formatCurrency($package->getDeliveryPrice() + $package->getPaymentPrice()) ?><br/>
            </td>
        </tr>
        <?php if($package->getUsedLoyaltyPoints()): ?>
        <tr>
            <td>
                <?php echo __('Paid with loyalty points') ?>:
            </td>
            <td class="amount">
                <?php foreach($package->getUsedLoyaltyPoints() as $loyaltyPoints): ?>
                    <?php echo link_to($loyaltyPoints->getsfGuardUser(), 'userListEdit', $package->getSfGuardUser()) ?>
                    (<?php echo $loyaltyPoints->getPoints() ?> <?php echo __('pts.'); ?>): -<?php echo $sf_user->formatCurrency($loyaltyPoints->getPointsValue()) ?><br/>
                <?php endforeach ?>
            </td>
        </tr>
        <?php endif ?>
        <tr class="summary">
            <td>
                <?php echo __('To pay') ?>:
            </td>
            <td class="amount">
                <?php echo $sf_user->formatCurrency($package->getSummaryTotalAmount()) ?><br/>
                <strong>
                    <?php echo $sf_user->formatCurrency($package->getSummaryTotalGrossAmount()) ?> <?php echo __('incl. VAT') ?>
                </strong>
            </td>
        </tr>
        </table>
    </div>
</div>
<?php include_partial('package/form/editPackageForm', 
        array(
            'form' => $formArray[0],
            'defaultPaymentId' => $defaultPaymentId,
            'personalCollection' => $personalCollection,
            'shipping' => $shipping,
            'shippingAdditionalInformationForm' => $shippingAdditionalInformationForm)) ?>

<div id="print-label-queue" style="display: none;">
    <?php include_partial('packagingInterface/printlabelQueue'); ?>
</div>

<?php include_partial('global/alertModalConfirm'); ?>

<script type="text/javascript">
var printAgain = '<?php echo url_for('@barcodePrintAgain') ?>';
var printlabelQueue = '<?php echo url_for('@barcodePrintlabels') ?>';
var packageId = <?php echo $package->getId(); ?>;
var intervalID = null;
var lastQueue;
function showPrintLabelModalQueue(printlabels)
{
    $('.modal-content #allPrintlabels tbody').html('');
    $.each(printlabels, function() 
    {
        $('.modal-content #allPrintlabels tbody').append('<tr><td>' + this.id + '</td><td>' + this.printer_name+
                    '</td><td>' + this.status + '</td></tr>');
    });
}
function printLabelQueue()
{
    $.ajax(
    {
        url: printlabelQueue,
        type: 'POST',
        dataType: "json",
        success: function(printlabels)
        {
            // puts data to table
            showPrintlabelQueue(printlabels);
            if(printlabels.length > 0) {
                lastQueue = printlabels[printlabels.length - 1].id;
                console.log("Last queue: " +lastQueue);
            }
            if(!$('.modal-content').is(':visible'))
            {
                // shows modal window
                $.modal(
                    {
                        content: $("#print-label-queue"),
                        title: '<?php echo __('Point of Print Workflow'); ?>',
                        width: 700,
                        height: 430,
                        resizable: false,
                        onOpen: function () {
                            $('.modal-content #allPrintlabels').show();
                            $('.modal-content #printlabelTitle').show();
                        },
                        onClose: function () {
                            clearInterval(intervalID);
                            window.location.reload();
                        },
                        buttons: {
                            '<?php echo __('OK'); ?>': function (win) {
                                win.closeModal();
                            }
                        }
                    });
            }
            hideButtonLoader();
        },
        error: function(request, status, error)
        {
            alert('<?php echo __("Problem with generating UPS label. Please contact to Mnumi Support"); ?>');
        }
    });
}

function printLabel()
{
    printLabelQueue();
    intervalID = setInterval(function()
    {
        printLabelQueue();
    }, 4000);
}
</script>
