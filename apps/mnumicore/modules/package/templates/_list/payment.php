<ul class="keywords">
    <?php if($row->isPaid()): ?>
        <li class="green-keyword"><?php echo __('paid'); ?></li>
    <?php else: ?>
        <li class="purple-keyword"><?php echo __('not paid'); ?></li>
    <?php endif; ?>
</ul>
<p>
    <?php echo $row->getPayment(); ?>
</p>
