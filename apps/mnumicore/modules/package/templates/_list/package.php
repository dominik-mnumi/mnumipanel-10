<?php use_helper('OrderPackage'); ?>

<?php echo link_to(order_package_name($row), url_for('client_package_details', $row)); ?><br />

<?php if($row->getNotDeletedOrders()->count()): ?>
<?php foreach($row->getNotDeletedOrders() as $rec): ?>
<?php $orderHrefArr[] = link_to($rec->getId(), 'orderListEdit', $rec); ?>
<?php endforeach; ?>
(<?php echo __('orders'); ?>: <?php echo implode(', ', array_slice($orderHrefArr, 0, 3)); ?><?php if(count($orderHrefArr) > 3) echo ', +'.(count($orderHrefArr)-3); ?>)
<?php endif; ?>

<?php if($row->getHangerNumber() != "") : ?>
    <p>
        <?php echo __('Numbered hanger tag') ?>:
        <?php echo $row->getHangerNumber() ?>
    </p>
<?php endif; ?>
