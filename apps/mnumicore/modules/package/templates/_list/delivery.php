<ul class="keywords">
    <?php if($row->getSendAt()): ?>
        <li class="green-keyword"><?php echo __('Sent') . ' / ' . __('Personally collected'); ?></li>
    <?php else: ?>
        <?php if($row->requiresPersonalCollection()): ?>
            <li class="purple-keyword"><?php echo __('not collected'); ?></li>
        <?php else: ?>
            <li class="purple-keyword"><?php echo __('not sent'); ?></li>
        <?php endif; ?>
    <?php endif; ?>
</ul>
<?php if($row->getCarrierId()): ?>
<p>
    <strong><?php echo $row->getCarrier(); ?></strong>
</p>
<?php endif; ?>
<p>
    <?php echo $row->getDeliveryPostcode() ?> <?php echo $row->getDeliveryCity() ?> <?php echo $row->getDeliveryStreet() ?>
</p>
