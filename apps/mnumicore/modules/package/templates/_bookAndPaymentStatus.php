<!-- if to_book set -->
<?php if($packageObj->getToBook()): ?>       
<!-- if package is booked then show payment status -->
<?php if($packageObj->isBooked()): ?>
<li class="tag-info"><?php echo __('Payment status'); ?>: <?php echo __($packageObj->getPaymentStatus()->getName()); ?></li>       
<?php else: ?>
<li class="tag-info"><?php echo __('To book'); ?></li>       
<?php endif; ?>
<?php endif; ?>