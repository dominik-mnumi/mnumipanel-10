<form id="additional-information-form" action="<?php echo $action ?>" method="post">

    <?php echo $form->renderHiddenFields() ?>

    <div id="modal-title" title="<?php echo __('Additional information for shipping'); ?>">
        <p class="validateTips"></p>

        <div class="block-border">
            <div class="block-content form no_border">
                <?php foreach($form as $name => $field): ?>

                    <?php if(!$field->isHidden()): ?>
                        <p class="required">
                        <?php echo $field->renderLabel(); ?>
                        <?php echo $field->render(); ?>
                        <p>
                    <?php endif; ?>

                <?php endforeach; ?>
            </div>
        </div>
    </div>
</form>