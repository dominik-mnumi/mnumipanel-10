<?php echo include_partial('dashboard/message') ?>

<form id="new_field_category_form" method = "post" class="form" action="<?php echo url_for('@reportCloseBook') ?>">
<?php if(count($tableOptions['pager']->getResults()) > 0): ?>
<div class="grey-bg clearfix" id="control-bar" style="opacity: 1;"><div class="container_12">
	<div class="float-right"> 
		<button type="subbmit"> <?php echo __('Close book')?></button>
	</div>
</div></div>
<?php endif ?>

<article class="container_12">
  <section class="grid_12">
    <div class="block-border"><div class="block-content form">
        <h1>
            <?php echo __('Delivery') ?>
        </h1><br/>
        <?php echo $carrierReportTypeForm['type']->render() ?><br/><br/>
        
        <?php if(count($tableOptions['pager']->getResults()) > 0): ?>
            <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
        <?php endif ?>
        <br/><br/><br/><br/>
        <div id="despatches"></div>
    </div>
    </div>
  </section>
</article>

<script type="text/javascript">
$(document).ready(function() {
	function loadDespatches()
	{
		$.ajax({
            type: 'POST',
            url: '<?php echo url_for('@reportLoadDespatches') ?>',
            data: { name: $('#reportType').val() },
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert('<?php echo __('There was error. Please try again later.') ?>');
            },
            success: function(result)
            {
                $('#despatches').html(result);
            }
        });
	}
	
    $('#reportType').change(function() {
    	loadDespatches();
    });
    
    loadDespatches();
});
</script>
</form>