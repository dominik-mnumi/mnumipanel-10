<form method="post" 
      class="form accounting-report"
      action="<?php echo url_for('accountingReportExport'); ?>">
    <article class="container_12">
        <div class="block-border">
            <div class="block-content">
                <h1>
                    <?php echo __('Report for accounting'); ?>             
                </h1>

                <div class="float-left">
                    <?php echo $accountingReportForm['from_date']->renderLabel(); ?>
                    <span class="input-type-text">
                        <?php echo $accountingReportForm['from_date']->render(); ?>
                    </span>
                </div>
                <div class="float-left">
                    <?php echo $accountingReportForm['to_date']->renderLabel(); ?>
                    <span class="input-type-text">
                        <?php echo $accountingReportForm['to_date']->render(); ?>
                    </span>
                </div>
                
                <div class="clear height-25px"></div>
                <div>
                    <?php echo $accountingReportForm['range']->renderLabel(); ?>
                    <?php echo $accountingReportForm['range']->render(); ?>
                </div>
                
                <div class="text-right">
                    <button><?php echo __('Generate'); ?></button>
                </div>
            </div>
        </div>
    </article>
</form>