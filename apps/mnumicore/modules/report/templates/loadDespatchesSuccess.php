<table class="despatch-table table sortable">
<thead>
<tr>
    <th class="date"><?php echo __('Year and month') ?></th>
    <th class="books"><?php echo __('Address books') ?></th>
</tr>
</thead>
<?php foreach($despatches as $key => $despatch): ?>
<tr>
    <td class="dates"><?php echo $key ?></td>
    <td>
    <?php $i = 1; $count = count($despatch) ?>
    <?php foreach($despatch as $row): ?>
        <?php echo link_to('[ R ]', '@settingsReportCarrierView?id='.$row['id'], array('target' => '__blank')) ?> 
        <?php echo date('d', strtotime($row['created_at'])) .' '. date('H:i', strtotime($row['created_at']))  ?>
        <?php if($i != $count) echo ', '; ?>
        <?php $i++ ?>
    <?php endforeach ?>
    </td>
</tr>
<?php endforeach ?>
</table>