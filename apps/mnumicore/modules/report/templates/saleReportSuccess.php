<article class="container_12">
    <section class="grid_12">
        <div class="block-border search-div">
            <form method="post" 
                  class="block-content form search-filter-form"
                  action="<?php echo url_for('saleReportPdf'); ?>"
                  target="_blank">
                <h1>
                    <?php echo __('Sale report'); ?>             
                </h1>

                <p>
                    <?php echo $form['range']->renderLabel(); ?>
                    <?php echo $form['range']->render(); ?>
                </p>

                <p id="date-range">
                    <?php echo $form['date_range']->renderLabel(); ?>
                    <?php echo $form['date_range']->render(); ?>
                </p>

                <?php if(isset($form['currency_id'])): ?>
                <p>
                    <?php echo $form['currency_id']->renderLabel(); ?>
                    <?php echo $form['currency_id']->render(); ?>
                </p>
                <?php endif; ?>

                <?php echo $form['_csrf_token']->render(); ?>

                <div class="text-right">
                    <button><?php echo __('Generate'); ?></button>
                </div>
            </form>
        </div>
    </section>
</article>
<script type="text/javascript">
    $(function() {

        $('form #invoice_filters_range').change(function() {

            if($('form #invoice_filters_range').val() == 'custom') {
                $('form #date-range').show();
            } else {
                $('form #date-range').hide();
            }
        });

        $('form #date-range').hide();
    });
</script>