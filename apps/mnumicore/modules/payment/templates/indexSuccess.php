<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_12">
        <div class="block-border"><form class="block-content form" id="table_form" method="post" action="<?php echo url_for('@settingsChangeFieldsetsPerPage') ?>">
                <h1>
                    <?php echo __('Group'); ?>: <?php echo __('Payment'); ?>
                    <a href="<?php echo url_for('@settingsPaymentAdd') ?>">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add pricelist') ?>"> <?php echo __('add'); ?>
                    </a>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </form>
        </div>
    </section>
</article>
