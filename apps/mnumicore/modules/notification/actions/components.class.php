<?php

/**
 * notification components.
 *
 * @package    mnumicore
 * @subpackage notification
 */
class notificationComponents extends sfComponents
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeNavigation(sfWebRequest $request)
    {
        $this->languages = sfConfig::get('app_cultures_enabled');
        $this->default = MnumiI18n::getCulture();
    }

    /**
     * Executes generateOrderFormSteps action
     *
     * @param sfWebRequest $request A request object
     */
    public function executeShortcodeInfo(sfWebRequest $request)
    {
        // global tags
        $notification = new Notifications($this->getContext());
        $this->globalTags = $notification->getGlobalShortcodes();

        // local tags
        $this->templates = NotificationTemplateTable::getInstance()->findAll();
    }

}
