<?php

/**
 * pricelist actions.
 *
 * @package    mnumicore
 * @subpackage pricelist
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pricelistActions extends tablesActions
{

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->displayTableFields = array('Name' => array());
        $this->displayGridFields = array(
            'Name'  => array('destination' => 'name'),
        );
        $this->sortableFields = array('name');
        $this->modelObject = 'Pricelist';
        
        $this->tableOptions = $this->executeTable();
        
        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }
    
    public function executeCreate(sfWebRequest $request)
    {
        $this->form = new PricelistForm();
        $this->proceedForm($this->form);
        $this->setTemplate('edit');
    }
    
    public function executeEdit(sfWebRequest $request)
    {
        $this->forward404Unless($this->getRoute()->getObject()->canEdit());

        $this->form = new PricelistForm($this->getRoute()->getObject());
        $this->proceedForm($this->form);
    }
    
    public function executeDelete(sfWebRequest $request)
    {
        try
        {
            $this->getRoute()->getObject()->delete();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Deleted successfully.',
                'messageType' => 'msg_success',
            ));
        }
        catch(Doctrine_Exception $e)
        {
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Deleting failed. Pricelist in use',
                'messageType' => 'msg_error',
            ));
        }
        
        $this->redirect('@settingsPricelist');
    }
    
    private function proceedForm($form)
    {
        if(!$this->getRequest()->isMethod('post'))
        {
            return;
        }

        $this->form->bind($this->getRequest()->getParameter($this->form->getName()));

        if($this->form->isValid())
        {
            $this->form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            $this->redirect('@settingsPricelistEdit?id='.$this->form->getObject()->id);
        }
    }
    
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Pricelist';
        parent::executeDeleteMany($request);
    }

}
