<?php echo include_partial('dashboard/message'); ?>

<article class="container_12">
    <section class="grid_4">
        <?php echo include_component('settings', 'navigation', array('id' => null)); ?>
    </section>
    <section class="grid_8">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('Group'); ?>: <?php echo __('Pricelist'); ?>
                    <a href="<?php echo url_for('@settingsPricelistAdd') ?>">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add pricelist') ?>"> <?php echo __('add'); ?>
                    </a>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section> 
</article>
