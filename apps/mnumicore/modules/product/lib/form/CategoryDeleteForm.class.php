<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Add category form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CategoryDeleteForm extends BaseCategoryForm
{

    public function configure()
    {
        $this->setValidator('id', new sfValidatorDoctrineChoice(
                array('model' => 'Category',
                    'column' => 'id',
                    'required' => true)));

        //post validator - checks if category can be deleted
        $this->validatorSchema->setPostValidator(new sfValidatorCallback(array(
                'callback' => array($this, 'canDelete'))));

        $this->useFields(array('id'));
    }

    public function save($con = null)
    {
        //deletes category
        //gets category
        $category = CategoryTable::getInstance()->find($this->getValue('id'));

        $category->getNode()->delete();
    }

    public function canDelete($validator, $values)
    {
        //gets category
        $category = CategoryTable::getInstance()->find($values['id']);

        if(!$category->canDelete())
        {
            $error = new sfValidatorError($validator, 'Error');
            throw new sfValidatorErrorSchema($validator, array('type' => $error));
        }
        return $values;
    }
}


