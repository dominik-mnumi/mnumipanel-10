<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductFixpriceFormColl form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFixpriceCollForm extends sfForm
{
    public function configure()
    {
        //gets records for current collection
        $productFixpriceColl = $this->getOption('productFixpriceColl');

        //gets pricelist obj
        $pricelistObj = $this->getOption('pricelistObj');
        
        if(!$pricelistObj instanceof Pricelist)
        {
            throw new Exception('You must define pricelistObj and it must be instance of Pricelist');
        }

        //if entries exist
        $key = 0;
        if(count($productFixpriceColl) > 0)
        {
            //gets existed forms
            foreach($productFixpriceColl as $key => $rec)
            {
                $fixpriceForm = new ProductFixpriceForm($rec);
                $this->embedForm($key, $fixpriceForm);
            }
            $key++;
        }

        
        $stopKey = $key + 20;
        for($key; $key < $stopKey; $key++)
        {
            //empty form
            $fixpriceForm = new ProductFixpriceForm();
            $fixpriceForm->getWidget('pricelist_id')->setAttribute('value', $pricelistObj->getId());
            $this->embedForm($key, $fixpriceForm);
        }
    }

    
}

