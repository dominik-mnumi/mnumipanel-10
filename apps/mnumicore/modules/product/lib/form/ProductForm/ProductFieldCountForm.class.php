<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Description of ProductCountForm
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class ProductFieldCountForm extends ProductFieldSimpleForm
{
    public function configure()
    {
        parent::configure(); 

        $countTypeChoices = array(
            '' => '- not set -',
            'even' => 'even',
            'odd' => 'odd'
        );

        $this->setWidget('countType', new sfWidgetFormChoice(array(
            'label' => 'Page count validation',
            'default' => $this->getObject()->getCountType(),
            'choices' => $countTypeChoices)));

        $this->setValidator('countType', new sfValidatorChoice(array('required' => false, 'choices' => array_keys($countTypeChoices))));

        $this->useFields(array('visible', 'required', 'change_label_checkbox', 'label', 'default_value', 'min', 'max', 'countType'));
    }
}

