<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductField form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFieldOtherForm extends ProductFieldAdvancedForm
{
    public function configure()
    {
        parent::configure();
        
        $fieldItemArr = $this->getFieldItemArr();
        $this->setWidget('valid', new sfWidgetFormInputHidden(array(), array('class' => 'valid_hidden_field')));
        $this->setValidator('valid', new sfValidatorPass());

        $this->setWidget('default_value', new sfWidgetFormChoice(array(
                'choices' => ($this->getObject()->isVisible())?$this->getObject()->getDefaultValuesChoiceArrayFixed():$fieldItemArr,
            )));

        // in other tabs is conditional validator
        $this->getValidator('product_field_item')->setOption('required', false);

        $this->useFields(array('visible', 'label',
                'required', 'product_field_item', 'default_value'));
    }

    public function checkAvailableValues($validator, $values)
    {
        if($values['valid'] && !$values['product_field_item'])
        {
            $error['product_field_item'] = new sfValidatorError($validator, $this->getFieldset()->getLabel().' - "Available values" are required.');
            throw new sfValidatorErrorSchema($validator, $error);
        }
        return $values;
    }

    /**
     * If flag 'required' was set, it's not allowed to select empty --- field
     *
     * @param sfValidatorCallback $validator
     * @param array $values
     * @return array
     * @throws sfValidatorErrorSchema
     */
    public function checkRequired($validator, $values)
    {
        $emptyId =  FieldItemTable::getInstance()->getEmptyField()->getId();
        if($values['required'] && is_array($values['product_field_item']) && in_array($emptyId, $values['product_field_item'])){
            $error['product_field_item'] = new sfValidatorError($validator, $this->getFieldset()->getLabel().' - "Available values" are required.');
            throw new sfValidatorErrorSchema($validator, $error);
        }
        return $values;
    }

    /**
     * Setting post validator
     */
    protected function setPostValidatior()
    {
        //post validator
        $this->validatorSchema->setPostValidator(
            new sfValidatorAnd(
                array(
                    //checkLabel inherited
                    new sfValidatorCallback(array('callback' => array($this, 'checkLabel'))),
                    new sfValidatorCallback(array('callback' => array($this, 'checkAvailableValues'))),
                    new sfValidatorCallback(array('callback' => array($this, 'checkRequired')))
                )
            )
        );
    }

    protected function doSave($con = null)
    {
        if($this['valid']->getValue() != 1)
        {
            $this->getObject()->delete();
            return true;
        }
        parent::doSave($con);
        
        return $this->getObject();
    }
}

