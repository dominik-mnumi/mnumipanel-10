<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ProductFixpriceFormColl form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ProductFieldFixedpriceForm extends ProductFieldForm
{
    public $pricelistObj;

    public function configure()
    {
        parent::configure();

        $this->useFields(array());
        
        //create colletion for default list
        $productFixpriceCollForm = new ProductFixpriceCollForm(array(),
                array('pricelistObj' => PricelistTable::getInstance()->getDefaultPricelist()));

        //embed this collection in form
        $this->addPricelistColl($productFixpriceCollForm, 1);

    }

    public function saveEmbeddedForms($con = null, $forms = null)
    {
        $forms = $this->embeddedForms['fixpriceList']->embeddedForms;
        foreach($forms as $key => $formCollList)
        {
            $formCollRow = $formCollList->embeddedForms;

            foreach($formCollRow as $key2 => $formRow)
            {
                $formRow->getObject()->setProduct($this->getProduct());

                //synchronize entries
                if($formRow->getObject()->getQuantity() == '' && $formRow->getObject()->getPrice() == '')
                {
                    $formRow->getObject()->delete();
                    unset($formCollRow[$key2]);
                }
            }

            parent::saveEmbeddedForms($con, $formCollRow);
        }    
    }

    public function bind(array $taintedValues = null, array $taintedFiles = null)
    {
        foreach($taintedValues['fixpriceList'] as $key => $newFixedpriceColl)
        {        
            if(!isset($this['fixpriceList'][$key]))
            {
                $pricelistId = $taintedValues['fixpriceList'][$key][5]['pricelist_id'];
                //gets product
                if($this->getProduct())
                {
                    $productObj = $this->getProduct();

                    $productFixpriceCollForm = new ProductFixpriceCollForm(array(),
                            array('pricelistObj' => PricelistTable::getInstance()->find($pricelistId),
                                'productFixpriceColl' => $productObj->getProductFixpriceByPriceListId($pricelistId)));
                }
                else
                {
                    $productFixpriceCollForm = new ProductFixpriceCollForm(array(),
                            array('pricelistObj' => PricelistTable::getInstance()->find($pricelistId)));
                }
                $this->getWidgetSchema()->setNameFormat($this->getName() . '_7[%s]');
                
                $this->addPricelistColl($productFixpriceCollForm, $key, true);
            }
        }
        parent::bind($taintedValues, $taintedFiles);
    }

    /**
     * 
     *
     * @param <type> $formColl
     * @param integer $fixpriceListNumber
     * @param boolean $merge
     */
    public function addPricelistColl($formColl, $fixpriceListNumber, $merge = false)
    {
        if($merge == true)
        {
            $this->embeddedForms['fixpriceList']->embedForm($fixpriceListNumber, $formColl);
            $this->embedForm('fixpriceList', $this->embeddedForms['fixpriceList']);
        }
        else
        {
            $newFrom = new sfForm();
            $newFrom->embedForm($fixpriceListNumber, $formColl);
            $this->embedForm('fixpriceList', $newFrom);
        }
    }

   
}

