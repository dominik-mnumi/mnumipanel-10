<?php
    $productForm = $productFormArray[0];
    $pricelists = $pricelists ? $pricelists : null;
    $defaultPriceList = $defaultPricelist ? $defaultPricelist : null;

    $tabs = array(
        'productTabGeneral',    // 0
        'productTabWizard',     // 1
        'productTabCount',      // 2
        'productTabQuantity',   // 3
        'productTabAdvanced',   // 4
        'productTabAdvanced',   // 5
        'productTabAdvanced',   // 6
        'productTabAdvanced',   // 7
        'productTabFixedPrice', // 8
    );

    foreach($productFormArray as $key => $rec):
        $attributes = array(
            'rec' => $rec,
            'key' => $key,
            'productForm' => $productForm,
            'categoryRootObj' => $categoryRootObj,
            'pricelists' => $pricelists,
            'defaultPricelist' => $defaultPriceList
        );

        $templateName = (array_key_exists($key, $tabs))
            ? $tabs[$key]
            : 'productTabOther'
        ;

        echo include_partial($templateName, $attributes);
    endforeach;
?>