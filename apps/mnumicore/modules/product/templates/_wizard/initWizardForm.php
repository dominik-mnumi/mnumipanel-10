<div id="dialog-add-wizard-form" title="<?php echo __('General information'); ?>" style="display: none;">
    <div class="block-border">
        <div class="block-content modal-padding-fix form no-border">  
            <ul id="wizard-form-error-ul" class="message error no-margin" style="display: none;"></ul>
            <p>
                <label><?php echo __('Project size'); ?> (mm)</label>
            </p>
            <div class="columns">
                <div class="colx2-left">
                    <p class="required">
                        <?php echo $rec['wizard_size_width']->renderLabel(); ?>
                        <?php echo $rec['wizard_size_width']->render(); ?>
                    </p>  
                </div>
                <div class="colx2-right">
                    <p class="required">
                        <?php echo $rec['wizard_size_height']->renderLabel(); ?>
                        <?php echo $rec['wizard_size_height']->render(); ?>
                    </p>  
                </div>
            </div>
            
            <p>
                <label><?php echo __('Crop lines'); ?></label>
            </p>
            <div class="columns">
                <div class="colx2-left">
                    <p class="required">
                        <?php echo $rec['wizard_crop_top']->renderLabel(); ?>
                        <?php echo $rec['wizard_crop_top']->render(); ?>
                    </p>  
                </div>
                <div class="colx2-right">
                    <p class="required">
                        <?php echo $rec['wizard_crop_bottom']->renderLabel(); ?>
                        <?php echo $rec['wizard_crop_bottom']->render(); ?>
                    </p>  
                </div>
            </div>
            <div class="columns">
                <div class="colx2-left">
                    <p class="required">
                        <?php echo $rec['wizard_crop_left']->renderLabel(); ?>
                        <?php echo $rec['wizard_crop_left']->render(); ?>
                    </p>  
                </div>
                <div class="colx2-right">
                    <p class="required">
                        <?php echo $rec['wizard_crop_right']->renderLabel(); ?>
                        <?php echo $rec['wizard_crop_right']->render(); ?>
                    </p>  
                </div>
            </div>
        </div>  
    </div>
</div>