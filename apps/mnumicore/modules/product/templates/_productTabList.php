<?php foreach($productFormArray as $key => $rec): ?>
    <?php if($rec->getFieldset()->getName() == 'OTHER'): ?>
    <li class="tab_other_li" style="<?php $valid = $rec['valid']->getValue(); if(empty($valid) && $rec->getObject()->isNew()){ echo 'display: none'; } ?>">
        <a title="<?php echo $rec->getFieldsetLabel(); ?>" href="#tab-<?php echo $key; ?>">
            <?php echo __($rec->getFieldsetLabel()); ?>
        </a>
    </li>
    <?php else: ?>
    <li<?php if($key == 0): ?> class="current"<?php endif; ?>>
        <a title="<?php echo $rec->getFieldsetLabel(); ?>" href="#tab-<?php echo $key; ?>">
            <?php echo __($rec->getFieldset()->getLabel()); ?>
            <?php if($rec->getFieldset()->getLabel() != $rec->getFieldsetLabel()): ?>
                <br/>
                <small><?php echo $rec->getFieldsetLabel(); ?></small>
            <?php endif; ?>
        </a>
    </li>
    <?php endif; ?>
<?php endforeach; ?>

