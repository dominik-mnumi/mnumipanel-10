<div class="block-border">
    <div class="block-content">
        <h1><?php echo __('Choose parent category'); ?></h1>
        
        <div class="content-columns left30">      
            <div class="content-left" style="width: 100%">
                <div class="with-head">             
                    <div class="dark-grey-gradient with-padding" style="height:200px; overflow: auto">
                        
                        <?php foreach($treeColl as $rec): ?>
                        <ul class="arbo with-title">
                            <li class="">
                                <a href="#" class="title-computer parent current" val="<?php echo $rec->getId(); ?>"><span><?php echo __($rec->getName()); ?></span></a>
                                <ul>
                                    <!-- main categories -->
                                    <?php foreach($rec->__children as $rec2): ?>
                                    <?php echo include_partial('chooseParentCategoryNavigation', array('rec' => $rec2)); ?>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>         
        </div>        
    </div>
</div>