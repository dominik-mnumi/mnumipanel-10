<div class="tabs-content tab_other" id="tab-<?php echo $key; ?>" style="<?php $valid = $rec['valid']->getValue(); if(empty($valid)){ echo 'display: none'; } ?>">
    <?php echo $rec->renderHiddenFields(); ?>
    <div style="float: right;">
        <button type="button" class="red delete_tab_other"><?php echo __('Remove this tab'); ?></button>
    </div>
    <p>
        <?php echo $rec['visible']->render(array('class' => 'visible_form_content_checkbox')); ?>
        <?php echo __($rec['visible']->renderLabel()); ?>
    </p>
    
    <div class="visible_form_content" style="<?php echo ($rec['visible']->getValue()) ? '' : 'display: none'?>">
	    <p>
	        <?php echo $rec['required']->render(); ?>
	        <?php echo __($rec['required']->renderLabel()); ?>
	    </p>
        <p class="change_label_product_form" style="padding-left: 15px;">
            <span>
                <?php echo __($rec['label']->renderLabel()); ?>
                <?php echo $rec['label']->render(array('class' => 'medium-width')); ?>
                <span class=""></span>
            </span>
        </p>
        <p>
            <?php  echo __($rec['product_field_item']->renderLabel()); ?>
            <span class="relative">
                <?php echo $rec['product_field_item']->render(array('class' => 'multiselect medium-width')); ?>
                <span class=""></span>
            </span>
        </p>

        <p class="available_values_button_p">
            <a href="<?php  echo url_for('settingsField', $rec->getFieldset()); ?>" target="settings">
                <?php echo __('Manage options'); ?>
            </a>
        </p>
    </div>
    <p class="default_value_p">
        <?php echo __($rec['default_value']->renderLabel()); ?>
        <span class="relative">
            <?php echo $rec['default_value']->render(array('class' => 'medium-width')); ?>
            <span class=""></span>
        </span>
    </p>
</div>
