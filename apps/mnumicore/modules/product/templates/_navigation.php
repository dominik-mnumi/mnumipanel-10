<?php use_helper('openModal'); ?>

<div class="block-border">
    <div class="block-content">
        <h1><?php echo __('Category list'); ?>
            <a href="#" id="dialog-add-category-click"><img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt = "Add category"> <?php echo __('Add category'); ?></a>
        </h1>

        <?php foreach($treeColl as $rec): ?>
        <ul class="tree-list with-bg">
            <!-- main categories -->
            <?php foreach($rec->__children as $rec2): ?>
                <?php include_partial('dashboard/navigationItem', array(
                    'node' => $rec2, 
                    'cat_id' => $catId,
                    'url_prefix' => 'productCategory'
                    )); ?>
            <?php endforeach; ?>
        </ul>
        <?php endforeach; ?>
        
    </div>
</div>
<?php use_helper('changeLanguage');getChangeLanguage($languages, $default);?>



