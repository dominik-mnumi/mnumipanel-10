<script type="text/javascript">
$(document).ready(function() 
{
    function updateTips(t) 
    {
        $(".validateTips")
        .text(t)
        .addClass("ui-state-highlight");
        setTimeout(function() {
            tips.removeClass("ui-state-highlight", 1500);
        }, 500 );
    }
    
    function checkCatNameRequired(o) 
    {
        if (o.val().length == 0)
        {
            o.addClass("ui-state-error");
            updateTips('<?php echo __('Category name required'); ?>');
            return false;
        } 
        else 
        {
            return true;
        }
    }

    function checkCatParentRequired(o) 
    {
        if (o.val().length == 0)
        {
            o.addClass("ui-state-error");
            updateTips('<?php echo __('Parent category required'); ?>');
            return false;
        } 
        else 
        {
            return true;
        }
    }

    //toggles current choose and puts data to hidden field
    $("a.parent").toggle(
        function()
        {
            $("a.parent").removeClass('current');
            $(this).addClass('current')

            //parent id
            var val = $(this).attr('val');
            $(".modal-content #catParent").val(val);
        },
        function()
        {
            $("a.parent").removeClass('current');
            $(".modal-content #catParent").val("");
        }

    );

    //category add form
    $("#dialog-add-category-click").click(function() 
    {
        $.modal({
            content: $("#dialog-add-category-form"),
            title: '<?php echo __('Add category'); ?>',
            maxWidth: 500,
            resizable: false,
            buttons: {
                '<?php echo __('Add category'); ?>': function(win) {
                    //overwriting variables with modal window data
                    var name = $(".modal-content #catName"),
                    parent = $(".modal-content #catParent"),
                    allFields = $([]).add(name),
                    tips = $(".modal-content .validateTips");
		    		
                    var bValid = true;
                    allFields.removeClass( "ui-state-error" );

                    bValid = bValid && checkCatNameRequired(name);
                    bValid = bValid && checkCatParentRequired(parent);
		            
                    if(bValid)
                    {
                        $('.modal-content #addCategorySubmitButton').trigger('click');
                        win.closeModal();
                    }
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });
        
    });

    // selects first pricelist
    $('.fix_price_li').first().find('a').click();
    
    // shops
    function shopSelect()
    {
        if(!$('#product_0_custom_shops___ALL__').is(':checked'))
        {
            $('#shops .checkbox_list li:not(:first)').show();
        }
        else
        {
            $('#shops .checkbox_list li:not(:first)').hide();
        }
    }
    
    $('#product_0_custom_shops___ALL__').click(function() 
    {
        shopSelect();
    });
    shopSelect();   

    $('.select2-custom-clients').select2({
        multiple: true,
        minimumInputLength: 3,
        id: function(client) {
            return client.id;
        },
        ajax: {
            url: '<?php echo url_for('@searchClientAutocompleteByPhrase'); ?>',
            dataType: 'json',
            quietMillis: 250,
            data: function (term, page) {
                return {
                    search_phrase: term
                };
            },
            results: function (data, page) {
                return { results: data };
            }
        },
        initSelection: function(element, callback) {
            var ids = $(element).val();
            if (ids !== "") {
                $.ajax("<?php echo url_for('@searchClientAutocompleteByIds'); ?>?ids=" + ids, {
                    dataType: "json"
                }).done(function(data) { callback(data); });
            }
        },
        formatResult: function(client) {
            return clientAutocompleteFormat(client);
        },
        formatSelection: function(client) {
            return clientAutocompleteFormat(client);
        },
        escapeMarkup: function (m) { return m; }
    });

    // Format client for autocomplete
    function clientAutocompleteFormat(client)
    {
        var formatted = client.name;
        if(client.taxId) {
            formatted += " (" + "<?php echo __("Tax ID"); ?>" + ": " + client.taxId + ")";
        }

        return formatted;
    }

    function clientsSelect()
    {
        $('#clients').toggle(
            !$('#product_0_apply_for_all_clients').is(':checked')
        );
    }

    $('#product_0_apply_for_all_clients').click(function()
    {
        clientsSelect();
    });
    clientsSelect();
});
</script>
