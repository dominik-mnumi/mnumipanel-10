<?php

/**
 * bookbinderInterface actions.
 *
 * @package    mnumicore
 * @subpackage bookbinderInterface
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bookbinderInterfaceActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
  }
  
  /**
   * Executes barcode action
   *
   * @param sfRequest $request A request object
   */
  public function executeBarcode(sfWebRequest $request)
  {
      $barcodeBookbinder = new BarcodeBookbinder($request->getParameter('barcode'));
      $type = $barcodeBookbinder->getTableNameFromType();
    
      //if type is user request is for authorize
      /** @var myUser $myUser */
      $myUser = $this->getUser();
      if($type == 'sfGuardUserTable')
      {
          $this->result = array('auth' => $myUser->authorizeUsingPasswordSaltAndId($barcodeBookbinder->getId(), $barcodeBookbinder->getAdditionalNumber()));
          return sfView::SUCCESS;
      }
      // save new worklog
      elseif($type == 'OrderTable')
      {
          if(!$myUser->isAuthorizedUsingPasswordSaltAndId())
          {
              throw new Exception('You have to be authorized to create new worklog.');
          }
          /** @var Order $order */
          $order = $barcodeBookbinder->getObject();
          $userId = $myUser->getAuthorizedUsingPasswordSaltAndIdUserId();
          $order->setEditorId($userId);
          $order->save();
          $order->refresh();
          
          $this->result = $barcodeBookbinder->setAsReady($myUser->getAuthorizedUsingPasswordSaltAndIdUserId());
          $this->object = $order;
          return sfView::SUCCESS;
      }
    
      throw new Exception('This type of barcode is not supported in printerInterface.');
  }
}
