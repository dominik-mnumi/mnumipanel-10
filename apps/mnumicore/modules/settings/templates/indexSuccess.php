<?php echo include_partial('dashboard/message'); ?>
<?php include_partial('global/alertModalConfirm') ?>
<article class="container_12">
    <?php include_partial('global/filterForm', $tableOptions); ?>
    <section class="grid_4">
        <?php echo include_component('settings', 'navigation', array('id' => isset($id) ? $id : null)); ?>
    </section>

    <section class="grid_8">
        <div class="block-border">
            <div class="block-content form" id="complex_form" >
                <h1>
                    <?php echo __('Settings'); ?>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)); ?>
            </div>
        </div>
    </section>
</article>

<!-- create form -->
<div id="new_field_category" class = "new_field_category" style = "display: none;">
    <?php include_component('settings', 'createFieldset') ?>
</div>
