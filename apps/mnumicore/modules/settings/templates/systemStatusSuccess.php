<?php echo include_partial('dashboard/message'); ?>

<article class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content">
                <h1>
                    <?php echo __('Group'); ?>: <?php echo __('Status report'); ?>
                </h1>
                <?php foreach($reportSequence as $type): ?>
                
                <?php if($reportArray->offsetExists($type)): ?>
                <!-- status <?php echo $type; ?> -->
                <?php foreach($reportArray[$type] as $rec): ?>
                <?php include_partial('reportBlock', array('rec' => $rec)); ?>
                <?php endforeach; ?>
                <?php endif; ?>
                
                <?php endforeach; ?>

                
            </div>
        </div>
    </section>
</article>
