<?php echo include_partial('dashboard/message'); ?>

<article class="container_12">
    <?php include_partial('global/filterForm', $tableOptions); ?>
    <section class="grid_4">
        <?php echo include_component('settings', 'navigation', array('id' => isset($fieldset->id) ? $fieldset->id : null)); ?>
    </section>

    <section class="grid_8">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('Group'); ?>: <?php echo __($fieldset->label); ?>
                    <a href="<?php echo url_for('@settingsCreateFieldItem?id='.$fieldset->id) ?>">
                    <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add setting') ?>"> <?php echo __('add'); ?>
                    </a>
                </h1>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section> 
</article>

<!-- create form -->
<div id="new_field_category" class = "new_field_category" style = "display: none;">
    <?php include_component('settings', 'createFieldset') ?>
</div>