<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * UPSConfigurationForm form.
 *
 * @author     Bartosz Dembek
 */
class UPSConfigurationForm extends BaseFormDoctrine
{

    protected static $serviceTypes = array(
        '01' => 'Next Day Air',
        '02' => '2nd Day Air',
        '03' => 'Ground',
        '07' => 'Express',
        '08' => 'Expedited',
        '11' => 'UPS Standard',
        '12' => '3 Day Select',
        '13' => 'Next Day Air Saver',
        '14' => 'Next Day Air Early AM',
        '54' => 'Express Plus',
        '59' => '2nd Day Air A.M.',
        '65' => 'UPS Saver',
        '82' => 'UPS Today Standard',
        '83' => 'UPS Today Dedicated Courier',
        '84' => 'UPS Today Intercity',
        '85' => 'UPS Today Express',
        '86' => 'UPS Today Express Saver',
        '96' => 'UPS Worldwide Express Freight');
    protected static $packageTypes = array(
        '01' => 'UPS Letter',
        '02' => 'Customer Supplied Package',
        '03' => 'Tube',
        '04' => 'PAK',
        '21' => 'UPS Express Box',
        '24' => 'UPS 25KG Box',
        '25' => 'UPS 10KG Box',
        '30' => 'Pallet',
        '2a' => 'Small Express Box',
        '2b' => 'Medium Express Box',
        '2c' => 'Large Express Box');
    protected static $unitOfWeightMeasurement = array(
        'LBS' => 'pounds',
        'KGS' => 'kilograms');
    protected static $unitOfLengthMeasurement = array(
        'IN' => 'inches',
        'CM' => 'centimeters',
        '00' => 'Metric Units Of Measurement',
        '01' => 'English Units of Measurement');

    public function configure()
    {

        $this->disableCSRFProtection();
        $contextObj = sfContext::getInstance();

        $this->defaults = $this->getObject()->getConfiguration();

        $this->setWidgets(array(
            'url' => new sfWidgetFormInput(),
            'access' => new sfWidgetFormInput(),
            'user_id' => new sfWidgetFormInput(),
            'password' => new sfWidgetFormInputPassword(
                    array(),
                    array('value' => empty($this->defaults['password']) ? '' : '0000000000')),
            'shipper_shipper_number' => new sfWidgetFormInput(),
            'shipper_name' => new sfWidgetFormInput(),
            'shipper_attention_name' => new sfWidgetFormInput(),
            'shipper_tax_identification_number' => new sfWidgetFormInput(),
            'shipper_address_address_line' => new sfWidgetFormInput(),
            'shipper_address_city' => new sfWidgetFormInput(),
            'shipper_address_postal_code' => new sfWidgetFormInput(),
            'shipper_address_country_code' => new sfWidgetFormI18nChoiceCountry(
                    array('culture' => $contextObj->getUser()->getCulture())),
            'service_code' => new sfWidgetFormChoice(
                    array('choices' => self::$serviceTypes)),
            'package_packaging_code' => new sfWidgetFormChoice(
                    array('choices' => self::$packageTypes)),
            'package_package_weight_unit_of_measurement_code' => new sfWidgetFormChoice(
                        array('choices' => self::$unitOfWeightMeasurement)
                    ),
            'package_package_weight_weight' => new sfWidgetFormInput(),
            'package_dimensions_unit_of_measurement_code' => new sfWidgetFormChoice(
                    array('choices' => self::$unitOfLengthMeasurement)),
            'package_dimensions_length' => new sfWidgetFormInput(),
            'package_dimensions_width' => new sfWidgetFormInput(),
            'package_dimensions_height' => new sfWidgetFormInput(),
        ));

        $this->widgetSchema->setLabels(array(
            'access' => 'Access key',
            'user_id' => 'User ID',
            'password' => 'Password',
            'url' => 'API url',
            'shipper_name' => 'Sender name',
            'shipper_attention_name' => 'Sender attention name',
            'shipper_tax_identification_number' => 'Taxpayer Identification Number (TIN)',
            'shipper_shipper_number' => 'Sender number',
            'shipper_address_address_line' => 'Sender address',
            'shipper_address_city' => 'Sender city',
            'shipper_address_postal_code' => 'Sender postcode',
            'shipper_address_country_code' => 'Sender country',
            'service_code' => 'Service type',
            'package_packaging_code' => 'Package type',
            'package_package_weight_unit_of_measurement_code' => 'Unit of weight measurement',
            'package_package_weight_weight' => 'Default package weight (in unit above)',
            'package_dimensions_unit_of_measurement_code' => 'Unit of length measurement',
            'package_dimensions_length' => 'Default package length (in unit above)',
            'package_dimensions_width' => 'Default package width (in unit above)',
            'package_dimensions_height' => 'Default package height (in unit above)',
        ));

        $this->setValidators(array(
            'access' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 25)),
            'user_id' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 25)),
            'password' => new sfValidatorPass(
                    array('required' => false)),
            'url' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 255)),
            'shipper_name' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 255)),
            'shipper_attention_name' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 255)),
            'shipper_tax_identification_number' =>  new sfValidatorString( // tax id validation is in postValidation()
                array('required' => false)),
            'shipper_shipper_number' => new sfValidatorRegex(
                    array('required' => false,
                        'pattern' => '/^[a-z0-9]{1,6}$/i'),
                    array('invalid' => 'Field "Sender number" is invalid (6 characters max)')),
            'shipper_address_address_line' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 255)),
            'shipper_address_city' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 255)),
            'shipper_address_postal_code' => new sfValidatorString(
                    array('required' => false,
                        'max_length' => 20)),
            'shipper_address_country_code' => new sfValidatorI18nChoiceCountry(
                    array('required' => false)),
            'service_code' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$serviceTypes))),
            'package_packaging_code' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$packageTypes))),
            'package_package_weight_unit_of_measurement_code' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$unitOfWeightMeasurement))),
            'package_package_weight_weight' =>new sfValidatorNumber(
                    array('required' => false),
                    array('invalid' => 'Field "%label%" is invalid')),
            'package_dimensions_unit_of_measurement_code' => new sfValidatorChoice(
                    array('choices' => array_keys(self::$unitOfLengthMeasurement))),
            'package_dimensions_length' => new sfValidatorNumber(
                    array('required' => false),
                    array('invalid' => 'Field "%label%" is invalid')),
            'package_dimensions_width' => new sfValidatorNumber(
                    array('required' => false),
                    array('invalid' => 'Field "%label%" is invalid')),
            'package_dimensions_height' => new sfValidatorNumber(
                    array('required' => false),
                    array('invalid' => 'Field "%label%" is invalid'))
        ));

        $this->widgetSchema->setHelps(array(
            'url' => 'API url, that will be used to generate UPS labels',
            'shipper_name' => 'e.g. Sample Company',
            'shipper_attention_name' => 'Additional sender name e.g. John Whittle',
            'shipper_shipper_number' => 'Field must be 6 alphanumeric characters long',
            'shipper_address_address_line' => 'Sender street and number'
        ));

        $this->setCssClasses('full-width');
        $this->setDefaults($this->defaults);
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        $printLabelConfiguration = new PrintLabelConfigurationForm($this->defaults);
        $this->embedForm('PrintLabelConfiguration', $printLabelConfiguration);
        $this->setValidator('PrintLabelConfiguration', $printLabelConfiguration->getValidatorSchema());
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save($con = NULL)
    {
        $values = $this->getValues();

        if(isset($values['PrintLabelConfiguration'])) {

            $printLabelConf = $values['PrintLabelConfiguration'];
            unset($values['PrintLabelConfiguration']);
            $values += $printLabelConf;
        }

        if($values['password'] == '0000000000') {
            $configuration = $this->getObject()->getConfiguration();
            $values['password'] = $configuration['password'];
        }

        $this->getObject()->setConfiguration($values);
    }

    public function getModelName()
    {
        return 'Carrier';
    }
}
