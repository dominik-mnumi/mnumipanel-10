<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CarrierWeightCollForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class CarrierWeightCollForm extends sfForm
{
    public function configure()
    {
        //gets carrier object for current collection
        $carrierObj = $this->getOption('carrierObj');

        if(!$carrierObj instanceof Carrier)
        {
            throw new Exception('You must define carrierObj and it must be instance of Carrier');
        }

        $key = 0;
        if($carrierObj->getId())
        {
            $carrierWeightColl = CarrierWeightTable::getInstance()->findByCarrierId($carrierObj->getId());

            //if entries exist

            if(count($carrierWeightColl) > 0)
            {
                //gets existed forms
                foreach($carrierWeightColl as $key => $rec)
                {
                    $carrierWeightForm = new CarrierWeightForm($rec);
                    $this->embedForm($key, $carrierWeightForm);
                }
                $key++;
            }
        }
        $stopKey = $key + 10;
        for($key; $key < $stopKey; $key++)
        {
            //empty form
            $carrierWeightForm = new CarrierWeightForm();
            $carrierWeightForm->getWidget('carrier_id')->setDefault($carrierObj->getId());
            $this->embedForm($key, $carrierWeightForm);
        }
    }
}
?>
