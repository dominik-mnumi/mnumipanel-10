<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CacheProductCalculationProductData class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */
class PrintLabelConfigurationForm extends BaseForm
{
    public function configure() {

        $orientations = array('landscape' => 'landscape', 'portrait' => 'portrait', 'reverse_landscape' => 'reverse_landscape');
        $this->setWidgets(array(
            'printlabel_large_name' => new sfWidgetFormInput(),
            'printlabel_large_resolution' => new sfWidgetFormInput(),
            'printlabel_large_width' => new sfWidgetFormInput(),
            'printlabel_large_height' => new sfWidgetFormInput(),
            'printlabel_large_orientation' => new sfWidgetFormChoice(array(
                'choices' => $orientations
            ))            
        ));

        $this->setValidators(array(
            'printlabel_large_name' => new sfValidatorString(array(
                'required' => false)),
            'printlabel_large_resolution' => new sfValidatorNumber(array(
                'required' => false,
                'max' => 300)),
            'printlabel_large_width' => new sfValidatorNumber(array(
                'required' => false)),
            'printlabel_large_height' => new sfValidatorNumber(array(
                'required' => false)),
            'printlabel_large_orientation' => new sfValidatorChoice(array(
                'choices' => $orientations)),
        ));

        $this->getWidgetSchema()->setLabels(array(
            'printlabel_large_name' => 'Printer name',
            'printlabel_large_resolution' => 'Printer resolution',
            'printlabel_large_width' => 'Width expressed in millimeters (eg. 101.6)',
            'printlabel_large_height' => 'Height expressed in millimeters (eg. 152.4)',
            'printlabel_large_orientation' => 'Paper orientation'
        ));

        $this->setCssClasses('full-width');
        $this->getWidgetSchema()->setNameFormat('[%s]');
    }
}