<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
  <section class="grid_12">
    <div class="block-border"><div class="block-content form">
        <h1>
            <?php echo __('Group'); ?>: <?php echo __('Carrier'); ?>
            <a href="<?php echo url_for('@settingsCarrierAdd') ?>">
                <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add pricelist') ?>"> <?php echo __('add'); ?>
            </a>
        </h1>
        <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
    </div>
    </div>
  </section>
  
</article>
