<h2 class="bigger margin-bottom-10px"><?php echo __('UPS data'); ?></h2>
<hr />

<div class="box">
    <p class="mini-infos color-blue bold">
        <?php echo __('To generate UPS labels properly all fields must be filled.'); ?>                            
    </p>
</div> 

<fieldset>
    <legend><?php echo __('API data'); ?></legend>
    <p>
        <?php echo $form['url']->renderLabel(); ?>
        <?php echo $form['url']->renderError(); ?>
        <?php echo $form['url']->render(); ?>
        <?php echo $form['url']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['access']->renderLabel(); ?>
        <?php echo $form['access']->renderError(); ?>
        <?php echo $form['access']->render(); ?>
        <?php echo $form['access']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['user_id']->renderLabel(); ?>
        <?php echo $form['user_id']->renderError(); ?>
        <?php echo $form['user_id']->render(); ?>
        <?php echo $form['user_id']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['password']->renderLabel(); ?>
        <?php echo $form['password']->renderError(); ?>
        <?php echo $form['password']->render(); ?>
        <?php echo $form['password']->renderHelp(); ?>
    </p>
</fieldset>

<fieldset>
    <legend><?php echo __('Sender data'); ?></legend>

    <p>
        <?php echo $form['shipper_shipper_number']->renderLabel(); ?>
        <?php echo $form['shipper_shipper_number']->renderError(); ?>
        <?php echo $form['shipper_shipper_number']->render(); ?>
        <?php echo $form['shipper_shipper_number']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['shipper_name']->renderLabel(); ?>
        <?php echo $form['shipper_name']->renderError(); ?>
        <?php echo $form['shipper_name']->render(); ?>
        <?php echo $form['shipper_name']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['shipper_attention_name']->renderLabel(); ?>
        <?php echo $form['shipper_attention_name']->renderError(); ?>
        <?php echo $form['shipper_attention_name']->render(); ?>
        <?php echo $form['shipper_attention_name']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['shipper_tax_identification_number']->renderLabel(); ?>
        <?php echo $form['shipper_tax_identification_number']->renderError(); ?>
        <?php echo $form['shipper_tax_identification_number']->render(); ?>
        <?php echo $form['shipper_tax_identification_number']->renderHelp(); ?>
    </p> 
    <p>
        <?php echo $form['shipper_address_address_line']->renderLabel(); ?>
        <?php echo $form['shipper_address_address_line']->renderError(); ?>
        <?php echo $form['shipper_address_address_line']->render(); ?>
        <?php echo $form['shipper_address_address_line']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['shipper_address_city']->renderLabel(); ?>
        <?php echo $form['shipper_address_city']->renderError(); ?>
        <?php echo $form['shipper_address_city']->render(); ?>
        <?php echo $form['shipper_address_city']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['shipper_address_postal_code']->renderLabel(); ?>
        <?php echo $form['shipper_address_postal_code']->renderError(); ?>
        <?php echo $form['shipper_address_postal_code']->render(); ?>
        <?php echo $form['shipper_address_postal_code']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['shipper_address_country_code']->renderLabel(); ?>
        <?php echo $form['shipper_address_country_code']->renderError(); ?>
        <?php echo $form['shipper_address_country_code']->render(); ?>
        <?php echo $form['shipper_address_country_code']->renderHelp(); ?>
    </p>
</fieldset>

<fieldset>
    <legend><?php echo __('Settings'); ?></legend>
    <p>
        <?php echo $form['service_code']->renderLabel(); ?>
        <?php echo $form['service_code']->renderError(); ?>
        <?php echo $form['service_code']->render(); ?>
        <?php echo $form['service_code']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['package_packaging_code']->renderLabel(); ?>
        <?php echo $form['package_packaging_code']->renderError(); ?>
        <?php echo $form['package_packaging_code']->render(); ?>
        <?php echo $form['package_packaging_code']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['package_package_weight_unit_of_measurement_code']->renderLabel(); ?>
        <?php echo $form['package_package_weight_unit_of_measurement_code']->renderError(); ?>
        <?php echo $form['package_package_weight_unit_of_measurement_code']->render(); ?>
        <?php echo $form['package_package_weight_unit_of_measurement_code']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['package_package_weight_weight']->renderLabel(); ?>
        <?php echo $form['package_package_weight_weight']->renderError(); ?>
        <?php echo $form['package_package_weight_weight']->render(); ?>
        <?php echo $form['package_package_weight_weight']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['package_dimensions_unit_of_measurement_code']->renderLabel(); ?>
        <?php echo $form['package_dimensions_unit_of_measurement_code']->render(); ?>
        <?php echo $form['package_dimensions_unit_of_measurement_code']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['package_dimensions_length']->renderLabel(); ?>
        <?php echo $form['package_dimensions_length']->renderError(); ?>
        <?php echo $form['package_dimensions_length']->render(); ?>
        <?php echo $form['package_dimensions_length']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['package_dimensions_width']->renderLabel(); ?>
        <?php echo $form['package_dimensions_width']->renderError(); ?>
        <?php echo $form['package_dimensions_width']->render(); ?>
        <?php echo $form['package_dimensions_width']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['package_dimensions_height']->renderLabel(); ?>
        <?php echo $form['package_dimensions_height']->renderError(); ?>
        <?php echo $form['package_dimensions_height']->render(); ?>
        <?php echo $form['package_dimensions_height']->renderHelp(); ?>
    </p>

</fieldset>

<fieldset>
    <legend><?php echo __('Label printer'); ?></legend>
    <?php echo $form['PrintLabelConfiguration'] ?>
</fieldset>