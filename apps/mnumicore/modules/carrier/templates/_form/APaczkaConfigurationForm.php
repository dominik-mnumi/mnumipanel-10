<h2 class="bigger margin-bottom-10px"><?php echo __('APaczka.pl data'); ?></h2>

<?php if(! \Mnumi\Bundle\PluginManagerBundle\Service\ManagerBundle::has('apaczka')): ?>
    <p class="mini-infos color-blue bold">
        <?php echo __('This module is not available. '); ?>
    </p>
<?php else: ?>
    <hr />

    <div class="box">
        <p class="mini-infos color-blue bold">
            <?php echo __('To generate APaczka.pl labels properly all fields must be filled.'); ?>
        </p>
    </div>

    <fieldset>
        <legend><?php echo __('API data'); ?></legend>
        <p>
            <?php echo $form['api_key']->renderLabel(); ?>
            <?php echo $form['api_key']->renderError(); ?>
            <?php echo $form['api_key']->render(); ?>
            <?php echo $form['api_key']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['api_login']->renderLabel(); ?>
            <?php echo $form['api_login']->renderError(); ?>
            <?php echo $form['api_login']->render(); ?>
            <?php echo $form['api_login']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['api_password']->renderLabel(); ?>
            <?php echo $form['api_password']->renderError(); ?>
            <?php echo $form['api_password']->render(); ?>
            <?php echo $form['api_password']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['api_url']->renderLabel(); ?>
            <?php echo $form['api_url']->renderError(); ?>
            <?php echo $form['api_url']->render(); ?>
            <?php echo $form['api_url']->renderHelp(); ?>
        </p>
    </fieldset>

    <fieldset>
        <legend><?php echo __('Sender data'); ?></legend>

        <p>
            <?php echo $form['shipper_name']->renderLabel(); ?>
            <?php echo $form['shipper_name']->renderError(); ?>
            <?php echo $form['shipper_name']->render(); ?>
            <?php echo $form['shipper_name']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['shipper_phone']->renderLabel(); ?>
            <?php echo $form['shipper_phone']->renderError(); ?>
            <?php echo $form['shipper_phone']->render(); ?>
            <?php echo $form['shipper_phone']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['shipper_email']->renderLabel(); ?>
            <?php echo $form['shipper_email']->renderError(); ?>
            <?php echo $form['shipper_email']->render(); ?>
            <?php echo $form['shipper_email']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['shipper_tax_id']->renderLabel(); ?>
            <?php echo $form['shipper_tax_id']->renderError(); ?>
            <?php echo $form['shipper_tax_id']->render(); ?>
            <?php echo $form['shipper_tax_id']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['shipper_address_street']->renderLabel(); ?>
            <?php echo $form['shipper_address_street']->renderError(); ?>
            <?php echo $form['shipper_address_street']->render(); ?>
            <?php echo $form['shipper_address_street']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['shipper_address_city']->renderLabel(); ?>
            <?php echo $form['shipper_address_city']->renderError(); ?>
            <?php echo $form['shipper_address_city']->render(); ?>
            <?php echo $form['shipper_address_city']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['shipper_address_postal_code']->renderLabel(); ?>
            <?php echo $form['shipper_address_postal_code']->renderError(); ?>
            <?php echo $form['shipper_address_postal_code']->render(); ?>
            <?php echo $form['shipper_address_postal_code']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['shipper_address_country_code']->renderLabel(); ?>
            <?php echo $form['shipper_address_country_code']->renderError(); ?>
            <?php echo $form['shipper_address_country_code']->render(); ?>
            <?php echo $form['shipper_address_country_code']->renderHelp(); ?>
        </p>
    </fieldset>

    <fieldset>
        <legend><?php echo __('Settings'); ?></legend>
        <p>
            <?php echo $form['service_code']->renderLabel(); ?>
            <?php echo $form['service_code']->renderError(); ?>
            <?php echo $form['service_code']->render(); ?>
            <?php echo $form['service_code']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['shipment_type_code']->renderLabel(); ?>
            <?php echo $form['shipment_type_code']->renderError(); ?>
            <?php echo $form['shipment_type_code']->render(); ?>
            <?php echo $form['shipment_type_code']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['package_weight']->renderLabel(); ?>
            <?php echo $form['package_weight']->renderError(); ?>
            <?php echo $form['package_weight']->render(); ?>
            <?php echo $form['package_weight']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['package_dimensions_length']->renderLabel(); ?>
            <?php echo $form['package_dimensions_length']->renderError(); ?>
            <?php echo $form['package_dimensions_length']->render(); ?>
            <?php echo $form['package_dimensions_length']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['package_dimensions_width']->renderLabel(); ?>
            <?php echo $form['package_dimensions_width']->renderError(); ?>
            <?php echo $form['package_dimensions_width']->render(); ?>
            <?php echo $form['package_dimensions_width']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['package_dimensions_height']->renderLabel(); ?>
            <?php echo $form['package_dimensions_height']->renderError(); ?>
            <?php echo $form['package_dimensions_height']->render(); ?>
            <?php echo $form['package_dimensions_height']->renderHelp(); ?>
        </p>

    </fieldset>

    <fieldset>
        <legend><?php echo __('Label printer'); ?></legend>
        <?php echo $form['PrintLabelConfiguration'] ?>
    </fieldset>
<?php endif; ?>