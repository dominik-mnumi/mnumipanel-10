<h2 class="bigger margin-bottom-10px"><?php echo __('Elektroniczy Nadawca data'); ?></h2>
<hr />

<div class="box">
    <p class="mini-infos color-blue bold">
        <?php echo __('To generate Elektroniczny Nadawca labels properly all fields must be filled.'); ?>
    </p>
</div>

<fieldset>
    <legend><?php echo __('API data'); ?></legend>

    <?php if(empty($form['sender_profile'])): ?>
        <ul class="message error no-margin"><li><?php echo __('Wrong configuration data. Check login, password and environment.'); ?></li></ul>
    <?php endif; ?>

    <p>
        <?php echo $form['api_login']->renderLabel(); ?>
        <?php echo $form['api_login']->renderError(); ?>
        <?php echo $form['api_login']->render(); ?>
        <?php echo $form['api_login']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['api_password']->renderLabel(); ?>
        <?php echo $form['api_password']->renderError(); ?>
        <?php echo $form['api_password']->render(); ?>
        <?php echo $form['api_password']->renderHelp(); ?>
    </p>
    <p>
        <?php echo $form['api_environment']->renderLabel(); ?>
        <?php echo $form['api_environment']->renderError(); ?>
        <?php echo $form['api_environment']->render(); ?>
        <?php echo $form['api_environment']->renderHelp(); ?>
    </p>

</fieldset>

<?php if(isset($form['sender_profile'])): ?>
    <fieldset>
        <p>
            <?php echo $form['sender_profile']->renderLabel(); ?>
            <?php echo $form['sender_profile']->renderError(); ?>
            <?php echo $form['sender_profile']->render(); ?>
            <?php echo $form['sender_profile']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['send_office']->renderLabel(); ?>
            <?php echo $form['send_office']->renderError(); ?>
            <?php echo $form['send_office']->render(); ?>
            <?php echo $form['send_office']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['package_type']->renderLabel(); ?>
            <?php echo $form['package_type']->renderError(); ?>
            <?php echo $form['package_type']->render(); ?>
            <?php echo $form['package_type']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['package_size']->renderLabel(); ?>
            <?php echo $form['package_size']->renderError(); ?>
            <?php echo $form['package_size']->render(); ?>
            <?php echo $form['package_size']->renderHelp(); ?>
        </p>
        <p>
            <?php echo $form['package_weight']->renderLabel(); ?>
            <?php echo $form['package_weight']->renderError(); ?>
            <?php echo $form['package_weight']->render(); ?>
            <?php echo $form['package_weight']->renderHelp(); ?>
        </p>

    </fieldset>

    <fieldset>
        <legend><?php echo __('Label printer'); ?></legend>
        <?php echo $form['PrintLabelConfiguration'] ?>
    </fieldset>

<?php endif; ?>