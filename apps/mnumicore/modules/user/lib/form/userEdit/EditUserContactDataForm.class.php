<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended ContactForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditUserContactDataForm extends UserContactForm
{
    public function configure()
    {
        parent::configure();
        
        $this->setWidget('notification_type_id', new sfWidgetFormInputHidden());
                
        $this->getValidator('value')->setOption('required', false);
        
        $this->useFields(array('value', 'notification_type_id'));
       
    }
    
 
}

