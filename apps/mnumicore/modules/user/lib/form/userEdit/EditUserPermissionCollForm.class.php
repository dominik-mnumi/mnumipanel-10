<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Extended sfGuardUserForm form.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class EditUserPermissionCollForm extends sfGuardUserForm
{
    private $toDeleteArray = array();
    
    public function configure()
    {
        parent::configure();

        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');

        // create form collection
        $formColl = new sfForm();

        // sets current group array
        $groupExistArray = array();

        // gets all existent user groups
        $userGroupColl = $this->getObject()->getSfGuardUserGroup();

        $i = 0;
        foreach($userGroupColl as $rec)
        {
            // creates form foreach
            $userGroupForm = new EditUserPermissionForm($rec);
            $userGroupForm->setDefault('value', true);

            $formColl->embedForm($i, $userGroupForm);

            $groupExistArray[] = $rec->getGroupId();

            $i++;
        }

        // gets all group id array
        $groupAllArray = sfGuardGroupTable::getInstance()->getAllIdArray();

        // gets difference
        $newGroupArray = array_diff($groupAllArray, $groupExistArray);

        // foreach group id which not in user group
        foreach($newGroupArray as $rec)
        {
            // prepares object
            $newUserGroupObj = new sfGuardUserGroup();
            $newUserGroupObj->setUserId($this->getObject()->getId());
            $newUserGroupObj->setGroupId($rec);

            // creates form 
            $userGroupForm = new EditUserPermissionForm($newUserGroupObj);

            // sets default
            $userGroupForm->setDefault('value', false);

            $formColl->embedForm($i, $userGroupForm);

            $i++;
        }

        // embeds UserGroups
        $this->embedForm('UserGroups', $formColl);

        $this->useFields(array('UserGroups'));
    }

    public function bind(array $taintedValues = null, array $taintedFiles = null)
    {        
        $forms = $this->embeddedForms['UserGroups']->embeddedForms;

        // gets values to delete
        foreach($taintedValues['UserGroups'] as $key => $rec)
        {
            if(!isset($rec['value']))
            {
                $this->toDeleteArray[] = $key;
            }        
        }
        
        parent::bind($taintedValues, $taintedFiles);
      
    }

    public function saveEmbeddedForms($con = null, $forms = null)
    {
        $forms = $this->embeddedForms['UserGroups']->embeddedForms;
        foreach($forms as $key => $form)
        {
            $form->getObject()->setUser($this->getObject());

            // synchronize entries
            if(in_array($key, $this->toDeleteArray))
            {
                $form->getObject()->delete();
                unset($forms[$key]);
            }
        }

        parent::saveEmbeddedForms($con, $forms);
    }

}
