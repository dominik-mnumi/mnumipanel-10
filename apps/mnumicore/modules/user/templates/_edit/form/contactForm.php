<form id="dialog-edit-user-contact-data-form" action="<?php echo url_for('reqEditUserContactDataCollForm', 
        array('model' => 'sfGuardUser', 
            'type' => 'edit')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('Contact data'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <?php $contactArrayForm = $form->getEmbeddedForm('Contacts'); ?>
        <div class="block-border">                             
            <div class="block-content form no_border">  
                <p class="required">
                     <label><?php echo __('To delete leave empty field'); ?></label>
                </p>
                <?php if(0 < $contactArrayForm->count()): ?>
                <?php foreach($contactArrayForm->getEmbeddedForms() as $key => $contactForm): ?>
                <p>
                    <?php echo $form['Contacts'][$key]->renderHiddenFields(); ?>
                    <label><?php echo $contactForm->getObject()->getNotificationType()->getName(); ?></label>
                    <?php if($contactForm->getObject()->getNotificationType()->getName() == NotificationTypeTable::$email): ?>
                    <?php echo $form['Contacts'][$key]['value']->render(array('type' => 'hidden')); ?>
                    <?php echo $form['Contacts'][$key]['value']->getValue(); ?>
                    <?php else: ?>
                    <?php echo $form['Contacts'][$key]['value']->render(); ?>
                    <?php endif; ?>
                </p>                       
                <?php endforeach; ?>

                <?php else: ?>
                <?php echo __('No contact data'); ?>
                <?php endif; ?>
            </div>
        </div>    
    </div>
</form>

