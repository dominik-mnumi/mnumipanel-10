<form id="dialog-add-user-form" action="<?php echo url_for('reqAddUserForm', 
        array('model' => 'sfGuardUser', 
            'type' => 'new')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('Add User'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                      
                <p>
                    <?php echo $form['username']->renderLabel(); ?>
                    <?php echo $form['username']->render(); ?>
                </p>                               
                <p  class="required">
                    <?php echo $form['first_name']->renderLabel(); ?>
                    <?php echo $form['first_name']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['last_name']->renderLabel(); ?>
                    <?php echo $form['last_name']->render(); ?>
                <p>               
                <p class="required">
                    <?php echo $form['email_address']->renderLabel(); ?>
                    <?php echo $form['email_address']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['password']->renderLabel(); ?>
                    <?php echo $form['password']->render(); ?>
                <p> 
                <p class="required">
                    <?php echo $form['password_again']->renderLabel(); ?>
                    <?php echo $form['password_again']->render(); ?>
                <p>
            </div>
        </div>
    </div>
</form>

