<script type="text/javascript">
    function renderError(errorArray)
    {
        var errorlist = '<ul class="message error no-margin">';
        for(i in errorArray)
        {
            errorlist += '<li>' + errorArray[i] + '</li>';
        }
        errorlist += '</ul>';
        
        return errorlist;
    }

    //loyalty points
    $("#dialog-add-loyalty-points-click").click(function() 
    {                          
        $.modal({
            content: $("#dialog-add-loyalty-points-form"),
            title: '<?php echo __('Add loyalty points'); ?>',
            maxWidth: 500,
            maxHeight: 450,
            resizable: false,
            buttons: 
                {
                '<?php echo __('Add loyalty points'); ?>': function(win) 
                {
                    var form = $(".modal-content #dialog-add-loyalty-points-form");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        success: function(dataOutput)
                        {
                            if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #dialog-add-loyalty-points-form .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();
                                    
                                    var href = "<?php echo url_for('@userListEdit?id='.$userObj->getId()); ?>";
                                    window.location.href = href;               
                                }
                                
                            }
                           
                        }
                    });
                },
                '<?php echo __('Close'); ?>': function(win) 
                {
                    win.closeModal();
                }
            }
        });      
    });
</script>