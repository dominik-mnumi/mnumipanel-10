<form id="dialog-add-edit-client-form" action="<?php echo url_for('userClientListEdit', $clientUserObj); ?>" enctype="multipart/form-data" method="post">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => '@userClientList?id='.$clientUserObj->getUser()->getId())); ?>
    <?php echo include_partial('dashboard/message'); ?>
    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content form">
                    <h1>
                        <?php echo __('Customer'); ?>: <?php echo $clientUserObj->getClient()->getFullname(); ?>
                    </h1>
                    <?php if($formArray[0]->hasErrors()): ?>
                    <?php foreach($formArray[0]->getFormFieldSchema() as $formField): ?>
                    <?php echo __($formField->renderError()); ?>
                    <?php endforeach; ?>
                    <?php endif ?>
                    <?php include_partial('user/edit/form/clientEditForm', array('form' => $formArray[0])); ?>               
                </div>
            </div>
        </section>
    </article>
</form>    
<?php echo include_partial('javascript'); ?>
