<?php

/**
 * shop actions.
 *
 * @package    mnumicore
 * @subpackage shop
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class shopActions extends tablesActions
{
    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        $this->displayTableFields = array(
            'Description' => array(),
            'ValidDate' => array('label' => 'Expiry date'),
            'Host' => array(),
            'Type' => array('label' => 'Type',
                'partial' => 'shop/type')     
        );
        $this->displayGridFields = array(
            'Description'  => array('destination' => 'name'), 
            'Host'  => array('destination' => 'keywords')
        );
        $this->sortableFields = array('description', 'valid_date', 'host', 'type');
        $this->modelObject = 'Shop';
        
        $this->tableOptions = $this->executeTable();
        
        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }

    public function executeCreate(sfWebRequest $request)
    {      
        $this->form = new ShopForm();
        $this->proceedForm($this->form);
        $this->setTemplate('edit');
    }

    public function executeEdit(sfWebRequest $request)
    {
        $this->form = new ShopForm($this->getRoute()->getObject());
        $this->proceedForm($this->form);
    }

    public function executeDelete(sfWebRequest $request)
    {
        $this->getRoute()->getObject()->delete();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Deleted successfully.',
                'messageType' => 'msg_success',
            ));
        $this->redirect('@settingsShop');
    }

    private function proceedForm($form, $redirect = 'settingsShopEdit')
    {
        if(!$this->getRequest()->isMethod('post'))
        {
            return;
        }

        $this->form->bind($this->getRequest()->getParameter($this->form->getName()));

        if($this->form->isValid())
        {
            $this->form->save();
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfully.',
                'messageType' => 'msg_success',
            ));
            $redirectUrl = $this->generateUrl($redirect, array('name' => $this->form->getObject()->name));
            $this->redirect($redirectUrl);
        }
    }
    
    public function executeDeleteMany(sfWebRequest $request)
    {
        $this->modelObject = 'Shop';
        parent::executeDeleteMany($request);
    }

    /**
     * Executes stores list
     *
     * @param \sfWebRequest $request A request object
     * @return
     */
    public function executeStores(sfWebRequest $request)
    {
        $this->displayTableFields = array(
            'Host' => array(),
            'Description' => array()
        );
        $this->displayGridFields = array(
            'Host'  => array('destination' => 'keywords'),
            'Description'  => array('destination' => 'name')
        );
        $this->sortableFields = array('description', 'valid_date', 'host', 'type');
        $this->modelObject = 'Shop';
        $this->removeAction('delete');

        $this->customQuery = ShopTable::getInstance()->createQuery('c')
                ->andWhere('c.type = ?', 'shop');

        $this->tableOptions = $this->executeTable();

        if($request->isXmlHttpRequest())
        {
            return $this->renderPartial('global/tableHtml', $this->tableOptions);
        }
    }

    /**
     * Executes edit shop options
     *
     * @param sfWebRequest $request
     */
    public function executeEditStore(sfWebRequest $request)
    {
        /** @var Shop $store */
        $store = $this->getRoute()->getObject();
        $this->form = new ShopOptionsForm($store);
        $this->proceedForm($this->form, 'settingsStoresEdit');
        
        $this->key = $store->getName();
    }
}
