<form id="new_field_category_form" method = "post" class="form"
      action="<?php echo url_for('settingsStoresEdit', $form->getObject())?>">
    <?php echo $form->renderHiddenFields() ?>

    <?php echo include_partial('dashboard/formActions',
            array('route' => 'settingsShop')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content form shop-options">
                    <p>
                        <button data-key="<?php echo $key; ?>" id="updateStoreLayout"><?php echo __('Update store layout');?></button>
                    </p>
                    <p>
                        <?php echo $form['currency_code']->renderError(); ?>
                        <?php echo $form['currency_code']->renderLabel(); ?>
                        <?php echo $form['currency_code']->render(); ?>
                    </p>
                </div>
            </div>
        </section>
    </article>
</form>
<script>
    $(document).ready(function()
    {
        $('#updateStoreLayout').on('click', function( event ) {
            event.preventDefault();

            var url = "/app.php/api/settings/stores/update/" + $(this).attr('data-key');
            $.ajax({
                url: url,
                type: "POST"
            })
            .error(function(data){
                var error = '<?php echo __("Unexpected error occured"); ?>';
                if (data.status == 400) {
                    error = '<?php echo __("Can not update store layout. Invalid url or key has been provided") ?>';
                }
                else if (data.status == 406) {
                    error = '<?php echo __("Store does not accept remote update") ?>';
                }
                notify('<b class="red-text">' + error +'</b>');
            })
            .success(function(data) {
                notify('<?php echo __("Store layout has been updated successfuly"); ?>');
            });
        });
    });

</script>