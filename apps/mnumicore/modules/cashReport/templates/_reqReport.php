<div id="reports-container">
<?php foreach($payments as $payment): ?>
    <?php
    // gets report month labels
    $paymentId = $payment->getId();
    ?>
    <div class="block-border margin-top10">
        <div class="block-content form" id="table_form">
            <h1><?php echo __('Reports') . ': ' . $payment->getLabel(); ?></h1>

            <div class="no-margin last-child">
                <div class="block-controls no-margin-bottom"></div>
                <div class="loader margin-top10 margin-bottom-10px">
                    <?php echo image_tag('/images/loader.gif'); ?>
                </div>
                <table id="reports-table_payment-<?php echo $paymentId; ?>" class="table sortable no-margin" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th scope="col" width="10%" class="sorting ">
                                <?php echo __('Year and month'); ?>
                            </th>

                            <th scope="col" class="sorting ">
                                <?php echo __('Reports'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach($cashReports[$paymentId] as $yearMonth => $daysReports): ?>
                        <tr>
                            <td>
                                <?php echo $yearMonth; ?>
                                <ul>
                                    <li>[<?php echo link_to(__('monthly report'), 'cashReportMonthlyPrint',
                                        array('paymentId' => $paymentId, 'date' => $yearMonth)); ?>]</li>
                                    <li>[<?php echo link_to(__('all documents'), 'cashReportMonthlyDocsPrint',
                                        array('paymentId' => $paymentId, 'date' => $yearMonth)); ?>]</li>
                                </ul>
                            </td>
                            <td>
                                <ul>
                                <?php foreach($daysReports as $day => $report): ?>

                                    <?php
                                        $currentReportClass = $report->getRawValue()->isCurrentReport($paymentId, isset($reportDate)?$reportDate:null) ? 'selected-a' : '';
                                    ?>
                                    <li>
                                    <a  class="filter-daily-docs-click <?php echo $currentReportClass; ?>" href="#" 
                                        data-payment-id="<?php echo $paymentId; ?>"
                                        data-date="<?php echo $yearMonth . '-' . $day; ?>">
                                            <?php echo __('day').' '.$day; ?>:
                                            <em>
                                                <?php echo $sf_user->formatCurrency($report->getDailyBalance()); ?>
                                            </em>
                                    </a>
                                    <?php echo link_to('[R]', url_for('cashReportDailyPrint', $report)); ?>;
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
<?php endforeach; ?>
</div>