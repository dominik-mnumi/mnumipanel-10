<p align=center>
    <font size="+1"><b><?php echo $reportObj; ?></b></font>
</p>
<p align=right><?php echo __('Document date'); ?>: <b><?php echo $reportObj->getDateTimeObject('created_at')->format('Y-m-d'); ?></b></p>
<p align=left>
    <?php echo sfConfig::get('app_company_data_seller_name'); ?><br />
    <?php echo sfConfig::get('app_company_data_seller_address'); ?><br />
    <?php echo sfConfig::get('app_company_data_seller_postcode'); ?> <?php echo sfConfig::get('app_company_data_seller_city'); ?>
</p>
<p align=center>
    <font size="+1"><b><?php echo __('Cash report'); ?>, <?php echo __('day'); ?>: <?php echo $reportObj->getDateTimeObject('created_at')->format('Y-m-d'); ?></b></font>
</p>
<table width="100%" border="1" cellspacing="0" cellpadding="2" align="center">
    <thead>
        <tr>
            <th style="font-size: 8pt;"><?php echo __('No.'); ?></th>
            <th style="font-size: 8pt;"><?php echo __('Date'); ?></th>
            <th style="font-size: 8pt;"><?php echo __('Document number'); ?></th>
            <th style="font-size: 8pt;"><?php echo __('Who'); ?></th>
            <th style="font-size: 8pt;"><?php echo __('Description'); ?></th>
            <th style="font-size: 8pt;"><?php echo __('Income'); ?></th>
            <th style="font-size: 8pt;"><?php echo __('Outcome'); ?></th>
            <th style="font-size: 8pt;"><?php echo __('Balance'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach($reportObj->getCashDesks() as $key => $rec): ?>
        <tr>
            <td align="center" style="font-size: 8pt;"><?php echo $key + 1; ?></td>
            <td style="font-size: 8pt;"><?php echo $rec->getDateTimeObject('created_at')->format('Y-m-d'); ?></td>
            <td style="font-size: 8pt;"><?php echo $rec->getName(); ?></td>
            <td style="font-size: 8pt;"><?php echo $rec->getType() == CashDeskTable::$IN ? $rec->getClient() : $rec->getSfGuardUser(); ?></td>
            <td style="font-size: 8pt;"><?php echo $rec->getDescription(); ?></td>
            <td style="font-size: 8pt;" align="right"><?php if($rec->getType() == CashDeskTable::$IN) { echo $sf_user->formatCurrency($rec->getValue()); }; ?></td>
            <td style="font-size: 8pt;" align="right"><?php if($rec->getType() != CashDeskTable::$IN) { echo $sf_user->formatCurrency($rec->getValue()); }; ?></td>
            <td style="font-size: 8pt;" align="right"><?php echo $sf_user->formatCurrency($rec->getBalance()); ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>
<p>
    <blockquote>
        <?php echo __('cashier signature'); ?>
        <br />
        <br />
        <br />
        ..............................
    </blockquote>
</p>