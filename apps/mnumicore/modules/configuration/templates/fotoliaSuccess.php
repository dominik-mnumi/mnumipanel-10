<form method = "post" class="form" action="<?php echo url_for('configurationFotolia'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationFotolia')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationFotolia')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_fiscal">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Fotolia'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Configuration'); ?></legend>
                            <p>
                                <?php echo $form['fotolia_api_key']->renderLabel(); ?>
                                <?php echo $form['fotolia_api_key']->render(); ?>
                                <?php echo $form['fotolia_api_key']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['fotolia_language_id']->renderLabel(); ?>
                                <?php echo $form['fotolia_language_id']->render(); ?>
                                <?php echo $form['fotolia_language_id']->renderHelp(); ?>
                            </p>
                        </fieldset>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

