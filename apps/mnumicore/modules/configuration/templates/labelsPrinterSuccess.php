<form method = "post" class="form" action="<?php echo url_for('configurationLabelsPrinter'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationLabelsPrinter')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationLabelsPrinter')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_label_printer">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Label printer'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Label printer'); ?></legend>
                            <p>
                                <?php echo $form['printlabel_name']->renderLabel(); ?>
                                <?php echo $form['printlabel_name']->render(); ?>
                                <?php echo $form['printlabel_name']->renderHelp(); ?>
                                <fieldset>
                                    <legend><?php echo __('Printer settings'); ?></legend>
                                    <p>
                                        <?php echo $form['printlabel_width']->renderLabel(); ?>
                                        <?php echo $form['printlabel_width']->render(); ?>
                                        <?php echo $form['printlabel_width']->renderHelp(); ?>
                                    </p>

                                    <p>
                                        <?php echo $form['printlabel_height']->renderLabel(); ?>
                                        <?php echo $form['printlabel_height']->render(); ?>
                                        <?php echo $form['printlabel_height']->renderHelp(); ?>
                                    </p>
                                    <p>
                                        <?php echo $form['printlabel_orientation']->renderLabel(); ?>
                                        <?php echo $form['printlabel_orientation']->render(); ?>
                                        <?php echo $form['printlabel_orientation']->renderHelp(); ?>
                                    </p>
                                </fieldset>
                            </p>

                            <p>
                                <fieldset>
                                    <?php echo __("UPS Printer settings has been moved to the carrier configuration. Open: %link% page, edit carrier and change its type to UPS.", array(
                                        '%link%' => link_to(__('Carriers'), '@settingsCarrier')
                                    )); ?>
                                </fieldset>
                            </p>
                            <div class="box">
                                <p class="mini-infos color-blue bold">
                                    <?php echo __('To operate your printer\'s software, you have to install Mnumi Printlabel software on your computer which is connected to label printer. You can download the latest version by clicking on the %link%. After successful installation, you must download the file %properties% and save it in:<br /> C:\Program Files\Mnumi Printlabel\app\config\properties.ini.',
                                            array('%link%' => '<a class="red" href="http://security.mnumi.com/PrintlabelInstaller-latest.exe" title="Mnumi Printlabel">PrintlabelInstaller-latest.exe</a>',
                                                '%properties%' => link_to('properties.ini', 
                                                        '@configurationLabelsPrinterProperties',
                                                        array('class' => 'red')))); ?>
                                    
                                </p>
                            </div>                           
                        </fieldset>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

