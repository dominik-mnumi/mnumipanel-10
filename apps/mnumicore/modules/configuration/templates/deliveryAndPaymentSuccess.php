<form method = "post" class="form" action="<?php echo url_for('configurationDeliveryAndPayment'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationDeliveryAndPayment')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationDeliveryAndPayment')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_finance">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Delivery and payment'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Delivery and payment'); ?></legend>
                            <p>
                                <?php echo $form['default_delivery']->renderLabel(); ?>
                                <?php echo $form['default_delivery']->render(); ?>
                                <?php echo $form['default_delivery']->renderHelp(); ?>
                            </p>
      
                            <p>
                                <?php echo $form['default_payment']->renderLabel(); ?>
                                <?php echo $form['default_payment']->render(); ?>
                                <?php echo $form['default_payment']->renderHelp(); ?>
                                <?php echo image_tag('loader.gif', array(
                                    'width' => 32, 
                                    'height' => 32,
                                    'class' => 'default-payment-img',
                                    'style' => 'display: none;')); ?>
                            </p>

                            <p>
                                <?php echo $form['default_delivery_days']->renderLabel(); ?>
                                <?php echo $form['default_delivery_days']->render(); ?>
                                <?php echo $form['default_delivery_days']->renderHelp(); ?>
                            </p>
                        </fieldset>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

<script type="text/javascript">
jQuery(document).ready(function($) 
{       
    // carrier change
    $("#ConfigurationDeliveryAndPaymentForm_default_delivery").change(
    function()
    {
        $("#ConfigurationDeliveryAndPaymentForm_default_payment").hide();
        $(".default-payment-img").show();
        
        $.ajax(
        {
            type: "GET",
            url: "<?php echo url_for('configurationGetPayments'); ?>" + "?carrierId=" + $(this).val(),
            dataType: 'json',
            error: function(xhr, ajaxOptions, thrownError)
            {
                alert('<?php echo __('There was error. Please try again later.') ?>');
            },
            success: function(dataOutput)
            {            
                var currentPaymentId = $("#ConfigurationDeliveryAndPaymentForm_default_payment").val(); 
                var html = '';
                for(i in dataOutput)
                {   
                    var selectValue = (i == currentPaymentId) ? ' selected="selected"' : '';
                    
                    html = html + '<option' + selectValue + ' value="' + i + '">' 
                                            + dataOutput[i] + '</option>\n';
                }
   
                $("#ConfigurationDeliveryAndPaymentForm_default_payment").html(html);
                $("#ConfigurationDeliveryAndPaymentForm_default_payment").show();
                $(".default-payment-img").hide();
            }
        });
    });
    
    // trigger event on load
    $("#ConfigurationDeliveryAndPaymentForm_default_delivery").change();
});    

</script>