<form method = "post" class="form" action="<?php echo url_for('configurationRegionalSettings'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationRegionalSettings')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationRegionalSettings')); ?>
                </div>        
            </div>
        </section>
        
        <section class="grid_8">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>

                    <div id="conf_finance">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Regional settings'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Regional settings'); ?></legend>
                            <p>
                                <?php echo $form['default_currency']->renderLabel(); ?>
                                <?php echo $form['default_currency']->render(); ?>
                                <?php echo $form['default_currency']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['price_tax']->renderLabel(); ?>
                                <?php echo $form['price_tax']->render(); ?>
                                <?php echo $form['price_tax']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['default_culture']->renderLabel(); ?>
                                <?php echo $form['default_culture']->render(); ?>
                                <?php echo $form['default_culture']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['default_country']->renderLabel(); ?>
                                <?php echo $form['default_country']->render(); ?>
                                <?php echo $form['default_country']->renderHelp(); ?>
                            </p>

                            <p>
                                <?php echo $form['default_export_encoding']->renderLabel(); ?>
                                <?php echo $form['default_export_encoding']->render(); ?>
                                <?php echo $form['default_export_encoding']->renderHelp(); ?>
                            </p>
                        </fieldset>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

