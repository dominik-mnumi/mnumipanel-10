<form method = "post" class="form" action="<?php echo url_for('configurationCompanyData'); ?>"
      enctype="multipart/form-data">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => 'configurationCompanyData')); ?>
    <?php echo include_partial('dashboard/message'); ?>

    <article class="container_12">
        <?php if($fileWritable && $fileSettingsWritable): ?>
        <section class="grid_4">
            <div class="block-border">
                <div class="block-content">
                    <h1><?php echo __('Configuration'); ?></h1>

                    <!-- navigation -->
                    <?php include_partial('navigation', array('route' => 'configurationCompanyData')); ?>
                </div>        
            </div>
        </section>
        <?php endif; ?>

        
        <section class="grid_<?php echo ($fileWritable && $fileSettingsWritable) ? 8 : 12; ?>">
            <div class="block-border">
                <div class="block-content">
                    <?php if(!$fileWritable): ?>
                    <div class="text-center">
                        <h3 class="red">
                            <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $appFilePath)); ?>
                        </h3>
                    </div>
                    <?php elseif(!$fileSettingsWritable): ?>
                        <div class="text-center">
                            <h3 class="red">
                                <?php echo __("You don't have permissions to change config file. Please change it by calling \"chmod 777 %file%\"", array('%file%' => $settingsFilePath)); ?>
                            </h3>
                        </div>
                    <?php else: ?>
                    <?php include_partial('dashboard/formErrors',
                            array('form' => $form)); ?>

                    <?php echo $form->renderGlobalErrors(); ?>
                    <?php echo $form->renderHiddenFields(); ?>
           
                    <div id="conf_company_name">
                        <h2 class="bigger margin-bottom-10px"><?php echo __('Company data'); ?></h2>
                        <hr />
                        <fieldset>
                            <legend><?php echo __('Company data'); ?></legend>
                            <p>
                                <?php echo $form['company_data_seller_name']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_name']->render(); ?>
                                <?php echo $form['company_data_seller_name']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['company_data_seller_address']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_address']->render(); ?>
                                <?php echo $form['company_data_seller_address']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['company_data_seller_postcode']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_postcode']->render(); ?>
                                <?php echo $form['company_data_seller_postcode']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['company_data_seller_city']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_city']->render(); ?>
                                <?php echo $form['company_data_seller_city']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['company_data_seller_country']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_country']->render(); ?>
                                <?php echo $form['company_data_seller_country']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['company_data_seller_tax_id']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_tax_id']->render(); ?>
                                <?php echo $form['company_data_seller_tax_id']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['company_data_seller_bank_name']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_bank_name']->render(); ?>
                                <?php echo $form['company_data_seller_bank_name']->renderHelp(); ?>
                            </p>
                            <p>
                                <?php echo $form['company_data_seller_bank_account']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_bank_account']->render(); ?>
                                <?php echo $form['company_data_seller_bank_account']->renderHelp(); ?>
                            </p>

                        </fieldset>

                        <fieldset>
                            <legend><?php echo __('Company logo'); ?></legend>
                            <p>
                                <?php echo $form['company_data_seller_logo_width']->renderLabel(); ?>
                                <?php echo $form['company_data_seller_logo_width']->render(); ?>
                            </p>
                            <p>
                                <?php echo $form['company_data_seller_logo_image']->render(); ?>
                                <?php echo $form['company_data_seller_logo_image']->renderHelp(); ?>
                            </p>
                        </fieldset>
                    </div>

                    <?php endif; ?>
                </div>
            </div>
        </section>
    </article>
</form>

