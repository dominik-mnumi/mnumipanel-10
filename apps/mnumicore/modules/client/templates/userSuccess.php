<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form no_top_padding" id="table_form">
                <h1>
                    <?php echo __('Customer users'); ?>: <?php echo $clientObj->getFullname(); ?>
                    <?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientTraderEdit)): ?>
                    <a id="dialog-add-new-user-click" href="#">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add attachment')  ?>"> <?php echo __('add'); ?>
                    </a>
                    <?php endif; ?>
                </h1>
                <div></div>
                <?php include_partial('client/edit/tabList', array(
                    'clientObj' => $clientObj,
                    'defaultDisableAdvancedAccountancy' => $defaultDisableAdvancedAccountancy)); ?>
                <div style="clear: both;">&nbsp;</div>
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>

<?php if($sf_user->hasCredential(sfGuardPermissionTable::$clientTraderEdit)): ?>
<?php echo include_partial('client/edit/form/userNewForm', array('form' => $formArray[0])); ?>
<?php endif; ?>

<?php echo include_partial('javascript'); ?>