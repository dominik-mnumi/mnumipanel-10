<?php foreach($notices as $clientNoticeObj): ?>
<ul class="message success grid_12">
    <li>
        <?php echo $clientNoticeObj->getNotice(); ?>
    </li>
    <li class="close-bt-order"></li>
    <input type="hidden" value="<?php echo $clientNoticeObj->getId(); ?>" />
</ul>
<?php endforeach; ?>

<script type="text/javascript">
jQuery(document).ready(function($) 
{
    /**
     * Deletes client notice.
     */
    $(".close-bt-order").live('click', function()
    {
        var selector = $(this);
        
        $.modal({
            content: '<br /><h3 class="align-center"><?php echo __('Are you sure?'); ?></h3>',
            title: '<?php echo __('Delete reminder'); ?>',
            maxWidth: 300,
            buttons: {
                '<?php echo __('Confirm'); ?>': function(win) 
                {
                    var clientNoticeId = selector.parents("ul").find("input").val();

                    $.post('/clientNotice/delete/' + clientNoticeId, {},
                    function(html)
                    {
                        selector.parents("ul").remove();
                        win.closeModal(); 
                    });  
                },
                '<?php echo __('Close'); ?>': function(win) 
                {         
                    win.closeModal(); 
                }
            }
        });
    });
});
</script>