<form id="dialog-add-new-user-form" action="<?php echo url_for('reqEditClientNewUserForm', 
        array('model' => 'ClientUser', 
            'type' => 'new')); ?>" enctype="multipart/form-data" method="post" style="display: none;">
    <div title="<?php echo __('User'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <?php echo $form['id']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">    
                <p class="required">
                    <label><?php echo __('Customer'); ?></label>               
                    <?php echo $form->getObject()->getClient()->getFullname(); ?>
                </p> 
                <p class="required">                
                    <?php echo $form['user']->renderLabel(); ?>
                    <?php echo $form['user']->render(); ?>
                    <?php echo $form['user_id']->render(); ?>
                    <?php echo $form['client_id']->render(); ?>
                </p>    
                <p class="required">
                    <?php echo $form['client_user_permission_id']->renderLabel(); ?>
                    <?php echo $form['client_user_permission_id']->render(); ?>
                </p>  
            </div>
        </div>
    </div>
</form>

