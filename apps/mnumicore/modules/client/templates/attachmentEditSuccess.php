<form id="dialog-add-edit-attachment-form" action="<?php echo url_for('attachmentListEdit', $attachmentObj); ?>" enctype="multipart/form-data" method="post">
    <?php echo include_partial('dashboard/formActions', 
            array('route' => '@attachmentList?id='.$attachmentObj->getClient()->getId())); ?>
    <?php echo include_partial('dashboard/message'); ?>
    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content form">
                    <h1>
                        <?php echo __('Attachment'); ?>: <?php echo $attachmentObj->getFilename(); ?>
                    </h1>
                    <?php if($formArray[0]->hasErrors()): ?>
                    <?php foreach($formArray[0]->getFormFieldSchema() as $formField): ?>
                    <?php echo __($formField->renderError()); ?>
                    <?php endforeach; ?>
                    <?php endif ?>
                    <?php include_partial('client/edit/form/attachmentEditForm', array('form' => $formArray[0])); ?>               
                </div>
            </div>
        </section>
    </article>
</form>    
