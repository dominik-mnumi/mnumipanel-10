<?php

/**
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */ 
class settingBulkOperationForm extends sfForm
{
    public function configure()
    {
        $choices = array(
            'new' => 'Add pricelist to the settings',
            'exist' => 'Update pricelist in the settings',
        );

        $this->setWidget('operation', new sfWidgetFormSelectRadioSingleable(array(
            'choices' => $choices,
        )));

        $this->setValidator('operation', new sfValidatorChoice(array(
            'choices' => array_keys($choices),
        )));

        $this->widgetSchema->setNameFormat('settingBulkOperation[%s]');
    }

}
