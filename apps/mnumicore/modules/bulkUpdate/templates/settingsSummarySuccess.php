<article class="container_12 bulk_update">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content">
                <form action="" method="post">
                <h1>
                    <?php echo __('Bulk update'); ?>
                    <?php echo sprintf(__('Step %s of %s'), 3, 3); ?>:
                    <?php echo __('Conform'); ?>

                </h1>
                <p>
                    <?php echo __('Please confirm the details of this operation.'); ?>
                </p>

                <p>
                    <input type="submit" value="<?php echo __('Conform'); ?>" />
                    <?php echo link_to(__('Cancel'), '@settingsFieldItemBulkUpdateCancel'); ?>
                </p>

                <table class="table">
                    <tbody>
                    <?php if($bulkAction == 'editNew'): ?>
                    <tr>
                        <th style="min-width: 200px;"><?php echo __('Action'); ?></th>
                        <td>
                            <?php echo sprintf(__('Relate pricelist <strong>%s</strong> for the settings'), $pricelist->getName()); ?>
                        </td>
                    </tr>
                    <tr>
                        <th style="min-width: 200px;"><?php echo __('Values'); ?></th>
                        <td>
                            <?php if(isset($prices['minimal_price'])): ?>
                            <p><?php echo __('Minimal price'); ?>: <?php echo $prices['minimal_price']; ?></p>
                            <?php endif; ?>
                            <?php if(isset($prices['maximal_price'])): ?>
                            <p><?php echo __('Maximal price'); ?>: <?php echo $prices['maximal_price']; ?></p>
                            <?php endif; ?>
                            <p>
                                <?php foreach($prices['type'] as $type => $items): ?>
                                <h4><?php echo __(MeasureUnitTable::getInstance()->getPrice($type)); ?></h4>
                                <ul>
                                <?php foreach($items as $price): ?>
                                    <?php if(isset($price['price'])): ?>
                                        <?php if($price['price'] == 0) continue; ?>
                                        <li>
                                            <?php echo $price['quantity']; ?>:
                                            <?php echo (strpos($price['price'], '%') === false) ?
                                                $sf_user->formatCurrency($price['price']) : $price['price'];
                                            ?>
                                        </li>
                                    <?php else: ?>
                                        <?php if($price['price_simplex'] == 0) continue; ?>
                                        <li>
                                            <?php echo $price['quantity']; ?>:
                                            <?php echo (strpos($price['price_simplex'], '%') === false) ?
                                                $sf_user->formatCurrency($price['price_simplex']) : $price['price_simplex'];
                                            ?>,
                                            <?php echo (strpos($price['price_duplex'], '%') === false) ?
                                                $sf_user->formatCurrency($price['price_duplex']) : $price['price_duplex'];
                                            ?>
                                        </li>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                                </ul>
                                <?php endforeach; ?>
                            </p>
                        </td>
                    </tr>
                    <?php elseif($bulkAction == 'editExist'): ?>
                    <tr>
                        <th style="min-width: 200px;"><?php echo __('Action'); ?></th>
                        <td>
                            <?php echo sprintf(__('Update pricelist <strong>%s</strong> for the settings'), $pricelist->getName()); ?>
                        </td>
                    </tr>
                    <tr>
                        <th style="min-width: 200px;">Values</th>
                        <td>
                            <p><?php echo __('Change'); ?>: <?php echo $prices; ?>%</p>
                        </td>
                    </tr>
                    <?php elseif($bulkAction == 'delete'): ?>
                    <tr>
                        <th style="min-width: 200px;"><?php echo __('Action'); ?></th>
                        <td>
                            <?php echo sprintf(__('Delete pricelist <strong>%s</strong> from the settings'), $pricelist->getName()); ?>
                        </td>
                    </tr>
                    <?php endif; ?>
                    </tbody>
                </table>

                <table class="table full-width">
                    <thead>
                    <tr>
                        <th><?php echo __('Number'); ?></th>
                        <th><?php echo __('Name'); ?></th>
                        <th><?php echo __('Label'); ?></th>
                        <th><?php echo __('Cost'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($fieldItems as $item): ?>
                    <tr>
                        <td><?php echo $item->getId(); ?></td>
                        <td><?php echo $item->getName(); ?></td>
                        <td><?php echo $item->getLabel(); ?></td>
                        <td><?php echo $item->getCost(); ?></td>
                    </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>

                <p>
                    <input type="submit" value="<?php echo __('Conform'); ?>" />
                    <?php echo link_to(__('Cancel'), '@settingsFieldItemBulkUpdateCancel'); ?>
                </p>
                </form>
            </div>
        </div>
    </section>
</article>