<article class="container_12 bulk_update">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content">
                <form action="" method="post">
                <h1>
                    <?php echo __('Bulk update'); ?>
                    <?php echo sprintf(__('Step %s of %s'), 1, 3); ?>:
                    <?php echo __('Choose Operation'); ?>

                </h1>
                <p>
                    <?php echo sprintf(__('Choose the operation you wish to perform on the selected <strong>%d</strong> setting(s).'),
                        $nb); ?>
                </p>

                <div>
                <?php echo $form; ?>
                </div>

                <p>
                    <input type="submit" value="<?php echo __('Next'); ?> >>" />
                    <?php echo link_to(__('Cancel'), '@settingsFieldItemBulkUpdateCancel'); ?>
                </p>
                </form>
            </div>
        </div>
    </section>
</article>