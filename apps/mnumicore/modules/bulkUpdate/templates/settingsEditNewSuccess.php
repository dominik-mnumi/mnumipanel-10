<article class="container_12 bulk_update">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content">
                <form action="" method="post">
                <h1>
                    <?php echo __('Bulk update'); ?>
                    <?php echo sprintf(__('Step %s of %s'), 2, 3); ?>:
                    <?php echo __('Add pricelist to settings'); ?>

                </h1>

                <h3>
                    <?php echo __(sprintf('Add pricelist <strong>%s</strong> for the <strong>%d</strong> setting(s)',
                        $pricelist, $count)); ?>:
                </h3>

                <?php echo $form->renderGlobalErrors(); ?>

                <div id="tab-pricelist-<?php echo $pricelist->getId(); ?>">
                    <?php include_partial('settings/field_item_price_'.$type, array(
                        'id' => $pricelist->getId(),
                        'itemPrice' => ($type != 'other')
                            ? array(
                                array(
                                    'quantity' => ($measureUnit->getName() == 'Square metre') ? 0 : 1,
                                    'price' => '',
                                    'price_duplex' => '',)
                            )
                            : array(
                                array(),array(),array(),array(),array(),
                            ),
                        'item' => array('min' => '', 'max' => ''),
                    )); ?>
                </div>

                <p>
                    <input type="submit" value="<?php echo __('Next'); ?> >>" />
                    <?php echo link_to(__('Cancel'), '@settingsFieldItemBulkUpdateCancel'); ?>
                </p>

                </form>
            </div>
        </div>
    </section>
</article>

<?php include_partial('settings/create_javascript'); ?>