<?php

/**
 * bulkUpdate actions.
 *
 * @package    mnumicore
 * @subpackage bulkUpdate
 * @author     Piotr Plenik <piotr.plenik@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bulkUpdateActions extends sfActions
{
    /**
    * Validation selected options and redirect to executeSettingsChooseAction
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    {
        $selected = $request->getParameter('selected', array());

        if(count($selected) == 0)
        {
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Please choose setting.',
                'messageType' => 'msg_error',
            ));
            $this->redirect('@settings');
        }

        $this->getUser()->setAttribute('selectedFieldItems', $selected, 'bulk');

        $this->redirect('@settingsFieldItemBulkUpdateAction');
    }

    /**
     * Display list of posible bulk actions
     *
     * @param sfRequest $request A request object
     */
    public function executeSettingsChoose(sfWebRequest $request)
    {
        $selected = $this->getUser()->getAttribute('selectedFieldItems', array(), 'bulk');

        if(count($selected) == 0)
        {
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Please choose setting.',
                'messageType' => 'msg_error',
            ));
            $this->redirect('@settings');
        }

        $items = FieldItemTable::getInstance()->findByIds($selected);

        $this->nb = count($items);

        $this->getUser()->setAttribute('type', $items[0]->getFieldset()->getType(), 'bulk');

        $this->form = new settingBulkUpdateForm(array(), array(
            'selected' => $selected,
        ));

        if($request->isMethod('post') && $request->hasParameter($this->form->getName()))
        {
            $this->form->bind($request->getParameter($this->form->getName()));
            if($this->form->isValid())
            {
                $this->getUser()->setAttribute('pricelist', $this->form->getValue('pricelist'), 'bulk');

                // redirect to propel action
                return $this->redirect('@'.$this->form->getValue('action'));
            }
        }
    }

    public function executeSettingsDelete(sfWebRequest $request)
    {
        $this->getUser()->setAttribute('action', 'delete', 'bulk');
        return $this->redirect('@settingsFieldItemBulkUpdateSummary');
    }

    /**
     * Display list of settings:
     * - with no selected pricelist
     * - with selected pricelist
     *
     * If only one group available - redirect to propel action
     * .
     * @param sfWebRequest $request
     */
    public function executeSettingsEditPricelist(sfWebRequest $request)
    {
        $selected = $this->getUser()->getAttribute('selectedFieldItems', array(), 'bulk');
        $pricelistId = $this->getUser()->getAttribute('pricelist', null, 'bulk');
        $pricelist = PricelistTable::getInstance()->findOneById($pricelistId);

        // validation

        if(count($selected) == 0)
        {
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Please choose setting.',
                'messageType' => 'msg_error',
            ));
            $this->redirect('@settings');
        }

        if(!$pricelist instanceof Pricelist)
        {
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Pricelist not selected.',
                'messageType' => 'msg_error',
            ));
            $this->redirect('@settingsFieldItemBulkUpdateAction');
        }

        $this->pricelist = $pricelist;

        $this->type = $this->getUser()->getAttribute('type', '', 'bulk');

        $withoutAssetPricelistQuery = FieldItemTable::getInstance()
            ->getNotAssetToPricelistQuery($pricelistId, $selected);

        $withAssetPricelistQuery = FieldItemTable::getInstance()
            ->getAssetToPricelistQuery($pricelistId, $selected);

        // if selected only assets with selected pricelist
        if($withAssetPricelistQuery->count() == count($selected))
        {
            $selected = array();
            foreach($withAssetPricelistQuery->execute() as $fieldItem)
            {
                $selected[] = $fieldItem->getId();
            }

            $this->getUser()->setAttribute('selectedFieldItems', $selected, 'bulk');

            return $this->redirect('@settingsFieldItemBulkUpdateEditExist');
        }

        // if selected only assets with NOT selected pricelist
        if($withoutAssetPricelistQuery->count() == count($selected))
        {
            $selected = array();
            foreach($withoutAssetPricelistQuery->execute() as $fieldItem)
            {
                $selected[] = $fieldItem->getId();
            }

            $this->getUser()->setAttribute('selectedFieldItems', $selected, 'bulk');

            return $this->redirect('@settingsFieldItemBulkUpdateEditNew');
        }

        // if selected have mixed settings (with and without associated pricelist)

        $this->form = new settingBulkOperationForm();

        if($request->isMethod('post') && $request->hasParameter($this->form->getName()))
        {
            $this->form->bind($request->getParameter($this->form->getName()));
            if($this->form->isValid())
            {
                $type = ucfirst($this->form->getValue('operation'));

                $selected = array();

                $query = ($type == 'New') ? $withoutAssetPricelistQuery : $withAssetPricelistQuery;

                foreach($query->execute() as $fieldItem)
                {
                    $selected[] = $fieldItem->getId();
                }

                $this->getUser()->setAttribute('selectedFieldItems', $selected, 'bulk');
                return $this->redirect('@settingsFieldItemBulkUpdateEdit'.$type);
            }
        }

        $this->fieldItemsWithoutPricelist = $withoutAssetPricelistQuery->execute();
        $this->fieldItemsWithPricelist = $withAssetPricelistQuery->execute();
    }


    public function executeSettingsEditNew(sfWebRequest $request)
    {
        $selected = $this->getUser()->getAttribute('selectedFieldItems', array(), 'bulk');

        $pricelistId = $this->getUser()->getAttribute('pricelist', null, 'bulk');
        $pricelist = PricelistTable::getInstance()->findOneById($pricelistId);

        if(!$pricelist instanceof Pricelist)
        {
            $this->getUser()->setFlash('info_data', array(
                'message' => 'Pricelist not selected.',
                'messageType' => 'msg_error',
            ));
            $this->redirect('@settingsFieldItemBulkUpdateAction');
        }

        $this->type = $this->getUser()->getAttribute('type', '', 'bulk');

        $pricelistQuery = FieldItemTable::getInstance()
            ->getNotAssetToPricelistQuery($pricelistId, $selected);

        $this->count = $pricelistQuery->count();

        $this->pricelist = $pricelist;

        $firstObj = $pricelistQuery->limit(1)->fetchOne();

        if(!$firstObj)
        {
            throw new Exception('Could not find any setting');
        }

        $this->measureUnit = $firstObj->getMeasureUnit();

        $form_name = SettingsForm::getFieldItemFormClassName($firstObj->getFieldset());
        $this->form = new $form_name($firstObj);

        if($request->isMethod('post'))
        {
            $pricelistArr = $this->getRequest()->getParameter('pricelist', array());
            $this->form->validatePrices($pricelistArr);

            if(count($this->form->getErrorSchema()->getErrors()) == 0)
            {
                $this->getUser()->setAttribute('action', 'editNew', 'bulk');
                $this->getUser()->setAttribute('prices', $pricelistArr, 'bulk');

                return $this->redirect('@settingsFieldItemBulkUpdateSummary');
            }
        }
    }

    public function executeSettingsEditExist(sfWebRequest $request)
    {
        $selected = $this->getUser()->getAttribute('selectedFieldItems', array(), 'bulk');
        $pricelistId = $this->getUser()->getAttribute('pricelist', null, 'bulk');
        $pricelist = PricelistTable::getInstance()->findOneById($pricelistId);


        $pricelistQuery = FieldItemTable::getInstance()
            ->getAssetToPricelistQuery($pricelistId, $selected);

        $this->form = new settingBulkEditForm();
        $this->pricelist = $pricelist;
        $this->count = $pricelistQuery->count();

        if($request->isMethod('post') && $request->hasParameter($this->form->getName()))
        {
            $this->form->bind($request->getParameter($this->form->getName()));
            if($this->form->isValid())
            {
                $this->getUser()->setAttribute('action', 'editExist', 'bulk');
                $this->getUser()->setAttribute('prices', $this->form->getValue('price_change'), 'bulk');

                return $this->redirect('@settingsFieldItemBulkUpdateSummary');
            }
        }
    }

    public function executeSettingsSummary(sfWebRequest $request)
    {
        $this->bulkAction = $this->getUser()->getAttribute('action', null, 'bulk');
        $this->pricelistId = $this->getUser()->getAttribute('pricelist', null, 'bulk');

        $prices = $this->getUser()->getAttribute('prices', null, 'bulk');
        $this->prices = (is_array($prices)) ? $prices[$this->pricelistId] : $prices;

        $this->pricelist = PricelistTable::getInstance()->findOneById($this->pricelistId);

        $selected = $this->getUser()->getAttribute('selectedFieldItems', array(), 'bulk');

        $this->fieldItems = FieldItemTable::getInstance()->findByIds($selected);

        if(count($this->fieldItems) == 0)
        {
            throw new Exception('Could not find any setting');
        }


        $firstObj = $this->fieldItems[0];

        $this->measureUnit = $firstObj->getMeasureUnit();

        if($request->isMethod('post'))
        {
            $methodName = 'save'.ucfirst($this->bulkAction);

            $this->$methodName($this->fieldItems, $prices, $this->pricelistId);

            $this->getUser()->setFlash('info_data', array(
                'message' => 'Saved successfull.',
                'messageType' => 'msg_success',
            ));
            $this->redirect('@settings');
        }
    }

    protected function saveEditExist($items, $values, $pricelistId)
    {
        if(!is_numeric($values))
        {
            throw new Exception('Wrong value of percent.');
        }

        foreach($items as $item)
        {
            $fieldItemPrice = $item->getFieldItemPrice($pricelistId);

            if(!$fieldItemPrice)
            {
                throw new Exception('Field item ' . $item->getId() . ' does not have pricelist id ' . $pricelistId);
            }

            foreach($fieldItemPrice->getFieldItemPriceQuantities() as $quantity)
            {
                $newAmount = ($quantity->getType() == 'amount')
                    ? round($quantity->getPrice() + ($values * $quantity->getPrice() / 100), 2)
                    : $quantity->getPrice() + $values;

                $quantity->setPrice($newAmount);
                $quantity->save();
            }
        }
    }

    protected function saveDelete($items, $values, $pricelistId)
    {
        if(!is_numeric($values))
        {
            throw new Exception('Wrong value of percent.');
        }

        foreach($items as $item)
        {
            $fieldItemPrice = $item->getFieldItemPrice($pricelistId);

            if(!$fieldItemPrice)
            {
                throw new Exception('Field item ' . $item->getId() . ' does not have pricelist id ' . $pricelistId);
            }

            $fieldItemPrice->delete();
        }
    }

    protected function saveEditNew($items, $values, $pricelistId)
    {
        $firstObj = $items[0];

        $form_name = SettingsForm::getFieldItemFormClassName($firstObj->getFieldset());

        foreach($this->fieldItems as $obj)
        {
            $form = new $form_name($obj);
            $form->setOption('pricelistArr', $values);
            $form->savePricelist();
        }
    }

    public function executeSettingsCancel(sfWebRequest $request)
    {
        // cleanup settings
        $this->getUser()->setAttribute('selectedFieldItems', array(), 'bulk');
        $this->getUser()->setAttribute('pricelist', null, 'bulk');
        $this->getUser()->setAttribute('price_change', null, 'bulk');
        $this->getUser()->setAttribute('action', null, 'bulk');
        $this->getUser()->setAttribute('prices', null, 'bulk');


        // back to settings page
        $this->redirect('@settings');
    }


}
