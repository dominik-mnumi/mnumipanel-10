<br />
<?php if($changePassForm->hasGlobalErrors()): ?>
    <p class="message error no-margin"><?php echo $changePassForm->renderGlobalErrors(); ?></p>
<?php endif; ?>
<?php echo $changePassForm['password']->renderError(); ?>    
<?php echo $changePassForm['password_again']->renderError(); ?>    
<form class="form with-margin" name="login-form" id="password-recovery"  action="" method="post">    
    <?php echo $changePassForm->renderHiddenFields(); ?> 
    <p class="inline-small-label">
        <?php echo $changePassForm['password']->renderLabel(null, array('class' => 'big')); ?>
        <?php echo $changePassForm['password']->render(array('class' => 'full-width')); ?>
    </p>
    
    <p class="inline-small-label">
        <?php echo $changePassForm['password_again']->renderLabel(null, array('class' => 'big')); ?>
        <?php echo $changePassForm['password_again']->render(array('class' => 'full-width')); ?>
    </p>    
    <button type="submit" class="float-right"><?php echo __('Change'); ?></button>   
    <br />
</form>  