<form class="form" id="password-recovery" method="post" action="">
    <fieldset class="grey-bg no-margin <?php echo ($forgotPassForm->isBound()) ? '' : 'collapsed' ; ?>">
        <?php echo $forgotPassForm->renderHiddenFields(); ?>
        <legend><a href="#"><?php echo __('Lost password?') ?></a></legend>
        <?php echo __($forgotPassForm['email_address']->renderError()); ?>
        <p class="input-with-button">
            <?php echo $forgotPassForm['email_address']->renderLabel(); ?>
            <?php echo $forgotPassForm['email_address']->render(); ?>
            <button type="submit"><?php echo __('Send'); ?></button>
        </p>
    </fieldset>
</form>