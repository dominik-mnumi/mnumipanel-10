<?php

require_once dirname(__FILE__) . '/../lib/BasesfGuardForgotPasswordActions.class.php';

/**
 * sfGuardForgotPassword actions.
 * 
 * @package    sfGuardForgotPasswordPlugin
 * @subpackage sfGuardForgotPassword
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 12534 2008-11-01 13:38:27Z Kris.Wallsmith $
 */
class sfGuardForgotPasswordActions extends BasesfGuardForgotPasswordActions
{    
    public function executeNewPassword($request)
    {
        $this->forward404unless($request->hasParameter('token'));

        $token = $request->getParameter('token');

        $forgotPassObj = sfGuardForgotPasswordTable::getInstance()->getActive($token);
        $this->forward404unless($forgotPassObj);
        
        // gets user object
        $userObj = $forgotPassObj->getUser();
        $this->forward404unless($userObj);

        $changePassForm = new myGuardChangeUserPasswordForm($userObj);
        if($request->isMethod('post'))
        {
            $changePassForm->bind($request->getParameter('change_password'));

            if($changePassForm->isValid())
            {
                $changePassForm->save();
                $this->deleteOldUserForgotPasswordRecords($userObj);

                $this->getUser()->setFlash('notice', 'Thank you. Your password has been changed successfully.');
                return $this->redirect('@homepage');
            }
        }

        //view objects
        $this->changePassForm = $changePassForm;
    }

    protected function getUserForgotPasswordRecord($userObj, $token)
    {
        return Doctrine_Core::getTable('sfGuardForgotPassword')
            ->createQuery('p')
            ->where('p.user_id = ?', $userObj->getId())
            ->addWhere('p.unique_key = ?', $token)
            ->addWhere('p.expires_at >= ?', date("Y-m-d G:i:s", time()))
            ->execute()
            ->getFirst();
    }

    protected function deleteOldUserForgotPasswordRecords($userObj)
    {
        Doctrine_Core::getTable('sfGuardForgotPassword')
            ->createQuery('p')
            ->delete()
            ->where('p.user_id = ?', $userObj->getId())
            ->execute();
    }
}
