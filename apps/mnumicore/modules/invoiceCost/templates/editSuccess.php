<?php echo include_partial('dashboard/message'); ?>
<article class="container_12">
    <section class="grid_12">
        <div class="block-border">
            <div class="block-content form" id="table_form">
                <h1>
                    <?php echo __('Invoice cost'); ?>
                    <a id="dialog-add-client-click" href="#">
                        <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add invoice cost')  ?>"> <?php echo __('add'); ?>
                    </a>
                </h1>                
                <?php include_partial('global/table', array('tableOptions' => $tableOptions)) ?>
            </div>
        </div>
    </section>
</article>
<?php echo include_partial('addInvoice', array('addInvoiceForm' => $addInvoiceForm)); ?>
<script type="text/javascript">
$(document).ready(function() {
    $('.float-left:first').append('<button type="button" id="payBtn"><?php echo __("Pay selected"); ?></button>');
    $('#payBtn').click(function(e)
    {
        e.preventDefault();
        url = "<?php echo url_for('invoiceCostPaySelected'); ?>";
        var form = document.createElement("form");
        form.setAttribute("method", 'POST');
        form.setAttribute("action", url);

            $('#table_form').find('input:checkbox[name="selected[]"]:checked').each(function()
            {
                var hiddenField = document.createElement("input");
                hiddenField.setAttribute("type", "hidden");
                hiddenField.setAttribute("name", "invoices[]");
                hiddenField.setAttribute("value", $(this).val());
                form.appendChild(hiddenField);
            });        

        document.body.appendChild(form);
        form.submit();
    });
    
    $('#dialog-add-client-click').click(
        function () {
       $.modal({
            content: $("#addClientForm"),
            title: "<?php echo __('Add invoice cost'); ?>",
            maxWidth: 700,
            maxHeight: 480,
            resizable: false,
            onOpen: function()
            {
            	$('.datepicker').datepicker("destroy");
            	$('.datepicker').removeClass("hasDatepicker");
            	$('.modal-content .ui-datepicker-trigger').remove();
            	
            	$('.datepicker').datepicker({
                    alignment: 'bottom',
                    dateFormat: 'yy-mm-dd',
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    showOn: "button",
                    buttonImage: "/images/icons/fugue/calendar-month.png",
                    buttonImageOnly: true,
                    renderer: {
                        picker: '<div class="datepick block-border clearfix form"><div class="mini-calendar clearfix">' +
                        '{months}</div></div>',
                        monthRow: '{months}',
                        month: '<div class="calendar-controls" style="white-space: nowrap">' +
                        '{monthHeader:M yyyy}' +
                        '</div>' +
                        '<table cellspacing="0">' +
                        '<thead>{weekHeader}</thead>' +
                        '<tbody>{weeks}</tbody></table>',
                        weekHeader: '<tr>{days}</tr>',
                        dayHeader: '<th>{day}</th>',
                        week: '<tr>{days}</tr>',
                        day: '<td>{day}</td>',
                        monthSelector: '.month',
                        daySelector: 'td',
                        rtlClass: 'rtl',
                        multiClass: 'multi',
                        defaultClass: 'default',
                        selectedClass: 'selected',
                        highlightedClass: 'highlight',
                        todayClass: 'today',
                        otherMonthClass: 'other-month',
                        weekendClass: 'week-end',
                        commandClass: 'calendar',
                        commandLinkClass: 'button',
                        disabledClass: 'unavailable'
                    }
                });

            	//copy all datepicker inputs from base form to modal window
            	$('.datepicker').change(function() {
            		$('.modal-content #'+$(this).attr('id')).val($('#'+$(this).attr('id')).val());
                });
            },
            buttons: 
                {
                '<?php echo __('Save'); ?>': function(win) 
                {
                    var form = $(".modal-content #addClientForm");
                    var dataInput = form.serialize();
                    var dataOutput;
                    var uri =  form.attr('action');
                    var method = form.attr('method');
                    $.ajax({
                        type: method,
                        url: uri,
                        data: dataInput,
                        dataType: 'json',
                        error: function(xhr, ajaxOptions, thrownError)
                        {
                            alertModal('<?php echo __('An error occurred'); ?>');
                        },
                        success: function(dataOutput)
                        {
                        	if('success' == dataOutput.result.status)
                            {         
                                if(0 != dataOutput.result.error.errorArray)
                                {
                                    var errorlist = renderError(dataOutput.result.error.errorArray);

                                    $(".modal-content #addClientForm .validateTips").html(errorlist);
                                }
                                else
                                {
                                    win.closeModal();
                                    
                                    var href = "<?php echo url_for('invoiceCost'); ?>";
                                    window.location.href = href;               
                                }
                            }
                        }
                    });
                },
                'Close': function(win) 
                {
                	//remove values from datepicker
                	$('.datepicker').val('');
                	
                    win.closeModal();
                }
            }
        });
        }
    );
});
</script>
