<style>

    table {
        font-size: 7px;
        font-weight: 1;
        width: 100%;
        text-align: center;
        border-collapse: collapse;
    }

    table tr {
        height: 43px;
        overflow: hidden;
    }

    table td {
        padding: 1em;
        width: 5%;
        border: 1px solid black;
    }

    table th {
        border: 1px solid black;
        background-color: #CCC;
        padding: 4em 0.5em;
    }

    h1 {
        text-align: center;
        font-size: 15px;
    }

    .lp {
        width: 3%;
    }

    .no-border {
        border: 0px solid black;
    }

    .report-sum {
        padding: 2em 0;
        background-color: #CCC;
        font-weight: 800;
    }

    .company-info {
        padding: 1em 0;
        line-height: 1.5em;
    }

    .page {
        position: relative;
        height: 920px;
        overflow: hidden;
    }

    .page-break{
    page-break-after:always;
    clear:both;
    display:block;
    }

    .page-index {
        border-top: 1px solid black;
        font-size: 9px;
        text-align: right;
        width: 100%;
        padding: 0.5em 0;
        position: absolute;
        bottom: 0px;
        font-weight: 900;
    }

    .h1-first, .h1-next {
        margin-bottom: 20px;
    }

    .h1-next {
        margin-top: 60px;
    }

    .page-generated {
        float: left;
    }

    .page-count {
        float: right;
    }


</style>

<?php if(count($invoices) == 0): ?>
    <div class="company-info">
        <span><?php echo __('Company'); ?>: <?php echo sfConfig::get('app_company_data_seller_name'); ?></span><br/>
        <span><?php echo __('Address'); ?>: <?php echo sprintf('%s, %s %s', sfConfig::get('app_company_data_seller_address'), sfConfig::get('app_company_data_seller_city'), sfConfig::get('app_company_data_seller_postcode')); ?></span><br />
        <span><?php echo __('Tax ID'); ?>: <?php echo sfConfig::get('app_company_data_seller_tax_id'); ?></span><br />
    </div>
    <h1>
        <?php echo __('No sale invoices'); ?>
        <?php echo $dateFrom ? sprintf(__('from') . ' %s', $dateFrom) : ''; ?>
        <?php echo $dateTo ? sprintf(__('to') . ' %s', $dateTo) : ''; ?>
    </h1>
<?php endif; ?>

<?php foreach($invoices as $invoice): ?>

<?php if($index%$rowsPerPage == 1): ?>

<div class="page">

<?php if($index == 1): ?>

    <div class="company-info">
        <span><?php echo __('Company'); ?>: <?php echo sfConfig::get('app_company_data_seller_name'); ?></span><br/>
        <span><?php echo __('Address'); ?>: <?php echo sprintf('%s, %s %s', sfConfig::get('app_company_data_seller_address'), sfConfig::get('app_company_data_seller_city'), sfConfig::get('app_company_data_seller_postcode')); ?></span><br />
        <span><?php echo __('Tax ID'); ?>: <?php echo sfConfig::get('app_company_data_seller_tax_id'); ?></span><br />
    </div>

<?php endif; ?>

<h1 class="h1-<?php echo $index==1 ? 'first' : 'next'; ?>">
    <?php echo __('List of sales invoices'); ?>
    <?php echo $dateFrom ? sprintf(__('od') . ' %s', $dateFrom) : ''; ?>
    <?php echo $dateTo ? sprintf(__('do') . ' %s', $dateTo) : ''; ?>
</h1>

<table>

    <tr>
        <th class="lp"><?php echo __('Lp.'); ?></th>
        <th><?php echo __('Name'); ?></th>
        <th><?php echo __('Date of issue'); ?></th>
        <th><?php echo __('Date of sale'); ?></th>
        <th><?php echo __('Date of payment'); ?></th>
        <th><?php echo __('Client name'); ?></th>
        <th><?php echo __('Client address'); ?></th>
        <th><?php echo __('Client tax id'); ?></th>
        <th><?php echo sprintf(__('Sale taxed') . ' %s', '0%'); ?></th>
        <?php foreach($taxes as $tax): ?>
            <?php if($tax->getValue() > 0): ?>
                <th><?php echo sprintf('%s%% ' . __('Net value'), $tax->getValue()); ?></th>
                <th><?php echo sprintf('%s%% ' . __('VAT value'), $tax->getValue()); ?></th>
            <?php endif; ?>
        <?php endforeach; ?>
        <th><?php echo __('Price net'); ?></th>
        <th><?php echo __('VAT amount'); ?></th>
        <th><?php echo __('Gross price'); ?></th>
    </tr>
<?php endif; ?>

    <tr>
        <td class="lp"><?php echo $index; ?></td>
        <td><?php echo $invoice->getName(); ?></td>
        <td><?php echo $invoice->getSellAt(); ?></td>
        <td><?php echo $invoice->getServiceRealizedAt(); ?></td>
        <td><?php echo $invoice->getPaymentDateAt(); ?></td>

        <td><?php echo $invoice->getClientName(); ?></td>
        <td><?php echo $invoice->getClientAddress(); ?></td>
        <td><?php echo $invoice->getClientTaxId(); ?></td>

        <?php foreach($taxes as $tax): ?>
            <td><?php echo $sf_user->formatCurrency(
                    $invoiceSaleReportArray[$invoice->getId()][$tax->getValue()]['priceNet'],
                    $currencyCode); ?></td>
            <?php if($tax->getValue() > 0): ?>
                <td><?php echo $sf_user->formatCurrency(
                        $invoiceSaleReportArray[$invoice->getId()][$tax->getValue()]['taxAmount'],
                        $currencyCode); ?></td>
            <?php endif; ?>
        <?php endforeach; ?>
                <td><?php echo $sf_user->formatCurrency($invoice->getPriceNet(), $currencyCode); ?></td>
        <td><?php echo $sf_user->formatCurrency($invoice->getPriceTax(), $currencyCode); ?></td>
        <td><?php echo $sf_user->formatCurrency($invoice->getPriceGross(), $currencyCode); ?></td>
    </tr>

    <?php if($index == count($invoices)): ?>
        <tr>
            <td class="no-border" colspan="7"></td>
            <td class="report-sum"><?php echo __('Sum'); ?></td>
            <?php foreach($taxes as $tax): ?>
                <td class="report-sum"><?php echo $sf_user->formatCurrency($reportSum['taxes'][$tax->getValue()]['priceNet'], $currencyCode); ?></td>
                <?php if($tax->getValue() > 0): ?>
                    <td class="report-sum"><?php echo $sf_user->formatCurrency($reportSum['taxes'][$tax->getValue()]['taxAmount'], $currencyCode); ?></td>
                <?php endif; ?>
            <?php endforeach; ?>
                <td class="report-sum"><?php echo $sf_user->formatCurrency($reportSum['priceNet'], $currencyCode); ?></td>
                <td class="report-sum"><?php echo $sf_user->formatCurrency($reportSum['priceTax'], $currencyCode); ?></td>
                <td class="report-sum"><?php echo $sf_user->formatCurrency($reportSum['priceGross'], $currencyCode); ?></td>
        </tr>

        </table>
        <div class="page-index">
            <div class="page-generated">
                <?php echo __('Generated from Mnumi System'); ?>
            </div>
            <div class="page-count">
                <?php echo sprintf(__('Page %d of %d'), $page++, $pagesCount ); ?>
            </div>
        </div>
        </div>
    <?php endif; ?>

<?php if($index !== count($invoices) && (++$index)%$rowsPerPage == 1): ?>
</table>
<div class="page-index">
    <div class="page-generated">
        <?php echo __('Generated from Mnumi System'); ?>
    </div>
    <div class="page-count">
        <?php echo sprintf(__('Page %d of %d'), $page++, $pagesCount ); ?>
    </div>
</div>
</div>
<div class="page-break"></div>
<?php endif; ?>

<?php endforeach; ?>
