<?php

/**
 * mnumiQL actions.
 *
 * @package    mnumicore
 * @subpackage mnumiQL
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mnumiQLActions extends sfActions
{

    /**
     * Executes index action.
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request)
    {
        // creates form
        $form = new MnumiQLForm();

        // prepares error message variable
        $errorMsg = '';

        if($request->isMethod('post'))
        {
            // gets i18NObj
            $i18NObj = $this->getContext()->getI18N();
            
            $query = $request->getParameter('query');
            $form->setDefault('query', $query);

            try
            {
                // gets 'from' clause
                $fromObj = new MnumiQLClauseFrom($query);
       
                // gets class name to array
                $modelArr = $fromObj->getModelsName();

                foreach($modelArr as $key => $rec)
                {
                    // without spaces in model name
                    if(count(explode(' ', $rec)) == 1)
                    {       
                        // if user then ommit
                        if($rec == 'User')
                        {
                            unset($modelArr[$key]); 
                            continue;
                        }
                        
                        // if more advanced joins (SfGuardUser.UserContact)
                        // then ommit
                        if(preg_match('/([.])/', $rec))
                        {
                            unset($modelArr[$key]); 
                            continue;
                        }
                        
                        // check two options of model (Order, Orders)
                        $className1 = sfInflector::camelize($rec).'Table';
                        $className2 = sfInflector::camelize(substr($rec, 0, -1)).'Table';
                        
                        if(class_exists($className1))
                        {
                            // sets formatted model name to array
                            $modelArr[$key] = $className1;  
                        }
                        elseif(class_exists($className2))
                        {
                            // unsets model if plural
                            unset($modelArr[$key]);  
                        }
                        else
                        {
                            throw new MnumiQLException('Model class "%input%" does not exist', 
                                            $i18NObj, array('%input%' => $rec));      
                        }                    
                    }
                    else
                    {
                        throw new MnumiQLException('Model class "%input%" cannot be divided by spaces', 
                                    $i18NObj, array('%input%' => $rec));  
                    }
                }

                // gets columns to display from form model
                $columnToDisplayArr = array();
                foreach($modelArr as $rec)
                {
                    // gets columns to display
                    $columnToDisplayArr += $rec::getInstance()->getDisplayTableFields();
                }

                // clause objects initialization
                $selectObj = new MnumiQLClauseSelect($query, $columnToDisplayArr);
                $whereObj = new MnumiQLClauseWhere($query);
                $groupObj = new MnumiQLClauseGroup($query);
                $orderObj = new MnumiQLClauseOrder($query);
                $limitObj = new MnumiQLClauseLimit($query);

                // generating Doctrine_Query based on query given by user
                $doctrineQuery = $fromObj->getQuery();            
                $doctrineQuery = $whereObj->getQuery($doctrineQuery);
                $doctrineQuery = $groupObj->getQuery($doctrineQuery);
                $doctrineQuery = $orderObj->getQuery($doctrineQuery);
                $doctrineQuery = $limitObj->getQuery($doctrineQuery);
                $doctrineQuery = $selectObj->getQuery($doctrineQuery);

                // view objects
                $this->coll = $doctrineQuery->execute();
                $this->columnToDisplayArr = $selectObj->getColumns();;
            }
            catch(MnumiQLException $e)
            {
                $errorMsg = $e->getMessage();
            }
            catch(Exception $e)
            {
                if(preg_match('/The root class/', $e->getMessage()))
                {
                    $errorMsg = 'The root class of the query must have at least one field selected.';
                }
                else
                {
                    $errorMsg = $e->getMessage();
                }
            }

            // if any exception (which cannot be translated 
            // - means Doctrine_Exception or other - non MnumiQLException) 
            // was thrown
            if(!empty($errorMsg))
            {
                $this->getUser()->setFlash('info_data',
                        array('messageType' => 'msg_error',
                    'message' => $errorMsg));
            }
        }

        // view objects 
        $this->form = $form;

        if($request->hasParameter('export-csv')) {
            $exportEncoding = sfConfig::get('app_default_export_encoding', 'windows-1250');

            ob_start();

            $out = fopen('php://output', 'w');

            // add header
            fputcsv($out, $this->columnToDisplayArr);

            // add content
            foreach($this->coll as $row) {
                $rowData = array();
                foreach($this->columnToDisplayArr as $key2 => $rec2) {
                    $rowData[] = $row->get($key2);
                }
                fputcsv($out, $rowData);
            }

            $content =  ob_get_clean();

            $content = iconv('UTF-8', $exportEncoding, $content);

            $response = $this->getResponse();
            $response->setContentType('text/csv');
            $response->setHttpHeader('Content-Disposition',
                'attachment; filename=mnumiql'.'_'.date('Y-m-d_G:i:s').'.csv');

            $response->setContent($content);

            return sfView::NONE;
        }
    }

    /**
     * Executes ReqColumnHint action.
     *
     * @param sfRequest $request A request object
     */
    public function executeReqColumnHint(sfWebRequest $request)
    {
        if(!$request->isXmlHttpRequest())
        {
            throw new Exception('Wrong request.');
        }

        $model = $request->getParameter('key');
                
        // column array
        $columnArr = array();
        
        // if class does not exist then cut off last char (Client, Clients)
        if(!class_exists($model))
        {
            $model = substr($model, 0, -1);
        }
        
        // if class exist then create table class name
        if(class_exists($model))
        {
            $class = $model.'Table';
        }

        $columnArr = array_keys($class::$displayTableFields);
        
        return $this->renderText(json_encode($columnArr));
    }
}
