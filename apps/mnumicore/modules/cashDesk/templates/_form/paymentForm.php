<form id="dialog-add-kp-form" action="<?php echo url_for('editNewKpForm', 
        array('model' => 'CashDesk', 
            'type' => 'new')); ?>" method="post" style="display: none;">
    <div title="<?php echo __('Add payment'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">                      
                <p class="required">
                    <?php echo $form['client']->renderLabel(); ?>
                    <?php echo $form['client']->render(); ?>
                    <?php echo $form['client_id']->render(); ?>
                </p>
                <p  class="required">
                    <?php echo $form['paymentAndType']->renderLabel(); ?>
                    <?php echo $form['paymentAndType']->render(); ?>
                <p>
                <p  class="required">
                    <?php echo $form['value']->renderLabel(); ?>
                    <?php echo $form['value']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['description']->renderLabel(); ?>
                    <?php echo $form['description']->render(); ?>
                <p>
            </div>
        </div>
    </div>
</form>