<form id="dialog-add-block-form" action="<?php echo url_for('editNewBlockForm',
        array('model' => 'CashDesk', 
            'type' => 'new')); ?>" method="post" style="display: none;">
    <div title="<?php echo __('Add advance payment'); ?>">
        <p class="validateTips"></p>
        <?php echo $form['_csrf_token']->render(); ?>
        <div class="block-border">
            <div class="block-content form no_border">       
                <p  class="required">
                    <?php echo $form['payment_id']->renderLabel(); ?>
                    <?php echo $form['payment_id']->render(); ?>
                <p>
                <p  class="required">
                    <?php echo $form['value']->renderLabel(); ?>
                    <?php echo $form['value']->render(); ?>
                <p>
                <p class="required">
                    <?php echo $form['description']->renderLabel(); ?>
                    <?php echo $form['description']->render(); ?>
                <p>
            </div>
        </div>
    </div>
</form>