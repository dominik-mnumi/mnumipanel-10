<div class="print-content">
    <p class="company-stamp-dots-p">...................................................</p>
    <p class="info-below-p">(<?php echo __('company stamp'); ?>)</p>
    <p class="headTitle-p"><?php echo $dataObj->getType() == CashDeskTable::$IN ? __('DEPOSIT SLIP') : __('PAYOFF SLIP'); ?> <?php echo __('NO.'); ?>:
        <span class="headDocNumber-span"> <?php echo $dataObj->getName(); ?></span>
    </p>
    <p class="date-of-issue-p"><?php echo __('Date of issue'); ?>: <?php echo format_datetime($dataObj->getCreatedAt()); ?></p>

    <table cellpadding="0" cellspacing="0" class="main-table">
        <tbody>
            <tr height="80">
                <td colspan="4" width="464" class="border-top-td border-left-td border-right-td vertical-align-top-td padding-5px">
                    <p class="head-p">
                        <?php echo ($dataObj->getType() == CashDeskTable::$IN) ? __('From whom') : __('To whom'); ?>:
                    </p>
                    <p class="content-p">
                        <?php if($dataObj->getType() == CashDeskTable::$BLOCK): ?>
                        <?php echo $dataObj->getSfGuardUser(); ?>
                        <?php else: ?>
                        <?php echo $dataObj->getClient(); ?>
                        <?php endif; ?>
                    </p>

                    <?php if($dataObj->getType() != CashDeskTable::$BLOCK): ?>
                    <p class="content-p">
                        <?php echo $dataObj->getClient()->getFullAddress(); ?>
                    </p>    
                    <?php endif; ?>
                </td>
                <td colspan="2" width="100" class="border-top-td border-right-td">
                    <p class="head-p center">
                        <?php echo $dataObj->getType() == CashDeskTable::$IN
                                ? __('Owes')
                                : __('Have'); ?>
                        <br />
                        <?php echo __('Cash desk'); ?>
                    </p>
                </td>
                <td width="100" class="border-top-td border-right-td">
                    <p class="head-p center">
                        <?php echo $dataObj->getType() == CashDeskTable::$IN
                                ? __('Have')
                                : __('Owes'); ?>
                        <br />
                        <?php echo __('Account'); ?>
                    </p>
                </td>
            </tr>
            <tr height="80">
                <td colspan="4" class="border-top-td border-left-td border-right-td vertical-align-top-td padding-5px">
                    <p class="head-p">
                        <?php echo __('Entitled'); ?>:
                    </p>
                    <p class="content-p">
                        <?php echo __($dataObj->getDescription()); ?>
                        <?php if($dataObj->getInvoiceId()): ?>
                            <br/>
                            <?php echo $dataObj->getInvoice()->getName(); ?>
                        <?php endif; ?>
                    </p>
                </td>
                <td colspan="3" class="border-top-td border-right-td vertical-align-top-td">
                    <table cellpadding="0" cellspacing="0" width="100%">                         
                        <tr height="20">
                            <td width="50" class="border-right-td border-bottom-td center">
                                <?php echo $sf_user->getCurrencySymbol(); ?>
                            </td>
                            <td width="49" class="border-right-td border-bottom-td center">
                                <?php echo __('cent'); ?>
                            </td>
                            <td class="border-bottom-td center">
                                <?php echo __('number'); ?>
                            </td>
                        </tr>
                        <tr height="59">
                            <td class="border-right-td center">
                                <?php echo $dataObj->getValueDec(); ?>
                            </td>
                            <td class="border-right-td center">
                                <?php echo $dataObj->getValueCent(); ?>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr height="80">
                <td colspan="3" class="border-top-td border-left-td border-right-td vertical-align-top-td padding-5px">
                    <p class="head-p">
                        <?php echo __('In words'); ?>:
                    </p>
                    <p class="content-p">
                        <?php echo $sf_user->getAmountInWords($dataObj->getValue()); ?>
                    </p>
                </td>
                <td class="border-top-td border-right-td center">
                    <?php echo __('Total'); ?>:
                </td>
                <td width="50" class="border-top-td border-right-td center">
                    <?php echo $dataObj->getValueDec(); ?>
                </td>
                <td width="49" class="border-top-td border-right-td center">
                    <?php echo $dataObj->getValueCent(); ?>
                </td>
                <td class="border-top-td border-right-td"></td>
            </tr>
            <tr height="100">
                <td width="17.5%" class="border-top-td border-bottom-td border-left-td border-right-td center vertical-align-top-td padding-top-10px">
                    <?php echo __('Issued by'); ?>:<br /><br />
                    <?php echo $dataObj->getSfGuardUser(); ?>
                </td>
                <td width="17.5%" class="border-top-td border-bottom-td border-right-td center vertical-align-top-td padding-top-10px">
                    <?php echo __('Checked by'); ?>:<br /><br />
                    <?php echo $dataObj->getSfGuardUser(); ?>
                </td>
                <td width="17.5%" class="border-top-td border-bottom-td border-right-td center vertical-align-top-td padding-top-10px">
                    <?php echo __('Confirmed by'); ?>:<br /><br />
                    <?php echo $dataObj->getSfGuardUser(); ?>
                </td>
                <td width="17.5%" class="border-top-td border-bottom-td border-right-td center vertical-align-top-td padding-top-10px">
                    <?php echo __('Cash report'); ?> <?php echo __('number'); ?>:<br />
                    <?php echo $dataObj->getCashReport()->getNumber(); ?>
                </td>
                <td colspan="3" class="border-top-td border-bottom-td border-right-td vertical-align-top-td padding-top-10px center">
                    <?php echo __('Amount of the above'); ?> <?php echo ($dataObj->getType() == CashDeskTable::$IN) ? __('received') : __('paid out'); ?>:<br /><br />
                    <p class="company-stamp-dots-p padding-left-20px">...................................................</p>
                    <p class="info-below-p padding-left-20px">(<?php echo __('cashier signature'); ?>)</p>
                </td>
            </tr>
        </tbody>
    </table>    
</div>