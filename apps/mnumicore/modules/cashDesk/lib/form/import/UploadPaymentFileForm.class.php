<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * UploadPaymentFileForm uploads file with payments.
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki
 */
class UploadPaymentFileForm extends BaseForm
{
    public function configure()
    {
        $encodingArr = array(
            0 => 'Detect automatically',
            'ISO-8859-1' => 'ISO-8859-1',
            'ISO-8859-2' => 'ISO-8859-2',
            'WINDOWS-1250' => 'WINDOWS-1250',
            'UTF-8' => 'UTF-8',);
        
        // widgets
        $this->setWidget('file',
                new sfWidgetFormInputFile());
        
        $this->setWidget('encoding',
                new sfWidgetFormChoice(array(
                    'label' => 'File encoding',
                    'choices' => $encodingArr)));
        
        // validators
        $this->setValidator('file',
                new sfValidatorFile());
        
        $this->setValidator('encoding',
                new sfValidatorChoice(array(
                    'required' => false, 
                    'choices' => array_keys($encodingArr))));
    
        // post validator 
        $this->validatorSchema->setPostValidator(new sfValidatorCallback(array(
                'callback' => array($this, 'validCsv'))));
        
        // sets default messages
        $this->setMessages();
        
        $this->getWidgetSchema()->setNameFormat(get_class($this).'[%s]');
    }
 
    /**
     * Returns filepath of saved file.
     */
    public function save()
    {
        $fileObj = $this->getValue('file');
        
        $filename = sha1($fileObj->getOriginalName().microtime().rand());
        $filepath = sfConfig::get('sf_cache_dir').'/'.$filename;
        $fileObj->save($filepath);     
        
        return $filepath;
    }
    
    /**
     * Checks if file is csv.
     * 
     * @param type $validator
     * @param type $values
     * @return type 
     */
    public function validCsv($validator, $values)
    {
        if($values['file'])
        {
            $error = new sfValidatorError($validator, 'This is not valid CSV file');

            $pathParts = explode('.', $values['file']->getOriginalName());
            if(end($pathParts) != 'csv')
            {
                throw new sfValidatorErrorSchema($validator, array('file' => $error));
            }

            // checks proper csv file
            $handle = fopen($values['file']->getTempName(), 'r');
            while(($data = fgetcsv($handle)) === FALSE) 
            {
                throw new sfValidatorErrorSchema($validator, array('file' => $error));
            }
            fclose($handle);
            
            // checks proper csv file
            $handle = fopen($values['file']->getTempName(), 'r');
            while(($data = fgetcsv($handle, 0, ';')) !== FALSE) 
            {
                if(count($data) != 8)
                {
                    $error = new sfValidatorError($validator, 'This CSV file is not valid (wrong number of columns)');
                    throw new sfValidatorErrorSchema($validator, array('file' => $error));
                } 
            }
            fclose($handle);
        }
        
        return $values;
    }
}