<?php

/**
 * packagingInterface actions.
 *
 * @package    mnumicore
 * @subpackage packagingInterface
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class packagingInterfaceActions extends myAction
{
   /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request)
    {
    }
    
    /**
  * Executes barcode action
  *
  * @param sfRequest $request A request object
  */
    public function executeBarcode(sfWebRequest $request)
    {
        $barcodePackaging = new BarcodePackaging($request->getParameter('barcode'));
        $type = $barcodePackaging->getTableNameFromType();
        //if type is user request is for authorize
        /** @var myUser $myUser */
        $myUser = $this->getUser();
        if($type == 'sfGuardUserTable')
        {
            $this->result = array('auth' => $myUser
                    ->authorizeUsingPasswordSaltAndId($barcodePackaging->getId(), 
                            $barcodePackaging->getAdditionalNumber()));
            
            return sfView::SUCCESS;
        }
        // save new worklog
        elseif($type == 'OrderTable')
        {
            if(!$myUser->isAuthorizedUsingPasswordSaltAndId())
            {
                throw new Exception('You have to be authorized to create new worklog.');
            }
            $this->result = $barcodePackaging->orderToShelf();
            $userId = $myUser->getAuthorizedUsingPasswordSaltAndIdUserId();
            /** @var Order $order */
            $order = $barcodePackaging->getObject();
            $order->setEditorId($userId);
            $order->save();
            $order->refresh();
            /** @var OrderPackage $orderPackage */
            $orderPackage = $order->getOrderPackage();

            $webapiKey = null;

            if($order instanceof Order) {
                $webapiKey = $order->getShopName();
            }

            // send notification
            if(isset($this->result['readyEvent']) && $this->result['readyEvent'] && $orderPackage->getOrderPackageStatusName() == OrderPackageStatusTable::$completed)
            {
                // triggers the event
                $this->dispatcher->notify(new sfEvent($this,
                        'package.ready',
                        array('obj' => $orderPackage, 'webapi_key' => $webapiKey)));
            }

            $this->tryPrintLabel($orderPackage, $webapiKey);
            
            $this->object = $order;
            return sfView::SUCCESS;
        }
      
        throw new Exception('This type of barcode is not supported in printerInterface.');
    }

    /**
     * Try to print label
     *
     * @param OrderPackage $orderPackage
     * @param string $webapiKey
     */
    private function tryPrintLabel(OrderPackage $orderPackage, $webapiKey)
    {
        $i18n = sfContext::getInstance()->getI18N();
        $queue = PrintlabelQueueTable::getInstance()->getOrderPackagePendingQueue($orderPackage->getId());
         $this->result['message']['printInfo'] = '';
        // triggers the event
        if(!$queue
                && $orderPackage->getOrderPackageStatusName() == OrderPackageStatusTable::$completed) {

            try {
                $this->dispatcher->notify(new sfEvent($this,
                        'package.printLabel',
                        array('obj' => $orderPackage)));

                $this->result['message']['printInfo'] = $i18n->__("Label has been provided to print.");

            } catch (Exception $e) {

                $this->result['message']['printInfo'] = $i18n->__("Error occured while printing label.");
                return;
            }

            // triggers the event
            $this->dispatcher->notify(new sfEvent($this,
                    'package.posted',
                    array('obj' => $orderPackage, 'webapi_key' => $webapiKey)));

        } elseif($orderPackage->getOrderPackageStatusName() == OrderPackageStatusTable::$completed) {

            $this->result['message']['printInfo'] = $i18n->__("Label is being printed.");

        }
    }

    /**
     * Executes checking if additional information is needed for sending package
     * @param sfWebRequest $request
     * @return string
     */
    public function executeCheckAdditionalInformationNeeded(sfWebRequest $request)
    {
        $barcodePackaging = new BarcodePackaging($request->getParameter('barcode'));
        $type = $barcodePackaging->getTableNameFromType();
        $result = array('isNeeded' => false);

        if($type == 'OrderTable')
        {
            $orderPackage = $barcodePackaging->getObject()->getOrderPackage();
            $additionalShippingForm = AdditionalInformationForm::getAdditionalShippingInformationForm($orderPackage);

            if($additionalShippingForm) {

                $formAction = $this->generateUrl('saveAdditionalInformation',
                            array('model' => 'OrderPackage',
                                'type' => 'edit'));

                $result = array(
                    'isNedded' => true,
                    'formContent' => $this->getPartial('package/additionalInformationForm', array(
                        'form' => $additionalShippingForm,
                        'action' => $formAction))
                    );
            }
        }

        return $this->renderText(json_encode($result));
    }

    /**
      * Executes saving additional information for package shipping
      *
      * @param sfRequest $request A request object
      */
     public function executeSaveAdditionalInformation(sfWebRequest $request)
     {
        if(!$this->getUser()->isAuthorizedUsingPasswordSaltAndId())
        {
            throw new Exception('You have to be authorized to create new worklog.');
        }

         $returnArray = parent::executeRequestAjaxProcessForm($request, true);
         return $this->renderText(json_encode($returnArray));
     }

    /**
     * Executes printlabels action
     *
     * @param sfRequest $request A request object
     */
    public function executePrintlabels(sfWebRequest $request)
    {
        $allPrintlabels = PrintlabelQueueTable::getInstance()
                ->loadQueue('PENDING')
                ->toArray();
        
        return $this->renderText(json_encode($allPrintlabels));
    }

    /**
     * Executes printlabels action
     *
     * @param sfRequest|sfWebRequest $request A request object
     * @return string
     * @throws Doctrine_Record_Exception
     * @throws sfError404Exception
     */
    public function executePrintAgain(sfWebRequest $request)
    {
        $queueId = $request->getParameter('queueId');
        if($queueId)
        {
            /** @var PrintlabelQueue $queue */
            $queue = PrintlabelQueueTable::getInstance()->find($queueId);
            $this->forward404unless($queue);

            $queueNew = $queue->printAgain();
            $queueNew->save();
            $queueNew->refresh();
        } else {
            $orderPackage = OrderPackageTable::getInstance()->find($request->getParameter('id'));
            $this->forward404unless($orderPackage);

            PrintlabelQueueTable::getInstance()
                ->saveDefaultPrintlabelQueueWithAttributes($orderPackage);
        }
        return sfView::NONE;
    }
}
