<?php include_partial('dashboard/message'); ?>
<?php include_partial('global/alertModalConfirm') ?>
<?php use_helper('openModal') ?>

<form method="post" class="form" action="<?php echo url_for('permission'); ?>">
    <article class="container_12">
        <section class="grid_12">
            <div class="block-border">
                <div class="block-content form text-right" id="table_form">
                    <h1>
                        <?php echo __('Permissions'); ?>
                        <a id="dialog-create-group-click" href="#">
                            <img src="/images/icons/fugue/plus-circle-blue.png" width="16" height="16" alt="<?php echo __('Add permission')  ?>"> <?php echo __('add'); ?>
                        </a>
                    </h1>
                    <?php include_partial('permissionTable', 
                            array('groupColl' => $groupColl,
                                'departmentArr' => $departmentArr,
                                'matrix' => $matrix)); ?>
                    
                     <br />
                     <button><?php echo __('Save'); ?></button>
                </div>        
            </div>          
        </section>
    </article>
</form>
<?php echo include_partial('permission/form/createForm', array('form' => $newGroupForm)); ?>

<div id="editFormContainer"></div>
<?php echo include_partial('permission/javascript/javascript'); ?>
