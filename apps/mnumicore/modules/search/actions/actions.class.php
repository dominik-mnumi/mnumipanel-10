<?php
/*
 * This file is part of the MnumiPrint package.
*
* (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

/**
 * search actions.
 *
 * @package    mnumicore
 * @subpackage search
 * @author     Adam Marchewicz <adam.marchewicz@itme.eu>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class searchActions extends sfActions
{
    /**
     * Autocomplete method
     *
     * @param sfRequest $request A request object
     */
    public function executeAutocomplete(sfWebRequest $request)
    {
        $this->setLayout(false);
        $query = trim($request->getParameter('search_phrase'));
      
        $searchTool = new SearchTool();
        $searchTool->setLimit(10);
        $results = $searchTool->search($query);
        if($results)
        {
            return $this->renderText(json_encode($results));
        }
        return sfView::NONE;
    }
    
    /**
     * Executes search method after "ENTER" click.
     *
     * @param sfRequest $request A request object
     */
    public function executeStrict(sfWebRequest $request)
    {
        $query = trim($request->getParameter('search_phrase'));
      
        // create search tool object
        $searchTool = new SearchTool();
        
        // searches exactly the same name of order, user, client, invoice, etc.
        $result = $searchTool->searchStrict($query);

        // if result exists then redirect
        if($result)
        {
            $this->redirect($result->getEditUrl());
        }

        $this->getUser()->setFlash('info_data', array(
            'message' => 'No results',
            'messageType' => 'msg_success',
        ));         
    }

    /**
     * Clients autocomplete by phrase method
     *
     * @param sfRequest $request A request object
     * @return json
     */
    public function executeClientsAutocompleteByPhrase(sfWebRequest $request)
    {
        $this->setLayout(false);

        $query = trim($request->getParameter('search_phrase'));
        $clients = ClientTable::getInstance()->searchClient($query);

        $results = $this->prepareClientsAutocompleteArray($clients);

        return $this->renderText(json_encode($results));
    }

    /**
     * Clients autocomplete by client ids method
     *
     * @param sfRequest $request A request object
     * @return json
     */
    public function executeClientsAutocompleteByIds(sfWebRequest $request)
    {
        $this->setLayout(false);

        $ids = trim($request->getParameter('ids'));
        $clients = array();

        if($ids) {

            $ids = explode(',', $ids);
            $clients = ClientTable::getInstance()->createQuery('c')
                    ->whereIn('id', $ids)
                    ->execute();

        }

        $results = $this->prepareClientsAutocompleteArray($clients);

        return $this->renderText(json_encode($results));
    }

    /**
     * Convert clients Doctrine_Collection into array
     * @param Doctrine_Collection $clients
     */
    protected function prepareClientsAutocompleteArray($clients)
    {
        $results = array();

        foreach($clients as $client) {
            $results[] = array(
                'id' => $client->getId(),
                'name' => $client->getFullname(),
                'taxId' => $client->getTaxId())
                ;
        }

        return $results;
    }
}
