<?php
$userLink = ($order->getsfGuardUser()->getId())
    ? link_to($order->getsfGuardUser(), 'userListEdit', $order->getsfGuardUser())
    : '-';
$editorLink = $order->getEditor()->getId()
    ? link_to($order->getEditor(), 'userListEdit', $order->getEditor())
    : '-';
?>
<div class="task with-legend client-status">
<div class="legend"><?php echo __('Customer'); ?></div>
<ul class="client-status-li">
    <li><?php echo __('Name'); ?>: <?php echo $order->getClient(); ?></li>
    <li>
        <?php echo __('Added by')?>:
        <?php echo $userLink; ?>,
        <?php echo format_date($order->getCreatedAt(), OrderPackageTable::$packageTimeFormat); ?>
    </li>
    <?php if($order->getCreatedAt() != $order->getUpdatedAt()): ?>
    <li>
        <?php echo __('Last updated by'); ?>:
        <?php echo $editorLink; ?>,
        <?php echo format_date($order->getUpdatedAt(), OrderPackageTable::$packageTimeFormat); ?>
    </li>
    <?php endif; ?>
    <li><?php echo __('Pricelist')?>: <?php echo $client->getPricelist(); ?></li>
    <li><?php echo __('Credit limit')?>: <?php echo ($order->getClient()->getCreditLimitAmount() > 0) ? $order->getClient()->getCreditLimitAmount().' '.$sf_user->getCurrency() : __('none'); ?></li>
    <li>
    <?php if($order->getShopName() != ''): ?>
        <?php echo __('Added from'); ?>: <?php echo $order->getShop(); ?>
    <?php else: ?>
        <?php echo __('Added by'); ?>: <?php echo __('Office'); ?>
    <?php endif; ?>
    </li>
</ul>
</div>
