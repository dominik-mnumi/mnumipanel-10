<?php if($row->getInvoiceItems()->getFirst()): ?>
<?php echo link_to(image_tag('icons/fugue/application-table.png', array('width' => 16, 'height' => 16)), 'invoiceListEdit', array('id' => $row->getInvoiceItems()->getFirst()->getInvoiceId()), array('title' => __('Edit invoice'))) ?>
<?php endif ?>