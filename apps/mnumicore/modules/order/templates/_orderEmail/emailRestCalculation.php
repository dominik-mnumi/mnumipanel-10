<?php foreach($calculation as $item): ?>
<?php echo $item['label']; ?>

<?php endforeach; ?>
<?php $priceItemArr = $priceReportArr['priceItems']; ?>
<?php foreach($priceItemArr as $key => $rec): ?>
<?php if($key == 'OTHER'): ?>
<?php foreach($rec as $rec2): ?>
<br />- <?php echo __($rec2['label']); ?>: <?php echo __($rec2['fieldLabel']); ?> - <?php echo $sf_user->formatCurrency($rec2['price']); ?>
<?php endforeach; ?>
<?php else: ?>
<?php if(!empty($rec['price'])): ?>
<br />- <?php echo __($rec['label']); ?>: <?php echo __($rec['fieldLabel']); ?> - <?php echo $sf_user->formatCurrency($rec['price']); ?>
<?php endif; ?>
<?php endif; ?>
<?php endforeach; ?>
<br />
<?php echo strtoupper(__('Total')); ?>: <?php echo $sf_user->formatCurrency($priceReportArr['summaryPriceNet']); ?> (<?php echo $sf_user->formatCurrency($priceReportArr['summaryPriceGross']); ?> <?php echo __('incl. VAT'); ?>)