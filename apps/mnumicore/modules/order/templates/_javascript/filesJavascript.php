<script type="text/javascript">
    var imageList = 0;
    var previewList = 0;
    var imageReloadIntervalHandle;
    var wizardPdfQueueHandle = new Array();
    
    /**
     * Reloads file list (normal files and wizards). 
     */
    function reloadFileList()
    {
        imageList = 0;
        previewList = 0;
        
        uploadLoader(true);
        previewLoader(true);
        $("#item_photos .new_image").remove();
        $("#dowload-zip").hide();
   
        // hides arrows
        manageNextPrevArrows();

        // renders attachment list and preview list
        renderAttachmentList();
        renderPreviewList();
    }
 
    /**
     * Renders attachment list and injects into list container.
     */
    function renderAttachmentList()
    {
        previewOnLoad();
        $.ajax(
        {
            url: '<?php echo url_for('orderAttachmentsList', 
                    array('id' => $order->getId(), 
                        'type' => 'attachment')); ?>',
            type: 'POST',
            data: 
            {
                id: "<?php echo $order->getId(); ?>",
                productName: "<?php echo $order->getProduct()->getSlug(); ?>",
                preview: "<?php echo (boolean)isset($preview); ?>"
            },
            dataType: 'json',
            success: function(content)
            {
                if(content)
                {       
                    // local file server
                    // checks if local server
                    <?php if($localFileServerEnabled): ?>
                    $.ajax(
                    {
                        url: 'http://<?php echo LocalFileServerManager::getInstance()->getGetWaitingFileListUrl(); ?>',
                        type: 'POST',
                        dataType: 'json',
                        data: 
                        {
                            orderId: '<?php echo $order->getId(); ?>'
                        },
                        success: function(content2)
                        {
                            // attachment list rendering
                            window.attachmentList = new AttachmentListView(content, content2); 
                            window.attachmentList.render();

                            if($('.file-li > a').size() >= 1)
                            {
                                $('#dowload-zip').show();
                            }

                            $('#stop_upload').hide();
                            $("#uploader-loader").hide();
                            $("#uploader-interface").show();    

                            // sets attachment list as loaded
                            imageList = 1;
                        }
                    }); 
                    <?php else: ?>
                    
                    // attachment list rendering
                    window.attachmentList = new AttachmentListView(content); 
                    window.attachmentList.render();

                    if($('.file-li > a').size() >= 1)
                    {
                        $('#dowload-zip').show();
                    }

                    $('#stop_upload').hide();
                    $("#uploader-loader").hide();
                    $("#uploader-interface").show();    
                    
                    // sets attachment list as loaded
                    imageList = 1;
                    
                    <?php endif; ?>
                }
            },
            error: function()
            {
            	$('#attachedFiles').html('<?php echo __('Unable to load attachments list. Check server configuration.') ?>');
            	$("#uploader-loader").hide();
                $("#uploader-interface").show();
            }
        });
        
        
    }
    
    /**
     * Renders preview list and injects into preview container.
     */
    function renderPreviewList()
    {
        // render preview list
        $.ajax(
        {
        	url: '<?php echo url_for('orderAttachmentsList', array('id' => $order->getId(), 'type' => 'preview')); ?>',
            type: 'POST',
            data: 
            {
                id: "<?php echo $order->getId(); ?>",
                productName: "<?php echo $order->getProduct()->getSlug(); ?>",
                preview: "<?php echo (boolean)isset($preview); ?>"
            },
            success: function(content)
            {
                if(content.trim() != '')
                {
                    $('#item_photos').html(content);

                    $('#item_photos div').hide();
                    $('#item_photos div:first-child').show();   
                }
                else
                {
                    previewLoader(false);
                }
                
                if($('#item_photos div').length == 0)
                {
                    $('#def_image').show();
                }
                else
                {
                    $('#def_image').hide();
                }

                // sets preview list as loaded
                previewList = 1;
            },
            error: function()
            {
            	$('#item_photos').html('<?php echo __('Unable to load files previews. Check server configuration.') ?>');
            	previewLoader(false);
            }
        });    
    }
    
    /**
     * Action after image load.
     */
    function previewOnLoad()
    {
        $("#preview-loader").hide();
        $("#item_photos").show();
        manageNextPrevArrows();
    }
    
    /**
     * Shows or hides arrows.
     */
    function manageNextPrevArrows()
    {
        if($('#item_photos > div').size() <= 1)
        {
            $('.next').hide();
            $('.prev').hide();
        }
        else
        {
            $('.next').show();
            $('.prev').show();
        }
    }

    /**
     * Sets notice for file. 
     */
    function setNotice(text)
    {
        $.ajax(
        {
            url: '<?php echo url_for('@fileSetNotice') ?>',
            type: 'POST',
            data: 
            {
                fileId: text.attr('rel'), 
                text: text.val()
            },
            success: function(){
                text.hide();
                text.parent().find('.notice-div').show().html(text.val());
            }
        });
        return false;
    }

    /**
     * Sets print number
     */
    function setPrintsNumber(number)
    {
        $.ajax(
        {
            url: '<?php echo url_for('@fileSetPrintsNumber') ?>',
            type: 'POST',
            data:
            {
                fileId: number.attr('rel'),
                printsNumber: number.val()
            }
        });
        return false;
    }
    
    /**
     * Function adds wizard.
     */
    function addWizard(projectName)
    {
        $.ajax(
        {
            url: '<?php echo url_for('addWizardOrder'); ?>',
            type: 'POST',
            data: 
            {
                productSlug: '<?php echo $order->getProduct()->getSlug(); ?>',
                orderId: '<?php echo $order->getId(); ?>',
                projectName: projectName
            },
            success: function(content)
            {
                reloadFileList();
            }
        });
    }
    
    /**
     * Executes refresh action (after wizard edit).
     */
    function editWizard()
    {
        reloadFileList();
    }
    
    /**
     * Wizard edit submit form action.
     */
    function submitEditForm(parameter, signature)
    {       
        $("#edit-parameter").val(parameter);
        $("#edit-signature").val(signature);

        // submits
        $("#edit-wizard-form").submit();   
    }   

    /**
     * Gets wizard pdf.
     * 
     * @deprecated new method -> getWizardPdfQueueAdd
     * 
     * @param parameter 
     * @param signature 
     * @param url 
     */
    function getWizardPdf(parameter, signature, url)
    {
        var url = url + '?parameter=' + parameter + '&signature=' + signature;
        
        <?php if(!empty($wizardId)): ?>
        url = url + '&id=' + '<?php echo $wizardId; ?>';
        <?php endif; ?>

        location.href = url;
    }

    /**
     * Adds pdf to queue.
     * 
     * @param selector
     * @param parameter
     * @param signature
     */
    function getWizardPdfQueueAdd(selector, parameter, signature)
    {
        // disables download button
        $(selector).attr('disabled', 'disabled');
        
        // shows loader
        $(selector).parents("li").find('.wizard-loader-img').show();
        
        // pdf queue add
        var url = '<?php echo WizardManager::getPdfQueueAddUrl(); ?>' + '?parameter=' + parameter + '&signature=' + signature;
        
        // pdf queue status
        var url2 = '<?php echo WizardManager::getPdfQueueStatusUrl(); ?>'  + '?parameter=' + parameter + '&signature=' + signature;
        
        <?php if(!empty($wizardId)): ?>
        url = url + '&id=' + '<?php echo $wizardId; ?>';
        url2 = url2 + '&id=' + '<?php echo $wizardId; ?>';
        <?php endif; ?>

        // checks pdf status (heartbeat)
        wizardPdfQueueStatus(selector, url, url2, signature);
    }

    /**
     * Checks wizard queue status.
     *
     * @param selector
     * @param url
     * @param url2
     * @param signature
     */
    function wizardPdfQueueStatus(selector, url, url2, signature)
    {
        // if queue does not exist in wizardPdfQueueHandle array then sets interval
        if(!(signature in wizardPdfQueueHandle))
        {
            wizardPdfQueueHandle[signature] = setInterval(function(){ wizardPdfQueueStatus(selector, url, url2, signature)}, 3000);
        }

        var wizardLiSel = $(selector).parents("li");
        
        // adds pdf to queue (if first time)
        $.ajax(
        {
            url: url,
            type: 'GET',
            dataType: 'json',
            async: false,
            success: function(response)
            {
                var statusCode = response.result.status.code;

                if(statusCode == 'error')
                {
                    $(selector).removeAttr('disabled');
                    wizardLiSel.children("span.wizard-info-li").html('');
                    wizardLiSel.children('.wizard-loader-img').hide();

                    alert('<?php echo __('Error while creating PDF file. Please contact with administrator.'); ?>');
                    clearInterval(wizardPdfQueueHandle[signature]);
                    delete wizardPdfQueueHandle[signature];
                }
                if(statusCode == 'ready')
                {
                    $(selector).removeAttr('disabled');
                    wizardLiSel.children("span.wizard-info-li").html('');
                    wizardLiSel.children('.wizard-loader-img').hide();
                    
                    clearInterval(wizardPdfQueueHandle[signature]);
                    window.location.href = url2;
                    delete wizardPdfQueueHandle[signature];
                }   
                else
                {
                    wizardLiSel.children("span.wizard-info-li").html('<i>(<?php echo __('waiting in queue'); ?>: ' + response.result.queueCount + ')</i>');
                }
            }
        });
    }

    /**
     * Switch loader or content.
     */
    function uploadLoader(on)
    {
        if(on)
        {
            $("#uploader-interface").hide();
            $("#uploader-loader").show(); 
        }
        else
        {
            $("#uploader-interface").show();
            $("#uploader-loader").hide(); 
        }
    }
    
    /**
     * Switch loader or content.
     */
    function previewLoader(on)
    {
        if(on)
        {
            $("#item_photos").hide();
            $('#def_image').hide();
            $("#preview-loader").show();
            
        }
        else
        {
            $("#item_photos").show();     
            $("#preview-loader").hide();           
        }
    }

    jQuery(document).ready(function($) 
    {
        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderFile)): ?>
        
        // PLUPLOAD section
        plupload.addI18n(
        {
            'Select files' : 'Dodaj plik(i)',
            'Add files to the upload queue and click the start button.' : 'Wybierz piki które chcesz dodać, a następie wybierz przycisk "Wyślij pliki".',
            'Filename' : 'Nazwa pliku',
            'Status' : 'Postęp',
            'Size' : 'Rozmiar',
            'Add files' : 'Wybierz pliki do przesłania',
            'Add files.' : 'Dodaj pliki.',
            'Stop current upload' : 'Przerwij wysyłkę plików',
            'Start uploading queue' : 'Rozpocznij przesyłanie',
            'Start upload' : 'Rozpocznij przesyłanie',
            'Drag files here.' : 'Przeciągnij pliki i upuść je tutaj.',
            'N/A' : 'N/D',
            'Uploaded' : 'Przesłano',
            'files' : 'pliki',
            'Uploaded %d/%d files': 'Przesłano: %d/%d plików'
        });
      
        // normal files uploader
        var pluploadOptions = 
        {
            runtimes : 'gears,html5,flash,silverlight,browserplus',
            browse_button : 'pickfiles',
            container : 'plupload-container',
            max_file_size : '<?php echo $maxFileSize; ?>',
            url : '<?php echo $localFileServerEnabled ? 'http://'.LocalFileServerManager::getInstance()->getUploadUrl($order->getId()) : url_for('@fileUpload?id='.$order->getId()); ?>',
            unique_names : true,
            flash_swf_url : '/js/plupload/plupload.flash.swf',
            silverlight_xap_url : '/js/plupload/plupload.silverlight.xap'
        }
  
        var uploader = new plupload.Uploader(pluploadOptions);
  
        uploader.bind('Init', function(up, params) 
        {
            $('#filelist').html("<div></div>");
        });
  
        uploader.init();
  
        uploader.bind('FilesAdded', function(up, files) 
        {
            $('#stop_upload').show();
            $.each(files, function(i, file) 
            {
                $('#attachedFiles').append(
                '<li id="' + file.id + '">' +
                    file.name + ' <b></b>'+
                    '</li>');
            });
            uploader.start();
            up.refresh(); // Reposition Flash/Silverlight
        });

        $('#stop_upload').click(function() 
        {
            uploader.stop();
            reloadFileList();
            return false;
        });
        
        uploader.bind('UploadProgress', function(up, file) 
        {
            $('#' + file.id + " b").html(file.percent + "%");
        });
  
        uploader.bind('Error', function(up, err) 
        {
            $('#filelist').append("<li>Error: " + err.code +
                ", Message: " + err.message +
                (err.file ? ", File: " + err.file.name : "") +
                "</li>"
        );
  
            up.refresh(); // Reposition Flash/Silverlight
        });
  
        uploader.bind('FileUploaded', function(up, file) 
        {
            $('#' + file.id + " b").html("100%");
        });

        /**
         * Checks if images (list and preview) are loaded and sets or unsets
         * "Title".
         */
        function checkIfImagesLoaded() 
        {
            if(imageList && previewList) 
            {
                // for first element and title was not changed
                if(uploader.files.length > 0 && !$("#title").hasClass("title-changed"))
                {
                    if($('#item_photos div').length == 1)
                    {
                        var basename = uploader.files[uploader.files.length - 1]['name'].split(".");
                        $('#title').text(basename[0]);
                        $('input#order_name').val(basename[0]);                       
                    }
                }
                
                clearInterval(imageReloadIntervalHandle);
            }
        }

        uploader.bind('StateChanged', function(uploader) 
        {
            if(uploader.files.length === (uploader.total.uploaded + uploader.total.failed)) 
            {                
                reloadFileList();
                imageReloadIntervalHandle = setInterval(checkIfImagesLoaded, 100);
            }
        });
     
        <?php endif; ?>
        
        // FILE LIST AND PREVIEW section        
        // reloads list at the beginning
        reloadFileList();
          
        // LIST section
        // sets action after loading fields
        $('#attachedFiles li a.img').click(function() 
        {
            $('.product-image img#image').attr('src', $(this).children().attr('src'));
            return false;
        });

        // sets delete action
        $('#attachedFiles li a.del_item').live('click', function() 
        {
	        // confirmation modal
	        if(!alertModalConfirm('<?php echo __('Delete item') ?>', '<h3><?php echo __('Do you really want to delete this item?') ?></h3>', 500, $(this).attr('id'), true))
	        {
		        return false;
	        }

            uploadLoader(true);

            var liSel = $(this).parents('li');

            var itemNo = $(this).attr('id').substring(5);
            $.ajax(
            {
                url: '<?php echo url_for('@fileDel?id='.$order->getId(), true); ?>?itemNo=' + itemNo,
                type: 'POST',
                success: function()
                {
                    reloadFileList();
                }
            });

            return false;
        });
        
        // download file
        $(".download").live('click', function()
        { 
            var id = $(this).attr('val');
            var filepath = $(this).attr('data-path');
            
            // local file server
            // checks if file exists on local server
            <?php if($localFileServerEnabled): ?>
            $.ajax(
            {
                url: 'http://<?php echo LocalFileServerManager::getInstance()->getHasUrl(); ?>',
                type: 'POST',
                dataType: 'json',
                data: 
                {
                    filepath: filepath
                },
                success: function(content)
                {
                    // if file exist locally
                    if(content.result == true)
                    {
                        location.href = 'http://<?php echo LocalFileServerManager::getInstance()->getGetUrl(); ?>?filepath=' + filepath
                    }
                    else
                    {
                        location.href = '<?php echo url_for('fileDownload'); ?>?id=' + id; 
                    }
                }
            }); 
            <?php else: ?>
            location.href = '<?php echo url_for('fileDownload'); ?>?id=' + id; 
            <?php endif; ?>
        });
        
        // set notice
        $('.notice').live('click', function() 
        {
            $(this).parent().find('textarea').toggle();
            $(this).parent().find('.notice-div').toggle();
        });

        $(".notice-text").bind("keypress", function(e) 
        {
            if (e.keyCode == 13) return setNotice($(this));
        });

        $('.notice-text').live('change', function() 
        {
            setNotice($(this));
        });

        $('.multiprint-count').live('change', function()
        {
            // updates for *this* file
            setPrintsNumber($(this));


            var totalCount = 0;
            $(".multiprint-count").each(function(i, n){
                totalCount += parseInt($(n).val());
            });

            $('#order_attributes_count').val(totalCount).change();
        });
        
        // IMAGE PREVIEW section
        // services next image action
        $('.next').click(function() 
        {
            $("#item_photos").show();
            $("#def_image").hide();
            
            var visible_img = $('#item_photos div:visible');
            var src = visible_img.next().attr('src-image');

            if(visible_img.next().children().attr('src'))
            {
                visible_img.hide();
                visible_img.next().show().attr('src', src);
            }
            return false;
        });
        
        // services previous image action
        $('.prev').click(function() 
        {
            $("#item_photos").show();
            $("#def_image").hide();
            
            var visible_img = $('#item_photos div:visible');
            var src = visible_img.next().attr('src-image');

            if(visible_img.prev().children().attr('src'))
            {
                visible_img.hide();
                visible_img.prev().show().attr('src', src);
            }
            return false;
        });
        
        // sub navigation image (next)
        $('.nextSubImage').live('click', function() 
        {
            var visible_img = $('.new_image:visible > img:visible');
            var src = visible_img.next().attr('src-image');
            if(src)
            {
                visible_img.hide();
                visible_img.next().show().attr('src', src);
                
                var index = $('.new_image:visible > img').index(visible_img.next()) + 1;
                
                $('.subNavImageSelect').val(index);
            }
            return false;
        });
        
        // sub navigation image (previous)
        $('.prevSubImage').live('click', function() 
        {
            var visible_img = $('.new_image:visible > img:visible');
            var src = visible_img.prev().attr('src-image');
            if(visible_img.prev().attr('src'))
            {
                visible_img.hide();
                visible_img.prev().show().attr('src', src);
                
                var index = $('.new_image:visible > img').index(visible_img.prev()) + 1;
                
                $('.subNavImageSelect').val(index);
            }
            return false;
        });
  
        // page select (when sub image)
        $('.subNavImageSelect').live('change', function() 
        {
            $(this).parents('.new_image').children('img').hide();
            var img = $(this).parents('.new_image').children('img:nth-child(' + $(this).val() + ')');
            img.show().attr('src', img.attr('src-image'));

            return false;
        });
        
        // preview list
        $(".previewList").live('click', function() 
        {     
            var count = $("#attachedFiles li").length;
            var nr = $("#attachedFiles li").index($(this).parent());
            
            // local server
            if($(this).hasClass('file-local-server'))
            {
                $("#item_photos").hide();
                $("#def_image").show();
            }
            else
            {
                $("#item_photos").show();
                $("#def_image").hide();
                
                $(".new_image").hide();
                $(".new_image:eq(" + nr + ")").show();  
            }
        });

        /** WIZARD SECTION
         ------------------------------------------------------*/    
        <?php if($sf_user->hasCredential(sfGuardPermissionTable::$orderFile)): ?>   
        $('#addproject').click(function() 
        {
            if(isInt($('#order_attributes_count').val()))
            {                           
                $.ajax(
                {
                    type: 'POST',
                    url: '<?php echo url_for('reqWizardEncodedParamAndSignature'); ?>',
                    data: { count: $("#order_attributes_count").val(), 
                        countChange: '<?php echo $order->getProduct()
                                ->getProductFieldByFieldsetName(FieldsetTable::$COUNT)
                                ->isVisible(); ?>',
                        productName: '<?php echo $order->getProduct()
                                ->getSlug(); ?>',
                        id: '<?php echo $order->getId(); ?>', 
                        barcode: '<?php echo $order->getBarcode(); ?>' },
                    dataType: 'json',
                    error: function(xhr, ajaxOptions, thrownError)
                    {
                        alert('<?php echo __('There was error. Please try again later.') ?>');
                    },
                    success: function(data)
                    {
                        if(data.status == 'success')
                        {
                            $('#parameter').val(data.result.encodedParameter);
                            $('#signature').val(data.result.signature);

                            $('#new-wizard-form').submit();
                        }             
                    }
                });               
            }
            else
            {
                alert('<?php echo __('"Page count" field must be valid integer.') ?>');
            }
 
            return false;
        });

        $('#attachedFiles li a.del_wiz').live('click', function() 
        {
	        // confirmation modal
	        if(!alertModalConfirm('<?php echo __('Delete item') ?>', '<h3><?php echo __('Do you really want to delete this item?') ?></h3>', 500, $(this).attr('id'), true))
	        {
		        return false;
	        }

	        uploadLoader(true);
            
            $.ajax(
            {
                url: '<?php echo url_for('deleteWizardOrder'); ?>',
                type: 'POST',
                data: 
                { 
                    orderId: '<?php echo $order->getId(); ?>',
                    projectName: $(this).attr("id")
                },
                success: function(content)
                {
                    reloadFileList();
                }
            });
            return false;
        });
        <?php endif; ?>
    });  
    
    
    // BACKBONE OBJECTS
    window.AttachmentListView = Backbone.View.extend(
    {  
        initialize: function(attachments, localServerAttachments) 
        {
            this.template = _.template($('#attachment-list').html());
            this.attachments = attachments;
            this.localServerAttachments = localServerAttachments;
        },
        render: function() 
        {
            renderedContent = this.template(
            {
                attachments: this.attachments,
                localServerAttachments: this.localServerAttachments
            });
            
            $("#attachedFiles").html(renderedContent);
            
            return this;
        }
    });
</script>
