<?php $fileInfoArray = $sf_data->getRaw('fileInfoArray'); ?>
<?php foreach($fileInfoArray as $file): ?>
<?php if(!$file['displayable']) continue; ?>
<div class="new_image" style="display:none;">
    <?php foreach($file['preview'] as $key => $rec): ?>    
    <img <?php if($key != 0){ echo 'style="display: none"'; } ?>
         class="file-img"
         alt="<?php echo __('Loading...'); ?>"
         <?php if($key > 0): ?>
             src-image="<?php echo $rec; ?>?rand=<?php echo rand(1, 1000000); ?>"
             src="/images/no-preview.jpg"
         <?php else: ?>
             src="<?php echo $rec; ?>?rand=<?php echo rand(1, 1000000); ?>"
         <?php endif; ?>
          />
    <?php endforeach; ?>
    <br/><br/>
    <!-- if more then one photo in current item (exp. pdf with 2 pages) -->
    <?php if(count($file['preview']) > 1): ?>
    <span class="photo_info">
        <span class="subNavImage prevSubImage">
            <img src="/images/icons/prev_photo_active.png">
        </span>                                        
        <span class="subNavImage" style="width: 330px; height: 10px; text-align: center;">
            <select class="subNavImageSelect">
                <?php for($l = 1; $l <=  count($file['preview']); $l++): ?>    
                <?php if($key == 0): ?>
                <option selected value="<?php echo $l; ?>"><?php echo $l; ?></option>
                <?php else: ?>
                <option value="<?php echo $l; ?>"><?php echo $l; ?></option>
                <?php endif; ?>
                <?php endfor; ?>           
            </select>
        </span>
        <span class="subNavImage nextSubImage">
            <img src="/images/icons/next_photo_active.png">
        </span>
    </span>
    <br/>
    <?php endif; ?>

    <!-- at the end add some info -->
    <span class="photo_info"><?php echo $file['info']; ?></span>
</div>
<?php endforeach; ?>

<?php foreach($wizardInfoArray as $key => $wizard): ?>
<div class="new_image" style="display:none;">
    <img class="wizard-img"
         alt="<?php echo __('Loading...'); ?>"  
         <?php if($key == 0 && count($fileInfoArray) == 0){ echo 'onLoad="previewOnLoad()"'; } ?>
         src="<?php echo $wizard['urlPreview']; ?>" />
</div>
<?php endforeach; ?>
