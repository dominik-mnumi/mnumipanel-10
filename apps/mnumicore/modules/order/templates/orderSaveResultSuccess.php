<?php 
$result = $sf_data->getRaw('result');

$result['message'] = __($result['message']);
if(!$form->isValid())
{
    $errors = array();

    $fields = $form->getWidget('attributes')->getWidget()->getFields();
    foreach($form->getErrorSchema()->getErrors() as $formField)
    {
        $messages = array();
        foreach($formField->getErrors() as $name => $error)
        {
            $label = __($fields[$name]->getLabel());
            $messages[] = sprintf('%s %s', $label, __($error->getMessage()));
        }
        $errors[] = $messages;
    }
    $result['errors'] = $errors;
}
echo json_encode($result);
