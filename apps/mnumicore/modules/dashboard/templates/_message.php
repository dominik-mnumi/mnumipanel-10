<?php

/**
 * Gets info data from flash 
 * 
 * Using from actions.class.php: 
 * {code}
 * $this->setFlash('info_data', array(
 *   'message' => 'Saved succesfull record %record%.,
 *   'messageParam' => array('%record% => 12),
 *   'messageType' => 'msg_success',
 * );
 * {code}
 */

?>
<?php if($sf_user->hasFlash('info_data')): ?>
<?php $infoData = $sf_data->getRaw('sf_user')->getFlash('info_data'); ?>

<!-- translates and prepares message -->
<?php $message = array_key_exists('messageParam', $infoData)
                 ? __($infoData['message'], $infoData['messageParam'])
                 : __($infoData['message']); ?>

<!-- removes flash from memory -->
<?php $sf_user->getAttributeHolder()->remove('info_data', null, 'symfony/user/sfUser/flash'); ?>
<?php $sf_user->getAttributeHolder()->remove('info_data', null, 'symfony/user/sfUser/flash/read'); ?>

<?php slot('msg_success'); ?>
<div class="clear"></div>
<article class="container_12">
    <ul class="message success grid_12">
        <li><?php echo $message; ?></li>
        <li class="close-bt"></li>
    </ul>
</article>
<?php end_slot(); ?>

<?php slot('msg_error'); ?>
<div class="clear"></div>
<article class="container_12">
    <ul class="message error grid_12">
        <li><?php echo $message; ?></li>
        <li class="close-bt"></li>
    </ul>
</article>
<?php end_slot(); ?>

<?php slot('info_green'); ?>
<div class="clear"></div>
<article class="container_12">
    <ul class="message warning no-margin info-container-green">
        <li><?php echo $message; ?></li>
        <li class="close-bt"></li>
    </ul>
</article>
<?php end_slot(); ?>

<?php slot('info_red'); ?>
<div class="clear"></div>
<article class="container_12">
    <ul class="message warning no-margin info-container-red">
        <li><?php echo $message; ?></li>
        <li class="close-bt"></li>
    </ul>
</article>
<?php end_slot(); ?>

<?php if(isset($infoData)): ?>
<?php include_slot($infoData['messageType']); ?>
<?php endif; ?>

<?php endif; ?>


