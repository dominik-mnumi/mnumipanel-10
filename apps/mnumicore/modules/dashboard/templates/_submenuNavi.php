<ul>
    <?php foreach($rec->getSubmenu() as $rec2): ?>
    <li class="<?php echo $rec2->getClass(); ?>">
        <?php if($rec2->hasSubmenu()): ?>
        <a href="#">
            <?php echo __($rec2->getTitle()); ?>
        </a>

        <?php if($rec2->hasSubmenu()): ?>
        <?php include_partial('dashboard/submenuNavi', 
                            array('rec' => $rec2)); ?>    
        <?php endif; ?> 

        <?php else: ?>
        <a href="<?php echo url_for($rec2->getRoute()); ?>">
            <?php echo __($rec2->getTitle()); ?>
        </a>
        <?php endif; ?>        
    </li>       
    <?php endforeach; ?>
</ul>       