<!-- Main block -->
<div class="block">
    <div id="head">

        <h1><?php echo __('Your browser version is not supported'); ?></h1>

    </div>
    <div id="body">

        <p><?php echo __('Your browser is too old to handle this web application, or may not display it correctly.'); ?></p>

        <h2><?php echo __('Please change or upgrade to the latest version (Chrome recommended):'); ?></h2>

        <ul id="links">
            <li id="link-chrome"><a href="http://www.google.com/chrome" title="Download Chrome"><span>Google<br /><strong>Chrome</strong></span></a></li>
            <li id="link-firefox"><a href="http://www.mozilla.com" title="Download Firefox"><span>Mozilla<br /><strong>Firefox</strong></span></a></li>
            <li id="link-safari"><a href="http://www.apple.com/safari/" title="Download Safari"><span>Apple<br /><strong>Safari</strong></span></a></li>
            <li id="link-opera"><a href="http://www.opera.com" title="Download Opera"><span><strong>Opera</strong></span></a></li>
        </ul>

    </div>
</div>
<!-- Main block end -->

<!-- Skip link -->
<div class="block">
    <div id="skip">
        <a href="<?php echo url_for('homepage'); ?>" title="<?php echo __('Click only if you are sure what you are doing'); ?>">
            <?php echo image_tag('icons/fugue/navigation-000-white.png', 
                    array('width' => 16, 'height' => 16)); ?>
            <?php echo __("I'm warned, <strong>let me in anyway</strong>"); ?>
        </a>
    </div>
</div>
<!-- Skip link end -->