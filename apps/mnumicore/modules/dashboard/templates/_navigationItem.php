<li class="<?php echo $node->hasDescandant($cat_id) ? 'open' : 'closed'; ?> 
<?php if('productCategory' == $url_prefix  && !$node->getActive()) echo 'inactive-category'?>">
    <span>   
        <?php $nameAddition = ('productCategory' == $url_prefix  && !$node->getActive()) ? ' ('.__('inactive').')' : '' ?>    
        <?php echo link_to(__($node->__toString()).$nameAddition, $url_prefix, $node, array('title' => __($node->__toString()))); ?>
        <?php if('productCategory' == $url_prefix || 'settingsField' == $url_prefix): ?>
            <?php echo link_to(image_tag('/images/icons/fugue/pencil.png', 
                    array('width' => 16, 'height' => 16)), 
                    $url_prefix.'Edit', 
                    $node, 
                    array('class' => 'edit'));
            ?>
        <?php endif; ?>
        <?php if($node->canDelete()): ?>
            <?php echo link_to(image_tag('/images/icons/fugue/cross-circle.png', 
                    array('width' => 16, 'height' => 16)), 
                    $url_prefix.'Delete', 
                    $node, 
                    array('class' => 'delete', 'onclick' => 'alertModalConfirm(\''.__('Delete item').'\',\'<h3>'.__('Do you really want to delete this item?').'</h3>\', 500, this.href); return false;'));
            ?>
        <?php endif; ?>
    </span>
 
    <?php foreach($node->__children as $subnode): ?>
    <ul>
        <?php  
        include_partial('dashboard/navigationItem', array(
            'node' => $subnode,
            'cat_id' => $cat_id,
            'url_prefix' => $url_prefix
        ));
        ?>
    </ul>
    <?php endforeach; ?>
</li>
