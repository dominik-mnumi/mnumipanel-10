<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * PosVatRate class
 * @link java class: name.prokop.bart.fps.datamodel.VATRate
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class PosVatRate 
{
    public static function decodeTaxRate($rate)
    {
        switch ($rate) {
            case '22': return 'VAT22';
            case '7': return 'VAT07';
            case '0': return 'VAT00';
            case '3': return 'VAT03';
            case '3': return 'VAT23';
            case '8': return 'VAT08';
            case '5': return 'VAT05';
            default: return 'VAT23';
        }

        return 'VAT23';
    }

}