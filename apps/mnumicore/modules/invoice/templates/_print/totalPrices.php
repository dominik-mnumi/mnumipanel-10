<tr class="total-prices-space">
    <td rowspan="8" colspan="5" style="border-right: 0;">&nbsp;</td>
    <td class="td-right"><b><?php echo $isCorrection ? __('Total correction') : __('Total'); ?>:</b></td>
    <td class="td-right td-important summary" bgcolor="#cccccc"><b><?php echo format_number($invoiceObj->getPriceNet()); ?></b></td>
    <td class="summary">&nbsp;</td>
    <td class="td-right td-important summary"><b><?php echo format_number($invoiceObj->getPriceTax()); ?></b></td>
    <td class="td-right td-important summary"><b><?php echo format_number($invoiceObj->getPriceGross()); ?></b></td>
</tr>

<?php if($groupPriceArr->count()): ?>
<tr>
    <td class="td-right"><b><?php echo __('Including'); ?>:</b></td>
    <td colspan="4"></td>
</tr>

<!-- defined tax amount -->
<?php foreach($groupPriceArr as $rec): ?>
<tr>
    <td>&nbsp;</td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $rec['priceAfterDiscountGroupedByTax'])); ?></td>
    <td class="td-center"><?php echo $rec['tax']; ?></td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $rec['taxGroupedByTax'])); ?></td>
    <td class="td-right"><?php echo format_number(sprintf('%01.2f', $rec['grossPriceGroupedByTax'])); ?></td>
</tr>
<?php endforeach; ?>
<?php endif; ?>

<tr class="clear summary">
    <td>&nbsp;</td>
    <td colspan="3" class="td-right"><strong><?php echo __('Net value'); ?></strong>: </td>
    <td class="td-right"><?php echo $sf_user->formatCurrency($invoiceObj->getPriceNet(), $invoiceObj->getCurrency()); ?></td>
</tr>
<tr class="clear">
    <td>&nbsp;</td>
    <td colspan="3" class="td-right"><strong>VAT</strong>: </td>
    <td class="td-right"><?php echo $sf_user->formatCurrency($invoiceObj->getCalculatedTaxAmount(), $invoiceObj->getCurrency()); ?></td>
</tr>
<tr class="clear">
    <td>&nbsp;</td>
    <td colspan="3" class="td-right"><strong><?php echo __('Gross value'); ?></strong>: </td>
    <td class="td-right"><?php echo $sf_user->formatCurrency($invoiceObj->getPriceGross(), $invoiceObj->getCurrency()); ?></td>
</tr>