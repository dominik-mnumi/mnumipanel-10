<tr class="item-header">
    <td><?php echo __('No.'); ?></td>
    <td><?php echo !$isCorrection ? __('Name of product / service') : __('Name of product / service - correction'); ?></td>
    <td><?php echo !$isCorrection ? __('Qty') : __('Quantity correction'); ?></td>
    <td><?php echo !$isCorrection ? __('Price net'): __('Price net correction'); ?></td> 
    <td><?php echo !$isCorrection ? __('Net value') : __('Net value correction'); ?></td> 
    <td><?php echo !$isCorrection ? __('Discount amount') : __('Discount correction'); ?></td>
    <td><?php echo !$isCorrection ? __('Price<br />after discount'): __('Price after discount correction'); ?></td> 
    <td>VAT %</td>
    <td><?php echo !$isCorrection ? __('VAT value'): __('VAT value correction'); ?></td>  
    <td><?php echo !$isCorrection ? __('Gross value'): __('Gross value correction'); ?></td>
</tr>
