<?php
/**
 * Arguments:
 * - withSave (default always, if set and false then remove),
 * - withReminder (default never, if set and true then add),
 * - route
 * - routeBack => ...,
 * - routeCancel => ...;
 * 
 * If routeBack or routeCancel does not exist then overwrite 
 * them by route parameter.
 */
?>

<?php if(!isset($routeCancel)): ?>
<?php $routeCancel = $route; ?>
<?php endif; ?>

<div class="grey-bg clearfix" id="control-bar" style="opacity: 1;">
    <div class="container_12">
        <div class="float-left">
            <?php echo link_to(image_tag('/images/icons/fugue/navigation-180.png'). ' ' .__('Back'), 
                    $routeBack, 
                    array('class' => 'big-button', 
                        'title' => __('Back'))); ?>
        </div>

        <div class="float-right">
            <button class="float-left grey" id="send-invoice" type="button"><?php echo __('Send') ?></button>

            <div id="steps" class="float-left button-menu">

                <div class="float-left with-menu big-button grey" <?php echo $form->getObject()->isNew() ? 'disabled' : ''; ?>>
                    <h1 class="invoice-print-a"><?php echo __('Print it'); ?></h1>

                    <div class="menu">
                        <?php echo image_tag('menu-open-arrow.png', array( 'width' => 16, 'height' => 16)); ?>
                        <ul>
                            <li class="invoice-print-a"><a href="#"><?php echo __('Print it'); ?></a></li>
                            <li class="invoice-duplicate-a" id="duplicate"><a href="#"><?php echo __('Duplicate'); ?></a></li>
                            <?php if($form->getObject()->getInvoiceTypeName() == 'receipt'): ?><li class="fiscal-print-a"><a href="#"><?php echo __('Wydruk fiskalny'); ?></a></li><?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>

            <button class="red" type="button" onclick="document.location.href='<?php echo url_for($routeCancel); ?>'" ><?php echo __('Discard') ?></button>
            <?php if(!isset($withSave) || $withSave): ?>
            <button type="submit" id="saveAndReturn"><img width="16" height="16" src="/images/icons/fugue/tick-circle.png"> <?php echo __('Save') ?></button>
            <?php if(isset($saveAndAdd) && $saveAndAdd): ?>
            <button type="submit" id="saveAndAddButton"><img width="16" height="16" src="/images/icons/fugue/tick-circle.png"><?php echo __('Save and add') ?></button>
            <input type = "hidden" name="saveAndAdd" id="saveAndAdd" value="0" />
            <script type="text/javascript" language="javascript">
                jQuery(document).ready(function($)
                {
                    $('#saveAndAddButton').click(function()
                    {
                        $('#saveAndAdd').val('1');
                        return true;
                    });
                });
            </script>
            <?php endif ?>
            <?php endif ?>
        </div>     
    </div>

    <div class="loader float-right margin-right20px">
        <?php echo image_tag('loader.gif', array('width' => 34, 'height' => 34)); ?>
    </div>
</div>

<script type="text/javascript">
    /**
     * Services submit form action.
     */
    $("#invoice-form").submit(function()
    {
        showLoaderAndDisableButtons();
    });

    /**
     * Shows loader, disables buttons.
     */
    function showLoaderAndDisableButtons()
    {
        // shows loader
        $(".loader").show();

        $("#control-bar button").attr("disabled", true);
    }
</script>