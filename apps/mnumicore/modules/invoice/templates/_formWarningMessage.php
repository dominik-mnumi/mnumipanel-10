<?php if($form->hasErrors()): ?>
<ul class="message error no-margin">    
    <?php foreach($form->getFormFieldSchema() as $key => $formField): ?>
    <?php if($formField->hasError()): ?>

    <?php if($key != 'item'): ?>  
    <li>
    <?php echo __($formField->getError(), array('%label%' => __($formField->renderLabelName()))); ?>       
    </li>
    <?php else: ?>

    <!-- errors for items -->
    <?php $i = 1; ?>
    <?php foreach($formField as $key2 => $formField2): ?>
    
    <?php foreach($formField2 as $key3 => $formField3): ?>
    <?php if(in_array($key3, array('price_net', 'amount', 'description', 
        'unit_of_measure', 'discount_price', 'quantity'))
            && $formField3->hasError()): ?>
    <li>
        <?php echo __('Invoice item no. %i%', array('%i%' => $i)).' - '.__($formField3->getError(), array('%label%' => __($formField3->renderLabelName()))); ?>       
    </li>
    <?php endif; ?>
    <?php endforeach; ?>

    <?php $i++; ?>
    <?php endforeach; ?>

    <?php endif; ?>

    <?php endif; ?>
    <?php endforeach; ?>
</ul>
<?php elseif($sf_user->hasFlash('info_invoice_notices')):?>
    <?php $infoData = $sf_data->getRaw('sf_user')->getFlash('info_invoice_notices'); ?>
    <!-- translates and prepares message -->
    <?php $message = array_key_exists('messageParam', $infoData)
        ? __($infoData['message'], $infoData['messageParam'])
        : __($infoData['message']); ?>
    <!-- removes flash from memory -->
    <?php $sf_user->getAttributeHolder()->remove('info_invoice_notices', null, 'symfony/user/sfUser/flash'); ?>
    <?php $sf_user->getAttributeHolder()->remove('info_invoice_notices', null, 'symfony/user/sfUser/flash/read'); ?>

    <ul class="message error no-margin">
        <li><?php echo $message;?></li>
    </ul>
<?php endif; ?>


