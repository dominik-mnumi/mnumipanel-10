<ul class="simple-list with-icon">
    <li class="icon-date"><span><?php echo format_date($row->getCreatedAt(), 'f') ?></span></li>
    <?php if($row->getCreatedAt() != $row->getUpdatedAt()): ?>
        <li class="icon-date"><span><?php echo __('edited')?>: <?php echo format_date($row->getUpdatedAt(), 'f') ?></span></li>
    <?php endif ?>
</ul>