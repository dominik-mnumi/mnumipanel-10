<div class="login-bg">
<!-- The template uses conditional comments to add wrappers div for ie8 and ie7 - just add .ie, .ie7 or .ie6 prefix to your css selectors when needed -->
<!--[if lt IE 9]><div class="ie"><![endif]-->
<!--[if lt IE 8]><div class="ie7"><![endif]-->
	
	<section id="login-block">
		<div class="block-border">
            <div class="block-content">
                <!--
                IE7 compatibility: if you want to remove the <h1>,
                add style="zoom:1" to the above .block-content div
                -->
                <h1>Admin</h1>
                <div class="block-header"><?php echo __('Please log in') ?></div>
                <?php echo get_partial('sfGuardAuth/signin_form', array('form' => $form)) ?>

                <?php if($sf_user->hasFlash('notice')): ?>
                <form class="form" id="password-recovery" method="post" action="">
                <fieldset class="grey-bg no-margin">
                    <legend><a href="#"><?php echo __('Lost password?') ?></a></legend>
                    <ul class="message success grid_12">
                        <li><?php echo __($sf_user->getFlash('notice')); ?></li>
                        <li class="close-bt"></li>
                    </ul>
                </fieldset>
                </form>
                <?php else: ?>
                <?php echo get_partial('sfGuardForgotPassword/forgotPassForm', array('forgotPassForm' => $forgotPassForm)) ?>
                <?php endif ?>

                
            </div>
        </div>
	</section>

<!--[if lt IE 8]></div><![endif]-->
<!--[if lt IE 9]></div><![endif]-->
</div>
