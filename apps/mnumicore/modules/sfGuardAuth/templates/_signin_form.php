<script type="text/javascript" language="javascript">
    $(function() 
    {
        $('#signin_username').focus();
    });    
</script>

<?php if($form->hasGlobalErrors()): ?>
<p class="message error no-margin"><?php echo $form->renderGlobalErrors(); ?></p>
<?php endif; ?>

<form class="form with-margin" name="login-form" id="login-form"  action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
    <?php echo $form['_csrf_token']; ?>
    <?php echo $form['username']->renderError(); ?>
    <p class="inline-small-label">
        <?php echo $form['username']->renderLabel('Username', array('class' => 'big')); ?>
        <?php echo $form['username']->render(array('class' => 'full-width')); ?>
    </p>
    <?php echo $form['password']->renderError(); ?>
    <p class="inline-small-label">
        <?php echo $form['password']->renderLabel(null, array('class' => 'big')); ?>
        <?php echo $form['password']->render(array('class' => 'full-width')); ?>
    </p>
    <button type="submit" class="float-right"><?php echo __('Login'); ?></button>
    <p class="input-height">
        <?php echo $form['remember']->render(array('class' => 'mini-switch')); ?>
        <?php echo $form['remember']->renderLabel('Keep me logged in', array('class' => 'inline')); ?>
    </p>    
</form>
