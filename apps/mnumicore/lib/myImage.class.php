<?php

/**
* @author Piotr Plenik <piotr.plenik@teamlab.pl>
* @copyright Studiouh.com Sp. z o.o.
* @package Extremouh
**/

class myImage extends sfImage
{
  /*
   * MIME type map and their associated file extension(s)
   * @var array
   */
  protected $types = array(
    'image/gif' => array('gif'),
    'image/jpeg' => array('jpg', 'jpeg'),
    'image/png' => array('png'),
    'image/svg' => array('svg'),
    'image/tiff' => array('tiff'),
    'application/pdf' => array('pdf')
  );
  
}