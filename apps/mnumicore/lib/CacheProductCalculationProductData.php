<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * CacheProductCalculationProductData class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class CacheProductCalculationProductData 
{
    /**
     * @var sfCache
     */
    private $cache;

    /**
     * @var Product
     */
    private $product;

    public function __construct(Product $product, sfCache $cache = null)
    {
        $this->cache = $cache;
        $this->product = $product;
    }

    /**
     * Load Product attributes from cache
     *
     * @return CalculationProductData|bool
     */
    public function getCalculationProductDataObject()
    {
        $cacheKey = $this->getCacheKey();

        $cachedCalculationProductData = $this->cache->get($cacheKey, false);

        return ($cachedCalculationProductData) ? unserialize($cachedCalculationProductData) : false;
    }

    /**
     * Save Product attributes
     * @param CalculationProductData $productData
     */
    public function cacheProductData($productData)
    {
        $cacheKey = $this->getCacheKey();

        $data = serialize($productData);

        $this->cache->set($cacheKey, $data);
    }

    /**
     * Clear cache for Product attributes
     */
    public function clearCache()
    {
        $cacheKey = $this->getCacheKey();

        $this->cache->remove($cacheKey);
    }

    /**
     * @return \Product
     */
    protected function getProduct()
    {
        return $this->product;
    }

    /**
     * @return string
     */
    protected function getCacheKey()
    {
        return 'product_' . $this->getProduct()->getId();
    }

    /**
     * @return \sfCache
     */
    protected function getCache()
    {
        return $this->cache;
    }

}