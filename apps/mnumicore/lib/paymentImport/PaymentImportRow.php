<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Payment import row.
 *
 * @package    mnumicore
 * @subpackage cashDesk
 * @author     Marek Balicki
 */

class PaymentImportRow
{   
    protected static $dateLabel = 'date';
    protected static $titleLabel = 'title';
    protected static $amountLabel = 'amount';
    
    protected $date;
    protected $title;
    protected $amount;

    protected $matchedInvoiceColl;
    
    /**
     * Creates instance of payment row.
     * 
     * @param array $rowArr
     * @param array $keyArr = array('date' => 2,
     *                              'title' => 4);
     * @param Doctrine_Collection
     * @throws Exception 
     */
    public function __construct($rowArr, $keyArr, $invoicesToPayColl)
    {
        if(!is_array($keyArr) || empty($keyArr))
        {
            throw new Exception('Keys argument must be array and cannot be empty.');
        }
        
        if(!array_key_exists(self::$dateLabel, $keyArr) || !array_key_exists($keyArr[self::$dateLabel], $rowArr))
        {
            throw new Exception('Date column key is not defined or does not exist in content array.');
        }
        
        if(!array_key_exists(self::$titleLabel, $keyArr) || !array_key_exists($keyArr[self::$titleLabel], $rowArr))
        {
            throw new Exception('Title column key is not defined or does not exist in content array.');
        }
        
        if(!array_key_exists(self::$amountLabel, $keyArr) || !array_key_exists($keyArr[self::$amountLabel], $rowArr))
        {
            throw new Exception('Amount column key is not defined or does not exist in content array.');
        }
        
        // sets values
        $this->date = $rowArr[$keyArr[self::$dateLabel]];
        $this->title = $rowArr[$keyArr[self::$titleLabel]];
        $this->amount = (float)str_replace(
                array(' ', ','), 
                array('', '.'), 
                $rowArr[$keyArr[self::$amountLabel]]);

        // creates parser object
        $paymentImportParserObj = new PaymentImportParser($this->title);
        
        // finds documents basing on parsed titles
        $this->matchedInvoiceColl = $paymentImportParserObj->findInvoices($invoicesToPayColl);
    }   

    /**
     * Returns date of payment.
     * 
     * @return string 
     */
    public function getDate()
    {
        return $this->date;
    }
    
    /**
     * Returns title of payment.
     * 
     * @return string 
     */
    public function getTitle()
    {      
        return $this->title;
    }
    
    /**
     * Returns amount of payment.
     * 
     * @return string 
     */
    public function getAmount()
    {      
        return $this->amount;
    }
    
    /**
     * Returns true if invoices are found, otherwise false.
     * 
     * @return boolean
     */
    public function hasMatchedInvoices()
    {
        return $this->matchedInvoiceColl->count() ? true : false;
    }
    
    /**
     * Returns collection of matched documents (Invoice model).
     * 
     * @return Doctrine_Collection 
     */
    public function getMatchedInvoices()
    {
        return $this->matchedInvoiceColl;
    }
    
    /**
     * Returns total amount of matched invoices.
     * 
     * @return float 
     */
    public function getTotalAmountOfMatchedInvoices()
    {
        $amount = 0;
        foreach($this->matchedInvoiceColl as $rec)
        {
            $amount += $rec->getPriceGross();
        }
        
        return round($amount, 2);
    }
}