<?php
/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Payment import adapter.
 *
 * @package    mnumicore
 * @subpackage cashDesk
 * @author     Marek Balicki
 */
abstract class PaymentImportAdapter implements PaymentImportAdapterInterface
{     
    protected $filename;
    protected $contentArr;
    protected $inputEncoding;
    protected $outputEncoding = 'UTF-8';
    
    /**
     * Automatically detects encoding.
     * 
     * @param string $filename 
     * @return string 
     */
    public static function detectEncoding($filename)
    {
        $win2utf = array(
            "\xb9" => "\xc4\x85", "\xa5" => "\xc4\x84", "\xe6" => "\xc4\x87", "\xc6" => "\xc4\x86",
            "\xea" => "\xc4\x99", "\xca" => "\xc4\x98", "\xb3" => "\xc5\x82", "\xa3" => "\xc5\x81",
            "\xf3" => "\xc3\xb3", "\xd3" => "\xc3\x93", "\x9c" => "\xc5\x9b", "\x8c" => "\xc5\x9a",
            "\x9f" => "\xc5\xba", "\xaf" => "\xc5\xbb", "\xbf" => "\xc5\xbc", "\xac" => "\xc5\xb9",
            "\xf1" => "\xc5\x84", "\xd1" => "\xc5\x83", "\x8f" => "\xc5\xb9");

        $countWin = 0;
        $found = 0;
        
        // checks encoding
        if(($handle = fopen($filename, 'r')) !== FALSE)
        { 
            while(($row = fgets($handle, 1000)) !== FALSE)
            {
                foreach($win2utf as $win => $utf)
                {
                    $pos = strpos($row, $win);
                    if($pos !== false)
                    {
                        $found++;
                    }

                    if($pos && substr(iconv('WINDOWS-1250', 'UTF-8', $row),
                                    $pos + $countWin, 2) == $utf)
                    {
                        $countWin++;
                    }
                }
            }

            fclose($handle);
        }

        if($countWin > 0)
        {
            return 'WINDOWS-1250';
        }
        if($found)
        {
            return 'ISO-8859-2';
        }
        
        return 'UTF-8';
    }
    
    /**
     * Prepares file content.
     * 
     * @param string $filename
     * @param string $inputEncoding
     * @throws Exception
     */
    public function __construct($filename, $inputEncoding = null)
    {
        if(!file_exists($filename))
        {
            throw new Exception('File is not found.');
        }
        
        $this->filename = $filename;   
        
        if($inputEncoding)
        {
            $this->inputEncoding = $inputEncoding;           
        }
        else
        {
            $this->inputEncoding = self::detectEncoding($this->filename);
        }
    }   

    /**
     * Returns collection of payment rows.
     */
    public function getRowColl()
    {
        // gets invoices which are not paid
        $invoicesToPayColl = InvoiceTable::getInstance()->getInvoicesToPay();

        $coll = array();
        foreach($this->contentArr as $rec)
        {
            $coll[] = new PaymentImportRow($rec, $this->keyArr, $invoicesToPayColl);
        }

        return $coll;
    }
  
}