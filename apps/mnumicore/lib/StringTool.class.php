<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * String handling methods
 *
 * @author Adam Marchewicz <adam.marchewicz@itme.eu>
 */
class StringTool
{
    /**
     * Splits name of column to presentation
     *
     * @param $strig string with column name
     *
     * @return string
     */
    static public function splitByCapitals($string)
    {
        return ucfirst(strtolower(trim(preg_replace('/([a-z0-9])?([A-Z])/','$1 $2', $string))));
    }

    /**
     * Get string length
     *
     * @param $string
     * @return int
     */
    static public function strlen($string)
    {
        if (!function_exists('mb_strlen')) {
            return strlen($string);
        }

        return mb_strlen($string, mb_internal_encoding());
    }
    /**
     * Get part of string
     *
     * @param $string
     * @return int
     */
    static public function substr($string, $start, $length = null)
    {
        if (!function_exists('mb_substr')) {
            return substr($string, $start, $length);
        }
        return mb_substr($string, $start, $length, mb_internal_encoding());
    }
}