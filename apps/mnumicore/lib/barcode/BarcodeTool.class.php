<?php

/*
 * This file is part of the MnumiCore package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * BarcodeTool class for barcode generation
 *
 * @author Michał Kopańko <michal.kopanko@itme.eu>
 */
class BarcodeTool
{
    /**
     * Get URI for barcode 
     * 
     * @param type $modelName
     * @param type $id
     * @return type 
     */
    public static function getUrl($modelName, $id)
    {
        $number = self::getNumber($modelName, $id);
        return url_for('reportBarcode', array('code' => $number));
    }
    
    /**
     * @param string $typetype of barcode (available options: sfGuardUser, Client, Order)
     * @param int $id id of object
     * 
     * @return string url for barcode image
     */
    public static function getNumber($modelName, $id)
    {
        $type = self::getType($modelName);
        return $type . str_pad($id, 8, '0', STR_PAD_LEFT);
    }

    /**
     * Get Barcode type
     * 
     * @param string $modelName
     * 
     * @return int
     * @throws Exception 
     */
    public static function getType($modelName)
    {
        $tableClass = $modelName.'Table';
        
        if(!class_exists($tableClass))
        {
            throw new Exception('Object "'.$modelName.'" does not exists.');
        }
        
        if(!property_exists($tableClass, 'barcodePrefix'))
        {
            throw new Exception('Object "'.$tableClass.'" does not have $barcode_prefix property.');
        }
        return $tableClass::$barcodePrefix;
    }

}
