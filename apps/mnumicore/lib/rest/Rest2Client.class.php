<?php

/*
 * This file is part of the MnumiPrint package.
 * 
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 * 
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Rest2Client class
 *
 * @author Piotr Plenik <piotr.plenik@mnumi.com>
 */
class Rest2Client 
{
    /**
     * @var string
     */
    private $host;
    /**
     * @var string
     */
    private $apiKey;

    /**
     * @param string $host
     * @param string $apiKey
     */
    public function __construct($host, $apiKey)
    {;
        $uri = sfContext::getInstance()->getRequest()->getUri();
        if(strpos($uri, 'mnumicore_dev.php')) {
            // if like "http://[host]/mnumicore_dev.php/api"
            $hostName = preg_replace('/app.php/', 'app_dev.php', $host);
        } else {
            // if like "http://[host]/api"
            $hostName = $host;
        }

        $this->host = $hostName;
        $this->apiKey = $apiKey;
    }

    /**
     * Get host name
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * Call post
     *
     * @param string $method
     * @param array|null $data
     *
     * @return Object
     * @throws sfError404Exception
     * @throws Exception
     */
    public function post($method, $data = null)
    {
        return $this->request($method, $data, true);
    }

    /**
     * Call get
     *
     * @param string $method
     * @param array|null $data
     *
     * @return Object
     * @throws sfError404Exception
     * @throws Exception
     */
    public function get($method, $data = null)
    {
        return $this->request($method, $data);
    }

    /**
     * Call request
     *
     * @param string $method
     * @param array|null $data
     * @param boolean $post
     *
     * @return json
     *
     * @throws sfError404Exception
     * @throws Exception
     */
    protected function request($method, $data = null, $post = false)
    {
        $ch = curl_init();
        $url = $this->getHost().'/'.$method . '.json';

        if(is_array($data)) {

            $data = http_build_query($data);
        }

        if($post) {

            curl_setopt($ch, CURLOPT_POST, 1);

            if($data) {

                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            }

        } elseif($data) {

            $getParams =  '?' . $data;
            $url .= $getParams;

        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->getApiKey() . ":" . $this->getApiKey());
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($httpCode === 500) {
            throw new \Exception('Incorrect REST response: ' . $result);
        }
        
        return array('message' => json_decode($result, true), 'code' => $httpCode);
    }

    /**
     * Get server session handle
     * (this attribute is initalized in productAction.class.php
     * in executeSetSession()
     *
     * @return string
     */
    protected function getSessionHandle()
    {
        if(!sfContext::getInstance()->getUser()->hasAttribute('webapi_server_session'))
        {
            // on one server must be different in frontend and backend
            $newSession = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789',20)),0,20);; 
            sfContext::getInstance()->getUser()->setAttribute('webapi_server_session', $newSession);
        }
        return sfContext::getInstance()->getUser()->getAttribute('webapi_server_session', '');
    }
}