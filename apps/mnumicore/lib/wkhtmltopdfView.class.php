<?php

class wkhtmltopdfView extends sfPHPView
{
    /**
     * Renders the presentation.
     *
     * @return string A string representing the rendered presentation
     */
    public function render()
    {
        $htmlTempFile = tempnam(sys_get_temp_dir(), "core3html") . '.html';
        $pdfTempFile = tempnam(sys_get_temp_dir(), "core3pdf") . '.pdf';

        $content = parent::render();

        file_put_contents($htmlTempFile, $content);

        $exec = sprintf('/usr/bin/xvfb-run /usr/bin/wkhtmltopdf -q -O %s %s %s',
            $this->getOrientation(),
            $htmlTempFile,
            $pdfTempFile
        );

        exec($exec);

        if(!file_exists($pdfTempFile))  {
            throw new \Exception('Could not generate PDF file. Command: ' . $exec);
        }

        $pdfContent = file_get_contents($pdfTempFile);

        unset($htmlTempFile, $pdfTempFile);

        /**
         * Set default language
         */
        $user = $this->context->getUser();
        if($user instanceof sfUser) {
            $user->setCulture(sfConfig::get('sf_default_culture', 'en'));
        }

        return $pdfContent;
    }

    /**
     * Returns orientation of page
     * @return string
     */
    private function getOrientation()
    {
        $orientationAttr = $this->getAttribute('orientation', 'portrait');
        $orientation = in_array($orientationAttr, array('landscape', 'portrait')) ?
            $orientationAttr : 'portrait';

        return $orientation;
    }
}