<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Doctrine1ShippingInformation class
 *
 * @author Bartosz Dembek <bartosz.dembek@mnumi.com>
 */

class Doctrine1ShippingInformation
{

    private $orderPackage;

    /**
     * @param \OrderPackage $orderPackage
     */
    public function __construct(OrderPackage $orderPackage)
    {
        $this->orderPackage = $orderPackage;
    }

    /**
     *
     * @return \Mnumi\Bundle\ShippingBundle\Library\ShippingInformation
     */
    public function get()
    {
        $orderPackage = $this->orderPackage;

        $shippingInformation = new \Mnumi\Bundle\ShippingBundle\Library\ShippingInformation();
        $shippingInformation->setName($orderPackage->getDeliveryName());
        $shippingInformation->setStreet($orderPackage->getDeliveryStreet());
        $shippingInformation->setCity($orderPackage->getDeliveryCity());
        $shippingInformation->setPostcode($orderPackage->getDeliveryPostcode());
        $shippingInformation->setCountryCode($orderPackage->getDeliveryCountry());
        $shippingInformation->setClientName($orderPackage->getDeliveryName(35));
        $shippingInformation->setPhoneNumber($orderPackage->getSfGuardUser()->getPhoneNumer());
        $shippingInformation->setPackageId($orderPackage->getId());
        $shippingInformation->setDescription($orderPackage->getDescription());
        $shippingInformation->setAdditionalInformation($orderPackage->getAdditionalShippingInformation());
        $shippingInformation->setTransportNumber($orderPackage->getTransportNumber());

        return $shippingInformation;
    }
}
