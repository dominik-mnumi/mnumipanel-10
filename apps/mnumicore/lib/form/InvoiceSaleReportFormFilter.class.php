<?php

/**
 * Invoice sale report filter form.
 *
 * @package    mnumicore
 * @author     Bartosz Dembek<bartosz.dembek@mnumi.com>
 */
class InvoiceSaleReportFormFilter extends BaseInvoiceFormFilter
{

    public function configure()
    {
        $ranges = array(
            'all' => 'all days',
            'cm' => 'current month',
            'pm' => 'previous month',
            'cq' => 'current quarter',
            'pq' => 'previous quarter',
            'cy' => 'current year',
            'py' => 'previous year',
            'custom' => 'exact date range',
        );

        $widgets = array(
            'range' => new sfWidgetFormChoice(
                    array(
                        'choices' => $ranges,
                        'label' => 'Creation date of invoice',
                    )
                ),
            'date_range' => new sfWidgetFormDateRange(
                    array(
                        'from_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
                        'to_date' => new sfWidgetFormInput(array(), array('class' => 'datepicker')),
                        'label' => 'exact date range',
                    ), array('class' => 'datepicker')
                ),
        );

        $hasCurrencyAvailable = class_exists('getFromNBP');

        // hide regional settings for unsupported version
        // PAN-1628
        if($hasCurrencyAvailable) {
            $widgets['currency_id'] = new sfWidgetFormDoctrineChoice(
                array('model' => $this->getRelatedModelName('Currency'), 'add_empty' => false)
            );
        }

        $this->setWidgets($widgets);

        $validators = array(
            'range' => new sfValidatorChoice(array('choices' => array_keys($ranges))),
            'date_range' => new sfValidatorDateRange(
                    array(
                        'from_date' => new sfValidatorDate(array('required' => false, 'datetime_output' => 'Y-m-d')),
                        'to_date' => new sfValidatorDate(array('required' => false, 'datetime_output' => 'Y-m-d')),
                    )
                ),
        );

        // hide regional settings for unsupported version
        // PAN-1628
        if($hasCurrencyAvailable) {
            $validators['currency_id'] = new sfValidatorDoctrineChoice(
                array('required' => true, 'model' => $this->getRelatedModelName('Currency'), 'column' => 'id')
            );
        }

        $this->setValidators($validators);

        $this->widgetSchema->setNameFormat(InvoiceSaleReport::BASE_FILTER_NAME_FORMAT . '[%s]');
    }

    protected function doBuildQuery(array $values)
    {
        switch ($values['range']) {
            case 'cm': // current month
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, date("m"), 1,   date("Y"))),
                    'to' => date('Y-m-d'),
                );
                break;
            case 'pm': // previous month
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, date("m")-1, 1,   date("Y"))),
                    'to' => date('Y-m-d', mktime(0, 0, 0, date("m"), 0,   date("Y"))),
                );
                break;
            case 'cq': // current quarter
                $startMonth = date('m') - ((date('m')-1) % 3 );
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, $startMonth, 1,   date("Y"))),
                    'to' => date('Y-m-d', mktime(0, 0, 0, $startMonth+3, 0,   date("Y"))),
                );
                break;
            case 'pq': // previous quarter
                $startMonth = date('m') - 3 - ((date('m')-1) % 3 );
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, $startMonth, 1,   date("Y"))),
                    'to' => date('Y-m-d', mktime(0, 0, 0, $startMonth+3, 0,   date("Y"))),
                );
                break;
            case 'cy': // current year
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1,   date("Y"))),
                    'to' => date('Y-m-d'),
                );
                $this->dateRange = date("Y");
                break;
            case 'py': // previous year
                $values['date_range'] = array(
                    'from' => date('Y-m-d', mktime(0, 0, 0, 1, 1,   date("Y")-1)),
                    'to' => date('Y-m-d', mktime(0, 0, 0, 1, 0,   date("Y"))),
                );
                break;
            case 'all': // all days
                $values['date_range'] = array(
                    'from' => null,
                    'to' => date('Y-m-d'),
                );
                break;
            case 'custom': // custom date range
                break;
        }

        $this->values = $values;
        return parent::doBuildQuery($values);
    }

    /**
     * Add custom query from_date
     *
     * @param Doctrine_Query $query
     * @param string $field
     * @value
     */
    public function addDateRangeColumnQuery(Doctrine_Query $query, $field, $value)
    {
        if(array_key_exists('from', $value) && $value['from'] != null)
        {
            $query->andWhere('r.created_at >= ?', $value['from']);
        }

        if(array_key_exists('to', $value) && $value['to'] != null)
        {
            $query->andWhere('r.created_at <= ?', $value['to']);
        }
    }
}
