<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationLabelsPrinterForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationLabelsPrinterForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();

        $this->useFields(array(
            'printlabel_name',
            'printlabel_width',
            'printlabel_height',
            'printlabel_orientation',
        ));
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['printlabel']['name'] = $values['printlabel_name'];
        $this->AppArray['all']['printlabel']['width'] = (empty($values['printlabel_width'])) ? 89 : $values['printlabel_width'];
        $this->AppArray['all']['printlabel']['height'] = (empty($values['printlabel_height']))? 36 : $values['printlabel_height'];
        $this->AppArray['all']['printlabel']['orientation'] = $values['printlabel_orientation'];

        // save app.yml file & clear cache
        parent::save();
    }
}
