<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationOnlinePaymentsForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationFiscalPrinterForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();
        
        $this->useFields(array(
            'fiscal_printer_com',
            'fiscal_printer_type',
        ));
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['fiscal_printer']['com'] = $values['fiscal_printer_com'];
        $this->AppArray['all']['fiscal_printer']['type'] = $values['fiscal_printer_type'];

        // save app.yml file & clear cache
        parent::save();
    }
}