<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationCompanyDataForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationCompanyDataForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();

       $this->useFields(array(
            'company_data_seller_name', 
            'company_data_seller_address',
            'company_data_seller_postcode',
            'company_data_seller_city',
            'company_data_seller_country',
            'company_data_seller_tax_id',
            'company_data_seller_bank_name',
            'company_data_seller_bank_account',
            'company_data_seller_logo_image',
            'company_data_seller_logo_width'));

        // validators
        $taxValidator = new sfValidatorSchemaTaxId(
            'company_data_seller_tax_id',
            array('countryField' => 'company_data_seller_country'),
            array('invalid' => 'Field "Tax ID" is not valid')
        );

        // sets post validators
        $this->mergePostValidator(new sfValidatorAnd(
                array($taxValidator))
        );
    }

    /**
     * Saves data into app.yml.
     */
    public function save()
    {
        $values = $this->getValues();
       
        $file = $values['company_data_seller_logo_image'];
        if($file)
        {
            $logoUri = '/logo/default/';
            // gets absolute path to dir
            $logoDir = sfConfig::get('sf_upload_dir').$logoUri;

            // if dir does not exist - create it
            if(!is_dir($logoDir))
            {
                if(!is_writable(sfConfig::get('sf_upload_dir')))
                {
                    throw new Exception('Upload dir is not writeable.');
                }

                mkdir($logoDir, 0700, true);
                chmod($logoDir, 0777);
            }

            $logoName = preg_replace('/[^A-z0-9\_\-\.]+/', '_', $file->getOriginalName());
            $logoPath = '/uploads' . $logoUri . $logoName;
            $file->save($logoDir.$logoName);
        }
        
        // if delete logo
        if($values['company_data_seller_logo_image_delete'])
        {
            $logoPath = '';
            $logoName = '';
        }

        $this->AppArray['all']['company_data']['seller_name'] = $values['company_data_seller_name'];
        $this->AppArray['all']['company_data']['seller_address'] = $values['company_data_seller_address'];
        $this->AppArray['all']['company_data']['seller_postcode'] = $values['company_data_seller_postcode'];
        $this->AppArray['all']['company_data']['seller_city'] = $values['company_data_seller_city'];
        $this->AppArray['all']['company_data']['seller_tax_id'] = $values['company_data_seller_tax_id'];
        $this->AppArray['all']['company_data']['seller_bank_name'] = $values['company_data_seller_bank_name'];
        $this->AppArray['all']['company_data']['seller_bank_account'] = $values['company_data_seller_bank_account'];
        $this->AppArray['all']['company_data']['seller_logo_width'] = $values['company_data_seller_logo_width'];
        $this->AppArray['all']['company_data']['seller_logo_image'] = isset($logoName) ? $logoName : $this->AppArray['all']['company_data']['seller_logo_image'];
        $this->AppArray['all']['company_data']['seller_logo_path'] = isset($logoPath) ? $logoPath : $this->AppArray['all']['company_data']['seller_logo_path'];
        $this->AppArray['all']['company_data']['seller_country'] = strtolower($values['company_data_seller_country']);

        // save app.yml file & clear cache
        parent::save();
    }
}