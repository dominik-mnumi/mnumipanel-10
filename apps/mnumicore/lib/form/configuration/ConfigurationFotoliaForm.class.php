<?php

/*
 * This file is part of the MnumiPrint package.
 *
 * (c) Mnumi Sp. z o.o. <mnumi@mnumi.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * ConfigurationNotificationsForm
 *
 * @package    mnumicore
 * @subpackage form
 * @author     Marek Balicki <marek.balicki@mnumi.com>
 */
class ConfigurationFotoliaForm extends AppYamlEditForm
{
    public function configure()
    {
        parent::configure();
        
        $this->useFields(array(
            'fotolia_api_key',
            'fotolia_language_id'
            ));
    }

    /**
     * Saves data into app.yml.
     *
     */
    public function save()
    {
        $values = $this->getValues();

        $this->AppArray['all']['fotolia']['api_key'] = $values['fotolia_api_key'];
        $this->AppArray['all']['fotolia']['language_id'] = $values['fotolia_language_id'];

        // save app.yml file & clear cache
        parent::save();
    }
}