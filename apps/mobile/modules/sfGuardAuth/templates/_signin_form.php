<?php use_helper('I18N') ?>

<script type="text/javascript" language="javascript">
    $(function() 
    {
        $('#signin_username').focus();
    });    
</script>

<?php if($form->hasGlobalErrors()): ?>
<p class="message error no-margin"><?php echo $form->renderGlobalErrors(); ?></p>
<?php endif; ?>

<form class="form block-content" name="login-form" id="login-form"  action="<?php echo url_for('@sf_guard_signin') ?>" method="post">
    <?php echo $form['_csrf_token']; ?>
    <?php echo $form['username']->renderError(); ?>
    <p class="inline-mini-label small-margin">
        <?php echo $form['username']->renderLabel('<span class="big">User</span>'); ?>
        <?php echo $form['username']->render(array('class' => 'full-width')); ?>
    </p>
    <?php echo $form['password']->renderError(); ?>
    <p class="inline-mini-label">
        <?php echo $form['password']->renderLabel('<span class="big">Pass</span>'); ?>
        <?php echo $form['password']->render(array('class' => 'full-width')); ?>
    </p>
    <button type="submit" class="float-right"><?php echo __('Login'); ?></button>
    <p class="input-height">
        <?php echo $form['remember']->render(array('class' => 'mini-switch')); ?>
        <?php echo $form['remember']->renderLabel('Keep me logged in', array('class' => 'inline')); ?>
    </p>    
</form>

